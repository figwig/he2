Hi!
Welcome to my dissertation project.

Within this folder, you'll find my project source, and a prebuilt version of the application.

The prebuilt version is a folder named "WorkingDir", and called DissMain.exe. All of it's dependecies are in it's folder, it does however rely on having some pretty powerful hardware to run - 
it will actually abort with the message "Vulkan Failed to submit command buffer" if any single GPU command takes more than 3 seconds. To resolve this - make sure the default GPU hardware
is your GPU, not your integrated graphics. The program will not run without a dedicated graphics card.

The source is in a visual studio solution  called "HE2". In order to build, you need to have the vulkan sdk installed - any recent version will do. You must also select the debug working directory as
$(SolutionDir)/WorkingDir. I've found this is the only visual studio parameter that isn't persistent between each computer.

Please enjoy the project, as I have enjoyed working on it!