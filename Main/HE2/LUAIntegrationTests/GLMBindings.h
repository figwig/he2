#pragma once
#include <HE2_GLM.h>
#include <LuaBridge.h>
#include "LuaState.h"

extern "C" {
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}

static void vec3Bind();

using namespace luabridge;

static void bindAllGLM()
{
	vec3Bind();
}

struct VecHelper
{
    template <unsigned index>
    static float get(glm::vec3 const* vec)
    {
        return (*vec)[index];
    }

    template <unsigned index>
    static void set(glm::vec3* vec, float value)
    {
        (*vec)[index] = value;
    }
};

static void vec3Bind()
{
    getGlobalNamespace(L)
        .beginNamespace("glm")
        .beginClass<glm::vec3>("vec3")
        .addProperty("x", &VecHelper::get <0>, &VecHelper::set <0>)
        .addProperty("y", &VecHelper::get <1>, &VecHelper::set <1>)
        .addProperty("z", &VecHelper::get <2>, &VecHelper::set <2>)
        .endClass()
        .endNamespace();
   }