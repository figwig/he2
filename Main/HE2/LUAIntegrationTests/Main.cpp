#include <iostream>
#include <string>
#include "LuaState.h"
#include <Windows.h>
#include "HE2_ObjectBinding.h"
#include "GLMBindings.h"
#include <HE2.h>
#include <VulkanBackend.h>
#include "HE2_ScriptedBehaviour.h"

void printMessage(const std::string& s) {
	std::cout << s << std::endl;
}


using namespace luabridge;
int main()
{
	L = luaL_newstate();

	luaL_openlibs(L);

	bindAllGLM();

	bindHE2_Object();

	getGlobalNamespace(L).addFunction("printMessage", printMessage);

	luaL_loadfile(L, "Scripts/script.lua");
	lua_pcall(L, 0, 0, 0);

	//LuaRef sumNumbers = getGlobal(L, "sumNumbers");
	//int result = sumNumbers(5, 4);
	//std::cout << "Result:" << result << std::endl;


	HE2_VulkanBackend* pipeline = nullptr;

	HE2_Instance* heInstance = nullptr;

	HE2_Model* cubeModel = nullptr;
	HE2_Model* planetModel = nullptr;

	HE2_Model* chaletModel;

	HE2_ObjectInstance cube1;

	HE2_ObjectInstance cube2;

	HE2_ObjectInstance skybox;

	HE2_SceneCreator* testScene = nullptr;


	try
	{
		pipeline = new HE2_VulkanBackend();

		heInstance = new HE2_Instance(pipeline);

		heInstance->Init();

		cubeModel = HE2_MeshManager::importModel("models/cubeSimple.obj");


		planetModel = HE2_MeshManager::importModel("models/geosphere.obj");
		//HE2_Model *chaletModel = HE2_MeshManager::importModel("models/chalet.obj");

		HE2_BlinnPhongMaterial* grassyMaterial = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage("Textures/grass02.jpg"), HE2_TextureManager::getImage("Textures/grass02_s.jpg"), HE2_TextureManager::getImage("Textures/grass02_n.jpg"), HE2_Instance::renderBackend->deferredShader);

		HE2_BlinnPhongMaterial* starsMaterial = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage("Textures/stars.jpg"), HE2_TextureManager::getImage("Textures/Grass02_s.jpg"), HE2_TextureManager::getImage("Textures/blankNormal.png"), HE2_Instance::renderBackend->deferredShader);

		cube1 = { cubeModel, grassyMaterial, glm::vec3(0, 0, -15), glm::vec3(1), glm::vec3(0) };

		cube2 = { planetModel, HE2_Instance::renderBackend->defaultMaterial, glm::vec3(0, 0, 15), glm::vec3(1), glm::vec3(0) };

		skybox = { StandardAssets::skybox, grassyMaterial, glm::vec3(0, 0, 0), glm::vec3(100), glm::vec3(0) };

		testScene = new HE2_SceneCreator("Test Scene", { &cube1, &cube2, &skybox });

		HE2_SceneCreator::CreateSceneObjects(testScene);

		HE2_Light* cameraLight = new HE2_Light(HE2_Light::HE2_LightType::HE2_PointLight, glm::vec4(0, 0, 10.0f, 0.0f), 200, 200);

		heInstance->RunGame();
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}

	if (pipeline) delete pipeline;
	if (heInstance) delete heInstance;


	while (true);
	return 0;
}