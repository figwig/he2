#pragma once
#include <HE2_Object.h>
#include "LuaState.h"

using namespace luabridge;
static void bindHE2_Object()
{
	getGlobalNamespace(L).
		beginNamespace("HE2_Object").
		beginClass<HE2_Object>("HE2_Object")
		.addFunction("setPos", &HE2_Object::setPos)
		.addFunction("getPos", &HE2_Object::getPos)
		.endClass()
		.endNamespace();

}