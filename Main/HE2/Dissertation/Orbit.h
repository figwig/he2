#pragma once
#include <HE2.h>
#include <HE2_ConsoleCommand.h>
class Orbit :public HE2_ObjectBehaviour, public HE2_ConsoleCommand
{
public:
	Orbit(HE2_Object* obj) : HE2_ObjectBehaviour(obj), HE2_ConsoleCommand("sun") {}
	void update()
	{
		if (!paused)
		{
			currAngle += Time::deltaTime * 0.05f;
			object->setPos(glm::vec3(cos(currAngle) * radius, 0, sin(currAngle) * radius));
		}
	}

	glm::vec3 lastRot;
	float radius = 8900.0f;
	float currAngle = 0.0f;

	void execute(std::string args)
	{
		if (args == "set day")
		{
			//Assumes planet is at (0,0,0)
			//object->setPos(glm::normalize(HE2_Camera::main->getPos()) * radius);
			currAngle = -atan2(HE2_Camera::main->getPos().x, HE2_Camera::main->getPos().z) + M_PI_2;
		}
		else if (args == "pause")
		{
			paused != paused;
		}
		else if (args == "play")
		{
			paused = true;
		}
	}

	bool paused = false;
};

