#pragma once
#include <HE2.h>
#include "AgentBasedPlanetGeneratorShader.h"
#include <cstdint>

class RidgeAndRiverPregen : public HE2_ShaderBufferGetter, public HE2_ShaderImageReturn, public HE2_GlobalBehaviour, public HE2_ShaderStorageBufferGetter
{

public:
	RidgeAndRiverPregen();


	HE2_BufferDataHandle getBufferHandle(int group, int binding) {

		return bufferHandle3;
	}

	struct LineDetails {
		glm::vec4 points[35] = { glm::vec4(0,0,0,0) };
		int actPoints = 0;
	};

	struct RidgeBlock
	{
		LineDetails ridges[30];
		int actRidges = 0;
	};


	void onGenerateFinished();

	void setImageOutput(HE2_Image* i, int index)
	{
		riverAndRidgeMap = i;
	}

	void update();

	HE2_Image* riverAndRidgeMap = nullptr;

	RidgeBlock ridgeBlock = {};

	HE2_GPUTask* generateTask = nullptr;

	HE2_StorageBufferDataHandle riversBuffer;

	HE2_StorageBufferDataHandle getStorageBufferHandle(int grouping, int handle)
	{
		return riversBuffer;
	}

private:
	friend class PlanetGenerationManager;

	HE2_BufferDataHandle bufferHandle3 = 0;

};

