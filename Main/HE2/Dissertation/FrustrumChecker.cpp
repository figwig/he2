#include "FrustrumChecker.h"
#include <HE2_Camera.h>
#include <imgui.h>
#include <GenericUIElements.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <HE2.h>
#include "AgentPlanetGenerator.h"
#include <HE2_RenderComponent.h>
glm::vec4 FrustumChecker::newSection = glm::vec4(0.0, 0.0, 1.0, 1.0);
glm::vec4 FrustumChecker::activeSectionBiome = glm::vec4(0.0, 0.0, 1.0, 1.0);
glm::vec4 FrustumChecker::activeSectionHeight = glm::vec4(0.0, 0.0, 1.0, 1.0);
glm::vec2 FrustumChecker::lastBotCen = glm::vec2(0, 1);
glm::vec2 FrustumChecker::lastTopCen = glm::vec2(0, 0);

glm::vec4 FrustumChecker::checkerSection = glm::vec4(0, 0, 1, 1);
AgentPlanetGenerator * FrustumChecker::pg = nullptr;

HE2_Object* FrustumChecker::TLObject;
HE2_Object* FrustumChecker::BRObject;

bool FrustumChecker::updateFoliage = false;

bool solveQuadratic(const float& a, const float& b, const float& c, float& x0, float& x1)
{
	float discr = b * b - 4 * a * c;
	if (discr < 0) return false;
	else if (discr == 0) x0 = x1 = -0.5 * b / a;
	else {
		float q = (b > 0) ?
			-0.5 * (b + (double)sqrt(discr)) :
			-0.5 * (b - (double)sqrt(discr));
		x0 = q / a;
		x1 = c / q;
	}
	if (x0 > x1) std::swap(x0, x1);

	return true;
}

void FrustumChecker::createIndicatorObjects()
{
	HE2_Model* sphere = HE2_MeshManager::importModel("Models/GeoSphere.obj");
	HE2_Image* img = HE2_TextureManager::getImage("Textures/bluefade.png");
	HE2_Image* img2 = HE2_TextureManager::getImage("Textures/greenfade.png");
	HE2_BlinnPhongMaterial* mat = new HE2_BlinnPhongMaterial(img, img, img, HE2_RenderBackend::deferredShader);
	HE2_BlinnPhongMaterial* mat2 = new HE2_BlinnPhongMaterial(img2, img2, img2, HE2_RenderBackend::deferredShader);


	HE2_RenderComponent* rc = new HE2_RenderComponent(sphere, mat);
	HE2_RenderComponent* rc2 = new HE2_RenderComponent(sphere, mat2);

	//TLObject = new HE2_Object({rc});
	//BRObject = new HE2_Object({rc2});



	//TLObject->setScale(glm::vec3(0.4f));
	//BRObject->setScale(glm::vec3(0.4f));
}

glm::vec4 FrustumChecker::calculateFromFrustrum(glm::vec3 planetPos, float radius, FrustrumDetails fd)
{
	std::vector<glm::vec3> farPlaneVectors = fd.frustumRays;
	glm::vec3 cameraPosition = HE2_Camera::main->getPos();

	//Put the camera in the middle!
	glm::vec3 planetPosRelative = planetPos - cameraPosition;

	glm::vec3 TL = farPlaneVectors[1];
	glm::vec3 BR = farPlaneVectors[3];
	glm::vec3 TCen = farPlaneVectors[4];
	glm::vec3 BCen = farPlaneVectors[5];
	glm::vec3 cen = glm::normalize(cameraPosition - HE2_Camera::main->getTarget());

	//Here is telling equation b* b - 4 * a * c
	//If it lass than 0, no intersection
	//If it equals 0, one intersection
	//Greater, then it has two intersections
	std::optional<glm::vec3> TLp;
	std::optional<glm::vec3> BRp;
	std::optional<glm::vec3> TCenp;
	std::optional<glm::vec3> BCenp;
	std::optional<glm::vec3> Cenp;

	TLp = findPointOnSphereIntersectingWith(planetPosRelative, radius, TL);

	BRp = findPointOnSphereIntersectingWith(planetPosRelative, radius, BR);

	TCenp = findPointOnSphereIntersectingWith(planetPosRelative, radius, TCen);

	BCenp = findPointOnSphereIntersectingWith(planetPosRelative, radius, BCen);
	//Cenp = findPointOnSphereIntersectingWith(planetPosRelative, radius, cen);

	glm::vec2 texTL, texBR, texCen, texTCen, texBCen;

	if (!TLp.has_value() || !BRp.has_value() || !TCenp.has_value() || ! BCenp.has_value())
	{
		return glm::vec4(0, 0, 1, 1);
		//texCen = glm::vec2(0.5, 0.5);
	}
	else
	{
		texTL = calcTexCoord(TLp.value(), planetPosRelative, radius);
		texBR = calcTexCoord(BRp.value(), planetPosRelative, radius);

		texTCen = calcTexCoord(TCenp.value(), planetPosRelative, radius);
		texBCen = calcTexCoord(BCenp.value(), planetPosRelative, radius);


		//TLObject->setPos(TLp.value() - planetPosRelative);
		//BRObject->setPos(BRp.value() - planetPosRelative);
	}

	if (texTL.x > texBR.x)
	{
		return glm::vec4(0, 0, 1, 1);
	}


	//Calculate cases where the top or bottom point has gone over the north/south pole
	glm::vec4 newSection;

	newSection.x = texTL.x;
	newSection.y = texTL.y;

	newSection.y = glm::min(newSection.y, texTCen.y);

	newSection.z = texBR.x ;
	newSection.w = texBR.y;

	newSection.w = glm::max(newSection.w, texBCen.y);

	lastBotCen = texBCen;
	lastTopCen = texTCen;

	return newSection;
}


glm::vec4 FrustumChecker::calculateFrustrum(glm::vec3 planetPos, float radius)
{
	return calculateFromFrustrum(planetPos, radius, HE2_Camera::main->getFrustrumDetails());
}

glm::vec2 FrustumChecker::calcTexCoord(glm::vec3 point, glm::vec3 planetPos, float radius)
{
	//Move the point so the planet pos = 0,0,0
	point -= planetPos;

	//point.y = -point.y;

	glm::vec3 n = glm::normalize(point);
	float u = atan2(n.x, n.z) / (2.0f * M_PI) + 0.25f;

	float v = acos(point.y /radius) / M_PI;

	glm::vec2 texCoord = glm::vec2(u,v);

	while (texCoord.x > 1.0f)
		texCoord.x -= 1.0f;
	while (texCoord.x < 0.0f)
		texCoord.x += 1.0f;

	while (texCoord.y > 1.0f)
		texCoord.y -= 1.0f;
	while (texCoord.y < 0.0f)
		texCoord.y += 1.0f;

	return texCoord;

}

std::optional<glm::vec3> FrustumChecker::findPointOnSphereIntersectingWith(glm::vec3 sphereLoc, float radius, glm::vec3 ray)
{
	std::optional<glm::vec3> value;

	//A is ray start pos (0,0,0), B is ray direction (ray) and C is sphere centre.

	//a = (xB - xA)� + (yB - yA)� + (zB - zA)�
	float a = pow((ray.x - 0), 2) + pow(ray.y - 0, 2) + pow(ray.z - 0, 2);

	//	b = 2 * ((xB - xA)(xA - xC) + (yB - yA)(yA - yC) + (zB - zA)(zA - zC))
	float b = 2.0f * (((ray.x - 0) * (0 - sphereLoc.x)) + ((ray.y - 0) * (0 - sphereLoc.y)) + ((ray.z - 0) * (0 - sphereLoc.z)));

	//	c = (xA - xC)� + (yA - yC)� + (zA - zC)� - r�
	float c = pow(0 - sphereLoc.x, 2) + pow(0 - sphereLoc.y, 2) + pow(0 - sphereLoc.z, 2) - pow(radius, 2);


	float delta = pow(b, 2) -( 4.0f * a * c);

	//No intersections
	if (delta < 0.0f)
		return value;

	//1 intersection
	if (delta == 0.0f)
	{
		float d = -b / 2.0f* a;

		value = glm::vec3(0 + d * (ray.x - 0), 0 + d*(ray.y - 0), 0 + d*(ray.z -0));
	}

	//2 intersections
	
	float d1, d2;
	d1 = (-b - sqrt(delta)) / 2.0f * a;
	d2 = (-b + sqrt(delta)) / 2.0f * a;


	glm::vec3 int1, int2;
	int1 = glm::vec3(0 + d1 * (ray.x - 0), 0 + d1 * (ray.y - 0), 0 + d1 * (ray.z - 0));
	int2 = glm::vec3(0 + d2 * (ray.x - 0), 0 + d2 * (ray.y - 0), 0 + d2 * (ray.z - 0));

	//Choose which intersection to return based on which is closer to the origin

	if (glm::distance(int1, glm::vec3(0)) > glm::distance(int2, glm::vec3(0)))
	{
		value =  int2;
	}
	else
	{
		value =  int1;
	}

	return value;
}

