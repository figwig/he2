#pragma once
#include <he2.h>
#include "AgentPlanetGenerator.h"
#include "PlanetRenderingComponent.h"
class PlanetGenerationManager;
class GeneratorMenu : public HE2_GlobalBehaviour
{
public:
	GeneratorMenu(AgentPlanetGenerator* pg, PlanetGenerationManager* pgm);
	void update();

private:
	AgentPlanetGenerator* pg;
	PlanetGenerationManager *pgm;
	bool done = false;


	struct PlanetSettings
	{
		bool slotFull = false;
		AgentPlanetGenerator::PlanetUniformBlock pub = {};
		PlanetRenderingComponent::FragmentInputBuffer fib = {};
		int ridgeSeed = 0;
	}allSettings[4];
};