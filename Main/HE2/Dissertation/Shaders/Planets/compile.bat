@echo off

:loop
echo Building Vertex Shader...
C:/VulkanSDK/1.1.114.0/Bin/glslangValidator.exe -V t_vertex_shader_planet.vert
echo .
echo .
echo Building Tess C Shader...
C:/VulkanSDK/1.1.114.0/Bin/glslangValidator.exe -V tcs_planet.tesc
echo .
echo .
echo Building Tess E Shader...
C:/VulkanSDK/1.1.114.0/Bin/glslangValidator.exe -V tes_planet.tese
echo .
echo .
echo Building Frag Shader...
C:/VulkanSDK/1.1.114.0/Bin/glslangValidator.exe -V t_fragment_shader_planet.frag

set /P DUMMY = ENTER to build again...
goto loop
pause