#version 450
#extension GL_ARB_seperate_shader_objects : enable


layout(set = 0,binding = 0) uniform InputBuffer
{
	mat4 model; //Model Matrix
	vec3 origin;// The centre of the sphere
	vec3 cameraPos_World;
};

// Input vertex packet
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 tangent;
layout(location = 3) in vec3 bitangent;
layout(location = 4) in vec2 textureCoord;

// Output vertex packet
layout(location = 0) out packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

	vec3 tangent;
	vec3 bitangent;
	vec3 origin;
	vec3 modelPoint;
	vec3 cameraPos_World;

} outputVertex;

mat4 transform;
mat3 TBN;
vec3 pos;
mat3 normalMat;

void main(void) {
	outputVertex.cameraPos_World = cameraPos_World;

	//Calculate transform matrix
	transform = model;

	//Calculate pos
	pos = position;
	
	outputVertex.modelPoint = pos;

	//Pass through the texture coords
	outputVertex.textureCoord = textureCoord;

	//Transform matrix dealt with
	pos = vec3(transform * vec4(pos,1.0));

	//Output vertex pos
	outputVertex.vert = pos.xyz;
	outputVertex.origin = mat3(transform) * origin;

	//Work out the normal for lighting
	normalMat = transpose(inverse(mat3(transform)));

	outputVertex.normal = normalize(normalMat * normal);

	vec3 T = normalize(vec3(transform * normalize(vec4(tangent,1.0))));

	vec3 B = normalize(vec3(transform * vec4(bitangent,1.0)));

	vec3 N = vec3(outputVertex.normal);

	//T = normalize(T - dot(T, N) * N);

	//B = cross(N, T);

	outputVertex.tangent =T;
	outputVertex.bitangent = B;

	gl_Position = vec4(pos,1.0);
}