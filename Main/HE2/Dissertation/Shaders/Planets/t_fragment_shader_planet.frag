#version 450

layout(set = 2, binding = 0)uniform sampler2D textureImage;
layout(set = 2, binding = 1)uniform sampler2D specularImage;
layout(set = 2, binding = 2)uniform sampler2D normalMap;

layout(set = 3, binding = 0)uniform sampler2D waterTex;
layout(set = 3, binding = 1)uniform sampler2D waterTexSpec;
layout(set = 3, binding = 2)uniform sampler2D waterTexNorm;

layout(set = 1 , binding = 1)uniform sampler2D fragHeightMap;

layout( set =2 , binding =3  ) uniform PlanetFragmentBlock {
	float tilingFactor;
	float radius;
};

#define MAX_LIGHTS 20
float M_PI = 3.14159265358979323846;
float delta = 0.001f;

uniform struct Light {
	vec4 position;
	vec3 colors;
	float attenuation;
	float ambientCoefficient;
	float coneAngle;
	vec3 coneDirection;
	mat4 finalLightMatrix;
};

float when_eq(float x, float y) {
	return 1.0 - abs(sign(x - y));
}

layout(location = 0)in packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

	vec3 cameraPos;
	vec3 debugTexture;

	float biomeMix;

	vec3 tangent;
	vec3 bitangent;

} inputFragment;

float distanceToCam;

float circumfrence;

float fadeDist;
float texelSize;
vec3 mapSpec;

mat3 calcedTBN;
vec3 calcedNormal;

float shadowTestingMultiplier = 1.0f;

// output packet
layout(location = 0) out vec4 fragmentColour;


float getNoiseAtPoint(vec2 tex)
{
	return texture(fragHeightMap, tex).r * 10;
}

float getBiomeMix(vec2 tex)
{
	return texture(fragHeightMap, tex).r * 10;
}

float getUnitLengthXAtY(float y)
{
	//Angle from top 
	float angle = y * M_PI;

	//Covert to latitude
	angle -= M_PI / 2.0f;

	float lengthOfLongitudeLine = cos(angle) * circumfrence;

	float unitX = 1.0f / lengthOfLongitudeLine;

	return unitX;
}

vec3 calculateNormal()
{
	vec3 tangent = inputFragment.tangent;

	vec3 bitangent = inputFragment.bitangent;

	vec2 pointCoord = inputFragment.textureCoord;


	//Delta is always delta in 

	vec2 pointCoordL = pointCoord + vec2(-getUnitLengthXAtY(pointCoord.y), 0);
	vec2 pointCoordR = pointCoord + vec2(getUnitLengthXAtY(pointCoord.y), 0);
	vec2 pointCoordU = pointCoord + vec2(0, delta);
	vec2 pointCoordD = pointCoord + vec2(0, -delta);

	//Use the original TBN matrix to calculate this properly
	float heightL = (radius + getNoiseAtPoint(pointCoordL));
	float heightR = (radius + getNoiseAtPoint(pointCoordR));
	float heightU = (radius + getNoiseAtPoint(pointCoordU));
	float heightD = (radius + getNoiseAtPoint(pointCoordD));

	vec3 tangentNormal = normalize(0.25f * vec3(2.0f * (heightL - heightR), 2.0f * (heightU - heightD), -4.0f));


	mat3 TBN = mat3(tangent, bitangent, normalize(-inputFragment.normal));


	vec3 worldNormal = normalize(TBN * tangentNormal);

	return worldNormal;
}


void main(void) {
	distanceToCam = length(inputFragment.vert - inputFragment.cameraPos);

	circumfrence = radius * 2.0f * M_PI;

	delta = 1.0f / (circumfrence / 2.0f);

	//get the base colour from the texture
	vec4 tempFragColor = texture(textureImage, inputFragment.textureCoord * tilingFactor);

	calcedNormal = calculateNormal();

	//calcedNormal = vec3(0, 1, 0);

	vec3 surfaceToCameraWorld = normalize((inputFragment.cameraPos) - (inputFragment.vert));

	vec4 final = vec4(tempFragColor);


	fragmentColour = final;
	//fragmentColour = vec4(inputFragment.debugTexture,1);
	//fragmentColour = vec4(calcedNormal, 1.0f);
	//fragmentColour = vec4(1, 0, 0, 1);
}


