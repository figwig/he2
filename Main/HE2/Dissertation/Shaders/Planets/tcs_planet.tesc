#version 450
layout (vertices = 3) out;

layout(location =0) in packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

	vec3 tangent;
	vec3 bitangent;
	vec3 origin;
	vec3 modelPoint;
	vec3 cameraPos_World;

} inputVertex[];

layout (location = 0)out packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

	vec3 cameraPos;
	vec3 tangent;
	vec3 bitangent;
	vec3 origin;
	vec3 modelPoint;

} outputVertex[];




float GetTessLevel(float Distance0, float Distance1)
{
	float AvgDistance = (Distance0 + Distance1) / 2.0;

	return 1.0;

	if (AvgDistance <= 100.0) {
		return 25.0;
	}
	else if (AvgDistance <= 150.0) {
		return 15.0;
	}
	else if (AvgDistance <= 300.0) {
		return 10.0;
	}
	else if (AvgDistance <= 600.0) {
		return 7.0;
	}
	else {
		return 1.0;
	}
}

void main(void) {
	if(gl_InvocationID==0){
		float EyeToVertexDistance0 = distance(inputVertex[gl_InvocationID].cameraPos_World, inputVertex[0].vert);
		float EyeToVertexDistance1 = distance(inputVertex[gl_InvocationID].cameraPos_World, inputVertex[1].vert);
		float EyeToVertexDistance2 = distance(inputVertex[gl_InvocationID].cameraPos_World, inputVertex[2].vert);

		gl_TessLevelOuter[0] = GetTessLevel(EyeToVertexDistance1, EyeToVertexDistance2);
		gl_TessLevelOuter[1] = GetTessLevel(EyeToVertexDistance2, EyeToVertexDistance0);
		gl_TessLevelOuter[2] = GetTessLevel(EyeToVertexDistance0, EyeToVertexDistance1);
		gl_TessLevelInner[0] = 1.0;
		gl_TessLevelInner[1] = 1.0;
	}
	
	
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

	outputVertex[gl_InvocationID].textureCoord = inputVertex[gl_InvocationID].textureCoord;

	outputVertex[gl_InvocationID].normal = inputVertex[gl_InvocationID].normal;

	outputVertex[gl_InvocationID].vert = inputVertex[gl_InvocationID].vert;

	outputVertex[gl_InvocationID].TBN = inputVertex[gl_InvocationID].TBN;


	outputVertex[gl_InvocationID].cameraPos = inputVertex[gl_InvocationID].cameraPos_World;

	outputVertex[gl_InvocationID].tangent = inputVertex[gl_InvocationID].tangent;
	outputVertex[gl_InvocationID].bitangent = inputVertex[gl_InvocationID].bitangent;
	outputVertex[gl_InvocationID].origin = inputVertex[gl_InvocationID].origin;
	outputVertex[gl_InvocationID].modelPoint = inputVertex[gl_InvocationID].modelPoint;
}