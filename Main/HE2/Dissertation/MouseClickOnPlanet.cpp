#include "MouseClickOnPlanet.h"
#include <HE2_Debug.h>
void MouseClickOnPlanet::update()
{
	
}

void MouseClickOnPlanet::OnMouseAction(HE2_MouseAction ma)
{
	if (ma.button != HE2_MouseButton::HE2_SCROLLWHEEL_PRESS) return;

	float x = (2.0f * lastX) / HE2_WindowHandler::WIDTH - 1.0f;
	float y = 1.0f - (2.0f * lastY) / HE2_WindowHandler::HEIGHT;
	float z = 1.0f;
	glm::vec3 ray_nds = glm::vec3(x, y, z);

	glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
	glm::vec4 ray_eye = glm::inverse(HE2_Camera::main->getProjectionMatrix()) * ray_clip;

	ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);

	glm::mat4 camMat = HE2_Camera::main->getCameraMatrix();
	glm::vec4 ray_wor = glm::inverse(camMat) * ray_eye;
	// don't forget to normalise the vector at some point
	ray_wor = glm::normalize(ray_wor);


	std::optional<glm::vec3> hitPos = findPointOnSphereIntersectingWith(planet->getPos() - HE2_Camera::main->getPos(), planet->getScale().x * 10.0f, ray_wor);

	if (hitPos.has_value())
	{
		//Do Something
		DebugLog("hit!");
		
		DebugLog(stringFromVec3(glm::normalize(hitPos.value() +HE2_Camera::main->getPos())));
		DebugLog(stringFromVec2(calcTexCoord(hitPos.value() + HE2_Camera::main->getPos(), planet->getPos(), planet->getScale().x * 10.0f)));
	}
}

void MouseClickOnPlanet::OnMouseMove(double x, double y)
{
	lastX = x;
	lastY = y;
}

std::optional<glm::vec3> MouseClickOnPlanet::findPointOnSphereIntersectingWith(glm::vec3 sphereLoc, float radius, glm::vec4 ray)
{
	std::optional<glm::vec3> value;

	//A is ray start pos (0,0,0), B is ray direction (ray) and C is sphere centre.

	//a = (xB - xA)� + (yB - yA)� + (zB - zA)�
	float a = pow((ray.x - 0), 2) + pow(ray.y - 0, 2) + pow(ray.z - 0, 2);

	//	b = 2 * ((xB - xA)(xA - xC) + (yB - yA)(yA - yC) + (zB - zA)(zA - zC))
	float b = 2.0f * (((ray.x - 0) * (0 - sphereLoc.x)) + ((ray.y - 0) * (0 - sphereLoc.y)) + ((ray.z - 0) * (0 - sphereLoc.z)));

	//	c = (xA - xC)� + (yA - yC)� + (zA - zC)� - r�
	float c = pow(0 - sphereLoc.x, 2) + pow(0 - sphereLoc.y, 2) + pow(0 - sphereLoc.z, 2) - pow(radius, 2);


	float delta = pow(b, 2) - (4.0f * a * c);

	//No intersections
	if (delta < 0.0f)
		return value;

	//1 intersection
	if (delta == 0.0f)
	{
		float d = -b / 2.0f * a;

		value = glm::vec3(0 + d * (ray.x - 0), 0 + d * (ray.y - 0), 0 + d * (ray.z - 0));
	}

	//2 intersections

	float d1, d2;
	d1 = (-b - sqrt(delta)) / 2.0f * a;
	d2 = (-b + sqrt(delta)) / 2.0f * a;


	glm::vec3 int1, int2;
	int1 = glm::vec3(0 + d1 * (ray.x - 0), 0 + d1 * (ray.y - 0), 0 + d1 * (ray.z - 0));
	int2 = glm::vec3(0 + d2 * (ray.x - 0), 0 + d2 * (ray.y - 0), 0 + d2 * (ray.z - 0));

	//Choose which intersection to return based on which is closer to the origin

	if (glm::distance(int1, glm::vec3(0)) > glm::distance(int2, glm::vec3(0)))
	{
		value = int2;
	}
	else
	{
		value = int1;
	}

	return value;
}

glm::vec2 MouseClickOnPlanet::calcTexCoord(glm::vec3 point, glm::vec3 planetPos, float radius)
{
	//Move the point so the planet pos = 0,0,0
	point -= planetPos;

	//point.y = -point.y;

	glm::vec3 n = glm::normalize(point);
	float u = atan2(n.x, n.z) / (2.0f * M_PI) + 0.25f;

	float v = 1.0f - (acos(point.y / radius) / M_PI);

	glm::vec2 texCoord = glm::vec2(u, v);

	while (texCoord.x > 1.0f)
		texCoord.x -= 1.0f;
	while (texCoord.x < 0.0f)
		texCoord.x += 1.0f;

	while (texCoord.y > 1.0f)
		texCoord.y -= 1.0f;
	while (texCoord.y < 0.0f)
		texCoord.y += 1.0f;

	return texCoord;

}
