#include "FoliageManager.h"
#include <imgui.h>
#include <HE2_BehaviourManager.h>
#include <GenericUIElements.h>
#include <HE2_RenderComponent.h>
#include <vulkan/vulkan.h>
#include "FrustrumChecker.h"
#include <HE2_Debug.h>
#include <Helpers.h>
#include <HE2_SceneGraph.h>
#include <ResourceGraph.h>
#include <HE2_Scene.h>


FoliageManager::FoliageManager(HE2_Image* heightMap, BiomeGenerator * bg) :bg(bg)
{
	fs = new FoliageShader();
	this->heightMap = heightMap;

	createObjects();
}

float FoliageManager::roundToDecimalPlace(float a, float b)
{
	a *= b;
	a = glm::round(a);
	a /= b;
	return a;
}

void FoliageManager::update()
{
	float distanceToPlanet = glm::distance(HE2_Camera::main->getPos(),glm::vec3(0));

	if (distanceToPlanet < 3500)
	{
		if (inactive)
		{
			//Set active
			inactive = false;
			for(auto a: groups)
				for (auto b : a.foliageComps)
				{
					b->object->active = true;
				}
		}
	}
	else
	{
		if (!inactive)
		{
			//Set Inactive
			inactive = true;
			for (auto a : groups)
				for (auto b : a.foliageComps)
				{
					b->object->active = false;
				}

		}
		return;
	}

	//This assumes planet is at 0,0,0
	glm::vec3 closestPoint = glm::normalize(HE2_Camera::main->getPos()) * 3150.0f;

	glm::vec2 coord = FrustumChecker::calcTexCoord(closestPoint, glm::vec3(0, 0, 0), 3150);

	float quadDivider = 1000.0f;

	coord.x = roundToDecimalPlace(coord.x, quadDivider);
	coord.y = roundToDecimalPlace(coord.y, quadDivider);

	//Have we moved enough to be centred on a different quad?
	if (coord != lastClosestPos || FrustumChecker::updateFoliage)
	{
		FrustumChecker::updateFoliage = false;
		lastClosestPos = coord;

		for (auto a : groups)
		{
			updateGroup(a, coord);
		}
	
	}
}

void FoliageManager::updateGroup(FoliageType ft, glm::vec2 closestCoord)
{
	int objsToCreate = glm::ceil(ft.density / ft.itemsPerRun);
	objsToCreate = 1;
	float quadDivider = 1000.0f;
	int range = ft.range;

	for (int x = -range; x <= range; x++)
		for (int y = -range; y <= range; y++)
			for (int repeat = 0; repeat < objsToCreate; repeat++)
			{
				int xIndex = x + range;
				int yIndex = y + range;


				FoliageRenderingComponent* fc = ft.foliageComps[((((xIndex * (2 * range + 1))) + yIndex) * objsToCreate) + repeat];

				glm::vec2 thisFCCoord = closestCoord;



				fc->buffer.seedOffset = ft.seedOffset;

				glm::vec2 offset = glm::vec2(x * 1 / quadDivider, y * 1 / quadDivider);

				//DebugLog(stringFromVec2(offset));

				thisFCCoord += offset;

				if (bg->getBiomeAtPoint(thisFCCoord)->getBiomeID() < 1)
				{
					fc->object->active = false;
				}
				else
				{
					//fc->object->active = true;
				}
				fc->updateVars(FrustumChecker::activeSectionHeight, thisFCCoord);
			}
}

void FoliageManager::createObjects()
{

	FoliageType treeA = {};
	treeA.name = "TreeA";
	treeA.modelFileName = "models/tree_a.obj";
	treeA.importSettings = { true, false };
	treeA.range = 1;
	//treeA.scale = 1.0f;
	

	createGroup(treeA);

	FoliageType grassA = {};
	grassA.name = "GrassA";
	grassA.modelFileName = "models/grass.obj";
	grassA.importSettings = { false, false };
	grassA.range = 1;
	grassA.scale = (0.02f);

	createGroup(grassA);

}

void FoliageManager::createGroup(FoliageType ft)
{
	ft.model = HE2_MeshManager::importModel(ft.modelFileName, ft.importSettings);

	//HE2_Instance::renderBackend->onAddModel(ft.model);

	std::vector<std::type_index> types2 = { typeid(HE2_ShaderBufferGetter), typeid(HE2_ShaderImageHandleGetter) };

	ft.parent = new HE2_Object();
	ft.parent->name = ft.name + " Parent";
	HE2_Scene::currentScene->sceneGraph.addToRootNodes(ft.parent);
	HE2_SimpleMaterial* mat = new HE2_SimpleMaterial(HE2_TextureManager::getImage(ft.model->defaultMaterial->getImageHandles()[0]),fs);

	int objsToCreate = glm::ceil(ft.density / ft.itemsPerRun);
	objsToCreate = 1;
	int range = ft.range;
	for (int x = -range; x <= range; x++)
		for (int y = -range; y <= range; y++)
			for (int repeat = 0; repeat < objsToCreate; repeat++)
			{

				HE2_RenderComponent* rc = new HE2_RenderComponent(ft.model, mat);
				rc->renderType = HE2_PassFlagTypes::Transparent | HE2_PassFlagTypes::Deferred;

				rc->instances = 100;

				ft.seedOffset = groups.size();

				FoliageRenderingComponent* foliageComp = new FoliageRenderingComponent(heightMap);

				HE2_Object * renderObject = new HE2_Object({ rc }, { std::make_pair(foliageComp, types2) });

				renderObject->name = ft.name+ " Group";

				renderObject->setParent(ft.parent);

				ft.foliageComps.push_back(foliageComp);

				foliageComp->buffer.seedOffset = ft.seedOffset * rc->instances;
				foliageComp->buffer.scale = ft.scale;
				foliageComp->object = renderObject;

				foliageComp->updateVars(FrustumChecker::activeSectionHeight, glm::vec2(0.5, 0.5));

				renderObject->active = false;

			}

	groups.push_back(ft);
}