#include "FoliageRenderingComponent.h"

FoliageRenderingComponent::FoliageRenderingComponent(HE2_Image * heightMap) : heightMap(heightMap)
{
	handle1 = HE2_Instance::renderBackend->makeGPUBuffer(&buffer, sizeof(buffer), true);

}

void FoliageRenderingComponent::updateVars(glm::vec4 section, glm::vec2 quadrant)
{
	buffer.sectionOfMap = section;
	buffer.cameraPos = glm::vec4(HE2_Camera::main->getPos(), 1.0f);
	buffer.quadCoord = glm::vec4(quadrant,0,0);

	HE2_Instance::renderBackend->updateGPUBuffer(handle1, &buffer, sizeof(buffer));
}
