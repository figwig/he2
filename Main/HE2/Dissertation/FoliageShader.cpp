#include "FoliageShader.h"

FoliageShader::FoliageShader()
{
	shaderSources.vertexShaderSource = "shaders/planets/foliagetest.vert.spv";
	shaderSources.fragmentShaderSource = "shaders/planets/foliagetest.frag.spv";
	//shaderSources.geometryShaderSource = "shaders/planets/foliagetest.geom.spv";

	HE2_ShaderFeature feature = {};

	vertexInputDesc = HE2_Vertex::getAttributeDescriptionsHE2();

	perVertexDataDesc.stride = sizeof(HE2_Vertex);

	HE2_ShaderFeature vpMat = viewProjectionFeature;
	//The vp feature in this is in the geometry shader

	features.push_back(vpMat);

	HE2_ShaderFeature coordFeature = {};
	coordFeature.size = sizeof(coordStruct);
	coordFeature.binding = 0;
	coordFeature.grouping = 2;
	coordFeature.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	coordFeature.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	coordFeature.stage = HE2_ShaderStage::HE2_SHADER_STAGE_VERTEX;

	features.push_back(coordFeature);

	HE2_ShaderFeature geomHeightMap = {};
	geomHeightMap.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	geomHeightMap.grouping = 2;
	geomHeightMap.binding = 1;
	geomHeightMap.stage = HE2_SHADER_STAGE_VERTEX;

	geomHeightMap.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	features.push_back(geomHeightMap);

	HE2_ShaderFeature f = HE2_SimpleMaterial::MaterialShaderFeatures()[0];

	features.push_back(f);

	shaderSettings.shaderType = HE2_ShaderType::HE2_SHADER_DEFERRED;


	shaderSettings.isInstanced = true;
	setupShader();
}
