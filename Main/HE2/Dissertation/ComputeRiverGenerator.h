#pragma once
#include <HE2.h>
#include "RidgeAgentComputeShader.h"

class AgentPlanetGenerator;

class ComputeRiverGenerator : public HE2_GlobalBehaviour,  public HE2_ShaderImageHandleGetter, public HE2_ShaderBufferGetter, public HE2_ShaderStorageBufferGetter
{
public:
	ComputeRiverGenerator(AgentPlanetGenerator * pg);

	void finishSetup(HE2_Image* i);

	HE2_GPUTask* generateTask = nullptr; 

	//Get the input image handle - either the input or output. In this case, they are the same image!
	HE2_ImageHandle getImageHandle(int, int);

	void onComplete();

	void update();

	HE2_Image* outputImage = nullptr;

	HE2_BufferDataHandle getBufferHandle(int group, int binding) { return bufferHandle; }
	
	HE2_StorageBufferDataHandle getStorageBufferHandle(int grouping, int handle) {return storageBufferHandle;}


	ComputeRiverShader::RiversOutputBlock riverOutputBlock = {};

	void updateBuffers();
	void Generate();
	AgentPlanetGenerator* pg = nullptr;
private:


	HE2_BufferDataHandle bufferHandle;
	HE2_StorageBufferDataHandle storageBufferHandle;

	HE2_Image* image = nullptr;


	bool generating = false;


	float ticker = 0.0f;
};

