#pragma once
#include <HE2_GLM.h>
#include <HE2_Camera.h>
#include <optional>
class HE2_Object;
class AgentPlanetGenerator;
class FrustumChecker
{
public:
	//xy top left of required section, zw is bottom right
	static glm::vec4 newSection;
	static glm::vec4 activeSectionHeight;
	static glm::vec4 activeSectionBiome;

	static glm::vec4 checkerSection;

	static glm::vec2 lastTopCen;
	static glm::vec2 lastBotCen;

	static bool updateFoliage;

	static glm::vec4 calculateFrustrum(glm::vec3 planetPos, float radius);

	static glm::vec4 calculateFromFrustrum(glm::vec3 planetPos, float radius, FrustrumDetails fd);

	static std::optional<glm::vec3> findPointOnSphereIntersectingWith(glm::vec3 sphereLoc, float radius, glm::vec3 ray);

	static glm::vec2 calcTexCoord(glm::vec3, glm::vec3 planetPos, float radius);

	static void createIndicatorObjects();

	static HE2_Object* TLObject;
	static HE2_Object* BRObject;

	static AgentPlanetGenerator* pg;
};

