#include "FoliageManager2.h"
#include <HE2_RenderComponent.h>
#include "FrustrumChecker.h"
#include "Biomes.h"
#include <HE2_Debug.h>

FoliageManager2::FoliageManager2(BiomeGenerator* bg, AgentPlanetGenerator* pg) : pg(pg), bg (bg)
{
    //Preloading so it doesn't happen first time during runtime
    tree = HE2_MeshManager::importModel("models/Tree_A.obj"); 
    //HE2_Model * tree2 = HE2_MeshManager::importModel("models/Tree_A.obj", { false, false, false }); 
    //HE2_MeshManager::importModel("models/cactus_a.obj", { false, false, true });

    FoliageParent = new HE2_Object();
    FoliageParent->name = "Foliage";

}


FoliageManager2::~FoliageManager2()
{
    for (auto a : objectPool)
        delete a;
}


void FoliageManager2::update()
{
    float distanceToPlanet = glm::distance(HE2_Camera::main->getPos(), glm::vec3(0));

    if (distanceToPlanet < 3800)
    {
        if (inactive)
        {
            //Set active
            inactive = false;
            for (auto a : allFoliageObjects)
                for (auto b : a.second)
                {
                    b->active = true;
                }
            FrustumChecker::updateFoliage = true;
        }
    }
    else
    {
        if (!inactive)
        {
            //Set Inactive
            inactive = true;
            for (auto a : allFoliageObjects)
                for (auto b : a.second)
                {
                    b->active = false;
                }

        }
        return;
    }

    //Anchors are chosen at equidistant points across the surface of the planet. They are used as base points for foliage placement. All anchors within a radius of a player are chosen to render foliage.
    //Foliage positions are generated using a hash of the nearest anchor as a base seed. This ensures that the trees are in the same position each time.

    //Find the closest anchor
    glm::vec3 closestPoint = glm::normalize(HE2_Camera::main->getPos()) * 3150.0f;

    glm::vec2 coord = FrustumChecker::calcTexCoord(closestPoint, glm::vec3(0, 0, 0), 3150);

    float quadDivider = 1000.0f;

    coord.x = roundToDecimalPlace(coord.x, quadDivider);
    coord.y = roundToDecimalPlace(coord.y, quadDivider);

    //Have we moved enough to be centred on a different quad?
    if (coord != lastClosestPos || FrustumChecker::updateFoliage)
    {
        FrustumChecker::updateFoliage = false;
        lastClosestPos = coord;

        //look at current foliage positions. Are they still valid? if not, pull them out and reposition them

        std::vector<glm::vec2> newFoliageAnchors;

        int range = 2;

        //Fill in the available positions based on the new nearest point
        for (int x = -range; x <= range; x++)
            for (int y = -range; y <= range; y++)
                newFoliageAnchors.push_back(coord + glm::vec2(x / quadDivider, y / quadDivider));


        //Make a copy so we can check them off. After we remove all available positions from this map, we have left a list that aren't relevant any more, and a map of the objects that need repositioning
        std::unordered_map<glm::vec2, std::vector<HE2_Object*>> outdatedFoliageObjects = allFoliageObjects;

        std::vector<glm::vec2> nakedFoliageAnchors;

        //Make a list of positions that need re-homing
        for (int i = 0 ; i < newFoliageAnchors.size(); i++)
        {
            glm::vec2 anchorPosition = newFoliageAnchors[i];

            if (outdatedFoliageObjects.count(anchorPosition) > 0)
            {
                //This one is fine, we can leave it how it is. Remove it from the list
                outdatedFoliageObjects.erase(anchorPosition);
            }
            else
            {
                //Make our list of nake foliage anchors
                nakedFoliageAnchors.push_back(anchorPosition);
                
            }
        }

        //Now we have: 
        //- A list of anchors that need filling
        //- A map of objects that are outdated, mapped to their old positions
        
        //First, remove all outdated foliage objects from the master list, and return all their objects to the pool
        for (auto a : outdatedFoliageObjects)
        {
            allFoliageObjects.erase(a.first);
            for (auto obj : a.second)
            {
                obj->active = false;
                objectPool.push_back(obj);
            }
        }

        for (auto anchorPosition : nakedFoliageAnchors)
            populateAnchorPosition(anchorPosition);

   

        int objs = 0;
        for (auto a : allFoliageObjects)
            for (auto b : a.second)
                objs++;

        DebugLog("All anchors updated, now total of " + std::to_string(objs) + " objects, " + std::to_string(nakedFoliageAnchors.size()) + " of which needed updating");
    }


}

void FoliageManager2::populateAnchorPosition(glm::vec2 anchorPosition)
{
    //Look at biome
    //Decide how many objects this wants
    //Grab em from pool (if pool is  empty make more)

    Biome* biome = bg->getBiomeAtPoint(anchorPosition);

    //For now, ignore biomes and place some trees around
    std::vector<HE2_Object* > allAnchorObjects;

    int x = anchorPosition.x * 1000;
    int y = anchorPosition.y * 1000;

    //Make a seed by bitshifting our y coord with our x
    int seed = x ^ (y << 2);

    //if (biome->getBiomeID() < 2) return;

    srand(seed);
    DebugLog("Seed for " + stringFromVec2(anchorPosition) + " is " + std::to_string(seed));

    for (auto foliage : biome->foliageDists)
    {
        int count = foliage.avgCount;
        count += (rand() % foliage.range) - foliage.range / 2;

        //Not lazy, this refers to a map that asks for the model if it's already been imported
        HE2_Model* model = HE2_MeshManager::importModel(foliage.modelName, { false,false,true });// , { true, false, false }

        for (int foliageIndex = 0; foliageIndex < count; foliageIndex++)
        {
            glm::vec3 worldPosTree = getWorldPoint(anchorPosition);

            //Add a random offset


            float newX = ((rand() % 1000) - 500.0f) / 20.0f;
            float newY = ((rand() % 1000) - 500.0f) / 20.0f;
            float newZ = ((rand() % 1000) - 500.0f) / 20.0f;

            worldPosTree += glm::vec3(newX, newY, newZ);
            worldPosTree = glm::normalize(worldPosTree) * 3150.0f;

            HE2_Object* tree1 = getObjectFromPool();

            HE2_RenderComponent* rc = tree1->getComponent<HE2_RenderComponent>();

            rc->setModel(model);
            rc->setMaterial(model->defaultMaterial);

            placeObjectAtPos(tree1, worldPosTree);

            allAnchorObjects.push_back(tree1);
        }
    }
    allFoliageObjects.insert(std::make_pair(anchorPosition, allAnchorObjects));
}

HE2_Object* FoliageManager2::getObjectFromPool()
{
    if (objectPool.size() == 0)
    {
        HE2_RenderComponent* rc = new HE2_RenderComponent(tree, tree->defaultMaterial);

        rc->renderType = HE2_PassFlagTypes::Transparent | HE2_PassFlagTypes::Deferred;

        HE2_Object* renderObject = new HE2_Object({ rc });

        renderObject->name = "Foliage Object";

        renderObject->setScale(glm::vec3(0.01f));
    
        renderObject->setParent(FoliageParent);

        return renderObject;
    }
    else
    {
        HE2_Object* obj = *objectPool.begin();

        objectPool.erase(objectPool.begin());
        return obj;
    }
}

void FoliageManager2::placeObjectAtPos(HE2_Object* object, glm::vec3 pos)
{

    //This method trusts that the position is already correct, ignoring the component of height from centre of planet
    glm::vec3 worldPos = pos;

    object->active = true;

    //DebugLog("World Pos: " + stringFromVec3(worldPos));

    glm::vec2 sphericalCoords = FrustumChecker::calcTexCoord(worldPos, glm::vec3(0), 3150.0f);

    glm::mat4 m = glm::mat4(1);

    //Add any number, normalize it, slightly difference world pos, vector between them is an x Axis
    glm::vec3 xAxis = glm::normalize(glm::normalize(glm::vec3(worldPos) + glm::vec3(0.0, 0.2, 0.0)) - glm::normalize(glm::vec3(worldPos)));
    //Planet is at 0,0,0 so world pos can be used as the normal and vector from origin
    glm::vec3 yAxis = glm::normalize(glm::vec3(worldPos));

    //DebugLog("YAxis is " + stringFromVec3(yAxis));

    //We should be able to cross the y and x axis like a normal and tangent to generate a bitangent
    glm::vec3 zAxis = -glm::normalize(glm::cross(yAxis, xAxis));

    glm::mat3 baseMat = glm::mat3(xAxis, yAxis, zAxis);

    m = glm::mat4(baseMat);

    float height = 12.0f * 3.15f *pg->getHeightAtCoord(sphericalCoords);

    //height = 0.0f;

    glm::mat4 m2 = glm::mat4(1);

    glm::vec3 trans = glm::vec3(0, 3150.0f + height, 0);

    glm::vec3 scale = object->getScale();

    m2[0][0] *= scale.x;
    m2[1][1] *= scale.y;
    m2[2][2] *= scale.z;

    m2[3][0] = trans.x;
    m2[3][1] = trans.y;
    m2[3][2] = trans.z;

    m = m * m2;

    object->setModelMat(m);
}
