#include "RidgeAndRiverGenerator.h"
#include <HE2_GPUTask.h>
RidgeAndRiverGenerator::RidgeAndRiverGenerator()
{
	generateTask = new HE2_GPUTask(new RiversAndRidgesShader(), 6);

	generateTask->addComponent(this, { typeid(RidgeAndRiverGenerator), typeid(HE2_ShaderImageHandleGetter), typeid(HE2_ShaderBufferGetter), typeid(HE2_ShaderImageReturn) });

	ridgeBlock.reqSection = glm::vec4(0, 0, 1, 1);

	//Default river
	


	bufferHandle = HE2_Instance::renderBackend->makeGPUBuffer(&ridgeBlock, sizeof(ridgeBlock), true);

	HE2_Instance::renderBackend->prepareGPUTask(generateTask);
}

void RidgeAndRiverGenerator::onComplete()
{

}

void RidgeAndRiverGenerator::updateBuffers()
{
	HE2_Instance::renderBackend->updateGPUBuffer(bufferHandle, &ridgeBlock, sizeof(ridgeBlock));
}

void RidgeAndRiverGenerator::update()
{

	timer -= Time::deltaTime;
	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_C) && timer < 0.0f)
	{
		timer = 0.3f;
		HE2_Instance::renderBackend->queueGPUTask(generateTask);
	}
}