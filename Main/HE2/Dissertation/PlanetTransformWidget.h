#pragma once
#include <HE2.h>
class PlanetTransformWidget : public HE2_ObjectBehaviour
{
public:
	PlanetTransformWidget(HE2_Object* object);
	~PlanetTransformWidget();

	void update();
private:

	bool widgetOpen = true;
	float scaleOfPlanet = 0.0f;
};

