#include "PlanetGenerationManager.h"
#include "BiomeGenerationShader.h"
#include "AgentBasedPlanetGeneratorShader.h"
#include <HE2_GPUTask.h>
#include "FrustrumChecker.h"
#include "GeneratorMenu.h"
#include <HE2_Debug.h>
#include <sstream>
#include "RiverConstructionShader.h"
#include <HE2_RenderComponent.h>
#include "FoliageManager.h"
#include "FoliageShader.h"
#include "RidgeAndRiverPregen.h"#
#include "RidgesAndRiversMap.h"
#include "RiverConstructionComponent.h"
#include "PerlinNoiseGenerator.h"
#include "FoliageManager2.h"

PlanetGenerationManager::PlanetGenerationManager()
{
	png = new PerlinNoiseGenerator();

	png->gpuTask->onComplete = [=] {};

	HE2_Instance::renderBackend->queueGPUTask(png->gpuTask);

	//Create a generator object
	pg= new AgentPlanetGenerator();

	//Create a new river gene - while this won't finish the river generator setup it will create the rivermap image, which we need for the planet generator
	crg = new ComputeRiverGenerator(pg);

	pg->rrPregen = crg->outputImage;
	pg->riversBuffer = crg->getStorageBufferHandle(0,0);



	//Finish setting up the agent planet generator
	std::vector<std::type_index> types = { typeid(HE2_ShaderBufferGetter), typeid(AgentPlanetGenerator), typeid(HE2_ShaderImageReturn), typeid(HE2_ShaderImageHandleGetter), typeid(HE2_ShaderStorageBufferGetter) };

	planetGenerateTask = new HE2_GPUTask(new AgentBasedPlanetGenerator(), 6);
	planetGenerateTask->addComponent(pg, types);


	rrPregen = new  RidgeAndRiverPregen();
	rrPregen->generateTask = new HE2_GPUTask(new RidgesAndRiversMap(), 6);
	rrPregen->generateTask->addComponent(rrPregen,types);
	rrPregen->riversBuffer = crg->getStorageBufferHandle(0, 0);
	rrPregen->bufferHandle3 = pg->ridgeDataHandle;


	HE2_Instance::renderBackend->prepareGPUTask(rrPregen->generateTask);

	//give the pg a handle to the rrpregen image
	pg->rrPregen = rrPregen->riverAndRidgeMap;

	//The planet generator has all of it's vars available, run the preps
	HE2_Instance::renderBackend->prepareGPUTask(planetGenerateTask);
	

	//Finish setting up the river generator
	crg->finishSetup(pg->heightMap);


	//Create a biome generator
	std::vector<std::type_index> types2 = { typeid(HE2_ShaderImageReturn), typeid(BiomeGenerator), typeid(HE2_ShaderImageHandleGetter), typeid(HE2_ShaderBufferGetter) };

	bg = new BiomeGenerator();
	bg->heightMap = pg->heightMap;
	bg->biomeReadMap = HE2_TextureManager::getImage("Textures/biomesReadable.bmp", true);
	biomeGenerateTask = new HE2_GPUTask(new BiomeGenerationShader(), 6);
	bg->biomeTask = biomeGenerateTask;
	bg->biomeTask->addComponent(bg, types2);
	HE2_Instance::renderBackend->prepareGPUTask(biomeGenerateTask);


	//Tell the planet generator who the biome generator is
	pg->bg = bg;
	pg->generateTask = planetGenerateTask;


	//Queue Renders for the planets and biomes
	bg->biomeTask->onComplete = [=] {bg->onGenerateFinished(); };
	planetGenerateTask->onComplete = [=] {
		pg->onGenerateFinished();

	rcc->input.reqSec = FrustumChecker::newSection; 
	HE2_Instance::renderBackend->updateGPUBuffer(rcc->inputBufferHandle, &(rcc->input), sizeof(rcc->input));
	};

	//Compute Generator has more responsibility on finish
	crg->generateTask->onComplete = [=] {
		crg->onComplete(); 
		
		HE2_Instance::renderBackend->queueGPUTask(rrPregen->generateTask); 

		//Now generate a more refined height map based on the new rr pregen map
		HE2_Instance::renderBackend->queueGPUTask(planetGenerateTask);

		//Finally, generate the biome map
		HE2_Instance::renderBackend->queueGPUTask(biomeGenerateTask);

	};

	rrPregen->generateTask->onComplete = [=] {rrPregen->onGenerateFinished(); };



	//GeneratorMenu* gm = new GeneratorMenu(pg);

	heightMap = pg->heightMap;
	biomeMap = bg->biomeMap;
	bg->pgm = this;


	//Foliage Display
	//fm = new FoliageManager(heightMap, bg);

	new FoliageManager2(bg,pg);

	//River mesh construction

	HE2_Material* riverMat = HE2_Material::noMaterial(new RiverConstructionShader());


	rcc = new RiverConstructionComponent();

	rcc->storageBufferHandle = crg->getStorageBufferHandle(0, 0);

	rcc->heightMap = pg->heightMap;
	HE2_Model* riverModel = new HE2_Model();
	riverModel->vertices.push_back({ glm::vec3(0) });
	riverModel->indices.push_back(0);
	riverModel->indices.push_back(0);
	riverModel->indices.push_back(0);

	HE2_Instance::renderBackend->onAddModel(riverModel);

	HE2_RenderComponent* rc = new HE2_RenderComponent(riverModel, riverMat);

	rc->instances = 108;

	std::vector<std::type_index> rccTypes = { typeid(HE2_ShaderBufferGetter), typeid(HE2_ShaderStorageBufferGetter), typeid(HE2_ShaderImageHandleGetter) };

	//HE2_Object* riverObj = new HE2_Object({ rc }, { std::make_pair(rcc, rccTypes) });
	//riverObj->name = "River construction Object";
}

void PlanetGenerationManager::firstGenerate()
{
	bg->biomeData.seaLevel = pg->planetObject->getComponent<PlanetRenderingComponent>()->fib.seaLevel;

	pg->pub.oceanLevel = pg->planetObject->getComponent<PlanetRenderingComponent>()->fib.seaLevel;
	pg->generateRidges();
	pg->generateRiverStartPositions();	
	crg->updateBuffers();

	generateStart = std::chrono::system_clock::now();
	startFrameID = Time::frameID;

	//First Generation!
	//Display the generated ridges from GPU onto the pregen map
	HE2_Instance::renderBackend->queueGPUTask(rrPregen->generateTask);

	//Generate base height, based on temp pregen image
	HE2_Instance::renderBackend->queueGPUTask(planetGenerateTask);

	//Generate rivers from this. the CRG's on finish procedure is more complex and sorts out the rest for us
	//HE2_Instance::renderBackend->queueGPUTask(crg->generateTask);

	HE2_Instance::renderBackend->queueGPUTask(biomeGenerateTask);
}

void PlanetGenerationManager::generatePlanet()
{
	pg->planetObject->getComponent<PlanetRenderingComponent>()->fib.seaLevel =pg->pub.oceanLevel;
	bg->biomeData.seaLevel = pg->pub.oceanLevel;
	

	pg->generateRidges();
	pg->generateRiverStartPositions();



	firstGenFinished = false;
	generateStart = std::chrono::system_clock::now();
	startFrameID = Time::frameID;

	pg->forceGenerate = false;
	pg->currentActivePlanet = pg->pub;
	pg->agentsBlock = pg->ab;
	pg->ab.requiredSection = FrustumChecker::newSection;


	HE2_Instance::renderBackend->updateGPUBuffer(pg->bufferHandle, &pg->pub, sizeof(pg->pub));

	HE2_Instance::renderBackend->updateGPUBuffer(pg->bufferHandle2, &pg->ab, sizeof(pg->agentsBlock));

	bg->biomeData.requiredSection = FrustumChecker::newSection;
	bg->updateBuffers();

	//HE2_Instance::renderBackend->queueGPUTask(crg->generateTask);

	HE2_Instance::renderBackend->queueGPUTask(rrPregen->generateTask);

	//Generate base height, based on temp pregen image
	HE2_Instance::renderBackend->queueGPUTask(planetGenerateTask);

	//Generate rivers from this. the CRG's on finish procedure is more complex and sorts out the rest for us
	crg->Generate();
	//HE2_Instance::renderBackend->queueGPUTask(crg->generateTask);

	//HE2_Instance::renderBackend->queueGPUTask(biomeGenerateTask);
}

PlanetGenerationManager::~PlanetGenerationManager()
{
}

void PlanetGenerationManager::update()
{
	if ( pg->forceGenerate)
	{
		generateStart = std::chrono::system_clock::now();
		startFrameID = Time::frameID;

		pg->forceGenerate = false;
		pg->currentActivePlanet = pg->pub;
		pg->agentsBlock = pg->ab;
		pg->ab.requiredSection = FrustumChecker::newSection;


		HE2_Instance::renderBackend->updateGPUBuffer(pg->bufferHandle, &pg->pub, sizeof(pg->pub));

		HE2_Instance::renderBackend->updateGPUBuffer(pg->bufferHandle2, &pg->ab, sizeof(pg->agentsBlock));

		bg->biomeData.requiredSection = FrustumChecker::newSection;
		bg->updateBuffers();

		
		reGenerate();
	}
}

void PlanetGenerationManager::generationFinished()
{
	if (!firstGenFinished)
	{
		//Generate rivers from this. the CRG's on finish procedure is more complex and sorts out the rest for us
		HE2_Instance::renderBackend->queueGPUTask(crg->generateTask);

		firstGenFinished = true;
	}


	generateFinish = std::chrono::system_clock::now();

	auto ms = std::chrono::duration<float, std::milli>(generateFinish - generateStart).count();



	std::ostringstream ss;
	ss << ms;
	DebugLog("Generation Finished, took " + ss.str() + " ms, over " + std::to_string(Time::frameID - startFrameID) + " frames");
}

void PlanetGenerationManager::reGenerate()
{

	HE2_Instance::renderBackend->queueGPUTask(planetGenerateTask);

	HE2_Instance::renderBackend->queueGPUTask(biomeGenerateTask);
}