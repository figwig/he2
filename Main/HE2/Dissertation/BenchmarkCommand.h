#pragma once
#include <HE2_ConsoleCommand.h>
#include <HE2.h>
class BenchmarkCommand:public HE2_ConsoleCommand, public HE2_GlobalBehaviour
{
public:
	static void addCommand();

	void execute(std::string) override;
	void update();
private:
	BenchmarkCommand();
	bool benching = false;

	int benchStateFrame = 0;
	float startTime= 0.0f;

	int defBenchSeconds = 120;
};

