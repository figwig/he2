#pragma once
#include <HE2.h>
#include "BiomeGenerator.h"
#include "AgentPlanetGenerator.h"
#include <map>

class FoliageManager2  : public HE2_GlobalBehaviour
{
public:
	FoliageManager2(BiomeGenerator*, AgentPlanetGenerator * pg);
	~FoliageManager2();
	void update();

private:
	bool inactive = true;


	std::vector<HE2_Object*> objects;
	BiomeGenerator* bg = nullptr;
	AgentPlanetGenerator* pg = nullptr;

	void placeObjectAtPos(HE2_Object* object, glm::vec3 pos);

	void populateAnchorPosition(glm::vec2);

	float roundToDecimalPlace(float a, float b)
	{
		a *= b;
		a = glm::round(a);
		a /= b;
		return a;
	}

	glm::vec3 getWorldPoint(glm::vec2 position)
	{
		float radius = 3150.0f;
		float s = (position.x - 0.25) * M_PI * 2;
		float t = position.y * M_PI;

		float z = radius * cos(s) * sin(t);
		float x = radius * sin(s) * sin(t);
		float y = radius * cos(t);


		return glm::vec3(x, y, z);
	}

	//This returns an objectfrom the pool, and creates one if nessecary
	HE2_Object* getObjectFromPool();

	glm::vec2 lastClosestPos = glm::vec2(0, 0);

	//Used so we don't have to create and destroy objects when we do and don't need them
	std::vector<HE2_Object* > objectPool;

	std::unordered_map<glm::vec2, std::vector<HE2_Object*>> allFoliageObjects;

	HE2_Model* tree = nullptr;

	HE2_Object* FoliageParent = nullptr;
};

