#pragma once
#include <HE2.h>
class PerlinNoiseShader : public HE2_Shader
{
public:
	PerlinNoiseShader();

private:
	const std::string vertName = "shaders/planets/agents.vert.spv";
	const std::string fragName = "shaders/planets/perlin.frag.spv";
};

