#include "FlyingCamera.h"
#include <imgui.h>
#include <Helpers.h>
#include <HE2_GLM.h>
#include "FrustrumChecker.h"
#include <GenericUIElements.h>
#include "AgentPlanetGenerator.h"
#include <HE2_RenderComponent.h>
#include <HE2_Debug.h>

/// <summary>
/// Initializes a new instance of the <see cref="FlyingCamera"/> class.
/// </summary>
/// <param name="planet">The planet.</param>
FlyingCamera::FlyingCamera(HE2_Object* planet, HE2_Object* innerAtmosphere, HE2_Object* outerAtmosphere) : HE2_ObjectBehaviour(HE2_Camera::main, false), planet(planet)
{
	this->innerAtmosphere = innerAtmosphere;
	this->outerAtmosphere = outerAtmosphere;

	//cameraLight = new HE2_Light(HE2_Light::HE2_LightType::HE2_PointLight ,glm::vec4(0.5f, 0.5f, 0.5f, 0.0f), 30, 20);
	//cameraLight->setParent(HE2_Camera::main);

	innerAtmosphere->active = false;

	calculateMapViewPosition();

	//HE2_Model* sphere = HE2_MeshManager::importModel("Models/GeoSphere.obj");
	//HE2_Image* img2 = HE2_TextureManager::getImage("Textures/greenfade.png");

	//HE2_BlinnPhongMaterial* mat = new HE2_BlinnPhongMaterial(img2, img2, img2, HE2_RenderBackend::deferredShader);


	//HE2_RenderComponent* rc = new HE2_RenderComponent(sphere, mat);
	//cameraPosIndicator = new HE2_Object({ rc });
}

/// <summary>
/// Finalizes an instance of the <see cref="FlyingCamera"/> class.
/// </summary>
FlyingCamera::~FlyingCamera()
{

}

/// <summary>
/// Updates this instance.
/// </summary>
void FlyingCamera::update()
{
	ticker -= Time::deltaTime;

	if ((ticker< 0.0f && glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_X)) || switchNextFrame)
	{
		switchNextFrame = false;
		localMode = !localMode;
		ticker = 0.2f;


		if (localMode)
		{
			

			rightFromMap = HE2_Camera::main->getCameraRight();
			glfwSetInputMode(HE2_WindowHandler::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
		else
		{
			glfwSetInputMode(HE2_WindowHandler::window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	}

	if (localMode)
	{
		calcluateLocalPosition();
	}
	else if (goingToWaypoint)
	{
		glm::vec3 diff = targetCameraPos - nextWaypointPos;
		targetCameraPos = targetCameraPos - (percentTravel * glm::min(Time::deltaTime, 0.005f) * diff);

		float len = glm::length(diff);
		if (len < 50.0f)
		{
			goingToWaypoint = false;
			vectorToOriginOfPlanet = glm::vec4(-targetCameraPos,0);
		}

		HE2_Camera::main->manualMatrix = false;

		glm::vec3 diff2 = HE2_Camera::main->getPos() - targetCameraPos;
		glm::vec3 actPos = HE2_Camera::main->getPos() - (percentTravel * glm::min(Time::deltaTime, 0.005f) * diff2);

		HE2_Camera::main->setPos(actPos);
		HE2_Camera::main->setTarget(cameraTargetLookAt);
		distanceToSurface = glm::length(vectorToOriginOfPlanet);

		//calcFrustrum();
	}
	else
	{
		calculateMapViewPosition();
	}



	//cameraLight->setPos(HE2_Camera::main->getPos());

	mouseTravelX = 0.0;
	mouseTravelY = 0.0;


	updateNearFar();
}

void FlyingCamera::updateNearFar()
{
	if (localMode)
	{
		HE2_Camera::main->setNearFar(glm::vec2(0.01f, 8000.0f));
	
	}
	else
	{
		HE2_Camera::main->setNearFar(glm::vec2(50.0f, 50000.0f));
	}
}

/// <summary>
/// Calcluates the local position.
/// </summary>
void FlyingCamera::calcluateLocalPosition()
{	//Find the radius!
	float radius = 10 * planet->getScale().x;

	innerAtmosphere->active = true;
	outerAtmosphere->active = false;

	float heightUnderCamera = 12.0f * 3.15f * FrustumChecker::pg->getHeightAtCoord(FrustumChecker::calcTexCoord(HE2_Camera::main->getPos(), glm::vec3(0,0,0), 3150.0f));

	if (glm::distance(HE2_Camera::main->getPos(), planet->getPos()) > heightUnderCamera + 3150.0f + 100.0f)
	{
		//Zoom time!

		//Set target to be the centre of the planet, plus the radius in our direction!
		targetCameraPos = planet->getPos() + glm::normalize(HE2_Camera::main->getPos() - planet->getPos()) * (heightUnderCamera + 3150.0f + 10.0f);
		//cameraTargetLookAt = targetCameraPos + glm::vec3(0, 10, 0);
	}

	//Calulate the viewing plane
	glm::vec3 normal = -glm::normalize((targetCameraPos - planet->getPos()));


	float rotY = (mouseTravelX / 500.0f);
	float rotX = (mouseTravelY / 500.0f);

	localRotX += rotX;
	localRotY += rotY;
	distanceToSurface = glm::length(targetCameraPos - planet->getPos());

	//So since I'm crap at vector math, we're grabbing the last "camera right" angle from the camera as this is guaranteed to be a tangent vector to the surface

	//We're then rotating this around based on the rotation from the mouse
	cameraTargetLookAt = glm::normalize(rightFromMap) * 5.0f;

	cameraTargetLookAt = glm::rotate(cameraTargetLookAt, localRotY, normal);


	glm::vec3 newCamRight = glm::cross(targetCameraPos - cameraTargetLookAt, normal);

	cameraTargetLookAt = glm::rotate(cameraTargetLookAt, localRotX, newCamRight);


	glm::vec3 directionVector = -glm::normalize(cameraTargetLookAt);
	glm::vec3 rightDirectionVector = -glm::normalize(newCamRight);

	float s = Time::deltaTime * localModeSpeed;

	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_A))
	{
		targetCameraPos -= rightDirectionVector * s;
	}
	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_D))
	{
		targetCameraPos += rightDirectionVector * s;
	}

	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_W))
	{
		targetCameraPos += directionVector * s;
	}
	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_S))
	{
		targetCameraPos -= directionVector * s;
	}

	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_R))
	{
		targetCameraPos += normal * s * 10.0f;
	}
	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_F))
	{
		targetCameraPos -= normal * s * 10.0f;
	}

	glm::vec3 diff = HE2_Camera::main->getPos() - targetCameraPos;
	glm::vec3 actPos = HE2_Camera::main->getPos() - (percentTravel * Time::deltaTime * diff);

	HE2_Camera::main->manualMatrix = false;

	HE2_Camera::main->setPos(actPos);
	HE2_Camera::main->setCameraUp(-normal);
	HE2_Camera::main->setTarget(actPos - cameraTargetLookAt);
}


/// <summary>
/// Calculates the map view position.
/// </summary>
void FlyingCamera::calculateMapViewPosition()
{
	HE2_Camera::main->setCameraUp(glm::vec3(0, 1, 0));
	cameraTargetLookAt = planet->getPos();

	if (glm::length(HE2_Camera::main->getPos()) > 3500)
	{
		innerAtmosphere->active = false;
		outerAtmosphere->active = true;
	}

	glm::vec3 camUp;
	glm::vec3 camRight;
	glm::vec3 cameraDirection = glm::normalize(planet->getPos() - HE2_Camera::main->getPos());
	camRight = glm::normalize(glm::cross(glm::vec3(0,1,0), cameraDirection));
	camUp = glm::cross(cameraDirection, camRight);

	float radius = 10 * planet->getScale().x;
	float distanceToSurface = glm::distance(HE2_Camera::main->getPos(), planet->getPos()) - radius;
	float portion = ( (glm::distance(vectorToOriginOfPlanet, glm::vec4(0))- radius) / 7875.0f);


	//Rotate in degs
	float rotY = (float)-  portion * (mouseTravelX / 500.0f);
	float rotX = (float)  portion * (mouseTravelY / 500.0f);



	vectorToOriginOfPlanet = glm::rotate(vectorToOriginOfPlanet, rotY, camUp);
	vectorToOriginOfPlanet = glm::rotate(vectorToOriginOfPlanet, rotX, camRight);

	targetCameraPos = planet->getPos() - glm::vec3(vectorToOriginOfPlanet);



	HE2_Camera::main->manualMatrix = false;

	glm::vec3 diff = HE2_Camera::main->getPos() - targetCameraPos;
	glm::vec3 actPos = HE2_Camera::main->getPos() - (percentTravel * glm::min(Time::deltaTime,0.005f) * diff);

	HE2_Camera::main->setPos(actPos);
	HE2_Camera::main->setTarget(cameraTargetLookAt);
	distanceToSurface = glm::length(vectorToOriginOfPlanet);
	calcFrustrum();

}

void FlyingCamera::calcFrustrum()
{
	//Frustrum stuff!
	glm::vec4 newFrustrum = FrustumChecker::calculateFrustrum(planet->getPos(), planet->getScale().x * 10.f);

	glm::vec2 newTL = glm::vec2(newFrustrum.x, newFrustrum.y);
	glm::vec2 oldTL = glm::vec2(FrustumChecker::checkerSection.x, FrustumChecker::checkerSection.y);

	glm::vec2 newBR = glm::vec2(newFrustrum.z, newFrustrum.w);
	glm::vec2 oldBR = glm::vec2(FrustumChecker::checkerSection.z, FrustumChecker::checkerSection.w);

	if (newTL == glm::vec2(0, 0) && oldTL == glm::vec2(0, 0))
	{
		return;
	}

	//If whole planet is requested by new frustrum, but isn't in exsiting frustrum
	if (newTL == glm::vec2(0, 0) && oldTL != glm::vec2(0, 0))
	{
		updateFrustrum(newFrustrum);

		DebugLog("Switched back to whole planet");
		return;
	}
	newTL = glm::vec2(newFrustrum.x, newFrustrum.y);
	newBR = glm::vec2(newFrustrum.z, newFrustrum.w);

	glm::vec2 oldDim = glm::vec2(0);
	glm::vec2 newDim = glm::vec2(0);

	oldDim = oldBR - oldTL;
	newDim = newBR - newTL;

	//Does the frustrum need to be smaller?
	if (newDim.x < (oldDim.x * 0.65f) || newDim.y < (oldDim.y * 0.65f))
	{
		DebugLog("Made frustrum smaller");

		updateFrustrum(newFrustrum);
	}
	else
	{

		//Has the frustrum moved, but size not changed?
		if (newTL.x < oldTL.x - oldDim.x * 0.1f  || newTL.y < oldTL.y - oldDim.y * 0.1f || newBR.x  > oldBR.x + oldDim.x * 0.1f || newBR.y  > oldBR.y + oldDim.x * 0.1f || (oldTL == glm::vec2(0, 0) && newTL != glm::vec2(0, 0)) || oldTL.y > FrustumChecker::lastTopCen.y || oldBR.y < FrustumChecker::lastBotCen.y )
		{
			newFrustrum.x -= newDim.x / 5.0f;
			newFrustrum.y -= newDim.y / 5.0f;

			newFrustrum.z += newDim.x / 5.0f;
			newFrustrum.w += newDim.y / 5.0f;


			DebugLog("Frustrum moved");

			updateFrustrum(newFrustrum);


		}
		//Checking for size changes
		else if (newDim.x * 0.7f > (oldDim.x) || newDim.y * 0.7f > (oldDim.y))
		{
			DebugLog("Made frustrum bigger");

			//Make the frustrum bigger - the user is zooming out, and could continue!
			newFrustrum.x -= newDim.x / 6.0f;
			newFrustrum.y -= newDim.y / 6.0f;

			newFrustrum.z += newDim.x / 6.0f;
			newFrustrum.w += newDim.y / 6.0f;

			updateFrustrum(newFrustrum);
		}
	}
	//ImGui::Begin("New and Old Frustrum");

	//GenericUIElements::Vec4ReadOut("New", newFrustrum);
	//GenericUIElements::Vec2ReadOut("Dim", newDim);

	//ImGui::End();
}

/// <summary>
/// Called when [mouse move].
/// </summary>
/// <param name="x">The x.</param>
/// <param name="y">The y.</param>
void FlyingCamera::OnMouseMove(double x, double y)
{
	if ((!localMode  && mousePressed) || localMode)
	{
		mouseTravelX = x - lastX;
		mouseTravelY = y - lastY;
	}

	lastX = x;
	lastY = y;
}

/// <summary>
/// Called when [mouse action].
/// </summary>
/// <param name="ma">The ma.</param>
void FlyingCamera::OnMouseAction(HE2_MouseAction ma)
{
	if (ma.button != HE2_MouseButton::HE2_LEFT_MOUSE) return;

	if (ma.action == HE2_ButtonAction::HE2_BUTTON_DOWN)
	{
		mousePressed = true;
	}
	else 
		mousePressed = false;
}

/// <summary>
/// Called when [scroll].
/// </summary>
/// <param name="x">The x.</param>
/// <param name="y">The y.</param>
void FlyingCamera::OnScroll(double x, double y)
{
	//Cheat - 1000 is known as the radius
	lengthToSurface = glm::distance(vectorToOriginOfPlanet, glm::vec4(0)) - 10 * planet->getScale().x;

	//This number needs to scale with magToSurface
	//@m3== 2000, r = 0.2
	//@mag == 20, r= 0.02

	float r = lengthToSurface / 100000.0f;
	//r = lengthToSurface/1000.0f;

	multiplier = (1.0f - ((y) * r));

	multiplier = glm::min(multiplier, 10.0f);
	multiplier = glm::max(multiplier, 0.001f);


	vectorToOriginOfPlanet *= (multiplier);
}

void FlyingCamera::goToWaypoint(glm::vec3 pos)
{
	localMode = false;
	goingToWaypoint = true;
	nextWaypointPos = pos;
	FrustumChecker::pg->forceGenerate = true;
	FrustumChecker::newSection = glm::vec4(0, 0, 1, 1);
	FrustumChecker::checkerSection = glm::vec4(0, 0, 1, 1);
}

void FlyingCamera::updateFrustrum(glm::vec4 newSection)
{


	if (newSection == glm::vec4(0.0f, 0.0f, 1.0f, 1.0f))
	{
		//Already 0,0,1,1? Don't force generation!
		if (eqVec4(FrustumChecker::newSection, newSection))
		{
			DebugLog("Change Cancelled!");
			return;
		}

		FrustumChecker::newSection = newSection;
		FrustumChecker::updateFoliage = true;
		FrustumChecker::checkerSection = newSection;
		FrustumChecker::pg->forceGenerate = true;
		return;
	}
	//Perform some hidden enlargening function


	glm::vec2 tl = glm::vec2(newSection.x, newSection.y);
	glm::vec2 br = glm::vec2(newSection.z, newSection.w);

	//Near the top? This will cause distortion and weirdness!
	if (tl.y < 0.25f || br.y > 0.75f)
	{	
		//Already 0,0,1,1? Don't force generation!
		if (FrustumChecker::newSection == glm::vec4(0.0f, 0.0f, 1.0f, 1.0f))
		{
			DebugLog("Change Cancelled!");
			return;
		}

		//Force to switch back to 0,0,1,1!
		newSection = glm::vec4(0, 0, 1, 1);
		FrustumChecker::newSection = newSection;
		FrustumChecker::checkerSection = newSection;
		FrustumChecker::pg->forceGenerate = true;
		FrustumChecker::updateFoliage = true;
		return;
	}
	FrustumChecker::updateFoliage = true;
	FrustumChecker::checkerSection = newSection;
	FrustumChecker::pg->forceGenerate = true;


	tl *= 0.98f;
	br *= 1.02f;



	tl = glm::clamp(tl, glm::vec2(0), glm::vec2(1));
	br = glm::clamp(br, glm::vec2(0), glm::vec2(1));

	FrustumChecker::newSection = glm::vec4(tl.x, tl.y, br.x, br.y);
	FrustumChecker::updateFoliage = true;
}