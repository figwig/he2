#include "RidgeAgentComputeShader.h"

ComputeRiverShader::ComputeRiverShader()
{
	shaderSources.computeShaderSource = ridgeName;
	shaderSettings.shaderType = HE2_ShaderType::HE2_SHADER_COMPUTE_GPU_TASK;

	HE2_ShaderFeature feature = {};

	feature.featureType = HE2_ShaderFeatureType::HE2_SHADER_FEATURE_STORAGE_IMAGE;
	feature.binding = 0;
	feature.grouping = 0;
	feature.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	feature.stage = HE2_SHADER_STAGE_COMPUTE;

	features.push_back(feature);


	//Change for input buffer
	feature.grouping = 1;
	feature.binding = 0;
	feature.featureType = HE2_ShaderFeatureType::HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	feature.size = sizeof(RiversStartsBlock);
	feature.offset = 0;

	features.push_back(feature);

	feature.binding = 0;
	feature.grouping = 2;
	feature.size = sizeof(RiversOutputBlock);
	feature.offset = 0;
	feature.featureType = HE2_ShaderFeatureType::HE2_SHADER_FEATURE_STORAGE_BUFFER;

	features.push_back(feature);



	setupShader();
}

ComputeRiverShader::~ComputeRiverShader()
{
}
