#include "RiversAndRidgesShader.h"

//This is the old fragment shader based ridge shader -spirv no longer exists!


RiversAndRidgesShader::RiversAndRidgesShader()
{
	shaderSources.vertexShaderSource = vertName;
	shaderSources.fragmentShaderSource = fragName;
	
	shaderSettings.allowBlending = false;
	shaderSettings.shaderType = HE2_SHADER_FRAGMENT_GPU_TASK;

	HE2_ShaderOutput output = {};
	output.format = HE2_ShaderOutputFormat::HE2_RGBA_HIGHP;
	output.outputLocation = 0;
	output.outputType = HE2_ShaderOutputType::HE2_SHADER_OUTPUT_CUSTOM_IMAGE;
	output.size = glm::vec2(4000, 2000);
	shaderSettings.outputs.push_back(output);
	shaderSettings.canBeWireframe = true;

	//HE2_ShaderFeature heightMapFeature = { };
	//heightMapFeature.binding = 0;
	//heightMapFeature.grouping = 0;
	//heightMapFeature.stage = HE2_ShaderStage::HE2_SHADER_STAGE_FRAGMENT;
	//heightMapFeature.offset = 0;
	//heightMapFeature.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	//heightMapFeature.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;

	//HE2_ShaderFeature biomeReadMapFeature = { };
	//biomeReadMapFeature.binding = 1;
	//biomeReadMapFeature.grouping = 0;
	//biomeReadMapFeature.stage = HE2_ShaderStage::HE2_SHADER_STAGE_FRAGMENT;
	//biomeReadMapFeature.offset = 0;
	//biomeReadMapFeature.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	//biomeReadMapFeature.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	//biomeReadMapFeature.exactRead = true;

	HE2_ShaderFeature biomeBlock = {};
	biomeBlock.stage = HE2_SHADER_STAGE_FRAGMENT;
	biomeBlock.binding = 0;
	biomeBlock.grouping = 1;
	biomeBlock.size = sizeof(RidgesBlock);
	biomeBlock.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	biomeBlock.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	

	//features.push_back(heightMapFeature);
	//features.push_back(biomeReadMapFeature);
	features.push_back(biomeBlock);

	perVertexDataDesc = {};
	perVertexDataDesc.nullData = true;

	shaderSettings.allowBlending = true;

	setupShader();
}

RiversAndRidgesShader::~RiversAndRidgesShader()
{

}