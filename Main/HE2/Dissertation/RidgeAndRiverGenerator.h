#pragma once
#include <HE2.h>
#include "RiversAndRidgesShader.h"
class RidgeAndRiverGenerator : public HE2_GlobalBehaviour,
	public HE2_ShaderImageReturn, public HE2_ShaderBufferGetter
{
public:
	RidgeAndRiverGenerator();


	HE2_GPUTask* generateTask = nullptr;

	HE2_Image* outputImage = nullptr;

	HE2_BufferDataHandle getBufferHandle(int group, int binding) { return bufferHandle; }

	RiversAndRidgesShader::RidgesBlock ridgeBlock = {};


	void onComplete();

	void setImageOutput(HE2_Image* i, int index)
	{
		outputImage = i;
	}


	void updateBuffers();

	void update();



private:
	float timer = 1.0f;

	HE2_BufferDataHandle bufferHandle;
};

