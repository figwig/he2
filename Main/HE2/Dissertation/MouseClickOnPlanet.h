#pragma once
#include <HE2.h>
class MouseClickOnPlanet : public HE2_GlobalBehaviour, public HE2_InputListener
{
public:
	MouseClickOnPlanet(HE2_Object* planet) :planet(planet) {}
	void update();
	void OnMouseAction(HE2_MouseAction);
	void OnMouseMove(double x, double y);

private:
	HE2_Object * planet = nullptr;

	double lastX = 0;
	double lastY = 0;

	glm::vec2 calcTexCoord(glm::vec3 point, glm::vec3 planetPos, float radius);

	std::optional<glm::vec3> findPointOnSphereIntersectingWith(glm::vec3 sphereLoc, float radius, glm::vec4 ray);
};

