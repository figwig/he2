#include "RiverConstructionShader.h"
#include "RidgeAgentComputeShader.h"

RiverConstructionShader::RiverConstructionShader()
{
	shaderSources.vertexShaderSource = "shaders/planets/riverconstruction.vert.spv";
	shaderSources.fragmentShaderSource = "shaders/planets/riverconstruction.frag.spv";
	shaderSources.geometryShaderSource = "shaders/planets/riverconstruction.geom.spv";
	shaderSettings.shaderType = HE2_ShaderType::HE2_SHADER_DEFERRED;

	HE2_ShaderFeature feature = {};

	vertexInputDesc = HE2_Vertex::getAttributeDescriptionsHE2();

	perVertexDataDesc.stride = sizeof(HE2_Vertex);

	//View projection and model matrix features
	HE2_ShaderFeature vpMat = viewProjectionFeature;
	//The vp feature in this is in the geometry shader
	vpMat.stage = HE2_ShaderStage::HE2_SHADER_STAGE_GEOMETRY;

	features.push_back(vpMat);

	feature = {};


	feature.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	feature.stage = HE2_SHADER_STAGE_GEOMETRY;
	feature.binding = 1;
	feature.grouping = 1;
	feature.size = sizeof(ComputeRiverShader::RiversOutputBlock);
	feature.offset = 0;
	feature.featureType = HE2_ShaderFeatureType::HE2_SHADER_FEATURE_STORAGE_BUFFER;

	features.push_back(feature);


	feature.size = sizeof(InputBlock);
	feature.binding = 0;
	feature.featureType = HE2_ShaderFeatureType::HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;

	features.push_back(feature);

	HE2_ShaderFeature heightMap = {};
	heightMap.grouping = 1;
	heightMap.binding = 2;
	heightMap.featureType = HE2_ShaderFeatureType::HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	heightMap.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	heightMap.stage = HE2_ShaderStage::HE2_SHADER_STAGE_GEOMETRY;

	features.push_back(heightMap);

	setupShader();

}