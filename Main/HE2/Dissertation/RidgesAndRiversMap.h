#pragma once
#include <HE2.h>
//This shader takes the result from the CPU ridge generation and the output from the GPU river generation and makes them into a heightmap the actual heightmap generator can read
class RidgesAndRiversMap : public HE2_Shader
{
public:
	RidgesAndRiversMap();

private:
	const std::string vertName = "shaders/planets/agents.vert.spv";
	const std::string fragName = "shaders/planets/riversandridgesmap.frag.spv";
};

