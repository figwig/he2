#pragma once
#include <HE2.h>
#include "PlanetDisplayShader.h"
#include "HE2_Scene.h"
class FPSUIElement;
class GeneratorMenu;
class FlyingCamera;
class PlanetGenerationManager;
class PlanetScene
{
public:
	void init();
	void makeShaders();
	void loadAllObjects(bool showDebugTextures = false);

	PlanetGenerationManager* pgm = nullptr;
	FlyingCamera* fc = nullptr;
private:
	HE2_SceneCreator* sceneObject = nullptr;
	HE2_Shader* generationShader = nullptr;
	HE2_Shader* displayShader = nullptr;
	HE2_Shader* biomeShader = nullptr;

	HE2_Model* geoSphere = nullptr;

	FPSUIElement* fpsUI = nullptr;
	HE2_BlinnPhongMaterial* grass = nullptr;
	HE2_BlinnPhongMaterial* dirt = nullptr;
	HE2_BlinnPhongMaterial* quadMat = nullptr;
	HE2_BlinnPhongMaterial* water = nullptr;
	HE2_BlinnPhongMaterial* rock = nullptr;
	HE2_Image* testHeightMap = nullptr;
	
	PlanetDisplayShader::PlanetMaterial* planetMaterial = nullptr;

	HE2_Object* viewQuad = nullptr;

	GeneratorMenu* genMenu = nullptr;

	HE2_Light* sun = nullptr;
};

