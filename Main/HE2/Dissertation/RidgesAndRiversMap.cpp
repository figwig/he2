#include "RidgesAndRiversMap.h"
#include "AgentPlanetGenerator.h"
#include "RidgeAgentComputeShader.h"

RidgesAndRiversMap::RidgesAndRiversMap()
{
	name = "Ridge and River pregen shader";

	shaderSources.vertexShaderSource = vertName;
	shaderSources.fragmentShaderSource = fragName;

	shaderSettings.shaderType = HE2_SHADER_FRAGMENT_GPU_TASK;

	HE2_ShaderOutput output = {};
	output.format = HE2_ShaderOutputFormat::HE2_RGBA_HIGHP;
	output.outputLocation = 0;
	output.outputType = HE2_ShaderOutputType::HE2_SHADER_OUTPUT_CUSTOM_IMAGE;
	output.size = glm::vec2(5000, 2500);
	output.storageCapability = true;

	shaderSettings.outputs.push_back(output);

	HE2_ShaderFeature ridgesBuffer = {};
	ridgesBuffer.binding = 0;
	ridgesBuffer.grouping = 0;
	ridgesBuffer.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	ridgesBuffer.size = sizeof(AgentPlanetGenerator::RidgeBlock);
	ridgesBuffer.offset = 0;
	ridgesBuffer.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	ridgesBuffer.stage = HE2_SHADER_STAGE_FRAGMENT;


	features.push_back(ridgesBuffer);


	HE2_ShaderFeature riversInput = {};

	riversInput.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	riversInput.stage = HE2_SHADER_STAGE_FRAGMENT;
	riversInput.binding = 1;
	riversInput.grouping = 0;
	riversInput.size = sizeof(ComputeRiverShader::RiversOutputBlock);
	riversInput.offset = 0;
	riversInput.featureType = HE2_ShaderFeatureType::HE2_SHADER_FEATURE_STORAGE_BUFFER;

	features.push_back(riversInput);


	perVertexDataDesc = {};
	perVertexDataDesc.nullData = true;

	shaderSettings.allowBlending = true;

	setupShader();
}
