#include "RiverConstructionComponent.h"

RiverConstructionComponent::RiverConstructionComponent()
{
	input.reqSec = glm::vec4(0, 0, 1, 1);

	inputBufferHandle = HE2_Instance::renderBackend->makeGPUBuffer(&input, sizeof(input), true);
}
