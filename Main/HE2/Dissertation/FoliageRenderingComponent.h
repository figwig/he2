#pragma once
#include <HE2.h>
#include "FoliageShader.h"
class FoliageRenderingComponent: public HE2_ShaderBufferGetter, public HE2_ShaderImageHandleGetter
{
public:
	FoliageRenderingComponent(HE2_Image * heightMap);
	FoliageShader::coordStruct buffer = { glm::vec4(0,0,1,1) };

	HE2_BufferDataHandle getBufferHandle(int group, int binding)
	{
		return handle1;
	}
	HE2_ImageHandle getImageHandle(int group, int binding)
	{
		return heightMap->handle;
	}


	void updateVars(glm::vec4 sect, glm::vec2 quad);
	HE2_Object* object = nullptr;
private:
	HE2_BufferDataHandle handle1;
	HE2_Image* heightMap = nullptr;
};

