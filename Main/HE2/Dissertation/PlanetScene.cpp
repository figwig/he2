#include "PlanetScene.h"
#include <HE2.h>
#include <FPSUIElement.h>
#include <HE2_RenderBackend.h>
#include "PlanetRenderingComponent.h"
#include "PlanetDisplayShader.h"
#include <HE2_TextureManager.h>
#include <functional>
#include "PlanetTransformWidget.h"
#include <FlameGraphBehaviour.h>
#include "FlyingCamera.h"
#include "AgentBasedPlanetGeneratorShader.h"
#include "AgentPlanetGenerator.h"
#include "OnScreenTextureShader.h"
#include "GeneratorMenu.h"
#include <StandardAssets.h>
#include "BiomeGenerator.h"
#include "BiomeGenerationShader.h"
#include <HE2_Camera.h>
#include <StandardAssets.h>
#include "FrustrumChecker.h"
#include <HE2_RenderComponent.h>
#include <HE2_GPUTask.h>
#include "RidgeAgentComputeShader.h"
#include "Orbit.h"
#include "MouseClickOnPlanet.h"
#include "PlanetGenerationManager.h"
#include <StandardAssets.h>
#include <HE2_TextureViewRect.h>
#include "AtmosphereShader.h"
#include "RidgeAndRiverPregen.h"
#include "PerlinNoiseGenerator.h"
#include "BenchmarkCommand.h"
#include "BiomeUnderCamera.h"
#include "InnerAtmosphereShader.h"


class SuperSimpleCamera : public HE2_GlobalBehaviour

{
public:
	void update()
	{
		glm::vec3 newPos = HE2_Camera::main->getPos();
		if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_A))
		{
			newPos -= glm::vec3(10, 0, 0);
		}
		if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_D))
		{
			newPos += glm::vec3(10, 0, 0);
		}

		if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_W))
		{
			newPos += glm::vec3(0, 10, 0);
		}
		if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_S))
		{
			newPos -= glm::vec3(0, 10, 0);
		}
		HE2_Camera::main->setTarget(newPos - glm::vec3(1, 0, 0));
		HE2_Camera::main->setPos(newPos);
	}
};


void PlanetScene::init()
{

}

void PlanetScene::makeShaders()
{
	FrustumChecker::createIndicatorObjects();

	testHeightMap = HE2_TextureManager::getImage("Textures/heightMap.jpg");
	HE2_Image* grassTex = HE2_TextureManager::getImage("Textures/grass02.jpg");
	HE2_Image* grassSpec = HE2_TextureManager::getImage("Textures/grass02_s.jpg");
	HE2_Image* grassNormal = HE2_TextureManager::getImage("Textures/grass02_n.jpg",true);

	HE2_Image* waterTexture = HE2_TextureManager::getImage("Textures/waterFar.jpg");
	HE2_Image* waterSpec = HE2_TextureManager::getImage("Textures/waterFarSpec.jpg");
	HE2_Image* waterNormal = HE2_TextureManager::getImage("Textures/waterFarNorm.jpg",true);

	HE2_Image* dirtTex = HE2_TextureManager::getImage("Textures/dirt1.png");
	HE2_Image* dirtSpec = HE2_TextureManager::getImage("Textures/grass02_s.jpg");
	HE2_Image* dirtNormal = HE2_TextureManager::getImage("Textures/dirt1_n.jpg",true);

	HE2_Image* rockTex = HE2_TextureManager::getImage("Textures/rock.jpg");
	HE2_Image* rockSpec = HE2_TextureManager::getImage("Textures/rock_s.jpg");
	HE2_Image* rockNormal = HE2_TextureManager::getImage("Textures/rock_n.jpg",true);
	

	displayShader = new PlanetDisplayShader();


	grass = new HE2_BlinnPhongMaterial(grassTex, grassSpec, grassNormal, HE2_RenderBackend::deferredShader);

	water = new HE2_BlinnPhongMaterial(waterTexture, waterSpec, waterNormal, HE2_RenderBackend::defaultShader);

	dirt = new HE2_BlinnPhongMaterial(dirtTex, dirtSpec, dirtNormal, HE2_RenderBackend::defaultShader);

	rock = new HE2_BlinnPhongMaterial(rockTex, rockSpec, rockNormal, HE2_RenderBackend::defaultShader);

	planetMaterial = new PlanetDisplayShader::PlanetMaterial(displayShader);

	//PlanetTransformWidget* ptw = new PlanetTransformWidget(planetObject.generatedObject);
	//FlameGraphBehaviour* fgb = new FlameGraphBehaviour();
}

void PlanetScene::loadAllObjects(bool showDebugTextures)
{
	BenchmarkCommand::addCommand();

	//Create a list of object instances
	std::vector<HE2_ObjectInstance*> objects;


	//Create Stars material
	HE2_BlinnPhongMaterial* starsMaterial = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage("Textures/stars.jpg"), HE2_TextureManager::getImage("Textures/stars.jpg"), HE2_TextureManager::getImage("Textures/stars.jpg",true), HE2_Instance::renderBackend->deferredShader);


	HE2_ObjectInstance skybox = HE2_ObjectInstance( StandardAssets::skybox, starsMaterial, glm::vec3(0, 0, 0), glm::vec3(3000));

	skybox.name = "Skybox";

	objects.push_back(&skybox);

		//Make a scene object based on this
	sceneObject = new HE2_SceneCreator("Planets", objects);

	//And create all of the objects
	HE2_SceneCreator::CreateSceneObjects(sceneObject);

	geoSphere = HE2_MeshManager::importModel("Models/geosphere3.obj");

	//Move the camera
	sceneObject->camera->setPos(glm::vec3(9450, 0, 0));


	PlanetRenderingComponent* prc = new PlanetRenderingComponent(testHeightMap, grass, water, dirt, rock);


	std::vector<std::type_index>  types = { typeid(HE2_ShaderBufferGetter), typeid(HE2_ShaderImageHandleGetter), typeid(PlanetRenderingComponent), typeid(HE2_UniformValueGetter), typeid(HE2_ShaderStorageBufferGetter) };



	//Setup planet
	pgm = new PlanetGenerationManager();
	prc->storageBufferHandle = pgm->crg->getStorageBufferHandle(0,0);
	prc->heightMap = pgm->heightMap;
	prc->biomeMap = pgm->biomeMap;
	prc->noiseMap = pgm->png->output;

	HE2_RenderComponent* rc = new HE2_RenderComponent(geoSphere, planetMaterial);

	std::vector<HE2_ComponentBase*> rcVec = { rc };
	HE2_Object* planet = new HE2_Object({ rc }, { std::make_pair(prc, types) });

	planet->name = "Planet";

	pgm->pg->planetObject = planet;


	//Set positions
	planet->setPos(glm::vec3(0, 0, 0));
	planet->setScale(glm::vec3(315)); //Scale of 100 means radius is 1000
	prc->setPlanet(planet);


	//Make atmospehere
	HE2_SimpleMaterial* atmosphereMaterial = new HE2_SimpleMaterial(HE2_TextureManager::getImage("Textures/blueFade.png"), new AtmosphereShader());

	HE2_SimpleMaterial* innerAtmosphereMaterial = new HE2_SimpleMaterial(HE2_TextureManager::getImage("Textures/blueFade.png"), new InnerAtmosphereShader());

	HE2_Model* invertedGeosphere = HE2_MeshManager::importModel("models/geosphere.obj", { true, true });
	HE2_Model* atmosGeosphere = HE2_MeshManager::importModel("models/geosphere.obj", { false, true });

	HE2_RenderComponent* aRc = new HE2_RenderComponent(atmosGeosphere, atmosphereMaterial);
	HE2_RenderComponent* iARc = new HE2_RenderComponent(invertedGeosphere, innerAtmosphereMaterial);

	HE2_Object* atmosphere = new HE2_Object({ aRc});
	atmosphere->setPos(planet->getPos());
	atmosphere->setScale(planet->getScale() * 1.03f);

	HE2_Object* invertedAtmosphere = new HE2_Object({ iARc});
	invertedAtmosphere->setPos(planet->getPos());
	invertedAtmosphere->setScale(planet->getScale() * 1.0199f);


	MouseClickOnPlanet* mcop = new MouseClickOnPlanet(planet);

	BiomeUnderCamera* buc = new BiomeUnderCamera();
	buc->bg = pgm->bg;

	//Setup the camera
	fc = new FlyingCamera(planet, invertedAtmosphere, atmosphere);
	//SuperSimpleCamera* ssc = new SuperSimpleCamera();

	if (showDebugTextures)
	{
		//Make a quad material for the height map
		HE2_Image* heightMap = pgm->heightMap;
		quadMat = new HE2_BlinnPhongMaterial(heightMap, heightMap, heightMap, OnScreenTextureShader::getInstance());

		HE2_RenderComponent* heightmapView = new HE2_RenderComponent(StandardAssets::quadModel, quadMat);

		viewQuad = new HE2_Object({ heightmapView });

		viewQuad->setPos(glm::vec3(-0.7, 0.7, 0.0));
		viewQuad->name = "Heightmap View Quad";


		//Make a quad for the biome
		HE2_Image* biomeMap = pgm->bg->biomeMap;
		HE2_TextureViewRect* biomeView = new HE2_TextureViewRect(biomeMap, OnScreenTextureShader::getInstance(), "Biome View Quad");
		biomeView->setPos(glm::vec3(0.7, 0.7, 0.0));


		HE2_Image* riverMpa = pgm->rrPregen->riverAndRidgeMap;
		HE2_TextureViewRect* riverView = new HE2_TextureViewRect(riverMpa, OnScreenTextureShader::getInstance(), "River View Quad");
		riverView->setPos(glm::vec3(0, 0.7, 0.0));
	}



	//Make a sun#
	HE2_Model* sunSphere = HE2_MeshManager::importModel("models/geosphere.obj", { false, true, true });
	HE2_Image* sunTex = HE2_TextureManager::getImage("Textures/sun.png");

	HE2_RenderComponent* sunRC = new HE2_RenderComponent(sunSphere,new HE2_BlinnPhongMaterial(sunTex, sunTex, sunTex, HE2_Instance::renderBackend->deferredShader));

	sun = new HE2_Light(HE2_Light::HE2_LightType::HE2_SpotLight, glm::vec4(190000.0f, 190000.0f, 150000.0f, 0.0f), 100000, 10000, { sunRC }, true);

	sun->name = "Sun";
	Orbit* orbit = new Orbit(sun);

	sun->setScale(glm::vec3(20));
}