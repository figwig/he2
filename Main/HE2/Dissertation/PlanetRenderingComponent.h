#pragma once
#include <HE2.h>

class PlanetRenderingBehaviour;


class PlanetRenderingComponent : virtual public HE2_ShaderBufferGetter, virtual public HE2_ShaderImageHandleGetter, virtual public HE2_UniformValueGetter, public HE2_ShaderStorageBufferGetter
{
public:
	PlanetRenderingComponent(HE2_Image* heightMap, HE2_BlinnPhongMaterial* grass, HE2_BlinnPhongMaterial* water, HE2_BlinnPhongMaterial * dirt, HE2_BlinnPhongMaterial* rock);

	HE2_ImageHandle getImageHandle(int group, int binding);

	HE2_BufferDataHandle getBufferHandle(int group, int binding);

	void* getData(int index);

	void update();

	void setPlanet(HE2_Object* planet);

	struct VertexInputBuffer
	{
		glm::mat4 modelMatrix;
		glm::vec3 origin;
		glm::vec3 cameraPos_World;
	};

	struct FragmentInputBuffer
	{
		float tilingFactor;
		float planetRadius;
		glm::vec4 section;
		glm::vec4 sectionBiome;
		glm::vec4 baseColourMultiplier = glm::vec4(1.0f);
		float seaLevel;
		float basePlanetMix;
	};


	struct TesseInputBuffer
	{
		glm::vec4 requiredSection;
	};

	HE2_Image* heightMap = nullptr;
	HE2_Image* biomeMap = nullptr;
	HE2_Image* noiseMap = nullptr;

	FragmentInputBuffer fib = {};

	HE2_StorageBufferDataHandle storageBufferHandle;

	HE2_StorageBufferDataHandle getStorageBufferHandle(int grouping, int handle)
	{
		return storageBufferHandle;;
	}
private:
	HE2_Object* planet = nullptr;

	HE2_BlinnPhongMaterial* grass = nullptr;
	HE2_BlinnPhongMaterial* water = nullptr;
	HE2_BlinnPhongMaterial* dirt = nullptr;
	HE2_BlinnPhongMaterial* rock = nullptr;


	HE2_BufferDataHandle bufferHandle0 = 0;
	HE2_BufferDataHandle bufferHandle1 = 0;
	HE2_BufferDataHandle bufferHandle2 = 0;
	PlanetRenderingBehaviour* prb = nullptr;

	float tilingFactor = 150000;
	float radius = 6300;

	glm::mat4 cameraMatrix;
};


class PlanetRenderingBehaviour : public HE2_ObjectBehaviour
{
public:
	PlanetRenderingBehaviour(HE2_Object* obj) : HE2_ObjectBehaviour(obj)
	{
		prc = obj->getComponent<PlanetRenderingComponent>();
	}
	void update()
	{
		prc->update();
	}
private:
	PlanetRenderingComponent* prc = nullptr;
};