#include "BiomeUnderCamera.h"
#include "FrustrumChecker.h"
#include <HE2_Debug.h>

void BiomeUnderCamera::execute(std::string args)
{
	glm::vec3 pos = HE2_Camera::main->getPos();

	pos = glm::normalize(pos);

	pos *= 3150;

	glm::vec2 tex = FrustumChecker::calcTexCoord(pos, glm::vec3(0), 3150.0f);

	Biome * b = bg->getBiomeAtPoint(tex);

	DebugLog("Biome under camera :" + b->getBiomeName());
}