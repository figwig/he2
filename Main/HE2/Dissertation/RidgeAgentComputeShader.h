#pragma once
#include <HE2.h>
#include <string>

class ComputeRiverShader :public HE2_Shader
{
public:
	ComputeRiverShader();
	~ComputeRiverShader();

	struct RiversStartsBlock
	{
		glm::vec4 points[200];
		int run = 0;
	};

	struct LineDetails {
		glm::vec4 points[100] = { glm::vec4(0,0,0,0) };
		int actPoints = 0;
	};

	struct RiversOutputBlock
	{
		LineDetails rivers[200];
	};
private:
	const std::string ridgeName = "shaders/planets/rr.comp.spv";

	

};
