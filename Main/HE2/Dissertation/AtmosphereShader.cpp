#include "AtmosphereShader.h"

AtmosphereShader::AtmosphereShader()
{
	shaderSources.vertexShaderSource = "Shaders/planets/atmos.vert.spv";
	shaderSources.fragmentShaderSource = "Shaders/planets/atmos.frag.spv";

	vertexInputDesc = HE2_Vertex::getAttributeDescriptionsHE2();


	perVertexDataDesc.stride = sizeof(HE2_Vertex);



	features.push_back(HE2_Shader::viewProjectionFeature);
	features.push_back(HE2_Shader::modelMatrixUniformFeature);

	for (auto a : HE2_SimpleMaterial::MaterialShaderFeatures())
		features.push_back(a);

	
	//shaderSettings.cullBack = false;
	shaderSettings.allowBlending = true;

	name = "Atmosphere Shader";

	
	setupShader();
}