#pragma once
#include <HE2.h>
#include "BiomeGenerator.h"
#include <HE2_ConsoleCommand.h>
class BiomeUnderCamera : public HE2_ConsoleCommand
{
public:
	BiomeUnderCamera() : HE2_ConsoleCommand("biome") {}
	void execute(std::string);

	BiomeGenerator* bg = nullptr;
};

