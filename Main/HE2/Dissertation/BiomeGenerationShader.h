#pragma once
#include <HE2.h>
class BiomeGenerationShader: public HE2_Shader
{
public:
	BiomeGenerationShader();
	~BiomeGenerationShader();



private:
	const std::string vertName = "Shaders/planets/biomes.vert.spv";
	const std::string fragName = "Shaders/planets/biomes.frag.spv";
};

