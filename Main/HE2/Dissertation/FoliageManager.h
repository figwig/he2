#pragma once
#include <HE2.h>
#include "FoliageShader.h"
#include "FoliageRenderingComponent.h"
#include "BiomeGenerator.h"

class FoliageManager : public HE2_GlobalBehaviour
{
public:
	FoliageManager(HE2_Image * heightMap, BiomeGenerator * bg);

	void update();

	HE2_Image* heightMap = nullptr;

private:
	BiomeGenerator* bg = nullptr;

	HE2_Model* treeModel = nullptr;

	HE2_BufferDataHandle handle1;

	void createObjects();

	FoliageShader* fs = nullptr;

	HE2_Object* planet = nullptr;

	glm::vec2 lastClosestPos;


	//1.0f rounds to nearest 1, 0.1f rounds to nearest 10, 10 rounds to nearest 0.1f;
	float roundToDecimalPlace(float a, float place);
	
	struct FoliageType
	{
		std::string name;
		std::string modelFileName;
		HE2_Model* model;
		HE2_ImportSettings importSettings;
		HE2_Object* parent;


		std::vector<FoliageRenderingComponent*> foliageComps;
		//This squared is the number of foliage groups!
		int range = 2;
		//This is the max allowed items per foliage group (geo shader can reduce this if biome doesn't suit)
		float density = 45;
		//This is the number of items per geo run. number of runs per foliage group is density/itemsperrun
		float itemsPerRun = 15;

		float scale = 0.005f;

		int seedOffset = 0;
	};

	void createGroup(FoliageType ft);

	void updateGroup(FoliageType ft, glm::vec2);

	std::vector<FoliageType> groups;

	bool inactive = true;
};

