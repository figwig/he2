#pragma once
#include <HE2.h>
class RiversAndRidgesShader : public HE2_Shader
{
public:
	RiversAndRidgesShader();
	~RiversAndRidgesShader();

	struct RidgesBlock
	{
		glm::vec4 reqSection;
	};

private:
	const std::string vertName = "Shaders/planets/rr.vert.spv";
	const std::string fragName = "Shaders/planets/rr.frag.spv";
};

