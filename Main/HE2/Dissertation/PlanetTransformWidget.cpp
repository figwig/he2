#include "PlanetTransformWidget.h"
#include <imgui.h>

PlanetTransformWidget::PlanetTransformWidget(HE2_Object* planet) : HE2_ObjectBehaviour(planet)
{
	scaleOfPlanet = planet->getScale().x;
}

PlanetTransformWidget::~PlanetTransformWidget()
{

}

void PlanetTransformWidget::update()
{
	ImGui::Begin("Planet Transform", &widgetOpen, 0);

	if (!&widgetOpen)
	{
		ImGui::End();
		return;
	}

	ImGui::Text("Scale of planet:");
	ImGui::SliderFloat("Scale", &scaleOfPlanet, 100.0f, 1000.0f, "%.3f", 1.0f);

	ImGui::End();

	object->setScale(glm::vec3(scaleOfPlanet));

	float distAway = 50.0f + scaleOfPlanet * 20.0f;

	glm::vec3 pos = object->getPos();

	pos.x = -distAway;

	object->setPos(pos);
}