#include "PlanetRenderingComponent.h"
#include <HE2_Camera.h>
#include "FrustrumChecker.h"
#include <GenericUIElements.h>

PlanetRenderingComponent::PlanetRenderingComponent(HE2_Image* heightMap, HE2_BlinnPhongMaterial* grass, HE2_BlinnPhongMaterial* water, HE2_BlinnPhongMaterial *dirt,HE2_BlinnPhongMaterial * rock) :rock(rock), grass(grass), water(water), heightMap(heightMap), dirt(dirt)
{
	VertexInputBuffer vib = {};

	vib.cameraPos_World = glm::vec3(0);
	void* data;
	data = &vib;
	bufferHandle0 = HE2_Instance::renderBackend->makeGPUBuffer(data, sizeof(VertexInputBuffer), true);

	TesseInputBuffer tib = {};
	tib.requiredSection = glm::vec4(0, 0, 1, 1);

	data = &tib;
	bufferHandle1 = HE2_Instance::renderBackend->makeGPUBuffer(data, sizeof(TesseInputBuffer), true);

	fib.seaLevel = 0.3f;
	fib.planetRadius = 200.0f;
	fib.tilingFactor = 1000.0f;
	fib.section = FrustumChecker::newSection;

	data = &fib;

	bufferHandle2 = HE2_Instance::renderBackend->makeGPUBuffer(data, sizeof(FragmentInputBuffer), true);
}

void PlanetRenderingComponent::setPlanet(HE2_Object* p) 
{
	planet = p;
	prb = new PlanetRenderingBehaviour(planet);
}

void PlanetRenderingComponent::update()
{
	//GenericUIElements::Vec4Slider("Required Area", FrustrumChecker::requiredSection, glm::vec4(0), glm::vec4(1), true);

	//Update the buffers
	VertexInputBuffer vib = {};

	vib.cameraPos_World = HE2_Camera::main->getPos();
	vib.modelMatrix = planet->getModelMatrix();
	vib.origin = planet->getPos();

	void* data = &vib;
	HE2_Instance::renderBackend->updateGPUBuffer(bufferHandle0, data, sizeof(VertexInputBuffer));


	TesseInputBuffer tib = {FrustumChecker::activeSectionHeight };
	data = &tib;
	HE2_Instance::renderBackend->updateGPUBuffer(bufferHandle1, data, sizeof(TesseInputBuffer));


	fib.planetRadius = radius;
	fib.tilingFactor = tilingFactor;
	fib.section = FrustumChecker::activeSectionHeight;
	fib.sectionBiome = FrustumChecker::activeSectionBiome;

	data = &fib;

	HE2_Instance::renderBackend->updateGPUBuffer(bufferHandle2, data, sizeof(FragmentInputBuffer));
}

void* PlanetRenderingComponent::getData(int index)
{

	
	throw std::runtime_error("Invalid uniform index");
	
}

HE2_ImageHandle PlanetRenderingComponent::getImageHandle(int group, int binding)
{
	switch (group)
	{
	case 1:
		switch (binding)
		{
		case 0:
		case 1:
			return heightMap->handle;
			break;
		case 3:
			return biomeMap->handle;
			break;
		case 4:
			return noiseMap->handle;
			break;
		}
		break;
	case 2: 
		switch (binding)
		{
		case 0:
			return grass->getImageHandles()[0];
			break;
		case 1:
			return grass->getImageHandles()[1];
			break;
		case 2:
			return grass->getImageHandles()[2];
			break;
		default:
			throw std::runtime_error("Invalid group/binding");
			break;
		}
		break;
	case 3:
		switch (binding)
		{
		case 0:
			return water->getImageHandles()[0];
			break;
		case 1:
			return water->getImageHandles()[1];
			break;
		case 2:
			return water->getImageHandles()[2];
			break;
		default:
			throw std::runtime_error("Invalid group/binding");
			break;
		}
		break;
	case 4:
		switch (binding)
		{
		case 0:
			return dirt->getImageHandles()[0];
			break;
		case 1:
			return dirt->getImageHandles()[1];
			break;
		case 2:
			return dirt->getImageHandles()[2];
			break;
		default:
			throw std::runtime_error("Invalid group/binding");
			break;
		}
		break;
	case 5:
		switch (binding)
		{
		case 0:
			return rock->getImageHandles()[0];
			break;
		case 1:
			return rock->getImageHandles()[1];
			break;
		case 2:
			return rock->getImageHandles()[2];
			break;
		default:
			throw std::runtime_error("Invalid group/binding");
			break;
		}
		break;
	default:
		throw std::runtime_error("Invalud group");
		break;
	}
}

HE2_BufferDataHandle PlanetRenderingComponent::getBufferHandle(int group, int binding)
{
	switch (group)
	{
	case 6:
		return bufferHandle0;
	case 1:
		return bufferHandle1;
	case 2:
		return bufferHandle2;
	default:
		throw std::runtime_error("Invalid Buffer ID");
	}
}
