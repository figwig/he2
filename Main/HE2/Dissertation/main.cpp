#include <HE2.h>
#include <VulkanBackend.h>
#include <iostream>
#include "PlanetScene.h"
#include <FlameGraphBehaviour.h>
#include <TestingBehaviours.h>
#include <FPSUIElement.h>
#include <SceneGraphDisplayBehaviour.h>
#include <ConsoleUI.h>
#include <WorkerUI.h>
#include <HE2_JobCentre.h>

void primeChecker(int start, int range)
{
	int i, num = start, primes = 0;

	while (num <= start + range) {
		i = 2;
		while (i <= num) {
			if (num % i == 0)
				break;
			i++;
		}
		if (i == num)
			primes++;


		num++;
	}
	printf("%d prime numbers calculated between %d and %d\n", primes, start, range);
}


int main()
{
	std::function <void()> job1 = [=]
	{
		primeChecker(1, 600000000);
	};

	HE2_Model* cubeModel = nullptr;
	HE2_Model* planetModel = nullptr;


	HE2_ObjectInstance cube1;

	HE2_ObjectInstance cube2;

	HE2_Scene* testScene = nullptr;

	CameraWire* camBeh = nullptr;

	FlameGraphBehaviour* flame = nullptr;
	try {
		HE2_InstanceSettings Is = { "Planet Generation Demo", true };

		HE2_VulkanBackend* vulkanBackend = new HE2_VulkanBackend();
		HE2_Instance* instance = new HE2_Instance(vulkanBackend, Is);

		instance->Init();

		PlanetScene* ps = new PlanetScene();
		ps->init();
		ps->makeShaders();
		ps->loadAllObjects();

		SceneGraphDisplayBehaviour* sceneGraph = new SceneGraphDisplayBehaviour();

		ConsoleUI* consoleUI = new ConsoleUI();
		//WorkerUI* workerUI = new WorkerUI();

		//HE2_JobCentre::singleton()->QueueJob("Prime finding", job1);

		instance->RunGame();

		HE2_JobCentre::singleton()->release();

		glfwTerminate();

		delete sceneGraph;
		delete consoleUI;
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;

		glfwTerminate();
		while (true);
	}
	catch (...)
	{
		std::cout << "Unknown Execption" << std::endl;
		glfwTerminate();
		while (true);
	}

	return 0;
}