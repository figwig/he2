#pragma once
#include<HE2.h>

class OnScreenTextureShader : public HE2_Shader
{
public:
	static OnScreenTextureShader* getInstance();
	
private:
	static OnScreenTextureShader* thisPointer;

	OnScreenTextureShader();
};

