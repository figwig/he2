#include "GeneratorMenu.h"
#include <imgui.h>
#include "AgentPlanetGenerator.h"
#include "FrustrumChecker.h"
#include "PlanetRenderingComponent.h"
#include "PlanetGenerationManager.h"

float randomNormalizedFloat()
{
	return (rand() % 1000) / 1000.0f;
}

GeneratorMenu::GeneratorMenu(AgentPlanetGenerator* pg, PlanetGenerationManager *pgm) : pg(pg), pgm(pgm)
{
	allSettings[1].slotFull = true;

	allSettings[1].pub = pg->pub;

	allSettings[1].pub.oceanLevel = 316.84f;
	allSettings[1].pub.radius = 131.0f;
	allSettings[1].pub.noiseOffset.x = 209.0f;

	allSettings[1].ridgeSeed = 0;


	allSettings[2].slotFull = true;
	allSettings[2].pub = pg->pub;
	allSettings[2].pub.noiseLayers[0].coordMod = 0.014f;
	allSettings[2].pub.noiseLayers[0].scale = 4.0f;
	allSettings[2].pub.noiseLayers[0].additionConst = 0.088f;
	allSettings[2].pub.noiseLayers[0].exponent = 1.598f;

	allSettings[2].pub.noiseLayers[1].active = 1.0f;
	allSettings[2].pub.noiseLayers[1].coordMod = 0.018f;
	allSettings[2].pub.noiseLayers[1].scale = 1.993f;
	allSettings[2].pub.noiseLayers[1].constructive = 0.0f;
	allSettings[2].pub.noiseLayers[1].exponent = 2.042f;
	allSettings[2].pub.noiseLayers[1].scaleDependsOn = -1.0f;
	allSettings[2].pub.noiseLayers[1].octaves = 8.0f;
	allSettings[2].ridgeSeed = 17;
	allSettings[2].pub.oceanLevel = 338.57f;
	allSettings[2].fib.planetRadius = 135.0f;
	allSettings[2].pub.noiseOffset.x = 5822.0f;
	allSettings[2].fib.baseColourMultiplier = glm::vec4(0.906, 0.289, 0.289, 1.0);

}

void GeneratorMenu::update()
{
	bool open;

	if (done) return;
	ImGui::Begin("World Generator Settings", &open,0);

	ImGui::Text("Planet Saves Below");

	for (int i = 0; i < 4; i++)
	{
		if (allSettings[i].slotFull)
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4( 0.05f,0.11f,0.4f, 1.0f ));

		bool changed = false;

		if (ImGui::Button(("Save " + std::to_string(i)).c_str()))
		{
			if (allSettings[i].slotFull)
			{
				pg->ridgeSeed = allSettings[i].ridgeSeed;
				pg->pub = allSettings[i].pub;

				pg->planetObject->getComponent<PlanetRenderingComponent>()->fib = allSettings[i].fib;

				//Generate
				pgm->generatePlanet();
			}
			else
			{
				allSettings[i].slotFull = true;
				changed = true;

				allSettings[i].ridgeSeed = pg->ridgeSeed;
				allSettings[i].pub = pg->pub;

				allSettings[i].fib = pg->planetObject->getComponent<PlanetRenderingComponent>()->fib;
			}
		}
		if (i != 3)
			ImGui::SameLine();

		//! Changed as if slot full changes this round it'll trip up
		if (allSettings[i].slotFull && !changed)
			ImGui::PopStyleColor();
	}

	if (ImGui::Button("Randomize Planet"))
	{
		srand(Time::time);

		pg->ridgeSeed = rand() % 100;
		pg->pub.radius = (rand() % 100) + 100;
		pg->pub.noiseOffset.x = (rand() % 10000) + 0;
		pg->pub.oceanLevel = pow(rand() % 180 + 10,2) / 100.0f;

		pg->planetObject->getComponent<PlanetRenderingComponent>()->fib.baseColourMultiplier = glm::vec4(randomNormalizedFloat(), randomNormalizedFloat(), randomNormalizedFloat(), randomNormalizedFloat());

		pgm->generatePlanet();
	}

	if (ImGui::CollapsingHeader("Shape Settings"))
	{

		ImGui::SliderInt("Ridges Seed", &pg->ridgeSeed, 0, 100);

		ImGui::SliderFloat("Planet Noise Scale", &pg->pub.radius, 45.0f, 200.0f);
		ImGui::SliderFloat("Planet Seed", &pg->pub.noiseOffset.x, 0.0f, 200.0f);

		ImGui::SliderFloat("SeaLevel", &pg->pub.oceanLevel, 0.01f, 200.0f, "%.3f", 3.0f);
	}

	if (ImGui::CollapsingHeader("Colour Settings"))
	{
		ImGui::SliderFloat("Planet Base Grass/Dirt Mix", &pg->planetObject->getComponent<PlanetRenderingComponent>()->fib.basePlanetMix, 0.0f, 1.0f, "%.3f");

		ImGui::ColorPicker4("Base Planet Colour Multipler", (float*)&pg->planetObject->getComponent<PlanetRenderingComponent>()->fib.baseColourMultiplier, ImGuiColorEditFlags_Float);
	}

	if (ImGui::CollapsingHeader("Biome Settings"))
	{
		ImGui::SliderFloat("Planet Temperature", &pg->bg->biomeData.tempAddition, - 0.8f, 0.8f, "%.3f");
		ImGui::SliderFloat("Planet Wetness", &pg->bg->biomeData.moistureAddtion, -0.8f, 0.8f, "%.3f");

	}

	for (int i = 0; i < 5; i++)
	{
		if (ImGui::CollapsingHeader(("Layer " + std::to_string(i)).c_str()))
		{
			bool constructive = pg->pub.noiseLayers[i].constructive ==1.0f ? true: false;
			bool active = pg->pub.noiseLayers[i].active == 1.0f ? true : false;


			ImGui::SliderFloat(("Coord Modifier" + std::to_string(i)).c_str(), &pg->pub.noiseLayers[i].coordMod, 0.0f, 10.0f, "%.3f", 3.0f);
			ImGui::SliderFloat(("Scale " + std::to_string(i)).c_str(), &pg->pub.noiseLayers[i].scale, 0.0f, 10.0f);
			ImGui::SliderFloat(("Exponent " + std::to_string(i)).c_str(), &pg->pub.noiseLayers[i].exponent, 0.0f, 8.0f);
			ImGui::Checkbox(("Constructive " + std::to_string(i)).c_str(), &constructive);

			ImGui::SliderFloat(("Scale Depends On " + std::to_string(i)).c_str(), &pg->pub.noiseLayers[i].scaleDependsOn, -1.0f, 20.0f);
			ImGui::SliderFloat(("Minimum " + std::to_string(i)).c_str(), &pg->pub.noiseLayers[i].additionConst, 0.0f, 0.1f);
			
			ImGui::SliderFloat(("octaves " + std::to_string(i)).c_str(), &pg->pub.noiseLayers[i].octaves, 1.0f, 40.0f);
			ImGui::Checkbox(("Active " + std::to_string(i)).c_str(), &active);

			pg->pub.noiseLayers[i].scaleDependsOn = glm::round(pg->pub.noiseLayers[i].scaleDependsOn);
			pg->pub.noiseLayers[i].octaves = glm::round(pg->pub.noiseLayers[i].octaves);


			if (constructive)
				pg->pub.noiseLayers[i].constructive = 1.0f;
			
			else
				pg->pub.noiseLayers[i].constructive = 0.0f;

			if (active)
				pg->pub.noiseLayers[i].active = 1.0f;
		
			else
				pg->pub.noiseLayers[i].active = 0.0f;

		}
	}
	//ImGui::ShowDemoWindow();
	
	if (ImGui::Button("Generate!"))
	{
		pgm->generatePlanet();
	}
	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();

	if (ImGui::Button("Next Step!"))
	{
		done = true;
	}

	ImGui::End();

	//ImGui::Begin("AgentPosition");
	//ImGui::SliderFloat("x", &pg->ab.IslandAgents[0].position.x, 0.0f, 1.0f);
	//ImGui::SliderFloat("y", &pg->ab.IslandAgents[0].position.y, 0.0f, 1.0f);
	//ImGui::End();
}