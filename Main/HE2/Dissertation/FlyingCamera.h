#pragma once
#include <HE2.h>
class FlyingCamera : public HE2_ObjectBehaviour, public HE2_InputListener
{
public:
	FlyingCamera(HE2_Object * planet, HE2_Object * innerAtmosphere,HE2_Object * outerAtmosphere);
	~FlyingCamera();

	void update();

	void OnMouseMove(double x, double y);
	void OnMouseAction(HE2_MouseAction ma);
	void OnScroll(double, double);

	bool switchNextFrame = false;

	void goToWaypoint(glm::vec3);

private:
	HE2_Object* innerAtmosphere;
	HE2_Object* outerAtmosphere;

	HE2_Object* cameraPosIndicator = nullptr;

	bool localMode = false;

	bool goingToWaypoint = false;
	glm::vec3 nextWaypointPos = glm::vec3(0);

	void calculateMapViewPosition();
	void calcluateLocalPosition();

	void calcFrustrum();

	HE2_Object* planet = nullptr;
	double lastX = 0.0;
	double lastY = 0.0;
	bool mousePressed = false;

	double mouseTravelX = 0.0;
	double mouseTravelY = 0.0;

	glm::vec4 vectorToOriginOfPlanet = glm::vec4(-9450, 0, 0,0);
	double scrollRange = 1.0f;

	float exponent = 3.2f;

	float percentTravel = 15.0f;

	glm::vec3 targetCameraPos;
	glm::vec3 cameraTargetLookAt;

	HE2_Light* cameraLight = nullptr;

	float multiplier;
	float lengthToSurface;
	float portion = 0.0f;

	float ticker = 0.0f;
	glm::vec3 lookVector = glm::vec3(10, 0, 0);

	glm::vec3 rightFromMap;

	float localRotX = 0.0f;
	float localRotY = 0.0f;
	float localModeSpeed = 3.1f;

	void updateFrustrum(glm::vec4 newSection);

	void updateNearFar();

	float distanceToSurface = 9000.0f;

};

