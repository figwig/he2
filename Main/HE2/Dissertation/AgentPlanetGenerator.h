#pragma once
#include <HE2.h>
#include "AgentBasedPlanetGeneratorShader.h"
#include <cstdint>
#include <stb_image.h>

class BiomeGenerator;

class AgentPlanetGenerator : public HE2_ShaderBufferGetter, public HE2_ShaderImageReturn, public HE2_GlobalBehaviour, public HE2_ShaderImageHandleGetter, public HE2_ShaderStorageBufferGetter
{
public:


	AgentPlanetGenerator();
	~AgentPlanetGenerator();

	HE2_BufferDataHandle getBufferHandle(int group, int binding) {
		if (group == 0)  
			return bufferHandle; 
		if (group == 1) 
		{ 
			if (binding == 0)
				return bufferHandle2; 
			else 
				return ridgeDataHandle;
		}
	}
	

	void generatePlanet();

	HE2_ImageHandle getImageHandle(int group, int binding) {
		return rrPregen->handle;
	}

	HE2_Image* rrPregen = nullptr;
	struct Layer
	{
		float coordMod;
		float exponent;
		float scale;
		float constructive;

		float scaleDependsOn;
		float additionConst;
		float octaves = 0.0f;
		float active;

		bool operator== (const Layer& other)const
		{
			return (other.coordMod == coordMod && other.exponent == exponent && other.scale == scale && constructive == other.constructive && other.scaleDependsOn == scaleDependsOn && additionConst == other.additionConst && octaves == other.octaves && active == other.active);
		}

	};

	struct PlanetUniformBlock
	{
		Layer noiseLayers[5];
		int actualLayerCount;
		float oceanLevel;
		float radius;
		glm::vec4 noiseOffset = glm::vec4(0,0,0,0);

		bool operator== (const PlanetUniformBlock& other) const
		{
			if (actualLayerCount != other.actualLayerCount || oceanLevel != other.oceanLevel || radius != other.radius)
				return false;

			for (int i = 0; i < 5; i++)
			{
				if (!(noiseLayers[i] == other.noiseLayers[i]))
				{
					return false;
				}
			}

			return true;
		}

	};

	struct LineDetails {
		glm::vec4 points[35] = { glm::vec4(0,0,0,0) };
		int actPoints = 0;
	};

	struct RidgeBlock
	{
		LineDetails ridges[30];
		int actRidges = 0;
	};

	struct AgentsBlock
	{
		glm::vec4 requiredSection;
	
		float minHeightForMountain = 0.85f;

		bool operator == (const AgentsBlock& other) const
		{
			return minHeightForMountain ==  other.minHeightForMountain;
			 
		}
	};

	void onGenerateFinished();
	glm::vec2 calcTexCoord(glm::vec3 point, glm::vec3 planetPos, float radius);

	void setImageOutput(HE2_Image* i, int index)
	{
		heightMap = i;
	}

	void update();
	HE2_Image* heightMap = nullptr;

	PlanetUniformBlock pub = {};
	AgentsBlock ab = { };


	HE2_GPUTask* generateTask = nullptr;
	HE2_GPUTask* biomeTask = nullptr;
	HE2_Object* planetObject = nullptr;

	BiomeGenerator* bg = nullptr;

	bool forceGenerate = false;

	HE2_StorageBufferDataHandle riversBuffer;

	HE2_StorageBufferDataHandle getStorageBufferHandle(int grouping, int handle)
	{
		return riversBuffer;
	}
	AgentsBlock agentsBlock;

	int ridgeSeed = 39;
	void generateRidges();
	int riverPosSeed = 0;
	void generateRiverStartPositions();

	ComputeRiverShader::RiversStartsBlock riverBlock = {};
	RidgeBlock ridgeBlock = {};

	float getHeightAtCoord(glm::vec2 tex);

private:
	friend class PlanetGenerationManager;

	bool isData = false;
	pixel64 pixels[5000 * 2500];


	HE2_BufferDataHandle bufferHandle = 0;
	HE2_BufferDataHandle bufferHandle2 = 0;
	HE2_BufferDataHandle ridgeDataHandle = 0;

	PlanetUniformBlock currentActivePlanet;


	float x = 0.5f;
	float y = 0.5f;

};

