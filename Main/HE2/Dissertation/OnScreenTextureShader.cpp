#include "OnScreenTextureShader.h"

OnScreenTextureShader* OnScreenTextureShader::thisPointer = nullptr;

OnScreenTextureShader::OnScreenTextureShader()
{
	shaderSettings.shaderType = HE2_ShaderType::HE2_SHADER_IMMEDIATE;
	//Transparency is ignored by this shader
	shaderSettings.allowBlending = false;

	for (auto x : HE2_BlinnPhongMaterial::MaterialShaderFeatures())
	{
		//x.grouping = 0;
		features.push_back(x);
	}

	features.push_back(HE2_Shader::modelMatrixUniformFeature);

	shaderSources.vertexShaderSource = "shaders/VQuad.vert.spv";
	shaderSources.fragmentShaderSource = "shaders/VQuad.frag.spv";

	perVertexDataDesc.stride = sizeof(HE2_Vertex);

	for (auto x : HE2_Vertex::getAttributeDescriptionsHE2())
		vertexInputDesc.push_back(x);
	name = "Textured Quad Shader";

	setupShader();
}

OnScreenTextureShader* OnScreenTextureShader::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new OnScreenTextureShader();
	}
	return thisPointer;
}