#pragma once
#include "AgentPlanetGenerator.h"
#include "BiomeGenerator.h"
#include "RidgeAndRiverGenerator.h"
#include <HE2_GPUTask.h>
#include <chrono>
#include "ComputeRiverGenerator.h"
#include "RidgeAgentComputeShader.h"


class RidgeAndRiverPregen;
class FoliageManager;
class RiverConstructionComponent;
class PerlinNoiseGenerator;
class PlanetGenerationManager : public HE2_GlobalBehaviour
{
public:

	PlanetGenerationManager();
	~PlanetGenerationManager();

	void update();

	HE2_Image* heightMap = nullptr;
	HE2_Image* biomeMap = nullptr;


	BiomeGenerator* bg = nullptr;
	AgentPlanetGenerator* pg = nullptr;
	ComputeRiverGenerator* crg = nullptr;
	RidgeAndRiverPregen* rrPregen = nullptr;
	RiverConstructionComponent* rcc = nullptr;
	PerlinNoiseGenerator* png = nullptr;

	HE2_GPUTask* biomeGenerateTask = nullptr;
	HE2_GPUTask* planetGenerateTask = nullptr;
	HE2_GPUTask* ridgeGenerateTask = nullptr;

	void generationFinished();

	void setRadiusSeed(float seed)
	{
		pg->pub.radius = seed;
		
	}

	void generatePlanet();

	void firstGenerate();

private:
	bool firstGenFinished = false;

	void reGenerate();

	std::chrono::system_clock::time_point generateStart;
	int startFrameID = 0;
	std::chrono::system_clock::time_point generateFinish;

	HE2_StorageBufferDataHandle riversBufferHandle;

	FoliageManager* fm = nullptr;
};

