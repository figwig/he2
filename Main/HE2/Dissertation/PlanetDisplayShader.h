#pragma once
#include <HE2.h>
#include "RidgeAgentComputeShader.h"
class PlanetDisplayShader : public HE2_Shader
{
public:
	PlanetDisplayShader();
	~PlanetDisplayShader();

	class PlanetMaterial : public HE2_BlinnPhongMaterial
	{
	public:
		PlanetMaterial(HE2_Shader* planetShader);
		~PlanetMaterial() {}

	private:

	};

	ComputeRiverShader::RiversOutputBlock riversInputStruct;


};

