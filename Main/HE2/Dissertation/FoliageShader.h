#pragma once
#include <HE2.h>
//This is a distributing foliage shader - it distributes foliage across the planet's surface when passed a single model
class FoliageShader: public HE2_Shader
{
public:
	FoliageShader();

	struct coordStruct
	{
		glm::vec4 sectionOfMap;
		glm::vec4 cameraPos;
		glm::vec4 quadCoord;
		float scale = 0.005f;
		int seedOffset = 0;
	};
};



