#include "InnerAtmosphereShader.h"


InnerAtmosphereShader::InnerAtmosphereShader()
{
	shaderSources.vertexShaderSource = "Shaders/planets/innerAtmosDef.vert.spv";
	shaderSources.fragmentShaderSource = "Shaders/planets/innerAtmosDef.frag.spv";

	vertexInputDesc = HE2_Vertex::getAttributeDescriptionsHE2();


	perVertexDataDesc.stride = sizeof(HE2_Vertex);



	features.push_back(HE2_Shader::viewProjectionFeature);
	features.push_back(HE2_Shader::modelMatrixUniformFeature);

	for (auto a : HE2_SimpleMaterial::MaterialShaderFeatures())
		features.push_back(a);


	//shaderSettings.cullBack = false;
	shaderSettings.allowBlending = true;

	name = "Inner Atmosphere Shader";

	shaderSettings.shaderType = HE2_ShaderType::HE2_SHADER_DEFERRED;

	setupShader();
}