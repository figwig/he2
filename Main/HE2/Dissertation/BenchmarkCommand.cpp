#include "BenchmarkCommand.h"
#include <HE2_Debug.h>
BenchmarkCommand::BenchmarkCommand() :HE2_ConsoleCommand("bench")
{
	//Nothing needed, the console command does this all for us
}

void BenchmarkCommand::addCommand()
{
	//Okay to leave this pointer - all commands are cleared up by debug on close
	BenchmarkCommand* bc = new BenchmarkCommand;
}

void BenchmarkCommand::update()
{
	if (benching && Time::time - startTime > defBenchSeconds)
	{
		benching = false;
		int noFrames = Time::frameID - benchStateFrame;
		float time = Time::time - startTime;

		float avgFPS = noFrames / time;
		DebugLog("Benchmark finished, average fps was " + std::to_string(avgFPS));

		HE2_Instance::renderBackend->renderUI = true;
	}
}
void BenchmarkCommand::execute(std::string)
{
	//Benchmark already started
	if (benching)
	{
		benching = false;
		int noFrames = Time::frameID - benchStateFrame;
		float time = Time::time - startTime;
		HE2_Instance::renderBackend->renderUI = true;
	}
	else
	{
		benchStateFrame = Time::frameID;
		startTime = Time::time;
		benching = true;
		HE2_Instance::renderBackend->renderUI = false;
	}
}
