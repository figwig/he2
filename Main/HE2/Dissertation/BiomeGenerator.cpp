#include "BiomeGenerator.h"
#include "FrustrumChecker.h"
#include <HE2_Debug.h>
#include "PlanetGenerationManager.h"
#include <half.h>
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#include "FrustrumChecker.h"

std::map<int, Biome*> BiomeGenerator::allBiomes;

BiomeGenerator::BiomeGenerator()
{
	biomeData.requiredSection = glm::vec4(0, 0, 1, 1);

	biomeBufferHandle = HE2_Instance::renderBackend->makeGPUBuffer(&biomeData, sizeof(BiomeData), true);

	//cpuVisibleResult = HE2_Instance::renderBackend->createEmptyImage(false, glm::vec2(1920, 1080), true);

	//Add all of the biomes
	allBiomes.insert(std::make_pair(-1, new NullBiome));
	allBiomes.insert(std::make_pair(0, new OceanBiome));
	allBiomes.insert(std::make_pair(1, new DesertBiome));
	allBiomes.insert(std::make_pair(2, new SavannaBiome));
	allBiomes.insert(std::make_pair(3, new TropSeasonRainforestBiome));
	allBiomes.insert(std::make_pair(4, new TropRainforestBiome));
	allBiomes.insert(std::make_pair(5, new GrasslandBiome));
	allBiomes.insert(std::make_pair(6, new TemperateForest));
	allBiomes.insert(std::make_pair(7, new TemperateRainforest));
	allBiomes.insert(std::make_pair(8, new Taiga));
	allBiomes.insert(std::make_pair(9, new Tundra));
}

BiomeGenerator::~BiomeGenerator()
{
	for (auto a : allBiomes)
		delete a.second;
}

void BiomeGenerator::onGenerateFinished()
{
	FrustumChecker::activeSectionBiome = FrustumChecker::newSection;

	size_t size = 0;
	isData = true;
	//This takes ~30ms (very rough measurement) on laptop. Optimizing it would be lovely. Not sure if possible.
	HE2_Instance::renderBackend->copyImageDataToBuffer(&pixels, size, biomeMap);

	//float r1 = half::ToFloat32Fast(pixels[0].r);
	//float g1 = half::ToFloat32Fast(pixels[0].g);
	//float b1 = half::ToFloat32Fast(pixels[0].b);

	//float r2 = half::ToFloat32Fast(pixels[1].r);
	//float g2 = half::ToFloat32Fast(pixels[1].g);
	//float b2 = half::ToFloat32Fast(pixels[1].b);

	//std::string name = "out" + std::to_string(generation) + ".png";

	//stbi_write_jpg(name.c_str(), 4000, 2000, 4, &pixels, 100);
	//stbi_write_png(name.c_str(), 4000, 2000, 4, &pixels, 16 * 4000);
	generation++;
	pgm->generationFinished();
}

void BiomeGenerator::updateBuffers()
{
	//biomeData.requiredSection = FrustrumChecker::newSection;

	HE2_Instance::renderBackend->updateGPUBuffer(biomeBufferHandle, &biomeData, sizeof(BiomeData));
}


glm::vec4 BiomeGenerator::getColourAtPoint(glm::vec2 x)
{
	if (!isData) return glm::vec4(0, 0, 0, 1);


	glm::vec2 ogTextureCoord = x;

	//Calulate the heightmap coord based on the adjusted heightmap
	glm::vec2 dimensions;

	dimensions.x =
		FrustumChecker::activeSectionBiome.z -
		FrustumChecker::activeSectionBiome.x;
	dimensions.y = (
		FrustumChecker::activeSectionBiome.w -
		FrustumChecker::activeSectionBiome.y);

	glm::vec2 xy = glm::vec2(
		FrustumChecker::activeSectionBiome.x,
		FrustumChecker::activeSectionBiome.y);

	x = (x -
		xy) * (1.0f / dimensions);


	//This essentially uses linear filtering - so should the shaders

	//This is now between (0,0) and (4000,2000)
	int xCo = x.x * 4000;
	int yCo = x.y * 2000;

	//Y * row length plus 
	int index = yCo * 4000 +xCo;
	
	pixel64 p = pixels[index];



	return glm::vec4(half::ToFloat32(p.r), half::ToFloat32(p.g), half::ToFloat32(p.b), half::ToFloat32(p.a));
}

Biome* BiomeGenerator::getBiomeAtPoint(glm::vec2 x)
{
	int biomeID = getColourAtPoint(x).b;

	//DebugLog("BiomeID: " + std::to_string(biomeID));

	if (allBiomes.count(biomeID) > 0)
	{
		return allBiomes[biomeID];
	}
	else
	{
		return allBiomes[-1];
	}

}