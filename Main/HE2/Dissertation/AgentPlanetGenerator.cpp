#include "AgentPlanetGenerator.h"
#include <imgui.h>
#include "FrustrumChecker.h"
#include <GenericUIElements.h>
#include "BiomeGenerator.h"
#include <random>
#include "AgentBasedPlanetGeneratorShader.h"

AgentPlanetGenerator::AgentPlanetGenerator()
{
	FrustumChecker::pg = this;

	//Islands Layer
	pub.noiseLayers[0].coordMod = 0.014;
	pub.noiseLayers[0].exponent = 1.598f;
	pub.noiseLayers[0].scale = 4.0f;
	pub.noiseLayers[0].constructive = 1.0f;
	pub.noiseLayers[0].active = 1.0f;
	pub.noiseLayers[0].scaleDependsOn = -1.0f;
	pub.noiseLayers[0].octaves = 6.0f;

	//New continent
	//pub.noiseLayers[0].coordMod = 0.014;
	//pub.noiseLayers[0].exponent = 0.9f;
	//pub.noiseLayers[0].scale = 2.276f;
	//pub.noiseLayers[0].constructive = 1.0f;
	//pub.noiseLayers[0].active = 1.0f;
	//pub.noiseLayers[0].scaleDependsOn = -1.0f;
	//pub.noiseLayers[0].octaves = 6.0f;


	//Mountains
	pub.noiseLayers[1].coordMod = 0.389f;
	pub.noiseLayers[1].exponent = 0.898f;
	pub.noiseLayers[1].scale = 4.0f;
	pub.noiseLayers[1].constructive = 1.0f;
	pub.noiseLayers[1].scaleDependsOn = 0.0f;
	pub.noiseLayers[1].active = 0.0f;
	pub.noiseLayers[1].octaves = 11.0f;

	//
	pub.noiseLayers[2].coordMod = 3.495;
	pub.noiseLayers[2].exponent = 0.451f;
	pub.noiseLayers[2].scale = 0.15f;
	pub.noiseLayers[2].constructive = 1.0f;
	pub.noiseLayers[2].scaleDependsOn = 0.0f;
	pub.noiseLayers[2].active = 0.0f;
	pub.noiseLayers[2].octaves = 8.0f;


	pub.noiseLayers[3].coordMod = 0.263;
	pub.noiseLayers[3].exponent = 1.0f;
	pub.noiseLayers[3].scale = 0.392f;
	pub.noiseLayers[3].constructive = 0.0f;
	pub.noiseLayers[3].scaleDependsOn = 0.0f;
	pub.noiseLayers[3].active = 1.0f;
	pub.noiseLayers[3].octaves = 8.0f;

	pub.noiseLayers[4].coordMod = 10.0;
	pub.noiseLayers[4].exponent = 2.307f;
	pub.noiseLayers[4].scale = 0.2f;
	pub.noiseLayers[4].constructive = 1.0f;
	pub.noiseLayers[4].scaleDependsOn = 0.0f;
	pub.noiseLayers[4].active = 1.0f;
	pub.noiseLayers[4].octaves = 10.0f;

	pub.actualLayerCount = 5;
	pub.radius = 112;

	currentActivePlanet = pub;

	void* data = &currentActivePlanet;

	bufferHandle = HE2_Instance::renderBackend->makeGPUBuffer(data, sizeof(PlanetUniformBlock), true);

	ab = {};
	ab.requiredSection = glm::vec4(0, 0, 1, 1);
	data = &ab;

	bufferHandle2 = HE2_Instance::renderBackend->makeGPUBuffer(data, sizeof(AgentsBlock), true);

	//riverBlock.actRivers = 2;

	//riverBlock.rivers[0].actPoints = 0;
	//riverBlock.rivers[0].points[0] = glm::vec4(0.5, 0.5, 0.5, 0);
	//riverBlock.rivers[0].points[1] = glm::vec4(0.6, 0.2, 0.28, 0);
	//riverBlock.rivers[0].points[2] = glm::vec4(0.45, 0.2, 0.17, 0);
	//riverBlock.rivers[0].points[3] = glm::vec4(0.3, 0.2, 0.06, 0);
	//riverBlock.rivers[0].points[4] = glm::vec4(0.20, 0.2, 0.02, 0);

	//riverBlock.rivers[1].actPoints = 10;
	//riverBlock.rivers[1].points[0] = glm::vec4(0.0f, 0.0f,  -0.901f, 0);
	//riverBlock.rivers[1].points[1] = glm::vec4(0.2f, 0.03f, -0.901f, 0);
	//riverBlock.rivers[1].points[2] = glm::vec4(0.24f, 0.06f, -0.901f, 0);
	//riverBlock.rivers[1].points[3] = glm::vec4(0.18f, 0.09f, -0.901f, 0);
	//riverBlock.rivers[1].points[4] = glm::vec4(0.16f, 0.12f, -0.901f, 0);
	//riverBlock.rivers[1].points[5] = glm::vec4(0.19f, 0.15f, -0.901f, 0);
	//riverBlock.rivers[1].points[6] = glm::vec4(0.24f, 0.18f, -0.901f, 0);
	//riverBlock.rivers[1].points[7] = glm::vec4(0.26f, 0.21f, -0.901f, 0);
	//riverBlock.rivers[1].points[8] = glm::vec4(0.3f, 0.24f, -0.901f, 0);
	//riverBlock.rivers[1].points[9] = glm::vec4(0.0f, 0.27f, -0.901f, 0);
	//riverBlock.rivers[1].points[10] = glm::vec4(0.65, 0.0f, 0.01f, 0);

	ridgeDataHandle = HE2_Instance::renderBackend->makeGPUBuffer(&ridgeBlock, sizeof(ridgeBlock), true);
	generateRidges();
	generateRiverStartPositions();

}


AgentPlanetGenerator::~AgentPlanetGenerator()
{
}


void AgentPlanetGenerator::generatePlanet()
{
}


void AgentPlanetGenerator::onGenerateFinished()
{
	//Changed this line to being in the biome shader - takes one frame!
	FrustumChecker::activeSectionHeight = FrustumChecker::newSection;
	size_t size;

	isData = true;

	HE2_Instance::renderBackend->copyImageDataToBuffer(&pixels, size, heightMap);

}

void AgentPlanetGenerator::update()
{
	//ImGui::Begin("Hit Points");

	//GenericUIElements::Vec4ReadOut("Required: ", FrustrumChecker::requiredSection);
	//glm::vec2 dims = glm::vec2(FrustrumChecker::requiredSection.z - FrustrumChecker::requiredSection.x, FrustrumChecker::requiredSection.w - FrustrumChecker::requiredSection.y);
	//GenericUIElements::Vec2ReadOut("Required: ",dims);

	//ImGui::End();


}

void AgentPlanetGenerator::generateRiverStartPositions()
{
	//This method defines a set of river starting points. They are essentially random points in a ridge. For now, I'm going to distribute them every interal fifth (not the edges) along a ridge
	int rivers = 0;
	for (int i = 0; i < ridgeBlock.actRidges && rivers < 200; i++)
	{
		LineDetails ridge = ridgeBlock.ridges[i];
		int fifthIndex = ridge.actPoints / 5;

		//riverBlock.points[rivers] = ridge.points[0] * 3150.0f;
		//rivers++;
		for (int seg = 1; seg < 4 && rivers < 200; seg++)
		{
			int index = seg * fifthIndex;

			riverBlock.points[rivers] = ridge.points[index] * 3150.0f;
			rivers++;
		}
	}

	for (rivers; rivers < 200; rivers++)
	{
		float x = ((rand() % 1000) - 500.0f) / 1000.0f;
		float y = ((rand() % 1000) - 500.0f) / 1000.0f;
		float z = ((rand() % 1000) - 500.0f) / 1000.0f;
		riverBlock.points[rivers] = glm::normalize(glm::vec4(x,y,z,0.0f)) * 3150.0f;
	}
}

float AgentPlanetGenerator::getHeightAtCoord(glm::vec2 x)
{

	if (!isData) return 0.0f;


	glm::vec2 ogTextureCoord = x;

	//Calulate the heightmap coord based on the adjusted heightmap
	glm::vec2 dimensions;

	dimensions.x =
		FrustumChecker::activeSectionHeight.z -
		FrustumChecker::activeSectionHeight.x;
	dimensions.y = (
		FrustumChecker::activeSectionHeight.w -
		FrustumChecker::activeSectionHeight.y);

	glm::vec2 xy = glm::vec2(
		FrustumChecker::activeSectionHeight.x,
		FrustumChecker::activeSectionHeight.y);

	x = (x -
		xy) * (1.0f / dimensions);


	//This essentially uses linear filtering - so should the shaders

	//This is now between (0,0) and (4000,2000)
	int xCo = x.x * AgentBasedPlanetGenerator::heightmapSize.x;
	int yCo = x.y * AgentBasedPlanetGenerator::heightmapSize.y;

	//Y * row length plus 
	int index = yCo * AgentBasedPlanetGenerator::heightmapSize.x + xCo;

	if (index > AgentBasedPlanetGenerator::heightmapSize.x * AgentBasedPlanetGenerator::heightmapSize.y || index < 0 )
	{
		return 0.0f;
	}

	pixel64 p = pixels[index];

	return p.r;
}

void AgentPlanetGenerator::generateRidges()
{
	srand(4354344 + ridgeSeed);

	//Random between 10 and 20
	ridgeBlock.actRidges = (rand() % 11) + 20;
	ridgeBlock.actRidges =30;
	//riverBlock.actRidges = 1;

	for (int ridgeCount = 0; ridgeCount < ridgeBlock.actRidges; ridgeCount++)
	{
		//Random number of points 
		ridgeBlock.ridges[ridgeCount].actPoints = (rand() % 16) + 20;


		float x = ((rand() % 1000)-500.0f) / 1000.0f;
		float y = ((rand() % 1000)-500.0f) / 1000.0f;
		float z = ((rand() % 1000)-500.0f) / 1000.0f;

		glm::vec4 startPoint = glm::normalize(glm::vec4(x, y, z, 0));

		x = ((rand() % 1000) - 500.0f) / 1000.0f;
		y = ((rand() % 1000) - 500.0f) / 1000.0f;
		z = ((rand() % 1000) - 500.0f) / 1000.0f;

		glm::vec4 direction = glm::normalize(glm::vec4(x, y, z, 0));

		ridgeBlock.ridges[ridgeCount].points[0] = startPoint;

		for (int pointCount = 1; pointCount < ridgeBlock.ridges[ridgeCount].actPoints; pointCount++)
		{
			float x = ((rand() % 100)- 50);
			float y = ((rand() % 100)- 50) ;
			float z = ((rand() % 100)- 50) ;

			//Range between 0.15 and 0.4
			float effect = pow(((rand() % 25) + 15) / 100.0f, 1.0f);

			glm::vec4 directionMod = glm::normalize(glm::vec4(x, y, z, 0));

			direction = glm::normalize(glm::mix(direction, directionMod, effect));

			float distance = pow((rand() % 10) / 8.0f, 1.12f) /15.0f;

			//Probably only ever going to add for now - this is going to be replaced with brownian motion!
			glm::vec4 newPoint = glm::normalize(ridgeBlock.ridges[ridgeCount].points[pointCount-1] +( direction * distance));

			ridgeBlock.ridges[ridgeCount].points[pointCount] = newPoint;
		}
	}

	HE2_Instance::renderBackend->updateGPUBuffer(ridgeDataHandle, &ridgeBlock, sizeof(ridgeBlock));
}


glm::vec2 AgentPlanetGenerator::calcTexCoord(glm::vec3 point, glm::vec3 planetPos, float radius)
{
	//Move the point so the planet pos = 0,0,0
	point -= planetPos;

	glm::vec3 n = glm::normalize(point);
	float u = atan2(n.x, n.z) / (2.0f * M_PI) + 0.25f;

	float v = acos(point.y / radius) / M_PI;

	glm::vec2 texCoord = glm::vec2(u, v);

	while (texCoord.x > 1.0f)
		texCoord.x -= 1.0f;
	while (texCoord.x < 0.0f)
		texCoord.x += 1.0f;

	while (texCoord.y > 1.0f)
		texCoord.y -= 1.0f;
	while (texCoord.y < 0.0f)
		texCoord.y += 1.0f;

	return texCoord;

}
