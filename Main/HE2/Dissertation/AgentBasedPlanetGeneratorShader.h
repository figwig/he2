#pragma once
#include <HE2.h>
#include <string>
#include "RidgeAgentComputeShader.h"
class AgentBasedPlanetGenerator:public HE2_Shader
{
public:
	AgentBasedPlanetGenerator();
	~AgentBasedPlanetGenerator();
	const static glm::vec2 heightmapSize;

private:
	const std::string vertName = "shaders/planets/agents.vert.spv";
	const std::string fragName = "shaders/planets/agents.frag.spv";

	ComputeRiverShader::RiversOutputBlock riversInputStruct;

	struct Layer
	{
		float coordModifier;
		float exponent;
		float scale;
		float constructive;

		float scaleDependsOn;
		float pad1;
		float pad2;
		float active;
	};

	struct FragmentBlock
	{
		Layer layers[20];
		int actualLayerCount;
		float oceanLevel;
		float radius;
	};


};

