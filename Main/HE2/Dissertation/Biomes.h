#pragma once
#include <string>
#include <vector>

struct FoliageDistribution
{
	std::string modelName;
	int avgCount;
	int range;

};

class Biome
{
public:
	virtual int getBiomeID() = 0;
	virtual std::string getBiomeName() = 0;
	std::vector<FoliageDistribution> foliageDists;
};

//This is only used in an error
class NullBiome : public Biome
{
public:
	int getBiomeID() { return -1; }

	std::string getBiomeName() { return "Null Biome"; }
};

//Nothing
class OceanBiome : public Biome
{
public:
	int getBiomeID() { return 0; }

	std::string getBiomeName() { return "Ocean Biome"; }
};

//Cactuses
class DesertBiome : public Biome
{
public:
	DesertBiome()
	{
		foliageDists.push_back(
			{
				"models/cactus_a.obj",
				10,
				10
			}
		);
	}
	int getBiomeID() { return 1; }

	std::string getBiomeName() { return "Desert Biome"; }
};

class SavannaBiome : public Biome
{
public:
	int getBiomeID() { return 2; }

	std::string getBiomeName() { return "Savanna Biome"; }
};

class TropSeasonRainforestBiome : public Biome
{
public:
	int getBiomeID() { return 3; }
	TropSeasonRainforestBiome()
	{
		foliageDists.push_back(
			{
				"models/tree_a.obj",
				40,
				10
			}
		);
	}

	std::string getBiomeName() { return "Tropical Seasonal Rainforest Biome"; }
};

class TropRainforestBiome : public Biome
{
public:
	TropRainforestBiome()
	{
		foliageDists.push_back(
			{
				"models/tree_a.obj",
				40,
				10
			}
		);
	}
	int getBiomeID() { return 4; }

	std::string getBiomeName() { return "Tropical Rainforest Biome"; }
};

class GrasslandBiome : public Biome
{
public:
	GrasslandBiome()
	{
		foliageDists.push_back(
			{
				"models/tree_a.obj",
				20,
				2
			}
		);
	}
	int getBiomeID() { return 5; }

	std::string getBiomeName() { return "Grassland Biome"; }
};

//Some rocks, semi-dense tree of a couple types
class TemperateForest : public Biome
{
public:
	TemperateForest()
	{
		foliageDists.push_back(
			{
				"models/tree_a.obj",
				30,
				10
			}
		);
		foliageDists.push_back(
			{
				"models/tree_b.obj",
				20,
				8
			}
		);
		foliageDists.push_back(
			{
				"models/boulder_a.obj",
				10,
				10
			}
		);
	}

	int getBiomeID() { return 6; }

	std::string getBiomeName() { return "Temperate Forest Biome"; }
};

//Lots of trees
class TemperateRainforest : public Biome
{
public:
	TemperateRainforest()
	{
		foliageDists.push_back(
			{
				"models/tree_a.obj",
				80,
				5
			}
		);
	}

	int getBiomeID() { return 7; }

	std::string getBiomeName() { return "Temperate Rainforest Biome"; }
};

//Sparse trees
class Taiga : public Biome
{
public:
	Taiga()
	{
		foliageDists.push_back(
			{
				"models/tree_a.obj",
				5,
				5
			}
		);
		foliageDists.push_back(
			{
				"models/tree_b.obj",
				15,
				8
			}
		);
	}

	int getBiomeID() { return 8; }

	std::string getBiomeName() { return "Taiga Biome"; }
};


//Essentially snow - no foliage needed
class Tundra : public Biome
{
public:
	int getBiomeID() { return 9; }

	std::string getBiomeName() { return "Tundra Biome"; }
};