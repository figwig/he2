#include "PerlinNoiseGenerator.h"
#include "PerlinNoiseShader.h"
#include <HE2_GPUTask.h>

PerlinNoiseGenerator::PerlinNoiseGenerator()
{
	gpuTask = new HE2_GPUTask(new PerlinNoiseShader(), 6);

	gpuTask->addComponent(this, { typeid(PerlinNoiseGenerator), typeid(HE2_ShaderImageReturn) });

	HE2_Instance::renderBackend->prepareGPUTask(gpuTask);
}
