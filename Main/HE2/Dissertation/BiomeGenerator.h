#pragma once
#include <HE2.h>
#include <stb_image.h>
#include "Biomes.h"
class PlanetGenerationManager;



class BiomeGenerator : public HE2_GlobalBehaviour,
	public HE2_ShaderImageReturn, public HE2_ShaderImageHandleGetter, public HE2_ShaderBufferGetter
{
public:

	BiomeGenerator();
	~BiomeGenerator();

	void updateBuffers();

	void setImageOutput(HE2_Image* i, int index)
	{
		biomeMap = i;
	}
	HE2_ImageHandle getImageHandle(int group, int binding)
	{
		if (binding == 0)
			return heightMap->handle;
		else
			return biomeReadMap->handle;
	}

	HE2_BufferDataHandle getBufferHandle(int group, int binding)
	{
		return biomeBufferHandle;
	}

	glm::vec4 getColourAtPoint(glm::vec2 coord);
	Biome * getBiomeAtPoint(glm::vec2 coord);

	HE2_Image* cpuVisibleResult = nullptr;
	HE2_Image* biomeReadMap = nullptr;
	HE2_Image* biomeMap = nullptr;
	HE2_Image* heightMap = nullptr;

	HE2_Object* generatorObject = nullptr;

	void onGenerateFinished();


	PlanetGenerationManager* pgm = nullptr;
	HE2_GPUTask* biomeTask = nullptr;
	struct BiomeData
	{
		glm::vec4 requiredSection;
		float seaLevel = 0.0f;
		float coordMod = 0.002f;
		float tempAddition = 0.0f;
		float moistureAddtion = 0.0f;
	} biomeData;
private:

	static std::map<int, Biome*> allBiomes;



	pixel64 pixels[4000 * 2000];

	bool isData = false;

	//pixel128 pixels[4000 * 2000];

	HE2_BufferDataHandle biomeBufferHandle = 0;


	
	int generation = 0;

	int oceanIntKey = 0;
	int desertIntKey = 1;
	int savannaIntKey = 2;
	int tropSeasonRainforestIntKey = 3;
	int tropRainforestIntKey = 4;
	int grasslandIntKey = 5;
	int temperateForestIntKey = 6;
	int temperateRainforestIntKey = 7;
	int taigaIntKey = 8;
	int tundraIntKey = 9;
};

