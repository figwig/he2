#include "ComputeRiverGenerator.h"
#include <HE2.h>
#include <HE2_GPUTask.h>
#include "RidgeAgentComputeShader.h"
#include <HE2_Debug.h>
#include "FrustrumChecker.h"
#include "AgentPlanetGenerator.h"


ComputeRiverGenerator::ComputeRiverGenerator(AgentPlanetGenerator* pg): pg(pg)
{
	generateTask = new HE2_GPUTask(new ComputeRiverShader(), glm::vec3(100,1,1), glm::vec3(10,1,1));

	generateTask->addComponent(this, { typeid(ComputeRiverGenerator), typeid(HE2_ShaderImageHandleGetter), typeid(HE2_ShaderBufferGetter), typeid(HE2_ShaderStorageBufferGetter) });


	bufferHandle = HE2_Instance::renderBackend->makeGPUBuffer(&pg->riverBlock, sizeof(pg->riverBlock), true);

	storageBufferHandle = HE2_Instance::renderBackend->makeStorageBuffer(sizeof(riverOutputBlock));
}

void ComputeRiverGenerator::finishSetup(HE2_Image* i)
{
	image = i;
	HE2_Instance::renderBackend->prepareGPUTask(generateTask);
}

HE2_ImageHandle ComputeRiverGenerator::getImageHandle(int, int binding)
{
	if(binding == 0)
		return image->handle;

	return outputImage->handle;
}

void ComputeRiverGenerator::onComplete()
{
	DebugLog("Compute COmplete");
	generating = false;

	//Call the river and ridge map generator
}


void ComputeRiverGenerator::update()
{
	ticker -= Time::deltaTime;

	//if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_C) && !generating && ticker < 0.0f)
	//{
	//	ticker = 1.0f;
	//	Generate();
	//	generating = true;
	//}
}

void ComputeRiverGenerator::updateBuffers()
{

	HE2_Instance::renderBackend->updateGPUBuffer(bufferHandle, &pg->riverBlock, sizeof(pg->riverBlock));
}

void ComputeRiverGenerator::Generate()
{
	updateBuffers();
	HE2_Instance::renderBackend->queueGPUTask(generateTask);
}