#include "AgentBasedPlanetGeneratorShader.h"
#include "AgentPlanetGenerator.h"

const glm::vec2 AgentBasedPlanetGenerator::heightmapSize = glm::vec2(5000,2500);

AgentBasedPlanetGenerator::AgentBasedPlanetGenerator()
{
	shaderSources.vertexShaderSource = vertName;
	shaderSources.fragmentShaderSource = fragName;

	shaderSettings.shaderType = HE2_SHADER_FRAGMENT_GPU_TASK;

	HE2_ShaderOutput output = {};
	output.format = HE2_ShaderOutputFormat::HE2_RGBA_HIGHP;
	output.outputLocation = 0;
	output.outputType = HE2_ShaderOutputType::HE2_SHADER_OUTPUT_CUSTOM_IMAGE;
	output.size = heightmapSize;
	output.storageCapability = true;

	shaderSettings.outputs.push_back(output);
	shaderSettings.canBeWireframe = true;
	


	HE2_ShaderFeature fragmentBlock = {};
	fragmentBlock.grouping = 0;
	fragmentBlock.binding = 0;
	fragmentBlock.size = sizeof(AgentPlanetGenerator::PlanetUniformBlock);
	fragmentBlock.offset = 0;
	fragmentBlock.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	fragmentBlock.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragmentBlock.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;

	features.push_back(fragmentBlock);


	HE2_ShaderFeature agentBuffer = {};
	agentBuffer.grouping = 1;
	agentBuffer.binding = 0;	
	agentBuffer.size = sizeof(AgentPlanetGenerator::AgentsBlock);
	agentBuffer.offset = 0;
	agentBuffer.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	agentBuffer.stage = HE2_SHADER_STAGE_FRAGMENT;
	agentBuffer.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;

	features.push_back(agentBuffer);

	HE2_ShaderFeature riversBuffer = {};
	riversBuffer.binding = 1;
	riversBuffer.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	riversBuffer.size = sizeof(AgentPlanetGenerator::RidgeBlock);
	riversBuffer.offset = 0;
	riversBuffer.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	riversBuffer.stage = HE2_SHADER_STAGE_FRAGMENT;
	riversBuffer.grouping = 1;

	features.push_back(riversBuffer);

	HE2_ShaderFeature riverMap = {};
	riverMap.binding = 2;
	riverMap.grouping = 1;
	riverMap.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	riverMap.exactRead = false;
	riverMap.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	riverMap.stage = HE2_SHADER_STAGE_FRAGMENT;

	features.push_back(riverMap);

	HE2_ShaderFeature riversInput = {};

	riversInput.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	riversInput.stage = HE2_SHADER_STAGE_FRAGMENT;
	riversInput.binding = 0;
	riversInput.grouping = 3;
	riversInput.size = sizeof(riversInputStruct);
	riversInput.offset = 0;
	riversInput.featureType = HE2_ShaderFeatureType::HE2_SHADER_FEATURE_STORAGE_BUFFER;

	features.push_back(riversInput);


	perVertexDataDesc = {};
	perVertexDataDesc.nullData = true;

	shaderSettings.allowBlending = true; 

	setupShader();
}


AgentBasedPlanetGenerator::~AgentBasedPlanetGenerator()
{

}