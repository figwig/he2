#pragma once
#include <HE2.h>
#include "RidgeAgentComputeShader.h"
#include "RiverConstructionShader.h"
class RiverConstructionComponent : public HE2_ShaderBufferGetter, public HE2_ShaderStorageBufferGetter, public HE2_ShaderImageHandleGetter
{
public:
	RiverConstructionComponent();

	RiverConstructionShader::InputBlock input;
	HE2_Image* heightMap = nullptr;

	HE2_ImageHandle getImageHandle(int group, int binding)
	{
		return heightMap->handle;
	}

	HE2_BufferDataHandle getBufferHandle(int group, int binding)
	{
		return inputBufferHandle;
	}

	HE2_StorageBufferDataHandle getStorageBufferHandle(int grouping, int handle)
	{
		return storageBufferHandle;
	}
	
	HE2_BufferDataHandle inputBufferHandle;
	HE2_StorageBufferDataHandle storageBufferHandle;
};

