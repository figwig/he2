#include "PerlinNoiseShader.h"

PerlinNoiseShader::PerlinNoiseShader()
{
	shaderSources.vertexShaderSource = vertName;
	shaderSources.fragmentShaderSource = fragName;

	shaderSettings.shaderType = HE2_SHADER_FRAGMENT_GPU_TASK;

	HE2_ShaderOutput output = {};
	output.format = HE2_ShaderOutputFormat::HE2_RGBA_HIGHP;
	output.outputLocation = 0;
	output.outputType = HE2_ShaderOutputType::HE2_SHADER_OUTPUT_CUSTOM_IMAGE;
	output.size = glm::vec2(1000, 1000);
	output.storageCapability = true;

	shaderSettings.outputs.push_back(output);
	shaderSettings.canBeWireframe = true;


	perVertexDataDesc = {};
	perVertexDataDesc.nullData = true;

	shaderSettings.allowBlending = true;

	setupShader();
}
