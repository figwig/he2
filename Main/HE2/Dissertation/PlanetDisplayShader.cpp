#include "PlanetDisplayShader.h"

#include "PlanetRenderingComponent.h"
PlanetDisplayShader::PlanetDisplayShader()
{
	//VertexShadeder Buffer = set=0, block at binding = 0
//HEight Map = set =1, binding = 0, 1
//Dirt Material = set 10, binding 0,1,2;
//Water Material  = set 11, binding 0,1,2;

	HE2_ShaderFeature vertShaderBuffer = {};
	vertShaderBuffer.binding = 0;
	vertShaderBuffer.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	vertShaderBuffer.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	vertShaderBuffer.stage = HE2_SHADER_STAGE_VERTEX;
	vertShaderBuffer.grouping = 6;
	vertShaderBuffer.size = sizeof(PlanetRenderingComponent::VertexInputBuffer);

	HE2_ShaderFeature tesseUniformBlock = {};
	tesseUniformBlock.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	tesseUniformBlock.stage = HE2_SHADER_STAGE_TESSE;
	tesseUniformBlock.size = sizeof(PlanetRenderingComponent::TesseInputBuffer);
	tesseUniformBlock.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	tesseUniformBlock.grouping = 1;
	tesseUniformBlock.binding = 2;


	HE2_ShaderFeature tesseHeightMap = {};
	tesseHeightMap.binding = 0;
	tesseHeightMap.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	tesseHeightMap.stage = HE2_SHADER_STAGE_TESSE;
	tesseHeightMap.grouping = 1;
	tesseHeightMap.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragHeightMap = {};
	fragHeightMap.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragHeightMap.grouping = 1;
	fragHeightMap.binding = 1;
	fragHeightMap.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragHeightMap.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragBiomeMap = {};
	fragBiomeMap.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragBiomeMap.grouping = 1;
	fragBiomeMap.binding = 3;
	fragBiomeMap.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragBiomeMap.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	fragBiomeMap.exactRead = true;

	//Actual textures
#pragma region Textures
	HE2_ShaderFeature fragGrassTexture = {};
	fragGrassTexture.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragGrassTexture.binding = 0;
	fragGrassTexture.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragGrassTexture.grouping = 2;
	fragGrassTexture.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragGrassSpecular = {};
	fragGrassSpecular.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragGrassSpecular.binding = 1;
	fragGrassSpecular.grouping = 2;
	fragGrassSpecular.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragGrassSpecular.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragGrassNormal = {};
	fragGrassNormal.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragGrassNormal.binding = 2;
	fragGrassNormal.grouping = 2;
	fragGrassNormal.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragGrassNormal.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragWaterTexture = {};
	fragWaterTexture.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragWaterTexture.binding = 0;
	fragWaterTexture.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragWaterTexture.grouping = 3;
	fragWaterTexture.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragWaterSpecular = {};
	fragWaterSpecular.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragWaterSpecular.binding = 1;
	fragWaterSpecular.grouping = 3;
	fragWaterSpecular.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragWaterSpecular.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragWaterNormal = {};
	fragWaterNormal.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragWaterNormal.binding = 2;
	fragWaterNormal.grouping = 3;
	fragWaterNormal.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragWaterNormal.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragDirtTexture = {};
	fragDirtTexture.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragDirtTexture.binding = 0;
	fragDirtTexture.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragDirtTexture.grouping = 4;
	fragDirtTexture.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragDirtSpecular = {};
	fragDirtSpecular.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragDirtSpecular.binding = 1;
	fragDirtSpecular.grouping = 4;
	fragDirtSpecular.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragDirtSpecular.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragDirtNormal = {};
	fragDirtNormal.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragDirtNormal.binding = 2;
	fragDirtNormal.grouping = 4;
	fragDirtNormal.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragDirtNormal.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragRockTexture = {};
	fragRockTexture.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragRockTexture.binding = 0;
	fragRockTexture.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragRockTexture.grouping = 5;
	fragRockTexture.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragRockSpecular = {};
	fragRockSpecular.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragRockSpecular.binding = 1;
	fragRockSpecular.grouping = 5;
	fragRockSpecular.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragRockSpecular.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragRockNormal = {};
	fragRockNormal.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragRockNormal.binding = 2;
	fragRockNormal.grouping = 5;
	fragRockNormal.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragRockNormal.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;

	HE2_ShaderFeature fragNoiseSampler = {};
	fragNoiseSampler.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	fragNoiseSampler.grouping = 1;
	fragNoiseSampler.binding = 4;
	fragNoiseSampler.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	fragNoiseSampler.stage = HE2_ShaderStage::HE2_SHADER_STAGE_FRAGMENT;
#pragma endregion
	//Other textures go here

	HE2_ShaderFeature fragTilingFactorInputBuffer = {};
	fragTilingFactorInputBuffer.stage = HE2_SHADER_STAGE_FRAGMENT;
	fragTilingFactorInputBuffer.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	fragTilingFactorInputBuffer.size = sizeof(PlanetRenderingComponent::FragmentInputBuffer);
	fragTilingFactorInputBuffer.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	fragTilingFactorInputBuffer.grouping = 2;
	fragTilingFactorInputBuffer.binding = 3;

	HE2_ShaderFeature riversInput = {};

	riversInput.source = HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC;
	riversInput.stage = HE2_SHADER_STAGE_FRAGMENT;
	riversInput.binding = 4;
	riversInput.grouping = 2;
	riversInput.size = sizeof(riversInputStruct);
	riversInput.offset = 0;
	riversInput.featureType = HE2_ShaderFeatureType::HE2_SHADER_FEATURE_STORAGE_BUFFER;

	HE2_ShaderFeature vpFeature = HE2_Shader::viewProjectionFeature;
	vpFeature.stage = HE2_ShaderStage::HE2_SHADER_STAGE_TESSE;


	HE2_ShaderSources sources = {};
	sources.vertexShaderSource = "Shaders/planets/vert.spv";
	sources.fragmentShaderSource = "Shaders/planets/frag.spv";
	sources.tessalationControlShaderSource = "Shaders/planets/tesc.spv";
	sources.tessalationEvaluationShaderSource = "shaders/planets/tese.spv";

	std::vector<HE2_DataInputDesc> dataInputs = HE2_Vertex::getAttributeDescriptionsHE2();

	HE2_PerVertexDataDesc dataDesc = {};
	dataDesc.stride = sizeof(HE2_Vertex);

	shaderSources = sources;
	features = { vertShaderBuffer, tesseHeightMap, tesseUniformBlock, fragHeightMap, fragGrassTexture, fragGrassSpecular, fragGrassNormal, fragWaterTexture, fragWaterSpecular, fragWaterNormal, fragDirtTexture, fragDirtSpecular, fragDirtNormal,fragTilingFactorInputBuffer, fragRockTexture, fragRockSpecular, fragRockNormal,fragBiomeMap, riversInput, vpFeature, fragNoiseSampler};

	vertexInputDesc = dataInputs;
	perVertexDataDesc = dataDesc;
	shaderSettings.primitiveType = HE2_PrimitiveType::HE2_PATCH_LIST;
	shaderSettings.shaderType = HE2_ShaderType::HE2_SHADER_DEFERRED;

	setupShader();
}

PlanetDisplayShader::~PlanetDisplayShader()
{

}

PlanetDisplayShader::PlanetMaterial::PlanetMaterial(HE2_Shader* planetShader) : HE2_BlinnPhongMaterial(planetShader)
{
	
}