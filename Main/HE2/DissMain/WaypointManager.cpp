#include "WaypointManager.h"
#include <HE2_RenderComponent.h>
#include <imgui.h>
#include <FlyingCamera.h>

WaypointManager::WaypointManager()
{
	flagMaterial = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage("textures/flagtex.png"), HE2_TextureManager::getImage("textures/flagtex.png"), HE2_TextureManager::getImage("textures/flagtex.png"), HE2_Instance::renderBackend->deferredShader);

	flagModel = HE2_MeshManager::importModel("models/flag.obj");
}

void WaypointManager::update()
{

	ticker -= Time::deltaTime;
	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_SPACE)&& ticker < 0.0f && !ImGui::GetIO().WantCaptureKeyboard)
	{
		//Create a flag
		createWaypoint();
		ticker = 1.0f;
	}

	ImGui::Begin("Waypoints Manager");
	

	ImGui::TextWrapped("Set a waypoint at your current location with space bar, then click on it here to visit it again!");

	for (auto i : waypoints)
	{
		if (ImGui::Button(i->name.c_str()))
		{
			fc->goToWaypoint(i->getPos());
		}

	}

	ImGui::End();
}

void WaypointManager::createWaypoint()
{
	HE2_RenderComponent* rc = new HE2_RenderComponent(flagModel, flagMaterial);

	HE2_Object* newFlag = new HE2_Object({ rc });

	newFlag->setPos( HE2_Camera::main->getPos());

	glm::mat4 flagTransMat;
	glm::vec3 xAxis = glm::normalize(HE2_Camera::main->getPos() + glm::vec3(0, 1, 0)) - glm::normalize(HE2_Camera::main->getPos());

	glm::vec3 yAxis = -glm::normalize(HE2_Camera::main->getPos()); //+planet pos if not at 0

	glm::vec3 zAxis = glm::normalize(glm::cross(yAxis, xAxis));

	flagTransMat = glm::mat3(xAxis, yAxis, zAxis);

	newFlag->setRotMat(flagTransMat);

	newFlag->name = "Waypoint " + std::to_string(waypoints.size());

	waypoints.push_back(newFlag);
}