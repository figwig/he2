#include <imgui.h>
#include "ExplorationTasks.h"


ExplorationTasks::ExplorationTasks(WaypointManager * wpm, AgentPlanetGenerator * pg) : wpm(wpm)
{
	allTasks.push_back(new WaypointQuantityTask(this));

	allTasks.push_back(new VisitAreaTypeTask(VisitAreaTypeTask::AreaType::Mountain, "mountain ranges", 5, pg));

	allTasks.push_back(new VisitAreaTypeTask(VisitAreaTypeTask::AreaType::Mountain, "oceans", 2, pg));
}

void ExplorationTasks::update()
{
	ImGui::Begin("Tasks");
	for (auto i : allTasks)
	{
		i->checkForUpdates();

		if (i->isDone())
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.8, 0.8, 0.8, 1.0));
	
		ImGui::Text(i->getReadOutString().c_str());

		if (i->isDone())
			ImGui::PopStyleColor();
	}
	ImGui::End();
}
