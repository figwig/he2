#pragma once
#include <HE2.h>
#include <FlyingCamera.h>

class WaypointManager : public HE2_GlobalBehaviour
{
public:
	WaypointManager();

	void update();

	std::vector<HE2_Object*> waypoints;
	FlyingCamera* fc = nullptr;
private:

	void createWaypoint();

	HE2_Material* flagMaterial = nullptr;
	HE2_Model* flagModel = nullptr;
	float ticker = 0.0f;
};

