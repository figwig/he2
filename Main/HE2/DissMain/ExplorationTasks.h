#pragma once
#include <HE2.h>
#include "WaypointManager.h"
#include <AgentPlanetGenerator.h>
#include <FrustrumChecker.h>

class ExplorationTasks;

class Task
{
public:
	virtual void checkForUpdates() =0;
	virtual bool isDone() =0;
	virtual std::string getReadOutString() =0;
};




class ExplorationTasks : public HE2_GlobalBehaviour
{
public:
	ExplorationTasks(WaypointManager * wpm, AgentPlanetGenerator * pg);

	WaypointManager* wpm = nullptr;
	void update();

	std::vector<Task* > allTasks;

};


class WaypointQuantityTask : public Task
{
public:
	WaypointQuantityTask(ExplorationTasks * et) : et(et)
	{
		goalS = std::to_string(goal);
	}


	void checkForUpdates()
	{
		wayPointsSet = et->wpm->waypoints.size();
	}
	bool isDone()
	{
		return wayPointsSet >= goal;
	}

	std::string getReadOutString()
	{
		return "Set at least " + goalS + " waypoints! (" + std::to_string(wayPointsSet) + "/" + goalS + ")";
	}

	ExplorationTasks* et = nullptr;
private:
	int wayPointsSet = 0;
	int goal = 10;
	std::string goalS;
};

class VisitAreaTask : public Task
{
public:
	VisitAreaTask(glm::vec3 position, std::string areaName)
	{
		destination = position;
		name = areaName;
	}

	void checkForUpdates()
	{
		if (!visited)
		{
			if (glm::distance(destination, HE2_Camera::main->getPos()) < range)
			{
				visited = true;
			}
		}
	}
	bool isDone()
	{
		return visited;
	}

	std::string getReadOutString()
	{
		return "Visit " + name;
	}

private:
	float range = 100.0f;
	glm::vec3 destination;
	std::string name;
	bool visited = false;
};



class VisitAreaTypeTask : public Task
{
public:
	enum AreaType
	{
		River,
		Mountain,
		Sea
	};

	VisitAreaTypeTask(AreaType, std::string areaName, int quantity, AgentPlanetGenerator * pg) : quantity(quantity), pg(pg)
	{
		name = areaName;
	}

	void checkForUpdates()
	{
		if (!complete)
		{
			glm::vec3 camPos = HE2_Camera::main->getPos();

			//But far enough from our visited locations?
			for (int y = 0; y < visitedLocations.size(); y++)
			{
				if (glm::distance(visitedLocations[y], camPos) < range * 1.5f)
				{
					return;
				}
			}

			switch (areaType)
			{
			case River:
				for (int i = 0; i < 200; i++)
				{
					//Are we close to a location
		
					if ((glm::distance(camPos, glm::vec3(pg->riverBlock.points[i]))) < range)
					{
					
						visitedLocations.push_back(pg->riverBlock.points[i]);
						quantity--;
					}
				}
				break;
			case Mountain:
				for (int i = 0; i < pg->ridgeBlock.actRidges; i++)
				{
					//Are we close to a location
					for(int b = 0; b < pg->ridgeBlock.ridges[i].actPoints; b++)
						if ((glm::distance(camPos, glm::vec3(pg->ridgeBlock.ridges->points[b]))) < range)
						{

							visitedLocations.push_back(pg->ridgeBlock.ridges->points[b]);
							quantity--;
						}
				}
				break;
			case Sea:

				if (pg->getHeightAtCoord(FrustumChecker::calcTexCoord(camPos, glm::vec3(0), 3150.0f)) * 1200.0f < pg->pub.oceanLevel)
				{
					visitedLocations.push_back(camPos);
					quantity--;
				}
				break;

	
			}
			if (quantity <= 0)
				complete = true;
		}
	}

	bool isDone()
	{
		return complete;
	}

	std::string getReadOutString()
	{
		return "Visit " +std::to_string(quantity) + " " + name;
	}

private:
	float range = 800.0f;
	std::string name;
	bool complete = false;
	int quantity;
	AreaType areaType;

	AgentPlanetGenerator* pg = nullptr;

	std::vector<glm::vec3> visitedLocations;
};