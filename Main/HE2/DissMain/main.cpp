#include <HE2.h>
#include <VulkanBackend.h>
#include <iostream>
#include <PlanetScene.h>
#include <FlameGraphBehaviour.h>
#include <TestingBehaviours.h>
#include <FPSUIElement.h>
#include <SceneGraphDisplayBehaviour.h>
#include <ConsoleUI.h>
#include <WorkerUI.h>
#include "WaypointManager.h"
#include <HE2_JobCentre.h>
#include <FlyingCamera.h>
#include <GeneratorMenu.h>
#include <PlanetGenerationManager.h>
#include "ExplorationTasks.h"
#include <SimpleCameraKeyboardMove.h>

void primeChecker(int start, int range)
{
	int i, num = start, primes = 0;

	while (num <= start + range) {
		i = 2;
		while (i <= num) {
			if (num % i == 0)
				break;
			i++;
		}
		if (i == num)
			primes++;


		num++;
	}
	printf("%d prime numbers calculated between %d and %d\n", primes, start, range);
}

class WelcomeUI : public HE2_GlobalBehaviour
{
public:

	void update()
	{
		if (!closed)
		{
			ImGui::Begin("Welcome!");
			ImGui::TextWrapped("Welcome to this planet generation demo! Use WASD to move, the mouse to look around, and X to swap between local view and map view. Use the spacebar to set a waypoint, and once you're done fiddling with the planet generation system, press next step to try out a simple game! Enjoy!");

			if (ImGui::Button("Close"))
			{
				closed = true;
				//fc->switchNextFrame = true;
			}

			ImGui::End();
		}
	}

	bool closed = false;
	FlyingCamera* fc = nullptr;
};


int main()
{
	std::function <void()> job1 = [=]
	{
		primeChecker(1, 600000000);
	};

	HE2_Model* cubeModel = nullptr;
	HE2_Model* planetModel = nullptr;


	HE2_ObjectInstance cube1;

	HE2_ObjectInstance cube2;

	HE2_SceneCreator* testScene = nullptr;

	CameraWire* camBeh = nullptr;

	FlameGraphBehaviour* flame = nullptr;

	WaypointManager* wpm = nullptr;
	ExplorationTasks* exp = nullptr;
	
	try {
		HE2_InstanceSettings Is = { "Dissertation Demo", true };

		HE2_VulkanBackend* vulkanBackend = new HE2_VulkanBackend();
		HE2_Instance* instance = new HE2_Instance(vulkanBackend, Is);

		instance->Init();

		//Planet Generation and Display stuff

		PlanetScene* ps = new PlanetScene();
		ps->init();
		ps->makeShaders();
		ps->loadAllObjects(false);


		ps->pgm->firstGenerate();

		wpm = new WaypointManager();
		exp = new ExplorationTasks(wpm, ps->pgm->pg);
		wpm->fc = ps->fc;

		WelcomeUI* ui = new WelcomeUI();
		ui->fc = ps->fc;

		GeneratorMenu* gm = new GeneratorMenu(ps->pgm->pg, ps->pgm);

		//Monitorring and Debugging UI
		SceneGraphDisplayBehaviour* sceneGraph = new SceneGraphDisplayBehaviour();

		ConsoleUI* consoleUI = new ConsoleUI();

		flame = new FlameGraphBehaviour();


		HE2_Model * treeModel = HE2_MeshManager::importModel("models/geosphere.obj");

		HE2_RenderComponent* rc = new HE2_RenderComponent(treeModel, treeModel->defaultMaterial);

		HE2_Object* tree = new HE2_Object({ rc });


		//HE2_Scene* testScene = new HE2_Scene("Tree Test Scene", {});
		//HE2_Scene::CreateSceneObjects(testScene);

		//SimpleCameraKeyboardMove* sckm = new SimpleCameraKeyboardMove(HE2_Camera::main);

		instance->RunGame();
		


		glfwTerminate();

		delete sceneGraph;
		delete consoleUI;
		delete wpm;
		delete exp;
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;

		glfwTerminate();
		while (true);
	}
	catch (...)
	{
		std::cout << "Unknown Execption" << std::endl;
		glfwTerminate();
		while (true);
	}

	return 0;
}