#pragma once
#include <string>
#include <vector>
#include <optional>
#include "HE2_ComponentOwner.h"
#include "HE2_Image.h"
#include "HE2_GLM.h"

enum HE2_ShaderFeatureType
{
	//Implementation is supported, Vulkan treated as a push constant
	HE2_SHADER_FEATURE_TYPE_UNIFORM,
	//Implementation is supported
	HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK,
	//Implementation is supported
	HE2_SHADER_FEATURE_TYPE_SAMPLER2D,
	//Supported, vulkan is treaed as an image2D
	HE2_SHADER_FEATURE_STORAGE_IMAGE,
	//Supported
	HE2_SHADER_FEATURE_STORAGE_BUFFER
};

enum HE2_ShaderStage {
	HE2_SHADER_STAGE_VERTEX,
	HE2_SHADER_STAGE_FRAGMENT,
	HE2_SHADER_STAGE_TESSC,
	HE2_SHADER_STAGE_TESSE,
	HE2_SHADER_STAGE_COMPUTE,
	HE2_SHADER_STAGE_GEOMETRY
};

enum HE2_ShaderDataInputTypes {
	HE2_DATA_INPUT_FLOAT,
	HE2_DATA_INPUT_UINT_32,
	HE2_DATA_INPUT_VEC2,
	HE2_DATA_INPUT_VEC3,
	HE2_DATA_INPUT_VEC4
};

enum HE2_ShaderType
{
	HE2_SHADER_IMMEDIATE =0 ,
	HE2_SHADER_DEFERRED = 1,
	HE2_SHADER_DEFERRED_RESOLVE = 2,
	HE2_SHADER_UI = 3,
	HE2_SHADER_FRAGMENT_GPU_TASK = 4,
	HE2_COMPUTE = 5,
	HE2_SHADER_COMPUTE_GPU_TASK = 6
};

typedef uint32_t HE2_ShaderFeatureUpdateFrequency;
typedef uint16_t HE2_BufferDataHandle;
typedef uint16_t HE2_StorageBufferDataHandle;

enum class HE2_PrimitiveType
{
	HE2_TRIANGLE_LIST,
	HE2_PATCH_LIST
};

enum HE2_ShaderFeatureSource
{
	//View Projection Buffer
	HE2_SHADER_BUFFER_SOURCE_VIEW_PROJECTION_BUFFER ,
	//Not yet implemented
	HE2_SHADER_BUFFER_SOURCE_DEFERRED_LIGHTING_BUFFER ,
	//Requests a value from he2_shaderfeature.valueGetter
	HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC ,
	//Model Buffer
	HE2_SHADER_BUFFER_SOURCE_MODEL_BUFFER ,
	//Material Buffer
	HE2_SHADER_BUFFER_SOURCE_MATERIAL_BUFFER,
	//Deferred Shading Image sources
	HE2_SHADER_IMAGE_SOURCE_GBUFFER_POSITION,
	HE2_SHADER_IMAGE_SOURCE_GBUFFER_NORMAL,
	HE2_SHADER_IMAGE_SOURCE_GBUFFER_ALBEDO
};

enum HE2_ShaderOutputType
{
	HE2_SHADER_OUTPUT_SCREEN,
	HE2_SHADER_OUTPUT_CUSTOM_IMAGE

};

enum HE2_ShaderOutputFormat
{
	HE2_RGBA, //In Vulkan, this is 8bits per comp
	HE2_RGBA_HIGHP, //In Vulkan, this is 16bits per comp
	HE2_DEPTH32 //In Vulkan, this is decided by the findDepthFormat() method
};

//"Getter" classes are called on once on object creation. They are designed to be overritten by behaviours or components, that can return buffer or image handles to match their object

//The group and binding values are there to ensure that one class can be the "getter" for multiple buffers

class HE2_ShaderBufferGetter : virtual public HE2_ComponentBase
{
public:
	virtual HE2_BufferDataHandle getBufferHandle(int group, int binding) = 0;
};

//These are designed to be overridden for non-material images. For example - something that generates a heightmap, might inherit from this and from privatebehaviour. Then the correct image handle could be returned and filled into the descriptor write
class HE2_ShaderImageHandleGetter : virtual public HE2_ComponentBase
{
public:
	virtual HE2_ImageHandle getImageHandle(int group, int binding) = 0;
};

class HE2_UniformValueGetter : virtual public HE2_ComponentBase
{
public: 
	virtual void* getData(int pushConstantIndex) = 0;
};

class HE2_ShaderImageReturn : virtual public HE2_ComponentBase
{
public: 
	virtual void setImageOutput(HE2_Image*, int imageIndex) = 0;
};

class HE2_ShaderStorageBufferGetter : virtual public HE2_ComponentBase
{
public:
	virtual HE2_StorageBufferDataHandle getStorageBufferHandle(int grouping, int handle) = 0;
};

struct HE2_ShaderFeature
{
public:
	HE2_ShaderFeatureType featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	HE2_ShaderStage stage = HE2_SHADER_STAGE_VERTEX;
	uint32_t binding = 0;
	
	//The source of this feature
	HE2_ShaderFeatureSource source = HE2_SHADER_BUFFER_SOURCE_VIEW_PROJECTION_BUFFER;

	//Only used for uniform variables. In Vulkan, this is only guaranteed as 128bytes!
	uint32_t size = 0;
	//Each group can be bound together.This determines what set this belongs to in Vulkan. 
	uint32_t grouping = 0;
	//This is only used for types that require an explicit offset
	uint32_t offset = 0;

	//Texture read type - only used for samplers
	bool exactRead = false;
};

struct HE2_DataInputDesc
{
public:
	uint32_t binding = 0;
	uint32_t location = 0;
	uint32_t format = 0;
	uint32_t offset = 0;
};

struct HE2_ShaderOutput
{
	uint32_t outputLocation = 0;
	HE2_ShaderOutputType outputType = HE2_ShaderOutputType::HE2_SHADER_OUTPUT_SCREEN;
	HE2_ShaderOutputFormat format = HE2_ShaderOutputFormat::HE2_RGBA;
	glm::vec2 size = glm::vec2(0);
	bool storageCapability = false;
};

struct HE2_ShaderSettings
{
public:
	int tessalationControlPoints = 3;
	HE2_PrimitiveType primitiveType = HE2_PrimitiveType::HE2_TRIANGLE_LIST;
	bool useVPFeature = false;
	HE2_ShaderType shaderType = HE2_ShaderType::HE2_SHADER_IMMEDIATE;

	std::vector<HE2_ShaderOutput> outputs;

	bool canBeWireframe = false;
	bool allowBlending = false;
	bool cullBack = true;
	bool isInstanced = false;
};

struct HE2_ShaderSources
{
public:
	std::optional <std::string> vertexShaderSource;
	std::optional <std::string> fragmentShaderSource;
	std::optional <std::string> geometryShaderSource;
	std::optional <std::string> tessalationControlShaderSource;
	std::optional <std::string> tessalationEvaluationShaderSource;
	std::optional <std::string> computeShaderSource;
};

struct HE2_PerVertexDataDesc
{
	uint32_t stride = 0;
	bool nullData = false;
};

struct HE2_PerInstanceDataDesc
{
	uint32_t stride = 0;
};

class HE2_Shader : public HE2_ComponentOwner
{
public:
	//Constructor standard shader that takes in a vertex and a fragment shader
	HE2_Shader(std::string name, HE2_ShaderSources shaderSources, std::vector<HE2_ShaderFeature> features, std::vector<HE2_DataInputDesc> dataInputDesc, HE2_PerVertexDataDesc dataPerVertexDesc, HE2_ShaderSettings settings);
	virtual ~HE2_Shader();
	std::string name;

	std::vector<HE2_ShaderFeature> features;
	std::vector<HE2_ShaderFeature> uniformFeatures;
	std::vector<std::pair<int, std::vector<HE2_ShaderFeature>>> shaderSpecificFeatures;

	uint32_t featureGroups = 0;
	HE2_ShaderSources shaderSources;
	std::vector<HE2_DataInputDesc> vertexInputDesc;
	std::vector<HE2_DataInputDesc> instancedInputDesc;

	HE2_PerVertexDataDesc perVertexDataDesc;
	HE2_PerInstanceDataDesc perInstanceDataDesc;

	static HE2_ShaderFeature viewProjectionFeature;
	static HE2_ShaderFeature modelMatrixUniformFeature;

	HE2_ShaderSettings shaderSettings = {};
protected:
	//Allow Children to create without parameters then setup later
	HE2_Shader() {}
	void setupShader();


};

