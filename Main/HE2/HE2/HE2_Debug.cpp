#include "HE2_Debug.h"
#include <sstream>


std::vector<HE2_Debug::DebugMessage> HE2_Debug::messages;
std::string HE2_Debug::time_format = { "%H:%M:%S" };
std::map<std::string, HE2_ConsoleCommand*> HE2_Debug::allCommands;

void HE2_Debug::executeCommand(std::string plainText)
{

	std::string cmd;
	std::string args;

	int index = plainText.find(" ");

	cmd = plainText.substr(0,index);

	if (index < plainText.length())
		args = plainText.substr(index + 1, plainText.length());
	else
		args = "";

	if (allCommands.count(cmd)!= 0)
	{
		allCommands[cmd]->execute(args);
	}
	else
	{
		DebugLog("Not a valid command!");
	}
}

void HE2_Debug::Log(std::string x)
{
	DebugMessage dm = {};
	dm.message = x;

	dm.time = std::chrono::system_clock::now();

	std::stringstream timeReadable;

	//Please move time - string stuff into debug itself
	timeReadable << dm.time;

	dm.timeAsString = timeReadable.str();

	messages.push_back(dm);
}