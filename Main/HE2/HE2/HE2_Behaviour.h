#pragma once
class HE2_Object;



class HE2_Behaviour
{
public:
	HE2_Behaviour(bool threadable = false);
	virtual ~HE2_Behaviour();
	virtual void update() {}
private:
	friend class HE2_BehaviourManager;
	int priority = 5;
protected:
	bool threadable = false;
};

//This is likley to become a host class for LUA scripts
class HE2_ObjectBehaviour : public HE2_Behaviour
{
public:
	HE2_ObjectBehaviour(HE2_Object* host, bool threadable = false);
	virtual ~HE2_ObjectBehaviour();

protected:
	HE2_Object* object = nullptr;
};


class HE2_GlobalBehaviour : public HE2_Behaviour 
{
public:
	HE2_GlobalBehaviour(bool threadable = false) : HE2_Behaviour(threadable) {}
	~HE2_GlobalBehaviour() {}
};