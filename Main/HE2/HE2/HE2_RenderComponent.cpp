#include "HE2_RenderComponent.h"
#include "HE2_Instance.h"
#include "HE2_RenderBackend.h"
#include <stdexcept>
#include "HE2_Material.h"
#include "HE2_Model.h"

HE2_RenderComponent::HE2_RenderComponent()
{
	renderType |= HE2_PassFlagTypes::OpaqueObjects;
	renderContext = HE2_Instance::renderBackend->finalContext;
}

HE2_RenderComponent::HE2_RenderComponent(HE2_Model* model, HE2_Material* material)
{
	setModel(model);
	setMaterial(material);

	renderType |= HE2_PassFlagTypes::OpaqueObjects;

	renderContext = HE2_Instance::renderBackend->finalContext;
}
HE2_RenderComponent::HE2_RenderComponent(HE2_Model* model)
{
	setModel(model);
	assert(model->defaultMaterial != nullptr);
	setMaterial(model->defaultMaterial);

	renderType |= HE2_PassFlagTypes::OpaqueObjects;
	renderContext = HE2_Instance::renderBackend->finalContext;
}

void HE2_RenderComponent::setMaterial(HE2_Material* mat)
{
	material = mat;

	changeFlag = true;
	if (material->shader->shaderSettings.shaderType == HE2_ShaderType::HE2_SHADER_DEFERRED)
	{
		renderType |= HE2_PassFlagTypes::Deferred;
	}
}

void HE2_RenderComponent::setModel(HE2_Model* mod)
{
	if (model == mod) return;

	changeFlag = true;
	model = mod;
}

HE2_Material* HE2_RenderComponent::getMaterial()
{
	return material;
}

HE2_Model* HE2_RenderComponent::getModel()
{
	return model;
}

bool HE2_RenderComponent::isValid()
{
	return (model != nullptr) && (material != nullptr);
}