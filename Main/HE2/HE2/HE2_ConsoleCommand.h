#pragma once
#include <string>
class HE2_ConsoleCommand
{
public:
	HE2_ConsoleCommand(std::string cmd);

	std::string cmd;
	std::string desc;

	virtual void execute(std::string params) = 0;
};

