#pragma once
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/set.hpp>
#include <cereal/cereal.hpp>
#include "HE2_Component.h"
#include <fstream>
#include <ostream>
#include <sstream>

class HE2_Object;
class HE2_Scene;

//Macro for registering components and assigning their derivation

#define HE2_RegisterComponent(Derived)							\
CEREAL_REGISTER_TYPE(Derived);									\
CEREAL_REGISTER_POLYMORPHIC_RELATION(HE2_ComponentBase, Derived)

#define HE2_RegisterObject(Derived)							\
CEREAL_REGISTER_TYPE(Derived);									\
CEREAL_REGISTER_POLYMORPHIC_RELATION(HE2_Object, Derived)

class HE2_Serialize
{
public:
	static void writeObjectToFile(HE2_Object* obj, std::string filename);
	static void writeObjectToFile(HE2_Object* obj, std::fstream& fileStream);

	static void readObjectFromFile(HE2_Object* obj, std::string fialename);
	static void readObjectFromFile(HE2_Object* obj, std::fstream& fileStream);

	static HE2_Object* getObjectFromFile(std::string filename);
	static HE2_Object* getObjectFromFile(std::fstream& fileStream);

	static HE2_Object* instantiate(HE2_Object* objectToCopy);

	static void writeSceneToFile(HE2_Scene* scene, std::string filename);
	static void readSceneFromFile(HE2_Scene* scene, std::string filename);
};

