#include "HE2_Serialize.h"
#include "HE2_Scene.h"
#include "HE2_Light.h"
#include "HE2.h"

void HE2_Serialize::writeObjectToFile(HE2_Object* obj, std::string filename)
{
	std::fstream stream(filename, std::ios::binary | std::fstream::out);
	writeObjectToFile(obj, stream);
	stream.close();
}

void HE2_Serialize::writeObjectToFile(HE2_Object* obj, std::fstream& fileStream)
{
	cereal::BinaryOutputArchive iarchive(fileStream); // Create an output archive

	iarchive(*obj); // Write the data to the archive
}

void HE2_Serialize::readObjectFromFile(HE2_Object* obj, std::string filename)
{
	std::fstream stream(filename, std::ios::binary | std::fstream::in);
	readObjectFromFile(obj, stream);
	stream.close();
}

void HE2_Serialize::readObjectFromFile(HE2_Object* obj, std::fstream& fileStream)
{

	cereal::BinaryInputArchive oarchive(fileStream); // Create an output archive

	oarchive(*obj); // Write the data to the archive
}

HE2_Object* HE2_Serialize::getObjectFromFile(std::string filename)
{
	HE2_Object* object = new HE2_Object();

	readObjectFromFile(object, filename);

	return object;
}

HE2_Object* HE2_Serialize::getObjectFromFile(std::fstream& fileStream)
{
	HE2_Object* object = new HE2_Object();

	readObjectFromFile(object, fileStream);

	return object;
}

HE2_Object* HE2_Serialize::instantiate(HE2_Object* objectToCopy)
{
	std::stringstream stream;
	{
		cereal::BinaryOutputArchive oarchive(stream); // Create an output archive

		oarchive(*objectToCopy); // Write the data to the archive
	}

	HE2_Object* newObject = new HE2_Object();

	{
		cereal::BinaryInputArchive oarchive(stream); // Create an output archive
		oarchive(*newObject);
	}
	return newObject;
}

void  HE2_Serialize::writeSceneToFile(HE2_Scene* scene, std::string filename)
{
	std::fstream fileStream(filename, std::ios::binary | std::fstream::out);

	cereal::BinaryOutputArchive iarchive(fileStream); // Create an output archive

	iarchive(*scene); // Write the data to the archive

	fileStream.close();
	
}

void HE2_Serialize::readSceneFromFile(HE2_Scene* scene, std::string filename)
{
}
