#pragma once
#include <string>
#include "HE2_GLM.h"
#include "HE2_ComponentOwner.h"
#include <cereal/cereal.hpp>
#include "HE2_Behaviour.h"
#include "HE2_Component.h"
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>

class HE2_Object : public virtual HE2_ComponentOwner
{
public:

	HE2_Object();
	HE2_Object(HE2_Object* parent);
	//Note: If you want this object to render, one component must be a HE2_Render_Component
	HE2_Object(HE2_Object* parent, std::vector<HE2_ComponentBase*> startingComponents);
	//Note: If you want this object to render, one component must be a HE2_Render_Component
	HE2_Object(std::vector<HE2_ComponentBase*> startingComponents);
	HE2_Object(std::vector<HE2_ComponentBase*> startingComponents, std::vector<std::pair<HE2_ComponentBase*, std::vector<std::type_index>>>);

	virtual ~HE2_Object();

	void addChild(HE2_Object*);
	void setParent(HE2_Object*);
	void removeChild(HE2_Object*);
	std::vector<HE2_Object*> getChildren();

	glm::mat4 getModelMatrix();
	glm::vec3 getPos();

	glm::vec3 getScale();
	glm::mat4 getScaleMat();

	glm::vec3 getRot();
	glm::mat4 getRotMat();

	void setPos(glm::vec3 x);
	void setScale(glm::vec3 x);
	void setRot(glm::vec3 x);
	void setRot(glm::quat x);

	void setRotMat(glm::mat4 x);
	void setModelMat(glm::mat4 modelMat);

	std::string name;

	bool active = true;


	template<class Archive>
	void save(Archive& archive) const
	{
		archive(transform, rotation, scale);

		archive(name);

		//archive(components);
	}

	template<class Archive>
	void load(Archive& archive)
	{
		archive(transform, rotation, scale);
		dirtyMatricies();
		getModelMatrix();
		archive(name);

		//archive(components);
	}

	//Not const as getModelMatrix CAN change members (model matrix isn't calculated until needed)
	bool operator== ( HE2_Object& other) 
	{
		if (transform != other.transform) return false;
		if (rotation != other.rotation) return false;
		if (scale != other.scale) return false;

		if (getModelMatrix() != other.getModelMatrix()) return false;

		if (name != other.name) return false;

		if (parent != other.parent) return false;
	}

	bool operator== (const HE2_Object& other) const
	{
		if (transform != other.transform) return false;
		if (rotation != other.rotation) return false;
		if (scale != other.scale) return false;


		if (name != other.name) return false;

		if (parent != other.parent) return false;
	}

protected:


	HE2_Object* parent = nullptr;
	std::vector<HE2_Object*> children;

private:

	void updateModelMatIfNeeded();
	void dirtyMatricies();

	glm::mat4 transform = glm::mat4(1);
	glm::mat4 rotation = glm::mat4(1);
	glm::vec3 rotAngles;
	glm::mat4 scale = glm::mat4(1);
	glm::vec3 scaleVec = glm::vec3(1);

	glm::mat4 modelMatrix = glm::mat4(1);

	bool modelMatrixDirty = false;

	HE2_Object(const HE2_Object&) = delete;


};