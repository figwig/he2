#pragma once
#include "HE2_RenderBackend.h"
#include "HE2_WindowHandler.h"
#include "HE2_GLM.h"
#include <string>

//Define to remove the standard asset imports
//#define NO_STANDARD_ASSETS

//An instance of the game engine


struct HE2_InstanceSettings
{
	std::string windowName = "HE2";

	bool useComputeShader = false;
};

class HE2_Instance
{
public:
	HE2_Instance(HE2_RenderBackend* renderPipeline, HE2_InstanceSettings = {});

	~HE2_Instance();

	void Init(bool createWindow = true);

	void RunGame();

	static HE2_RenderBackend* renderBackend;

	static HE2_InstanceSettings instanceSettings;
private:
	friend class HE2_Shader;

	bool firstFrame = true;

	void checkReady();
};

