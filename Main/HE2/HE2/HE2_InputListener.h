#pragma once
#include "HE2_Input.h"
class HE2_InputListener
{
private:
	friend class HE2_Input;

	virtual void OnMouseOver() {}
	virtual void OnMouseAction(HE2_MouseAction) {}
	virtual void OnKeyPress() {}
	virtual void OnScroll(double x, double y) {}
	virtual void OnMouseMove(double x, double y) {}

protected:
	HE2_InputListener();
	virtual ~HE2_InputListener();
	HE2_InputState getInputState();

};

