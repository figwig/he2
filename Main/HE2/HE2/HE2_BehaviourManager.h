#pragma once
#include "HE2_Component.h"
#include "HE2_Behaviour.h"
#include <vector>

class HE2_BehaviourManager
{
public:
	static void addBehaviour(HE2_Behaviour*);
	static void removeBehaviour(HE2_Behaviour*);

	static void addComponentAsBehaviour(HE2_ComponentBase*);

private:

	static void updateBehaviours();

	friend class HE2_Instance;

	static std::vector<HE2_Behaviour*> behaviours;
	static std::vector<HE2_Behaviour*> unThreadableBehaviours; 
	static std::vector<HE2_ComponentBase*> componentAsBehaviours;
};

