#include "HE2_JobCentre.h"


HE2_JobCentre* HE2_JobCentre::thisPointer = nullptr;

HE2_JobCentre* HE2_JobCentre::singleton()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new HE2_JobCentre();
	}
	return thisPointer;
}

void HE2_JobCentre::release()
{
	if (thisPointer != nullptr)
	{
		delete thisPointer;
		thisPointer = nullptr;
	}
}



HE2_JobCentre::HE2_JobCentre()
{
	initialize();
}

HE2_JobCentre::~HE2_JobCentre()
{
	isDone = true;

	for (auto& worker : workers)
	{
		worker->thisThread.join();
		delete worker;
	}
}

void HE2_JobCentre::initialize()
{
	int supportedThreads = std::thread::hardware_concurrency() - 1;

	for (size_t i = 0; i < supportedThreads; ++i)
	{
		Worker* worker = new Worker();

		worker->jc = this;
		worker->thisThread = std::thread(&Worker::work, worker);
		workers.push_back(worker);
	}
}




void Worker::work()
{
	while (true)
	{
		if (jc->isDone) return;
		idle = false;

		Job currentJob;

		bool found = jc->jobQueue.try_dequeue(currentJob);
		if (found)
		{
			idle = false;

			currentJobName = currentJob.jobName;

			currentJob.function();

			idle = true;
		}
		else
		{
			idle = true;

			currentJobName = "Idle";

			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}

	}
}

void HE2_JobCentre::QueueJob(std::string jobName, std::function<void()> function)
{
	Job newJob = {};

	newJob.jobName = jobName;
	newJob.function = function;

	jobQueue.enqueue(newJob);
}

void HE2_JobCentre::waitUntilAllIdle()
{
	bool allIdle = false;
	while (!allIdle)
	{

		allIdle = true;
		int sizeOfQueue = (int)jobQueue.size_approx();

		allIdle = !(sizeOfQueue > 0);

		for (auto i : workers)
		{
			if (!i->idle)
			{
				allIdle = false;
			}
		}
		sizeOfQueue = (int)jobQueue.size_approx();

		allIdle = !(sizeOfQueue > 0);
	}
}
