#pragma once
#include "HE2_Component.h"
#include "HE2_RenderPass.h"
#include "HE2_RenderRun.h"
#include "HE2_Object.h"
#include "HE2_Serialize.h"
class HE2_Material;
class HE2_Model;

class HE2_RenderComponent : public HE2_Component<HE2_Object>
{
public:
	HE2_RenderComponent();
	HE2_RenderComponent(HE2_Model * , HE2_Material*);
	HE2_RenderComponent(HE2_Model *);
	HE2_Material* getMaterial();
	HE2_Model* getModel();


	void setMaterial(HE2_Material* mat);
	void setModel(HE2_Model* model);

	//Material stuff

	bool isValid();

	HE2_PassFlags renderType = 0;

	int instances = 1;

	bool changeFlag = false;
	HE2_RenderContext* renderContext = nullptr;

	template<class Archive>
	void save(Archive& archive) const
	{
		archive(renderType);
		archive(instances);
		archive(changeFlag);
		archive(model->name);
		//archive(material);
	}

	template<class Archive>
	void load(Archive& archive)
	{
		archive(renderType);
		archive(instances);
		archive(changeFlag);
		std::string modelFilename;
		archive(modelFilename);
		setModel(HE2_MeshManager::importModel(modelFilename));

		//archive(material);

		//Need to do: render context
	}
	
private:
	HE2_Model* model = nullptr;
	HE2_Material* material = nullptr;
};

HE2_RegisterComponent(HE2_RenderComponent);

//CEREAL_REGISTER_TYPE(HE2_RenderComponent);
//
//CEREAL_REGISTER_POLYMORPHIC_RELATION(HE2_ComponentBase, HE2_RenderComponent);