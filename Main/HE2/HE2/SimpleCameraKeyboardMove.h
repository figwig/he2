#pragma once
#include "HE2.h"
class SimpleCameraKeyboardMove : public HE2_ObjectBehaviour, public HE2_InputListener
{
public:
	SimpleCameraKeyboardMove(HE2_Object* obj) :HE2_ObjectBehaviour(obj) { //glfwSetInputMode(HE2_WindowHandler::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);  
		light = new HE2_Light(HE2_Light::HE2_LightType::HE2_PointLight, glm::vec4(2.0f, 2.0f, 2.0f, 0.0f), 200, 200, {}, false);
	}
	void update();

	void OnMouseMove(double x, double y);
private:

	double lastX = 0.0f;
	double lastY = 0.0f;

	double mouseX = 0.0f;
	double mouseY = 0.0f;

	glm::vec3 cameraTargetLookAt = glm::vec3(0, 0, -10);
	glm::vec3 cameraTargetPos = glm::vec3(0, 5, -10);

	float speed = 80.0f;

	float percentTravel = 15.0f;

	HE2_Object* light = nullptr;
	bool moveLight = true;

	float ticker = 0.0f;
};

