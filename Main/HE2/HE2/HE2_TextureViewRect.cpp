#include "HE2_TextureViewRect.h"
#include "HE2_RenderComponent.h"
#include "HE2_Scene.h"


HE2_TextureViewRect::HE2_TextureViewRect(HE2_Image* texToView, HE2_Shader * shader)
{
	setup(texToView, shader);

}
HE2_TextureViewRect::HE2_TextureViewRect(HE2_Image* texToView, HE2_Shader* shader, std::string name)
{
	this->name = name;
	setup(texToView, shader);

}

void HE2_TextureViewRect::setup(HE2_Image* texToView, HE2_Shader* shader)
{
	HE2_SimpleMaterial* mat = new HE2_SimpleMaterial(texToView, shader);

	HE2_RenderComponent* rc = new HE2_RenderComponent(StandardAssets::quadModel, mat);

	addComponent(rc);

	HE2_Scene::currentScene->sceneGraph.addToRootNodes(this);

	HE2_Instance::renderBackend->onAddObject(this);
}