#pragma once
#include <string>
#include <map>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <iostream>
#include "HE2_GLM.h"
#include "Helpers.h"

class HE2_Model;


struct HE2_ImportSettings
{
	bool reverseWindingOrder = false;
	bool centreMesh = true;
	bool reverseNormals = false;
	bool operator == (const HE2_ImportSettings& other) const;
	bool operator < (const HE2_ImportSettings& other) const;
};


struct ModelReference
{
	std::string filepath = "";
	HE2_ImportSettings importSettings = {};

	bool operator == (const ModelReference& other) const;
	bool operator< (const ModelReference& other) const;
};


//A class used to import and manage meshes within the engine
class HE2_MeshManager
{
public:
	static HE2_Model* importModel(std::string filename, HE2_ImportSettings importSettings = HE2_ImportSettings());
private:


	static std::map<ModelReference, HE2_Model*> models;

	static void lookThroughNode(aiNode*, glm::mat4, const aiScene*, int depth, HE2_Model *parentMesh);

	static void dealWithMesh(aiMesh*, const aiScene*, glm::mat4,HE2_Model*);

	static std::string currentFileName;

	static HE2_Model* parentModel;

	static HE2_ImportSettings currentImportSettings;
};

