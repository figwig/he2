#pragma once
#include "HE2_Image.h"
#include <vector>
#include <map>
class HE2_TextureAllocator;
//This is a singleton that manages Textures. It relies on a Texture Allocator provided by the render backend
class HE2_TextureManager
{
public:

	static HE2_Image* getImage(std::string filepath, bool gammaCorrected = false);
	static HE2_Image* getImage(HE2_ImageHandle handle);
private:

	static std::map < std::string, HE2_Image* > images;

	static HE2_TextureAllocator* allocator;
	friend class HE2_Instance;
	friend class HE2_RenderBackend; 
	static std::string removeLongFileName(std::string filePath);
};

