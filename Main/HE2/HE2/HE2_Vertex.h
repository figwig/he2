#pragma once
#include "HE2_GLM.h"
#include "HE2_Shader.h"
#include <vector>

struct HE2_Vertex
{
	HE2_Vertex() {}

	HE2_Vertex(glm::vec3 posi)
	{
		pos = posi;
		normal = glm::vec3(0);
		tangent = glm::vec3(0);
		bitangent = glm::vec3(0);
		texCoord.x = pos.x;
		texCoord.y = pos.y;
	}

	glm::vec3 pos = glm::vec3(0);
	glm::vec3 normal = glm::vec3(0);
	glm::vec3 tangent = glm::vec3(0);
	glm::vec3 bitangent = glm::vec3(0);
	glm::vec2 texCoord = glm::vec2(0);


	bool operator == (const HE2_Vertex& other) const
	{
		return pos == other.pos && texCoord == other.texCoord && normal == other.normal && tangent == other.tangent && bitangent == other.bitangent;
	}

	static std::vector<HE2_DataInputDesc> getAttributeDescriptionsHE2()
	{
		std::vector<HE2_DataInputDesc> attributeDescriptions;

		attributeDescriptions.resize(5);

		//Details for pos
		attributeDescriptions[0].binding = 0;
		attributeDescriptions[0].location = 0;
		attributeDescriptions[0].format = HE2_DATA_INPUT_VEC3;
		attributeDescriptions[0].offset = offsetof(HE2_Vertex, pos);

		//Normal
		attributeDescriptions[1].binding = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format = HE2_DATA_INPUT_VEC3;
		attributeDescriptions[1].offset = offsetof(HE2_Vertex, normal);

		//Tangent
		attributeDescriptions[2].binding = 0;
		attributeDescriptions[2].location = 2;
		attributeDescriptions[2].format = HE2_DATA_INPUT_VEC3;
		attributeDescriptions[2].offset = offsetof(HE2_Vertex, tangent);

		//BiTangent
		attributeDescriptions[3].binding = 0;
		attributeDescriptions[3].location = 3;
		attributeDescriptions[3].format = HE2_DATA_INPUT_VEC3;
		attributeDescriptions[3].offset = offsetof(HE2_Vertex, bitangent);

		//Details for texCoord
		attributeDescriptions[4].binding = 0;
		attributeDescriptions[4].location = 4;
		attributeDescriptions[4].format = HE2_DATA_INPUT_VEC2;
		attributeDescriptions[4].offset = offsetof(HE2_Vertex, texCoord);

		return attributeDescriptions;
	}
};


namespace std
{
	template<> struct hash<HE2_Vertex> {

		size_t operator()(HE2_Vertex const& vertex) const {
			return (
				(
					(
				(hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.tangent) << 1)) 
				>> 1) ^
				(hash<glm::vec2>()(vertex.texCoord) << 1)
				>>1) ^
				(hash<glm::vec3>()(vertex.normal) << 1)
				>>1) ^ 
				(hash<glm::vec3>()(vertex.bitangent) <<1 )
				>>1;
		}
	};
}
