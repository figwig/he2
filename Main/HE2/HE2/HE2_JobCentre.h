#pragma once
#include <concurrentqueue.h>
#include <thread>
#include <atomic>
#include <functional>

class HE2_JobCentre;

class Worker
{
public:
	std::string currentJobName;
private:
	bool idle = true;

	std::thread thisThread;

	friend class HE2_JobCentre;

	HE2_JobCentre* jc = nullptr;

	void work();
};

struct Job
{
public:

	std::function<void()> function;

	std::string jobName;

};

class HE2_JobCentre
{
public:
	static HE2_JobCentre* singleton();
	static void release();

	void QueueJob(std::string, std::function<void()> function);

	void waitUntilAllIdle();

private:
	std::atomic<bool> isDone = false;

	HE2_JobCentre();
	~HE2_JobCentre();

	static HE2_JobCentre* thisPointer;	

	friend class Worker;
	friend class WorkerUI;
	void initialize();

	std::vector<Worker*> workers;

	moodycamel::ConcurrentQueue<Job> jobQueue;
};

