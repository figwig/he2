#pragma once
#include <vector>
class HE2_Light;

class HE2_Lighting
{
public:
	static void addLight(HE2_Light* light);
	static void removeLight(HE2_Light* light);

	static std::vector<HE2_Light*> getLights();
private:
	static std::vector<HE2_Light*> lights;
};

