#include "HE2_RenderBackend.h"
#include "HE2_TextureManager.h"
#include "HE2_Material.h"
#include "HE2_RenderRun.h"


HE2_Shader* HE2_RenderBackend::defaultShader = nullptr;
HE2_Material* HE2_RenderBackend::defaultMaterial = nullptr;
HE2_Material* HE2_RenderBackend::defaultImmediateMaterial = nullptr;

HE2_Shader* HE2_RenderBackend::deferredResolveShader = nullptr;
HE2_Shader* HE2_RenderBackend::deferredShader = nullptr;
HE2_RenderContext* HE2_RenderBackend::finalContext = nullptr;


bool HE2_RenderBackend::dirty = true;
uint32_t HE2_RenderBackend::imageCount = 0;

HE2_RenderBackend::~HE2_RenderBackend()
{ delete finalContext; 
}

void HE2_RenderBackend::setTextureManagerAllocator(HE2_TextureAllocator* alloc)
{
	HE2_TextureManager::allocator = alloc;
}

HE2_RenderBackend::HE2_RenderBackend(HE2_RenderBackendSettings settings) :settings(settings)
{
	finalContext = new HE2_RenderContext();
}
