#pragma once

#include "HE2_GLM.h"
#include <queue>
#include "HE2_Shader.h"
#include "imgui.h"
#include "HE2_WindowHandler.h"

class HE2_RenderRun;
class HE2_RenderContext;
class HE2_Instance;
class HE2_Object;
class HE2_TextureAllocator;
class HE2_Model;
class HE2_Material;
class HE2_Image;
class HE2_GPUTask;


struct HE2_RenderBackendSettings
{
	bool sceneEditorMode = false;
};


class HE2_RenderBackend
{
public:
	HE2_RenderBackend(HE2_RenderBackendSettings settings = {});

	virtual void onAddObject(HE2_Object*) = 0;

	virtual void onAddModel(HE2_Model*) = 0;

	virtual void onAddMaterial(HE2_Material*) = 0;

	virtual void prepareGPUTask(HE2_GPUTask*) = 0;

	virtual HE2_Image* createEmptyImage(bool forCompute,glm::vec2, bool cpuVisible = false) = 0;

	static HE2_Shader* defaultShader;
	static HE2_Shader* deferredResolveShader;
	static HE2_Shader* deferredShader;
	static HE2_Material* defaultMaterial; 
	static HE2_Material* defaultImmediateMaterial;
	static HE2_RenderContext* finalContext;

	static bool dirty;

	virtual HE2_BufferDataHandle makeGPUBuffer(void* data, size_t size, bool updatable) = 0;
	//Make an empty GPU buffer

	virtual void updateGPUBuffer(HE2_BufferDataHandle, void* data, size_t size) =0;

	virtual void queueGPUTask(HE2_GPUTask * task) = 0;

	virtual std::vector<HE2_Image*> getDeferredBuffers() = 0;

	static uint32_t imageCount;

	virtual void rebuildShader(HE2_Shader* shader) = 0;
	
	virtual HE2_StorageBufferDataHandle makeStorageBuffer(size_t size) = 0;

	virtual void copyImage(HE2_Image* srcImage, HE2_Image* dstImage) = 0;

	virtual void copyImageDataToBuffer(void* data, size_t& size, HE2_Image * image) =0;

	bool renderUI = true;

	virtual ImTextureID getImGuiTextureHandle(HE2_Image*) = 0;

	virtual HE2_Image* getGameImage() = 0;


private:

	friend class HE2_Instance;
	friend class HE2_Shader;
	friend class HE2_MeshManager;

	virtual void setupAPI() {}

	virtual void render() {}

	virtual void cleanup() {}


	virtual void buildShader(HE2_Shader*,bool = false) = 0;
	virtual void destroyShader(HE2_Shader*) = 0;
	virtual HE2_TextureAllocator* getAllocator() = 0;


protected:

	virtual ~HE2_RenderBackend();

	void setTextureManagerAllocator(HE2_TextureAllocator* alloc);

	//Used for things like GPU side computes/generation. Pushed back whenever a single time render is needed, rendered in the next frame
	std::queue<HE2_GPUTask*> gpuTasks;
	std::queue<HE2_GPUTask*> gpuComputeTasks;

	HE2_RenderBackendSettings settings = {};

	//Each context is a completely seperate world - for example, a game within a scene editor, or a minigame on a screen
	std::vector<HE2_RenderContext*> contexts;



};