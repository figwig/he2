#include "HE2_InputListener.h"


HE2_InputListener::HE2_InputListener()
{
	HE2_Input::addListener(this);
}

HE2_InputListener::~HE2_InputListener()
{
	HE2_Input::removeListener(this);
}

HE2_InputState HE2_InputListener::getInputState()
{
	return HE2_Input::currentState;
}