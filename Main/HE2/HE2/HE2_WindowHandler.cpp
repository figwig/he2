#include "HE2_WindowHandler.h"
#include "HE2_Instance.h"

GLFWwindow* HE2_WindowHandler::window = nullptr;


void HE2_WindowHandler::CreateWindow()
{
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	window = glfwCreateWindow(WIDTH, HEIGHT, HE2_Instance::instanceSettings.windowName.c_str(), nullptr, nullptr);

	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetScrollCallback(window, scroll_callback);
}

void HE2_WindowHandler::DestroyWindow()
{
	glfwDestroyWindow(window);

	glfwTerminate();
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	HE2_InputPassThrough::KeyAction(key, action, mods);
}

void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	HE2_InputPassThrough::MouseMove(xpos, ypos);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	HE2_InputPassThrough::MouseButtonAction(button, action);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	HE2_InputPassThrough::ScrollEvent(xoffset, yoffset);
}