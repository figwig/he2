#include "InstancedShader.h"

InstancedShader* InstancedShader::thisPointer = nullptr;

InstancedShader* InstancedShader::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new InstancedShader();
	}
	return thisPointer;
}


InstancedShader::InstancedShader()
{
	name = "Instanced Deffered Shader";

	shaderSources.vertexShaderSource = "shaders/deferred/instancedmrt.vert.spv";
	shaderSources.fragmentShaderSource = "shaders/deferred/mrt.frag.spv";

	shaderSettings.shaderType = HE2_ShaderType::HE2_SHADER_DEFERRED;


	features.push_back(HE2_Shader::viewProjectionFeature);
	features.push_back(HE2_Shader::modelMatrixUniformFeature);

	for (auto a : HE2_BlinnPhongMaterial::MaterialShaderFeatures())
		features.push_back(a);

	vertexInputDesc = HE2_Vertex::getAttributeDescriptionsHE2();

	perVertexDataDesc.stride = sizeof(HE2_Vertex);

	shaderSettings.isInstanced = true;

	perInstanceDataDesc.stride = sizeof(glm::mat4);

	HE2_DataInputDesc instancedModelMatrix = {};
	instancedModelMatrix.binding = 1;
	instancedModelMatrix.location = 5;
	instancedModelMatrix.offset = 0;
	instancedModelMatrix.format = HE2_DATA_INPUT_VEC4;

	//Model matricies are effectively 4 vec4s - push them as this!
	instancedInputDesc.push_back(instancedModelMatrix);
	instancedModelMatrix.location++;
	instancedModelMatrix.offset += 16;
	instancedInputDesc.push_back(instancedModelMatrix);
	instancedModelMatrix.location++;
	instancedModelMatrix.offset += 16;
	instancedInputDesc.push_back(instancedModelMatrix);
	instancedModelMatrix.location++;
	instancedModelMatrix.offset += 16;
	instancedInputDesc.push_back(instancedModelMatrix);

	setupShader();
}