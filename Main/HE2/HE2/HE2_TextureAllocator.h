#pragma once
#include "HE2_Image.h"

//Pure Virtual that can generate images. One is generated per backend type
class HE2_TextureAllocator
{
public:
	virtual HE2_Image* getImage(std::string filePath, bool gammaCorrected = true) = 0;
	virtual HE2_Image* getImage(HE2_ImageHandle) = 0;
	HE2_TextureAllocator() {}
	virtual ~HE2_TextureAllocator() {}
};

