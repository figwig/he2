#pragma once
#include <vector>
#include "HE2_Component.h"
#include <typeindex>
#include <map>
#include <stdexcept>
#include <cassert>
#include <string>
#include <memory>


class HE2_ComponentOwner
{
protected:
	std::map<std::type_index, std::shared_ptr<HE2_ComponentBase>> components;

public:

	HE2_ComponentOwner()
	{}
	virtual ~HE2_ComponentOwner()
	{}



#pragma region Component Template Methods

	template<typename T,
		typename = std::enable_if_t<std::is_base_of_v<HE2_ComponentBase, T>>>
		void addComponent(T* component)
	{
		components.insert(std::make_pair(std::type_index(typeid(*component)), component));
		component->OnAddComp(this);
		//component->host = this;
	}

	void addComponent(HE2_ComponentBase* component, std::vector<std::type_index> typesFilingUnder)
	{
		for (auto x : typesFilingUnder)
			components.insert(std::make_pair(x, component));
		//component->host = this;
		component->OnAddComp(this);
	}

	template<typename CompType,
		typename = std::enable_if_t<std::is_base_of_v<HE2_ComponentBase, CompType>>>
		inline void removeComponent()
	{
		auto it = components.find(std::type_index(typeid(CompType)));

		if (it == components.end())
			return;

		components.erase(it->first);
	}


	template<typename CompType,
		typename = std::enable_if_t<std::is_base_of_v<HE2_ComponentBase, CompType>>>
		inline CompType* getComponent()
	{
		auto it = components.find(std::type_index(typeid(CompType)));

		if (it == components.end())
		{
			//std::cout<<"Object does not contain this component!" << std::endl;
			return nullptr;
		}
		

		return dynamic_cast<CompType*>(it->second.get());
	}




#pragma endregion


};
