#pragma once
#include "HE2_Vertex.h"
#include <vector>
#include <string>
#include "HE2_ComponentOwner.h"
#include "HE2_RenderBackend.h"
#include <stdexcept>
#include "HE2_Extents.h"

class HE2_Material;

class HE2_Model : public HE2_ComponentOwner
{
public:
	HE2_Model()
	{
		defaultMaterial = HE2_RenderBackend::defaultMaterial;
		if (!defaultMaterial)
		{
			throw std::runtime_error("Model Created too soon! Run Setup API first");
		}
	}

	std::string name;
	std::vector<HE2_Vertex> vertices;
	std::vector<HE2_Model*> childMeshes;
	std::vector<uint32_t> indices;

	bool retainVertexData = false;

	HE2_Material* defaultMaterial = nullptr;

	HE2_Extents extents;

	HE2_Image* previewImage = nullptr;
};

