#pragma once
#include "HE2_ComponentOwner.h"
#include <functional>
#include "HE2_GLM.h"
class HE2_Shader;

class HE2_GPUTask : public HE2_ComponentOwner
{
public:
	//Shaded constructor
	HE2_GPUTask(HE2_Shader * shader, int drawCount);
	//Compute constructor
	HE2_GPUTask(HE2_Shader * shader, glm::vec3 workGroupSize, glm::vec3 tileSize);
	virtual ~HE2_GPUTask();
	bool isCompute = false;
	HE2_Shader* shader = nullptr;
	int drawCount = 0;

	std::function<void()> onComplete;
	//Other compute vars like work size etc....

	glm::vec3 workGroupCompute;
	glm::vec3 tileSizeCompute;
protected:
	
};

