#pragma once
#include <string>
#include <vector>
#include <optional>
#include "imgui.h"

typedef uint16_t HE2_ImageHandle;

class HE2_Image
{
public:
	//Assigned on import, different backends use this differently. Vulkan backend treats this as an ID to an imageview
	HE2_ImageHandle handle = 0;
	ImTextureID imGuiHandle = 0;
};

