#pragma once
#include "HE2.h"
class HE2_TextureViewRect : public HE2_Object
{
public:
	HE2_TextureViewRect(HE2_Image* texToView, HE2_Shader * shader);
	HE2_TextureViewRect(HE2_Image* texToView, HE2_Shader * shader, std::string name);

private:
	void setup(HE2_Image*, HE2_Shader*);
};

