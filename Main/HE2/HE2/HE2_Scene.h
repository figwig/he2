#pragma once
#include "HE2_GLM.h"
#include <vector>
#include <string>
#include "HE2_Camera.h"
#include "HE2_SceneGraph.h"
#include "HE2_Serialize.h"

class HE2_Model;
class HE2_Material;
class HE2_Object;

struct HE2_ObjectInstance
{
	HE2_ObjectInstance(HE2_Model*model, HE2_Material*mat, glm::vec3 pos, glm::vec3 scale)
		:model(model), material(mat), position(pos), scale(scale)
	{

	}
	HE2_ObjectInstance() {}

	HE2_Model* model = nullptr;
	HE2_Material* material = nullptr;

	glm::vec3 position = glm::vec3(0);
	glm::vec3 scale = glm::vec3(1);
	glm::vec3 rotation = glm::vec3(0);

	HE2_ObjectInstance* parent = nullptr;

	HE2_Object* generatedObject = nullptr;

	std::vector<HE2_ComponentBase*> startingComponents;

	std::vector<std::pair<HE2_ComponentBase*, std::vector<std::type_index>>> hintedStartingComponents;

	std::string name;
};


class HE2_SceneCreator
{
public:
	HE2_SceneCreator(std::string name, std::vector<HE2_ObjectInstance*> objects);
	HE2_SceneCreator(std::string name);

	
	std::string name;
	
	std::vector<HE2_ObjectInstance*> objectsInScene;

	HE2_Camera* camera = nullptr;

	static void CreateSceneObjects(HE2_SceneCreator* scene);

};

class HE2_Scene
{
public:
	static HE2_Scene* currentScene;

	std::string fileName;

	HE2_SceneGraph sceneGraph = HE2_SceneGraph();

	template<class Archive>
	void serialize(Archive& archive)
	{
		archive(sceneGraph);
	}

};