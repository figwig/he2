#include "HE2_Material.h"
#include <iostream>

std::map<HE2_Shader*, HE2_Material*> HE2_Material::blankMaterialMap;

HE2_Material* HE2_Material::noMaterial(HE2_Shader* shader)
{
	if (blankMaterialMap.count(shader) == 0)
	{
		blankMaterialMap[shader] = new HE2_NoMaterial(shader);
	}

	return blankMaterialMap[shader];
}

HE2_BlinnPhongMaterial::HE2_BlinnPhongMaterial(HE2_Image* diff, HE2_Image* spec, HE2_Image* norm, HE2_Shader* shader) : diffImage(diff), specImage(spec), normImage(norm)
{
	this->shader = shader;

	materialFeatures = MaterialShaderFeatures(1);

	HE2_Instance::renderBackend->onAddMaterial(this);
}

HE2_BlinnPhongMaterial::HE2_BlinnPhongMaterial( HE2_Shader* shader) 
{
	this->shader = shader;

	HE2_Instance::renderBackend->onAddMaterial(this);
}

std::vector<HE2_ImageHandle> HE2_BlinnPhongMaterial::getImageHandles()
{
	return { diffImage->handle, specImage->handle, normImage->handle };
}

std::vector<HE2_ShaderFeature> HE2_BlinnPhongMaterial::getMaterialShaderFeatures()
{
	return materialFeatures;
}

std::vector<HE2_ShaderFeature>  HE2_BlinnPhongMaterial::MaterialShaderFeatures(int grouping )
{
	HE2_ShaderFeature diffF = {};
	diffF.stage = HE2_SHADER_STAGE_FRAGMENT;
	diffF.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	diffF.source = HE2_SHADER_BUFFER_SOURCE_MATERIAL_BUFFER;
	diffF.binding = 0;
	diffF.grouping = grouping;

	HE2_ShaderFeature specF = {};
	specF.stage = HE2_SHADER_STAGE_FRAGMENT;
	specF.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	specF.source = HE2_SHADER_BUFFER_SOURCE_MATERIAL_BUFFER;
	specF.binding = 1;
	specF.grouping = grouping;

	HE2_ShaderFeature normF = {};
	normF.stage = HE2_SHADER_STAGE_FRAGMENT;
	normF.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	normF.source = HE2_SHADER_BUFFER_SOURCE_MATERIAL_BUFFER;
	normF.binding = 2;
	normF.grouping = grouping;

	return {diffF, specF, normF};
}

HE2_SimpleMaterial::HE2_SimpleMaterial(HE2_Image* colour, HE2_Shader* shader) : colourImage(colour)
{

	materialFeatures = MaterialShaderFeatures(1);

	this->shader = shader;

	HE2_Instance::renderBackend->onAddMaterial(this);
}


std::vector<HE2_ShaderFeature> HE2_SimpleMaterial::getMaterialShaderFeatures()
{
	return materialFeatures;
}


std::vector<HE2_ShaderFeature>  HE2_SimpleMaterial::MaterialShaderFeatures(int grouping)
{
	HE2_ShaderFeature diffF = {};
	diffF.stage = HE2_SHADER_STAGE_FRAGMENT;
	diffF.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	diffF.source = HE2_SHADER_BUFFER_SOURCE_MATERIAL_BUFFER;
	diffF.binding = 0;
	diffF.grouping = grouping;

	return { diffF };
}
