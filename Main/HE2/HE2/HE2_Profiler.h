#pragma once
#include <array>
#include <chrono>
#include "imgui.h"




class HE2_Profiler
{
public:
	enum Stage
	{
		GlfwPollEvents,
		Update, 
		InputActions,
		BehaviourUpdates,
		Physics,
		Render,
		RebuildObjectCommandBuffers,
		RebuildUICommandBuffers,
		RenderObjects,
		WaitForFences,
		UpdateBuffers,
		WaitForQueueIdle,
		Present,
		GPUTaskManagement,
		AquireImage,


		StageCount
	};


	struct Scope
	{
		ImU8 level;
		std::chrono::system_clock::time_point start;
		std::chrono::system_clock::time_point end;
		bool finalized = false;
	};

	struct Entry
	{
		std::chrono::system_clock::time_point frameStart;
		std::chrono::system_clock::time_point frameEnd;
		std::array<Scope, StageCount> stages;
	};

	static void Frame();

	static void Begin(Stage stage);
	static void End(Stage stage);

	static ImU8 GetCurrentEntryIndex();

	static const ImU8 bufferSize = 100;
	static std::array<Entry, bufferSize> entries;
	//static const std::array<const char*, StageCount> stageNames;

	static void GetSomeValues(float* startTimestamp, float* endTimestamp, ImU8* level, const char** caption, const void* data, int idx);
private:
	static ImU8 currentEntry;
	static ImU8 currentLevel;
};

