#include "HE2_Input.h"
#include "imgui.h"
#include "HE2_InputListener.h"

HE2_InputState HE2_Input::currentState;
bool HE2_Input::mouseMoved = false;
double HE2_Input::mouseX = 0.0;
double HE2_Input::mouseY = 0.0;
std::vector<HE2_InputListener*> HE2_Input::listeners;
HE2_MouseAction HE2_Input::mouseAction;
bool HE2_Input::mouseClicked, HE2_Input::scrolled;
double HE2_Input::scrollX, HE2_Input::scrollY;
HE2_InputState HE2_Input::keystate = 0;
std::map<int, HE2_InputFlags> HE2_Input::keystateMap;

void HE2_Input::addListener(HE2_InputListener* x)
{
	listeners.push_back(x);
}

void HE2_Input::removeListener(HE2_InputListener* x)
{
	int i = 0;
	for (auto z : listeners)
	{
		if (x == z)
		{
			listeners.erase(listeners.begin() + i);
			return;
		}
		i++;
	}
}


HE2_InputState HE2_Input::getInputState()
{
	return currentState;
}


void HE2_Input::informListeners()
{
	if (mouseMoved)
	{
		for (HE2_InputListener* inputListener : listeners)
		{
			inputListener->OnMouseMove(mouseX, mouseY);
		}
		mouseMoved = false;
	}

	if (mouseClicked)
	{
		for (HE2_InputListener* inputListener : listeners)
		{
			inputListener->OnMouseAction(mouseAction);
		}
		mouseClicked = false;
	}
	if (scrolled)
	{
		for (HE2_InputListener* inputListener : listeners)
		{
			inputListener->OnScroll(scrollX, scrollY);
		}
		scrolled = false;
	}
}

void HE2_Input::KeyAction(int key, int action, int mods)
{
	keystate |= keystateMap[key];
}

void HE2_Input::MouseMove(double x, double y)
{
	mouseMoved = true;
	mouseX = x;
	mouseY = y;
}

void HE2_Input::MouseButtonAction(int button, int action)
{
	ImGuiIO io = ImGui::GetIO();
	if (io.WantCaptureMouse) return;

	switch (button)
	{
	case GLFW_MOUSE_BUTTON_LEFT:
		mouseAction.button = HE2_MouseButton::HE2_LEFT_MOUSE;
		break;
	case GLFW_MOUSE_BUTTON_RIGHT:
		mouseAction.button = HE2_MouseButton::HE2_RIGHT_MOUSE;
		break;
	case GLFW_MOUSE_BUTTON_MIDDLE:
		mouseAction.button = HE2_MouseButton::HE2_SCROLLWHEEL_PRESS;
		break;
	}
	switch (action)
	{
	case GLFW_PRESS:
		mouseAction.action = HE2_ButtonAction::HE2_BUTTON_DOWN;
		break;
	case GLFW_RELEASE:
		mouseAction.action = HE2_ButtonAction::HE2_BUTTON_UP;
		break;

	}

	mouseClicked = true;
}

void HE2_Input::ScrollEvent(double x, double y)
{
	scrollX = x;
	scrollY = y;
	scrolled = true;
}

void HE2_Input::initMap()
{
	//Fill this in please
	keystateMap[GLFW_KEY_A] = HE2_InputFlags::HE2_A;
	keystateMap[GLFW_KEY_S] = HE2_InputFlags::HE2_S;
	keystateMap[GLFW_KEY_UP] = HE2_InputFlags::HE2_UP;
	keystateMap[GLFW_KEY_DOWN] = HE2_InputFlags::HE2_DOWN;
}
