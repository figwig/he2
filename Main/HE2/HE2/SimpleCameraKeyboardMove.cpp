#include "SimpleCameraKeyboardMove.h"
#include "HE2_GLM.h"


void SimpleCameraKeyboardMove::update()
{
	object->setRot( glm::vec3(mouseY / 500.0f, -mouseX/500.0f, 0));

	glm::mat3 rotMat = object->getRotMat();

	float s = Time::deltaTime * speed;

	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_A))
	{
		cameraTargetPos += rotMat * glm::vec3(1,0,0) * s;
	}
	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_D))
	{
		cameraTargetPos -= rotMat * glm::vec3(1, 0, 0) * s;
	}

	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_W))
	{
		cameraTargetPos += rotMat * glm::vec3(0,0,1)  * s;
	}
	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_S))
	{
		cameraTargetPos -= rotMat * glm::vec3(0,0,1) * s;
	}

	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_R))
	{
		cameraTargetPos += glm::vec3(0, 1, 0) * s * 10.0f;
	}
	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_F))
	{
		cameraTargetPos -= glm::vec3(0,1,0) * s * 10.0f;
	}


	if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_SPACE) && ticker < 0.0f)
	{
		ticker = 1.0f;
		moveLight = !moveLight;
	}
	else
	{
		ticker -= Time::deltaTime;
	}
	
	cameraTargetLookAt = glm::normalize(rotMat * glm::vec3(0, 0, -1));


	glm::vec3 diff = HE2_Camera::main->getPos() - cameraTargetPos;
	glm::vec3 actPos = HE2_Camera::main->getPos() - (percentTravel * Time::deltaTime * diff);

	HE2_Camera::main->setPos(actPos);

	HE2_Camera::main->setTarget(actPos - cameraTargetLookAt);

	if(moveLight)
		light->setPos(actPos);


	//if (movingToObject)
	//{
	//	glm::vec3 vec = glm::normalize(getPos() - movingToPosition);

	//	setPos(movingFromPosition + 10.0f * vec * Time::deltaTime);

	//	if (glm::length(getPos() - movingToPosition) < 10)
	//	{
	//		movingToObject = false;
	//	}
	//}
}

void SimpleCameraKeyboardMove::OnMouseMove(double x, double y)
{

	if (glfwGetMouseButton(HE2_WindowHandler::window, 1))
	{
		mouseX += x - lastX;
		mouseY += y - lastY;
	}

	lastX = x;
	lastY = y;
}
