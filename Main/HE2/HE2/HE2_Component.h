#pragma once
#include <typeindex>
class HE2_ComponentOwner;

class HE2_ComponentBase
{
public:
	HE2_ComponentBase()
	{

	}
	virtual ~HE2_ComponentBase() {}
	
	virtual void update() {}

	virtual void OnAddComp(HE2_ComponentOwner*) {}

	template<class Archive>
	void serialize(Archive& archive)
	{
		//Nothing that needs to be serialized
	}
protected:
	friend class HE2_ComponentOwner;

};


template<typename T,
	typename = std::enable_if_t<std::is_base_of_v<HE2_ComponentOwner, T>>>
	class HE2_Component : public HE2_ComponentBase
{
public:
	T* host = nullptr;

	void OnAddComp(HE2_ComponentOwner* host)
	{
		host = dynamic_cast<T*>(host);
	}
protected:
	friend class HE2_ComponentOwner;

};
