#include "HE2_SceneGraph.h"


void HE2_SceneGraph::removeFromRootNodes(HE2_Object* obj)
{
	for (int i = 0; i < rootNodes.size(); i++)
	{
		if (rootNodes[i] == obj)
		{
			rootNodes.erase(rootNodes.begin() + i);
			return;
		}
	}
}

void HE2_SceneGraph::addToRootNodes(HE2_Object* obj)
{
	rootNodes.push_back(obj);
	allObjects.insert(std::shared_ptr<HE2_Object>(obj));
	//no Children are added yet - they should be
}

std::vector<HE2_Object*> HE2_SceneGraph::getRootNodes()
{
	return rootNodes;
}

HE2_Object* HE2_SceneGraph::findObjectWithName(std::string s)
{
	for(auto o : rootNodes)
		if (o->name == s)
		{
			return o;
		}

	for (auto o : rootNodes)
	{
		HE2_Object* c = findChildWithName(o, s);
			if (c != nullptr)
				return c;
	}

	return nullptr;
}


HE2_Object* HE2_SceneGraph::findChildWithName(HE2_Object * o,std::string s)
{
	for (auto c : o->getChildren())

	{
		if (c->name == s)
		{
			return c;
		}
	}

	for (auto c : o->getChildren())
	{
		HE2_Object* o = findChildWithName(c, s);
		if (o != nullptr)
			return o;
	}
}