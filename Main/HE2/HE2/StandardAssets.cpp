#include "StandardAssets.h"
#include "HE2_TextureManager.h"
HE2_Model* StandardAssets::skybox = nullptr;
HE2_Model* StandardAssets::quadModel = nullptr;

void StandardAssets::importStandardAssets()
{
	//HE2_TextureManager::getImage("textures/chalet.jpg");
	//HE2_TextureManager::getImage("textures/brickwall.jpg");
	//HE2_TextureManager::getImage("textures/brickwall_normal.jpg");
	//HE2_TextureManager::getImage("textures/brickwall_s.jpg");

	HE2_ImportSettings importSettings = HE2_ImportSettings();
	importSettings.reverseWindingOrder = true;
	//importSettings.reverseNormals = true;
	skybox = HE2_MeshManager::importModel("models/cubeSimple.obj", importSettings);

	quadModel = new HE2_Model();
	quadModel->vertices = {
		{HE2_Vertex(glm::vec3(-1.0, -1.0, 0.0))},
		{HE2_Vertex(glm::vec3(1.0, 1.0, 0.0))},
		{HE2_Vertex(glm::vec3(-1.0, 1.0, 0.0))},
		{HE2_Vertex(glm::vec3(-1.0, -1.0, 0.0))},
		{HE2_Vertex(glm::vec3(1.0, -1.0, 0.0))},
		{HE2_Vertex(glm::vec3(1.0, 1.0, 0.0))}
	};
	quadModel->indices = { 0,1,2,3,4,5 };

	HE2_Instance::renderBackend->onAddModel(quadModel);
}