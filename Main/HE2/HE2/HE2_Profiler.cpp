#include "HE2_Profiler.h"

static const std::array<const char*, HE2_Profiler::StageCount> stageNames = {
"GLFWPollEvents",
"Update",
"Input Actions",
"Behaviour Updates",
"Physics",
"Render",
"RebuildObjectCommandBuffers",
"RebuildUICommandBuffers",
"RenderObjects",
"WaitForFences",
"UpdateBuffers",
"WaitForQueueIdle",
"Present",
"GPUTaskManagement",
"Aquire Image"
};

ImU8 HE2_Profiler::currentEntry = bufferSize - 1;
ImU8 HE2_Profiler::currentLevel = 0;
std::array<HE2_Profiler::Entry, HE2_Profiler::bufferSize> HE2_Profiler::entries;

void HE2_Profiler::Frame()
{
	//New frame
	//Grab the last frame's entry
	auto& prevEntry = entries[currentEntry];

	//Up the currentEntry
	currentEntry = (currentEntry + 1) % bufferSize;

	//Record the end of the last frame
	prevEntry.frameEnd = entries[currentEntry].frameStart = std::chrono::system_clock::now();
}

void HE2_Profiler::Begin(Stage stage)
{
	//Make sure we aren't too deep
	assert(currentLevel < 255);
	
	//Grab an entry from the current entry's array of scopes ( a scope is entry within an entry)
	auto& entry = entries[currentEntry].stages[stage];

	//Assign a level
	entry.level = currentLevel;

	//Up the level - we've started a task
	currentLevel++;
	//Record the time
	entry.start = std::chrono::system_clock::now();
	entry.finalized = false;
}


void HE2_Profiler::End(Stage stage)
{
	//Can't end when there is nothing to end
	assert(currentLevel > 0);
	//Grab current entry
	auto& entry = entries[currentEntry].stages[stage];
	assert(!entry.finalized);
	currentLevel--;
	assert(entry.level == currentLevel);
	entry.end = std::chrono::system_clock::now();
	entry.finalized = true;
}

ImU8 HE2_Profiler::GetCurrentEntryIndex()
{
	return (currentEntry + bufferSize - 1) % bufferSize;
}
void HE2_Profiler::GetSomeValues(float* startTimestamp, float* endTimestamp, ImU8* level, const char** caption, const void* data, int idx)
{
	auto entry = reinterpret_cast<const HE2_Profiler::Entry*>(data);
	auto& stage = entry->stages[idx];
	if (startTimestamp)
	{
		std::chrono::duration<float, std::milli> fltStart = stage.start - entry->frameStart;
		*startTimestamp = fltStart.count();
	}
	if (endTimestamp)
	{
		*endTimestamp = stage.end.time_since_epoch().count() / 1000000.0f;

		std::chrono::duration<float, std::milli> fltEnd = stage.end - entry->frameStart;
		*endTimestamp = fltEnd.count();
	}
	if (level)
	{
		*level = stage.level;
	}
	if (caption)
	{
		*caption = stageNames[idx];
	}
}