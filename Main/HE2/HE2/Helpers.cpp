#include "Helpers.h"
#include "Helpers.h"
#include "HE2_Object.h"

glm::vec2 makeVec3xzVec2(glm::vec3 x)
{
	returny.x = x.x;
	returny.y = x.z;
	return returny;
}

glm::vec2 makeVec3xyVec2(glm::vec3 x)
{
	returny.x = x.x;
	returny.y = x.y;
	return returny;
}

float heightAboveTriangleGivenXZ(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos) {
	float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
	float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
	float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
	float l3 = 1.0f - l1 - l2;
	return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}
bool eqVec4(glm::vec4 a, glm::vec4 b)
{
	return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w;
}


glm::vec3 getAverageOf3PointsXZ(glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
	return (a + b + c) / 3.0f;
}

float distSquared(glm::vec3 a, glm::vec3 b)
{
	float distSq = pow(a.x - b.x, 2) + pow(a.y - b.y, 2) + pow(a.z - b.z, 2);
	return distSq;
}

glm::vec3 makeVec3Vec4IgnoreW(glm::vec4 x)
{
	returny3.x = x.x;
	returny3.y = x.y;
	returny3.z = x.z;
	return returny3;
}

bool isPointInTriangle(glm::vec2 pt, glm::vec2 v1, glm::vec2 v2, glm::vec2 v3)
{
	float d1, d2, d3;
	bool has_neg, has_pos;

	d1 = sign(pt, v1, v2);
	d2 = sign(pt, v2, v3);
	d3 = sign(pt, v3, v1);

	has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
	has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

	return !(has_neg && has_pos);
}

float sign(glm::vec2 p1, glm::vec2 p2, glm::vec2 p3)
{
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

void gameobjectLookAt(HE2_Object* host, HE2_Object* target, float maxTurn)
{
	//glm::vec3 diff = target->getPos() - host->getPos();

	//float targetRot = atan(diff.x / diff.z);

	//float currRot = host->getRot().y;

	//host->setRot(glm::vec3(0, -targetRot, 0));
}

//btVector3 convert(glm::vec3 x)
//{
//	return btVector3(x.x, x.y, x.z);
//}
//
//glm::vec3 convert(btVector3 x)
//{
//	return glm::vec3(x.x(), x.y(), x.z());
//}

std::string returnNumber(std::string x)
{
	char b;
	size_t indexOfFirstNumber = x.length();
	for (int count = 0; count < x.length(); count++)
	{
		b = x[count];
		if (isdigit(b))
		{
			//it is a number
			indexOfFirstNumber = count;
			break;
		}
	}
	return x.substr(indexOfFirstNumber, x.length());
}
glm::mat4 convert(aiMatrix4x4 x)
{
	return glm::transpose(glm::mat4(
		glm::vec4(x.a1, x.a2, x.a3, x.a4),
		glm::vec4(x.b1, x.b2, x.b3, x.b4),
		glm::vec4(x.c1, x.c2, x.c3, x.c4),
		glm::vec4(x.d1, x.d2, x.d3, x.d3)));
}

std::string convert(aiString x)
{
	std::string string;
	string = x.C_Str();
	return string;
}

std::string stringFromVec3(glm::vec3 x)
{
	return (std::to_string(x.x) + ", " + std::to_string(x.y) + ", " + std::to_string(x.z));
}
std::string stringFromVec2(glm::vec2 x)
{
	return (std::to_string(x.x) + ", " + std::to_string(x.y));
}