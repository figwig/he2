#include "HE2_Time.h"
#include <glfw/glfw3.h>
#include "HE2_WindowHandler.h"
#include <string>
#include <iostream>
#include <sstream>
#include <chrono>

float Time::deltaTime = 0.0f;
float Time::time = 0.0f;
float Time::readableDeltaTime = 0.0f;
float Time::ticker = 0.0f;
int Time::frameID = 0;
std::chrono::time_point<std::chrono::system_clock> Time::start;

void Time::Tick()
{
	float newTime = (float)glfwGetTime();

	deltaTime = newTime - time;

	time = newTime;
	
	frameID++;

	ticker += deltaTime;
	if (ticker > 0.5f)
	{
		ticker = 0.0f;
		readableDeltaTime = deltaTime;
	}
}