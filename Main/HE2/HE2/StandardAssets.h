#pragma once
#include "HE2.h"


class StandardAssets
{
public:
	static HE2_Model* skybox;
	static HE2_Model* quadModel;
	
private:
	static void importStandardAssets();

	friend class HE2_Instance;
};

