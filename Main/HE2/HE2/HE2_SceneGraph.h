#pragma once
#include <vector>
#include "HE2_Object.h"
#include "HE2_Serialize.h"

//This effectively acts a big daddy node, with all base-level nodes 
class HE2_SceneGraph
{
public:
	//Used when an object is no longer a rootNode
	void removeFromRootNodes(HE2_Object* obj);
	void addToRootNodes(HE2_Object* obj);

	std::vector<HE2_Object*> getRootNodes();
	HE2_Object* findObjectWithName(std::string);

	template<class Archive>
	void serialize(Archive& archive)
	{
		archive(allObjects);
	}

private:
	std::vector<HE2_Object*> rootNodes;
	std::set<std::shared_ptr<HE2_Object>> allObjects;
	HE2_Object* findChildWithName(HE2_Object*,std::string);
};

