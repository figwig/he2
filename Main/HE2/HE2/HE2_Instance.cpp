#include "HE2_Instance.h"
#include "HE2_Time.h"
#include "HE2_TextureManager.h"
#include "HE2_BehaviourManager.h"
#include "HE2_Profiler.h"
#include "HE2_Camera.h"
#include "StandardAssets.h"
#include "HE2_Scene.h"

HE2_RenderBackend* HE2_Instance::renderBackend = nullptr;
HE2_InstanceSettings HE2_Instance::instanceSettings;

HE2_Instance::HE2_Instance(HE2_RenderBackend* renderPipeline, HE2_InstanceSettings settings) 
{
	Time::start = std::chrono::system_clock::now();

	instanceSettings = settings;
	renderBackend = renderPipeline;
	HE2_TextureManager::allocator = renderBackend->getAllocator();

	//Create a scene
	HE2_Scene::currentScene = new HE2_Scene();

}

HE2_Instance::~HE2_Instance()
{
	renderBackend->cleanup();

	HE2_WindowHandler::DestroyWindow();
}


void HE2_Instance::Init(bool createWindow)
{
	if(createWindow)
		HE2_WindowHandler::CreateWindow();

	renderBackend->setupAPI();

#ifndef NO_STANDARD_ASSETS
	StandardAssets::importStandardAssets();
#endif 

	ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_DockingEnable;
}

void HE2_Instance::RunGame()
{



	while (!glfwWindowShouldClose(HE2_WindowHandler::window))
	{

		HE2_Profiler::Frame();
		Time::Tick();

		//Poll events
		HE2_Profiler::Begin(HE2_Profiler::Stage::GlfwPollEvents);
		glfwPollEvents();
		HE2_Profiler::End(HE2_Profiler::Stage::GlfwPollEvents);

		//Update all scripts/motion etc
		HE2_Profiler::Begin(HE2_Profiler::Stage::Update);
		HE2_Profiler::Begin(HE2_Profiler::Stage::InputActions);

		HE2_Input::informListeners();

		HE2_Profiler::End(HE2_Profiler::Stage::InputActions);
		HE2_Profiler::Begin(HE2_Profiler::Stage::BehaviourUpdates);

		HE2_BehaviourManager::updateBehaviours();

		HE2_Profiler::End(HE2_Profiler::Stage::BehaviourUpdates);
		HE2_Profiler::End(HE2_Profiler::Stage::Update);
		//Render


		HE2_Profiler::Begin(HE2_Profiler::Stage::Render);
		renderBackend->render();
		HE2_Profiler::End(HE2_Profiler::Stage::Render);

		if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_ESCAPE))
		{
			break;
		}
	}
}

void HE2_Instance::checkReady()
{
	if (HE2_Camera::main == nullptr)
		throw std::runtime_error("Error: At least one camera must be in the scene");
}
