#include "HE2_Behaviour.h"
#include "HE2_BehaviourManager.h"

HE2_ObjectBehaviour::HE2_ObjectBehaviour(HE2_Object
* host, bool threadable) : object(host), HE2_Behaviour(threadable)
{

}

HE2_ObjectBehaviour::~HE2_ObjectBehaviour()
{


}

HE2_Behaviour::HE2_Behaviour(bool threadable)
{
	this->threadable = threadable;
	HE2_BehaviourManager::addBehaviour((this));
}

HE2_Behaviour::~HE2_Behaviour()
{
	HE2_BehaviourManager::removeBehaviour((this));
}