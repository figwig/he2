#pragma once
#include <glfw/glfw3.h>
#include "HE2_Input.h"

class HE2_WindowHandler
{
public:

	static const int WIDTH = 1800;
	static const int HEIGHT = 1000;

	static GLFWwindow * window;
private:
	friend class HE2_Instance;

	static void CreateWindow();

	static void DestroyWindow();
};

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);