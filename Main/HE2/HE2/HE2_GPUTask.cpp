#include "HE2_GPUTask.h"

HE2_GPUTask::HE2_GPUTask(HE2_Shader* shader, int drawCount) : shader(shader), drawCount(drawCount)
{
	isCompute = false;
}

HE2_GPUTask::HE2_GPUTask(HE2_Shader* shader, glm::vec3 workGroup, glm::vec3 tileSize): shader(shader), workGroupCompute(workGroup), tileSizeCompute(tileSize)
{
	isCompute = true;
}

HE2_GPUTask::~HE2_GPUTask()
{
}