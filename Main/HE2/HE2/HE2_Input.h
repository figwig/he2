#pragma once
#include <glfw/glfw3.h>
#include <cstdint>
#include <vector>
#include <map>

class HE2_InputListener;

typedef uint64_t HE2_InputState;
typedef uint16_t HE2_InputAction;
typedef uint8_t HE2_InputButton;



enum HE2_InputFlags :uint64_t
{
	HE2_UP =	1,
	HE2_DOWN =	2,
	HE2_LEFT =  4,
	HE2_RIGHT = 8,
	HE2_R =		16,
	HE2_F =		32,
	HE2_SPACE = 64,
	HE2_NUM1 =  128,
	HE2_NUM2 =  256,
	HE2_NUM3 =  512,
	HE2_NUM4 =  1024,
	HE2_ESC =   2048,
	HE2_X =		4096,
	HE2_SHIFT = 0b100000000000000,
	HE2_CTRL =  0b1000000000000000,
	HE2_Q =		0b10000000000000000,
	HE2_E =		0b100000000000000000,
	HE2_Z =		0b1000000000000000000,
	HE2_NUM5 =  0b10000000000000000000,
	HE2_F3 =    0b100000000000000000000,
	HE2_F4 =    0b1000000000000000000000,
	HE2_F5 =    0b10000000000000000000000,
	HE2_F6 =    0b100000000000000000000000,
	HE2_F7 =    0b1000000000000000000000000,
	HE2_F8 =    0b10000000000000000000000000,
	HE2_F9 =    0b100000000000000000000000000,
	HE2_F10 =   0b1000000000000000000000000000,
	HE2_F11 =   0b10000000000000000000000000000,
	HE2_F12 =   0b100000000000000000000000000000,
	HE2_A =     0b1000000000000000000000000000000,
	HE2_D =     0b10000000000000000000000000000000,
	HE2_S =     0b100000000000000000000000000000000,
	HE2_W =     0b1000000000000000000000000000000000
};

enum class HE2_ButtonAction
{
	HE2_BUTTON_DOWN,
	HE2_BUTTON_UP
};

enum class HE2_MouseButton
{
	HE2_NONE,
	HE2_LEFT_MOUSE,
	HE2_RIGHT_MOUSE,
	HE2_SCROLLWHEEL_PRESS
};

struct HE2_MouseAction
{
	HE2_ButtonAction action;
	HE2_MouseButton button;
};

class HE2_Input 
{
public:
	static HE2_InputState getInputState();
private:
	friend class HE2_Instance;
	friend class HE2_InputListener;
	friend class HE2_WindowHandler;
	friend class HE2_InputPassThrough;

	static void addListener(HE2_InputListener*);

	static void removeListener(HE2_InputListener*);

	static void informListeners();

	static void KeyAction(int key, int action, int mods);

	static void MouseMove(double x, double y);

	static void MouseButtonAction(int button, int action);

	static void ScrollEvent(double, double);

	static HE2_InputState currentState;

	static std::vector<HE2_InputListener*> listeners;

	static bool mouseMoved;
	static bool mouseClicked;
	static bool scrolled;
	
	static HE2_MouseAction mouseAction;
	static double mouseX;
	static double mouseY;

	static double scrollX, scrollY;

	static std::map<int, HE2_InputFlags> keystateMap;

	static void initMap();

	static 	HE2_InputState keystate;
};

class HE2_InputPassThrough {
public:
	static void KeyAction(int key, int action,int mods)
	{
		HE2_Input::KeyAction(key, action, mods);
	}

	static void MouseMove(double x, double y)
	{
		HE2_Input::MouseMove(x, y);
	}

	static void MouseButtonAction(int button, int action)
	{
		HE2_Input::MouseButtonAction(button, action);
	}

	static void ScrollEvent(double x, double y)
	{
		HE2_Input::ScrollEvent(x, y);
	}
};