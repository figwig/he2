#include "HE2_ConsoleCommand.h"
#include "HE2_Debug.h"
HE2_ConsoleCommand::HE2_ConsoleCommand(std::string cmd) : cmd(cmd)
{
	HE2_Debug::allCommands.insert(std::make_pair(cmd, this));
}