#include "HE2_MeshManager.h"
#include "HE2_Model.h"
#include "HE2_Instance.h"
#include "Helpers.h"
#include "HE2_Extents.h"
#include "HE2_Vertex.h"
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include <map>
#include <unordered_map>
#include "HE2_Material.h"
#include "HE2_TextureManager.h"
#include "HE2_Debug.h"

std::string HE2_MeshManager::currentFileName;
HE2_Model* HE2_MeshManager::parentModel = nullptr;

std::map<ModelReference, HE2_Model*> HE2_MeshManager::models;

HE2_ImportSettings HE2_MeshManager::currentImportSettings;


bool HE2_ImportSettings::operator==(const HE2_ImportSettings& other)const
{
	return (reverseWindingOrder == other.reverseWindingOrder) && (centreMesh == other.centreMesh) && (reverseNormals == other.reverseNormals);
}

bool HE2_ImportSettings::operator < (const HE2_ImportSettings& other) const
{
	return reverseWindingOrder + 2 * reverseNormals + 4 * centreMesh < other.reverseWindingOrder + 2 * other.reverseNormals + 4  * other.centreMesh;
}
bool ModelReference::operator == (const ModelReference& other) const
{
	return filepath == other.filepath && importSettings == other.importSettings;
}
bool ModelReference::operator< (const ModelReference& other) const
{
	return std::tie(filepath, importSettings) < std::tie(other.filepath, other.importSettings);
}

HE2_Model* HE2_MeshManager::importModel(std::string filename, HE2_ImportSettings importSettings)
{	
	ModelReference mr = { filename, importSettings };

	bool exists = models.count(mr) > 0;

	std::string settings = "";
	
	if (importSettings.centreMesh)
		settings += " CentreMesh";
	if (importSettings.reverseNormals)
		settings += " Reverse Normals";
	if (importSettings.reverseWindingOrder)
		settings += " Reverse Winding";

	if (exists)
	{
		DebugLog("Returned an Exising model under filename " + filename+ "." + "with settings " + settings);
		return models[mr];
	}

	currentImportSettings = importSettings;

	parentModel = new HE2_Model();
	currentFileName = filename;

	// Create an instance of the Importer class
	Assimp::Importer importer;
	
	unsigned int flags =
		aiProcess_CalcTangentSpace | aiProcess_GenNormals | aiProcess_Triangulate | aiProcess_FixInfacingNormals;

	//if (importSettings.reverseWindingOrder)
	//	flags |= aiProcess_FlipWindingOrder;

	const aiScene* scene = importer.ReadFile(filename, flags);//aiProcess_PreTransformVertices  could be useful for weird meshes

	// If the import failed, report it
	if (!scene)
	{
		throw std::runtime_error( importer.GetErrorString());
		return nullptr;
	}

	lookThroughNode(scene->mRootNode, convert(scene->mRootNode->mTransformation), scene,0, parentModel);

	HE2_Instance::renderBackend->onAddModel(parentModel);

	models.insert(std::make_pair(mr, parentModel));

	if (parentModel->defaultMaterial == nullptr && parentModel->childMeshes.size() != 0 )
		parentModel->defaultMaterial = parentModel->childMeshes[0]->defaultMaterial;


	importer.FreeScene();

	DebugLog("Imported a model under filename " + filename + "." + "with settings " + settings);
	return parentModel;
}

void HE2_MeshManager::lookThroughNode(aiNode* node, glm::mat4 carryThroughTrans, const aiScene* scene, int depth, HE2_Model* parent)
{
	//NodeMesh = parent unless this node has a mesh
	HE2_Model* nodeMesh = parent;
	glm::mat4 x = convert(node->mTransformation) * carryThroughTrans;

	size_t childIndex = 0;

	if (node->mNumMeshes > 0)
	{
		dealWithMesh(scene->mMeshes[node->mMeshes[0]], scene, x, nodeMesh);
		childIndex++;
		//Parent won't recieve more than one mesh, make them all children
		for (int i = 1; i < node->mNumMeshes; i++)
		{
			HE2_Model* childModel = new HE2_Model();
			dealWithMesh(scene->mMeshes[node->mMeshes[i]], scene, x, childModel);
			nodeMesh->childMeshes.push_back(childModel);
		}
	}
	else if(node->mNumChildren > 0)
	{
		//No meshes, make child 1 this mesh
		for (size_t i = 0; i < node->mNumChildren; i++)
		{
			childIndex++;
			if (node->mChildren[i]->mNumMeshes > 0 || node->mChildren[i]->mNumChildren > 0)
			{
				lookThroughNode(node->mChildren[i], x, scene, depth + 1, nodeMesh);

				break;
			}
		}
	}

	for (childIndex; childIndex < node->mNumChildren; childIndex++)
	{
		//Parent is last node to contain meshes
		HE2_Model* childModel = new HE2_Model();
		lookThroughNode(node->mChildren[childIndex], x, scene, depth + 1, childModel);
		nodeMesh->childMeshes.push_back(childModel);
	}
}

void  HE2_MeshManager::dealWithMesh(aiMesh* mesh, const aiScene* scene, glm::mat4 trans,HE2_Model * meshToAddTo)
{
	meshToAddTo->name = convert(mesh->mName);

	glm::vec3 minPoint = glm::vec3(10000000);
	glm::vec3 maxPoint = glm::vec3(-10000000);
	glm::vec3 averageTot = glm::vec3(0);

	std::unordered_map<HE2_Vertex, uint32_t> uniqueVertices = { };


	for (size_t vCount = 0; vCount < mesh->mNumVertices; vCount++)
	{
		HE2_Vertex vertex = {};

		glm::vec3 v = trans * glm::vec4(mesh->mVertices[vCount].x, mesh->mVertices[vCount].y, mesh->mVertices[vCount].z, 0.0);

		averageTot += v;

		vertex.pos = v;

		if (v.x > maxPoint.x) maxPoint.x = v.x;
		if (v.x < minPoint.x) minPoint.x = v.x;
		if (v.y > maxPoint.y) maxPoint.y = v.y;
		if (v.y < minPoint.y) minPoint.y = v.y;
		if (v.z > maxPoint.z) maxPoint.z = v.z;
		if (v.z < minPoint.z) minPoint.z = v.z;


		vertex.normal = glm::vec3(mesh->mNormals[vCount].x, mesh->mNormals[vCount].y, mesh->mNormals[vCount].z);

		if (currentImportSettings.reverseNormals)
			vertex.normal = -vertex.normal;

		if (mesh->mTextureCoords[0])
		{
			vertex.texCoord = glm::vec2(mesh->mTextureCoords[0][vCount].x, mesh->mTextureCoords[0][vCount].y);


			vertex.tangent = glm::vec3(mesh->mTangents[vCount].x, mesh->mTangents[vCount].y, mesh->mTangents[vCount].z);

			vertex.bitangent = glm::vec3(mesh->mBitangents[vCount].x, mesh->mBitangents[vCount].y, mesh->mBitangents[vCount].z);
		}

		if (uniqueVertices.count(vertex) == 0)
		{
			uniqueVertices[vertex] = static_cast<uint32_t>(meshToAddTo->vertices.size());
			meshToAddTo->vertices.push_back(vertex);
		}

		meshToAddTo->indices.push_back(uniqueVertices[vertex]);
	}
	

	if (currentImportSettings.reverseWindingOrder)
	{
		//Reverse the order of each group of 3 vertices
		std::vector<uint32_t> newIndices;
		for (int i = 0; i < meshToAddTo->indices.size() / 3; i++)
		{
			size_t indexBy3 = i * 3;
			newIndices.push_back(meshToAddTo->indices[indexBy3 + 2]);
			newIndices.push_back(meshToAddTo->indices[indexBy3 + 1]);
			newIndices.push_back(meshToAddTo->indices[indexBy3 + 0]);

			for (int i = 0; i < meshToAddTo->vertices.size(); i++)
			{
				//meshToAddTo->vertices[i].normal = -meshToAddTo->vertices[i].normal;
				//meshToAddTo->vertices[i].tangent = -meshToAddTo->vertices[i].tangent;
				//meshToAddTo->vertices[i].bitangent = -meshToAddTo->vertices[i].bitangent;
			}
		}

		meshToAddTo->indices = newIndices;

	}

	averageTot /= mesh->mNumVertices;

	glm::mat4 neu = glm::mat4(1);

	
	if (currentImportSettings.centreMesh) 
	{
		//Centre the mesh on (0,0,0)
		for (int i = 0; i < meshToAddTo->vertices.size(); i++)
		{
			meshToAddTo->vertices[i].pos -= averageTot;
		}
	}

	HE2_Extents e = HE2_Extents(minPoint, maxPoint);
	e.centre = averageTot;

	meshToAddTo->extents = e;

	aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];

	aiString diffuse;
	aiString normal;
	aiString spec;

	mat->GetTexture(aiTextureType_DIFFUSE, 0, &diffuse);
	mat->GetTexture(aiTextureType_SPECULAR, 0, &spec);
	mat->GetTexture(aiTextureType_HEIGHT, 0, &normal);

	std::string diffStr = convert(diffuse);
	std::string normStr = convert(normal);
	std::string specStr = convert(spec);

	if (diffStr == "") return;


	HE2_Material* material = nullptr;
	
	if (normStr == "" || specStr == "")
	{
		//Make a simple material
		material = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage(diffStr), HE2_TextureManager::getImage(diffStr), HE2_TextureManager::getImage(diffStr), HE2_Instance::renderBackend->deferredShader);
	}
	else
	{
		//Make a blinn-phong material
		material = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage(diffStr), HE2_TextureManager::getImage(specStr), HE2_TextureManager::getImage(normStr,true), HE2_Instance::renderBackend->deferredShader);
	}

	meshToAddTo->defaultMaterial = material;
	//TextureExaminer::singleton()->setDirectory("");
	//Texture* diff = TextureExaminer::singleton()->getTexture(diffuse.C_Str());
	//Texture* specular = TextureExaminer::singleton()->getTexture(spec.C_Str());
	//Texture* norm = TextureExaminer::singleton()->getNormalTexture(normal.C_Str());

	//Material* material = MaterialHandler::singleton()->makeMaterial(diff, specular, norm);

	//md->defaultMaterial = material;
}


//Parent Node
//Look through, if mesh add to parent
//For each child, add as a child to the current parent