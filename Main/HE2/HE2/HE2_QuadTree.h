#pragma once
#include "HE2_GLM.h"
#include <vector>
class HE2_QuadTreeSortable
{
private:
	virtual glm::vec2 getQuadTreePosition() =0;
};

class HE2_QuadTree
{
private:
	std::vector<HE2_QuadTree*> nodes;
	void splitIntoNodes();
	void moveAllObjectsToNodes();
	void tryMoveObjectToNode(HE2_QuadTreeSortable*);

	HE2_QuadTree* parent = nullptr;
	int thisLevel = 0;

	glm::vec2 minBound;
	glm::vec2 maxBound;

	const static int MAX_OBJECTS_PER_NODE = 25;
	const static int MAX_LEVELS = 11;
	std::vector<HE2_QuadTreeSortable*> objects;
	std::vector<HE2_QuadTreeSortable*> objectsToRetain;
};

