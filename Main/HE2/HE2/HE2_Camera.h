#pragma once
#include "HE2_GLM.h"
#include "HE2_Object.h"
#include "HE2_Debug.h"
#include "HE2_ConsoleCommand.h"
#include "HE2_Serialize.h"

struct FrustrumDetails
{
	glm::vec3 pos;
	glm::vec3 target;
	float fov;
	float ratio;

	std::vector<glm::vec3> frustumRays;
};

class HE2_Camera : public HE2_Object, public HE2_ConsoleCommand
{
public:
	HE2_Camera();
	virtual ~HE2_Camera();

	glm::mat4 getCameraMatrix();
	glm::mat4 getProjectionMatrix();
	static HE2_Camera* main;
	void setTarget(glm::vec3);
	glm::vec3 getTarget();
	void setCameraUp(glm::vec3);
	void setManualMatrix(glm::mat4);

	virtual void moveToTarget(glm::vec3 newPos);

	glm::vec3 getCameraRight();
	bool manualMatrix = false;

	//Returns the vectors towards the far plane, assuming camPos = 0,0,0
	//Clockwise starting at TL
	FrustrumDetails getFrustrumDetails();

	glm::vec2 getNearFar()
	{
		return glm::vec2(near, far);
	}
	void setNearFar(glm::vec2 nf)
	{
		near = nf.x;
		far = nf.y;
	}

	void execute(std::string args);
	template<class Archive>
	void serialize(Archive& archive)
	{

	}

private:
	bool movingToObject = false;
	glm::vec3 movingToPosition;
	glm::vec3 movingFromPosition;
	
	glm::mat4 matrix;
	glm::mat4 projection;
	glm::vec3 target;

	glm::vec3 cameraUp = glm::vec3(0,1,0);
	glm::vec3 cameraRight = glm::vec3(0, 0, 1);

	float near = 0.01f;
	float far = 1000.0f;
	float fov = glm::radians(45.0f);

	FrustrumDetails currentFrustrumDetails = {};
};

HE2_RegisterObject(HE2_Camera);
