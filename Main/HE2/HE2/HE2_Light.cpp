#include "HE2_Light.h"
#include "HE2_Lighting.h"
#include "HE2.h"

HE2_Light::HE2_Light()
{
	HE2_Lighting::addLight(this);
}

HE2_Light::HE2_Light(HE2_LightType type, glm::vec4 colour, float intensity, float angle, std::vector<HE2_ComponentBase* > comps, bool render) : type(type), colour(colour), intensity(intensity), angle(angle)
{
	for (auto i : comps)
		addComponent(i);

	if(render)
		HE2_Instance::renderBackend->onAddObject(this);
	HE2_Lighting::addLight(this);
}

HE2_Light::HE2_Light(HE2_LightType type, glm::vec4 colour, float radius) : type(type), colour(colour), radius(radius)
{
	HE2_Lighting::addLight(this);
}