#pragma once
#include "HE2_Object.h"
#include "HE2_Serialize.h"
class HE2_Light : public HE2_Object
{
public:

	HE2_Light();


	enum HE2_LightType
	{
		HE2_PointLight,
		HE2_SpotLight,
		HE2_DirectionalLight
	};

	HE2_Light(HE2_LightType type, glm::vec4 colours, float intensity, float angle, std::vector<HE2_ComponentBase* > comps = {}, bool render = false);

	HE2_Light(HE2_LightType type, glm::vec4 colours, float radius);


	//Colour, in 0-1 = 0-255 space
	glm::vec4 colour = glm::vec4(0,0,1.0f,0.0f);
	//Intensity
	float intensity = 2.0f;
	//Angle(if cone type)
	float angle = 10.0f;

	float radius = 200.0f;

	HE2_LightType type = HE2_PointLight;

	template<class Archive>
	void serialize(Archive& archive)
	{

	}
};

HE2_RegisterObject(HE2_Light);
