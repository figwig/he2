#pragma once
#include <chrono>

class Time
{
public:

	static float deltaTime;
	static float readableDeltaTime;
	static float time;
	static int frameID;

	static std::chrono::system_clock::time_point start;

private:
	friend class HE2_Instance;

	static void Tick();

	static float ticker;

};