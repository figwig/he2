#include "HE2_Lighting.h"
#include "HE2_RenderBackend.h"
#include "HE2_Instance.h"

std::vector<HE2_Light*> HE2_Lighting::lights;

void HE2_Lighting::addLight(HE2_Light*l)
{
	lights.push_back(l);

}
void HE2_Lighting::removeLight(HE2_Light* l)
{
	for (int i = 0; i < lights.size(); i++)
	{
		if (lights[i] == l)
		{
			lights.erase(lights.begin() + i);

			return;
		}
	}
}


std::vector<HE2_Light*> HE2_Lighting::getLights()
{
	return lights;
}