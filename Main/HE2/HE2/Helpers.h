#pragma once
#include <vector>
#include "HE2_GLM.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <assimp/Importer.hpp>
#include <assimp/StringComparison.h>

class HE2_Object;

glm::vec2 makeVec3xzVec2(glm::vec3 x);
glm::vec2 makeVec3xyVec2(glm::vec3 x);

glm::vec3 makeVec3Vec4IgnoreW(glm::vec4);

static glm::vec2 returny;
static glm::vec3 returny3;

float heightAboveTriangleGivenXZ(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos);

glm::vec3 getAverageOf3PointsXZ(glm::vec3, glm::vec3, glm::vec3);

bool isPointInTriangle(glm::vec2 pt, glm::vec2 v1, glm::vec2 v2, glm::vec2 v3);

static glm::vec2 v1;
static glm::vec2 v2;
static glm::vec2 v3;

bool eqVec4(glm::vec4 a, glm::vec4 b);
float sign(glm::vec2 p1, glm::vec2 p2, glm::vec2 p3);

void gameobjectLookAt(HE2_Object* host, HE2_Object* target, float maxTurn = 2 * M_PI);

//btVector3 convert(glm::vec3);
//glm::vec3 convert(btVector3);
glm::mat4 convert(aiMatrix4x4);

//template <typename T>
//bool vectorContainsReference( T ref, std::vector<T>* vector)
//{
//	for each (T iterator in (&vector))
//	{
//		if (iterator == ref)
//			return true;
//	}
//
//	return false;
//}
std::string returnNumber(std::string x);

std::string convert(aiString x);

std::string stringFromVec3(glm::vec3);
std::string stringFromVec2(glm::vec2);

float distSquared(glm::vec3, glm::vec3);
