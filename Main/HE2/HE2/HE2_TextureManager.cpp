#include "HE2_TextureManager.h"
#include "HE2_TextureAllocator.h"

HE2_TextureAllocator* HE2_TextureManager::allocator = nullptr;
std::map < std::string, HE2_Image* > HE2_TextureManager::images;

HE2_Image* HE2_TextureManager::getImage(std::string filePath, bool gammaCorrected)
{
	filePath = removeLongFileName(filePath);

	std::string savedName = filePath;
	if (gammaCorrected)
		savedName += "_gc";
	auto i = images.find(savedName);
	if (i == images.end())
	{
		//Allocate a new image here
		HE2_Image* newImage = allocator->getImage(filePath, gammaCorrected);
		images.insert(std::make_pair(savedName, newImage));
		return  newImage;
	}
	else
	{
		return i->second;
	}
}

HE2_Image* HE2_TextureManager::getImage(HE2_ImageHandle handle)
{
	return allocator->getImage(handle);
}

std::string HE2_TextureManager::removeLongFileName(std::string filePath)
{

	
	int index = filePath.find("Textures");
	if (index < 0)
	{
		index = filePath.find("textures");
		if (index < 0) return filePath;
	}
	std::string f = filePath.substr(index, filePath.length());

	return  f;
	
}
