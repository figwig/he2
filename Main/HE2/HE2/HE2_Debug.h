#pragma once
#include <string>
#include <vector>
#include <iosfwd>
#include <chrono>
#include <iomanip>
#include <ctime>
#include "HE2_ConsoleCommand.h"
#include <cstdint>
#include <map>


class HE2_Debug
{
public:

	struct DebugMessage
	{
		std::string message;
		std::chrono::system_clock::time_point time;
		std::string timeAsString;
	};

	static void Log(std::string);

	static std::vector<DebugMessage> messages;

	static std::string time_format;

	static std::map<std::string, HE2_ConsoleCommand*> allCommands;

	static void executeCommand(std::string plainText);
};

#ifndef NDEBUG
inline void DebugLog(std::string x)
{
	HE2_Debug::Log(x);
}
#else
inline void DebugLog(std::string) {}
#endif

template <typename Duration>
std::ostream& operator<<(std::ostream& os,
	const std::chrono::time_point<std::chrono::system_clock, Duration>& timep)
{
	using system_clock_duration = std::chrono::system_clock::duration;
	auto converted_timep = std::chrono::time_point_cast<system_clock_duration>(timep);
	auto seconds_since_epoch = std::chrono::system_clock::to_time_t(timep);

	os << std::put_time(std::localtime(&seconds_since_epoch), HE2_Debug::time_format.c_str());
	return os;
}

