#pragma once
#include "HE2.h"
#include <string>

class HE2_RenderComponent;

class HE2_RenderContext
{
	//A render context 
public:
	HE2_RenderContext()
	{
		renderContextID = renderContextTally;
		renderContextTally++;
	}

	std::string name = "Unnamed Render Context";

	int renderContextID = 0;

	static int renderContextTally;
};

class HE2_RenderRun
{
	//A render run is a set of passes, rending to a specific target. Render run 0 always renders to the screen, and is called renderFinal.
public:

	HE2_RenderRun(HE2_RenderContext* renderContext)
	{
		this->renderContext = renderContext;
	}

	//When a new object is created, this is called in order to correctly add the object to all render runs that want it
	virtual bool doesWantObject(HE2_RenderComponent* obj) = 0; 

	HE2_RenderContext* renderContext = nullptr;
	HE2_RenderRun* parentRun = nullptr;
};