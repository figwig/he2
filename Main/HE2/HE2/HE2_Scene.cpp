#include "HE2_Scene.h"
#include "HE2_Object.h"
#include "HE2_Model.h"
#include "HE2_Camera.h"
#include "HE2_RenderComponent.h"

HE2_Scene* HE2_Scene::currentScene = nullptr;

HE2_SceneCreator::HE2_SceneCreator(std::string name) : name(name)
{

}

HE2_SceneCreator::HE2_SceneCreator(std::string name, std::vector<HE2_ObjectInstance*> objects) : name(name), objectsInScene(objects)
{

}
void HE2_SceneCreator::CreateSceneObjects(HE2_SceneCreator * scene)
{
	scene->camera = new HE2_Camera();

	for (HE2_ObjectInstance* instance : scene->objectsInScene)
	{
		
		if (instance->parent != nullptr && instance->parent->generatedObject == nullptr)
		{
			scene->objectsInScene.push_back(instance);
			continue;
		}

		HE2_RenderComponent* rc = new HE2_RenderComponent(instance->model, instance->material);
		instance->startingComponents.push_back(rc);

		HE2_Object* newObject = new HE2_Object(instance->startingComponents, instance->hintedStartingComponents);

		instance->generatedObject = newObject;

		newObject->setPos(instance->position);
		newObject->setScale(instance->scale);
		newObject->setRot(instance->rotation);

		if (instance->name == "")
		{
			newObject->name = instance->model->name;
		}
		else
		{
			newObject->name = instance->name;
		}
	}
}