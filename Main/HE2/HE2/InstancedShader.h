#pragma once
#include "HE2.h"
class InstancedShader : public HE2_Shader
{
public:
	static InstancedShader* getInstance();

private:
	static InstancedShader* thisPointer;
	InstancedShader();
};

