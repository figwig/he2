#include "HE2_Camera.h"
#include "HE2_WindowHandler.h"
#include "HE2_SceneGraph.h"
#include "HE2_GLM.h"
#include "Helpers.h"
#include "imgui.h"
#include "HE2_Time.h"

HE2_Camera* HE2_Camera::main = nullptr;

HE2_Camera::HE2_Camera() : HE2_ConsoleCommand("camera")
{
	name = "Camera";
	desc = "The 'camera' command works like a normal gameobject command, where you can add 'getPos' or 'getRot' etc to get details about the object";

	target = glm::vec3(0);

	matrix = glm::lookAt(getPos(), target, glm::vec3(0, 1, 0));


	//Setup main camera
	if (main == nullptr)
	{
		main = this;
		name = "Main Camera";
	}

	//HE2_SceneGraph::addToRootNodes(this);
}

void HE2_Camera::execute(std::string args)
{
	if (std::strcmp(args.c_str(), "getPos"))
	{
		DebugLog(stringFromVec3(getPos()));
	}
}

HE2_Camera::~HE2_Camera()
{

}

glm::mat4 HE2_Camera::getCameraMatrix()
{



	matrix = glm::lookAt(getPos(), target, cameraUp);

	cameraRight = glm::cross(getPos() - target, cameraUp);
	

	return matrix;
}

glm::mat4 HE2_Camera::getProjectionMatrix()
{
	projection = glm::perspective(fov, (float)HE2_WindowHandler::WIDTH / (float)HE2_WindowHandler::HEIGHT, near, far);

	projection[1][1] *= -1;
	return projection;
}

void HE2_Camera::setTarget(glm::vec3 x)
{
	target = x;
}

glm::vec3 HE2_Camera::getTarget()
{
	return target;
}

void HE2_Camera::setCameraUp(glm::vec3 x)
{
	cameraUp = x;
}

void HE2_Camera::setManualMatrix(glm::mat4 x)
{
	matrix = x;
}

void HE2_Camera::moveToTarget(glm::vec3 newPos)
{
	movingFromPosition = getPos();
	movingToObject = true;
	movingToPosition = newPos;
}

glm::vec3 HE2_Camera::getCameraRight()
{
	return cameraRight;
}

FrustrumDetails HE2_Camera::getFrustrumDetails()
{
	if (currentFrustrumDetails.fov == fov && currentFrustrumDetails.pos == getPos() && currentFrustrumDetails.ratio == ((float)HE2_WindowHandler::WIDTH / (float)HE2_WindowHandler::HEIGHT) && currentFrustrumDetails.target == target)
	{
		return currentFrustrumDetails;
	}

	currentFrustrumDetails.fov = fov;
	currentFrustrumDetails.pos = getPos();
	currentFrustrumDetails.target = target;
	currentFrustrumDetails.ratio = ((float)HE2_WindowHandler::WIDTH / (float)HE2_WindowHandler::HEIGHT);

	float nearHeight = 2.0f * std::tan(fov / 2.0f) * near;
	float nearWidth = nearHeight * currentFrustrumDetails.ratio;

	float farHeight = 2.0f * std::tan(fov / 2.0f) * far;
	float farWidth = farHeight * currentFrustrumDetails.ratio;

	//Viewing direction  = target - pos
	glm::vec3 farCentre = glm::normalize(target - getPos()) * far;

	glm::vec3 up = glm::normalize(cameraUp);
	glm::vec3 right = glm::normalize(cameraRight);

	glm::vec3 farTR = glm::normalize(farCentre + (up * farHeight / 2.0f) + ((right * farWidth) / 2.0f));
	glm::vec3 farBL = glm::normalize(farCentre - (up * farHeight / 2.0f) - ((right * farWidth) / 2.0f));
	glm::vec3 farBR = glm::normalize(farCentre - (up * farHeight / 2.0f) + ((right * farWidth) / 2.0f));
	glm::vec3 farTL = glm::normalize(farCentre + (up * farHeight / 2.0f) - ((right * farWidth) / 2.0f));

	glm::vec3 farTopCen = glm::normalize(farCentre + (up * farHeight / 2.0f));
	glm::vec3 farBotCen = glm::normalize(farCentre - (up * farHeight / 2.0f));

	currentFrustrumDetails.frustumRays = { farTL, farTR, farBR, farBL, farTopCen, farBotCen };

	return currentFrustrumDetails;
}
