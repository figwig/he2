#include "HE2_Shader.h"
#include "HE2_Instance.h"
#include <map>
#include <list>

HE2_ShaderFeature HE2_Shader::viewProjectionFeature = { HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK, HE2_SHADER_STAGE_VERTEX, 0, HE2_SHADER_BUFFER_SOURCE_VIEW_PROJECTION_BUFFER };

HE2_ShaderFeature HE2_Shader::modelMatrixUniformFeature = { HE2_SHADER_FEATURE_TYPE_UNIFORM, HE2_SHADER_STAGE_VERTEX, 0, HE2_SHADER_BUFFER_SOURCE_MODEL_BUFFER , sizeof(glm::mat4)};


HE2_Shader::HE2_Shader(std::string name, HE2_ShaderSources shaderSources, std::vector<HE2_ShaderFeature> features, std::vector<HE2_DataInputDesc> desc, HE2_PerVertexDataDesc dataPerVertexDesc, HE2_ShaderSettings settings) :features(features), shaderSources(shaderSources), vertexInputDesc(desc), perVertexDataDesc(dataPerVertexDesc), name(name)
{
	shaderSettings = settings;
	setupShader();
}

/// <summary>
/// Setups a shader.
/// </summary>
void HE2_Shader::setupShader()
{
	//Can't have a shader without any features
	//assert(features.size() > 0);

	for (auto x : features)
	{
		if (x.featureType == HE2_SHADER_FEATURE_TYPE_UNIFORM)
		{
			uniformFeatures.push_back(x);
			continue;
		}

		featureGroups = glm::max(featureGroups, x.grouping);

		if (x.source == HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC)
		{
			bool foundGroup = false;
			//Add to the shader specific group listings
			for (auto& featureGrouping : shaderSpecificFeatures)
			{
				if (featureGrouping.first == x.grouping)
				{
					featureGrouping.second.push_back(x);
					foundGroup = true;
					break;
				}
			}
			if (!foundGroup)
				shaderSpecificFeatures.push_back(std::make_pair(x.grouping,  std::vector<HE2_ShaderFeature>(1, x) ));
		}

		if (x.source == HE2_SHADER_BUFFER_SOURCE_VIEW_PROJECTION_BUFFER) shaderSettings.useVPFeature = true;
	}
	//Feature groups is now equal to the highest grouping, add one to get the total number
	featureGroups++;


	HE2_Instance::renderBackend->buildShader(this);


}

HE2_Shader::~HE2_Shader()
{
	HE2_Instance::renderBackend->destroyShader(this);
}