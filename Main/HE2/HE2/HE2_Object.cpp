#include "HE2_Object.h"
#include "HE2_Scene.h"
#include "HE2_Instance.h"
#include "HE2_RenderBackend.h"
#include "HE2_RenderComponent.h"
#include "HE2_GLM.h"
#include <stdexcept>

HE2_Object::HE2_Object()
{
	if(HE2_Scene::currentScene)
		HE2_Scene::currentScene->sceneGraph.addToRootNodes(this);
}

HE2_Object::HE2_Object(HE2_Object* parent)
{
	//addComponent(new HE2_RenderComponent());

	parent->addChild(this);
}

HE2_Object::HE2_Object(std::vector<HE2_ComponentBase*> components)
{
	for(auto x : components)
		addComponent(x);

	HE2_Scene::currentScene->sceneGraph.addToRootNodes(this);

	//This wants changing over to render component
	HE2_Instance::renderBackend->onAddObject(this);
}

HE2_Object::HE2_Object(std::vector<HE2_ComponentBase*> startingComponents, std::vector<std::pair<HE2_ComponentBase*, std::vector<std::type_index>>> forcedStartingComponents) 
{
	for (auto x : startingComponents)
		addComponent(x);

	for (auto x : forcedStartingComponents)
		addComponent(x.first, x.second);

	HE2_Scene::currentScene->sceneGraph.addToRootNodes(this);

	//This wants changing over to redner compoent
	HE2_Instance::renderBackend->onAddObject(this);
}

HE2_Object::HE2_Object(HE2_Object* parent, std::vector<HE2_ComponentBase*> components)
{
	for (auto x : components)
		addComponent(x);

	parent->addChild(this);

	HE2_Scene::currentScene->sceneGraph.addToRootNodes(this);

	//This wants changing over to redner compoent
	HE2_Instance::renderBackend->onAddObject(this);
}



HE2_Object::~HE2_Object()
{

}

void HE2_Object::addChild(HE2_Object* child)
{
	children.push_back(child);
	child->parent = this;
}

void HE2_Object::setParent(HE2_Object* newParent)
{
	for (auto i : children)
	{
		if (i == newParent)
		{
			throw std::runtime_error("Can't set a child as new parent");
		}
	}


	dirtyMatricies();

	if (parent == nullptr)
	{
		HE2_Scene::currentScene->sceneGraph.removeFromRootNodes(this);
	}
	if (parent != nullptr)
	{
		//Find the previous parent, inform it this is no longer it's child
		parent->removeChild(this);
	}

	if (newParent != nullptr)
		newParent->addChild(this);
	else
		HE2_Scene::currentScene->sceneGraph.addToRootNodes(this);
}

void HE2_Object::removeChild(HE2_Object* obj)
{
	for (int i = 0; i < children.size(); ++i)
	{
		if (children[i] == obj)
		{
			children.erase(children.begin() + i);
			obj->setParent(nullptr);
			return;
		}
	}
}

glm::vec3 HE2_Object::getPos()
{
	glm::vec3 pos = glm::vec3(getModelMatrix()[3]);
	if (parent == nullptr) return pos;
	return parent->getModelMatrix()  * glm::vec4(pos, 1.0f);
}

glm::mat4 HE2_Object::getModelMatrix()
{
	updateModelMatIfNeeded();

	if (parent == nullptr)
		return modelMatrix;

	return parent->getModelMatrix() * modelMatrix;
}

void HE2_Object::setModelMat(glm::mat4 m)
{
	modelMatrix = m;
}

void HE2_Object::updateModelMatIfNeeded()
{
	if (modelMatrixDirty)
	{
		modelMatrixDirty = false;
		if (parent != nullptr)
		{
			modelMatrix = parent->getModelMatrix() * transform * rotation * scale;
		}
		else
		{
			modelMatrix = transform * rotation * scale;
		}
	}
}

void HE2_Object::setPos(glm::vec3 x)
{
	transform = glm::translate(glm::mat4(1), x);
	dirtyMatricies();
}

void HE2_Object::dirtyMatricies()
{
	modelMatrixDirty = true;

	for (auto i : children)
	{
		i->modelMatrixDirty = true;
	}
}

void HE2_Object::setScale(glm::vec3 x) 
{
	scale = glm::scale(glm::mat4(1), x);
	scaleVec = x;
	dirtyMatricies();
}

glm::vec3 HE2_Object::getScale()
{
	return scaleVec;
}

glm::mat4 HE2_Object::getScaleMat()
{
	return scale;
}

glm::vec3 HE2_Object::getRot()
{
	return rotAngles;
}

glm::mat4 HE2_Object::getRotMat()
{
	return rotation;
}

void HE2_Object::setRot(glm::vec3 x)
{
	rotAngles = x;	
	rotation = glm::toMat4(
		glm::quat(glm::vec3(0.0f, x.y, 0.0f)) *
		glm::quat(glm::vec3(x.x, 0.0f, 0.0f)) *
		glm::quat(glm::vec3(0.0f, 0.0f, x.z))
	);
	dirtyMatricies();
}

void HE2_Object::setRot(glm::quat q)
{
	rotation = glm::toMat4(q);
	rotAngles = glm::eulerAngles(q);

	dirtyMatricies();
}

std::vector<HE2_Object*> HE2_Object::getChildren() {
	return children;
}

void HE2_Object::setRotMat(glm::mat4 modelMat)
{
	glm::vec3 scale;
	glm::quat rotation;
	glm::vec3 translation;
	glm::vec3 skew;
	glm::vec4 perspective;
	glm::decompose(glm::mat4(glm::mat3(modelMat)), scale, rotation, translation, skew, perspective);

	setRot(rotation);

}