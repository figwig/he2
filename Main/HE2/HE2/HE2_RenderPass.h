#pragma once
#include <cstdint>

typedef uint32_t HE2_PassFlags;

enum HE2_PassFlagTypes
{
	AllObjects = 0b1,
	OpaqueObjects = 0b10,
	Transparent = 0b100,
	Reflective = 0b1000,
	GUI1 = 0b10000,
	GUI2 = 0b100000,
	Text =	   0b1000000,
	Deferred = 0b10000000
};


class HE2_RenderPass
{
public:
	HE2_PassFlags flagsWantedByPass = 0;
};

class HE2_ScreenBuffer : public HE2_RenderPass
{
public:
	HE2_ScreenBuffer(HE2_PassFlags pf)
	{
		flagsWantedByPass = pf;
	}
};