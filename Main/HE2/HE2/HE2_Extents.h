#pragma once
#define GLM_SWIZZLE_XYZW

#include "HE2_GLM.h"
struct HE2_Extents
{
	HE2_Extents operator *(glm::mat4 mat)const
	{
		HE2_Extents e;
		e.minExtent = glm::vec3(mat * glm::vec4(minExtent, 1));
		e.maxExtent = glm::vec3(mat * glm::vec4(maxExtent, 1));
		e.centre = glm::vec3(mat * glm::vec4(centre, 1));
		e.maxPoint = glm::vec3(mat * glm::vec4(maxPoint, 1));
		e.minPoint = glm::vec3(mat * glm::vec4(minPoint, 1));
		return e;
	}

	HE2_Extents operator-(glm::vec3 avg) const
	{
		HE2_Extents e;
		e.minExtent = minExtent - avg;
		e.minPoint = minPoint - avg;
		e.maxExtent = maxExtent - avg;
		e.maxPoint = maxPoint - avg;
		e.centre = centre - avg;
		return e;
	}
	HE2_Extents() {}

	HE2_Extents(glm::vec3 minExtent, glm::vec3 maxExtent) :minExtent(minExtent), maxExtent(maxExtent) { extent = maxExtent - minExtent; }
	glm::vec3 minExtent;
	glm::vec3 maxExtent;
	glm::vec3 centre;
	glm::vec3 maxPoint;
	glm::vec3 minPoint;
	glm::vec3 extent;
};