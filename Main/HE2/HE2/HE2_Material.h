#pragma once
#include "HE2_Instance.h"
#include "HE2_ComponentOwner.h"
#include "HE2_Shader.h"
#include <map>

class HE2_Image;
class HE2_Shader;
class HE2_NoMaterial;


class HE2_Material : public HE2_ComponentOwner
{
public:

	virtual std::vector<HE2_ShaderFeature> getMaterialShaderFeatures() = 0;
	HE2_Shader* shader = nullptr;

	virtual std::vector<HE2_ImageHandle> getImageHandles() = 0;

	static HE2_Material* noMaterial(HE2_Shader * shader);

protected:
	std::vector<HE2_ShaderFeature> materialFeatures;
	static std::map<HE2_Shader*, HE2_Material*> blankMaterialMap;
};

class HE2_NoMaterial : public HE2_Material
{
public:
	HE2_NoMaterial(HE2_Shader* shader) { this->shader = shader;	HE2_Instance::renderBackend->onAddMaterial(this);
	}

	virtual std::vector<HE2_ShaderFeature> getMaterialShaderFeatures() { return {}; }

	virtual std::vector<HE2_ImageHandle> getImageHandles() { return {};}
private:

};

class HE2_SimpleMaterial : public HE2_Material
{
public:
	HE2_SimpleMaterial(HE2_Image* colour, HE2_Shader* shader);

	HE2_Image* colourImage = nullptr;

	std::vector<HE2_ShaderFeature> getMaterialShaderFeatures();

	std::vector<HE2_ImageHandle> getImageHandles() { return { colourImage->handle }; }


	static std::vector<HE2_ShaderFeature>  MaterialShaderFeatures(int grouping = 1);
};

class HE2_BlinnPhongMaterial :public HE2_Material
{
public:
	HE2_BlinnPhongMaterial(HE2_Image* diff, HE2_Image* spec, HE2_Image* norm, HE2_Shader* shader);
	virtual ~HE2_BlinnPhongMaterial() {}
	HE2_Image* diffImage = nullptr;
	HE2_Image* specImage = nullptr;
	HE2_Image* normImage = nullptr;


	std::vector<HE2_ShaderFeature> getMaterialShaderFeatures();

	std::vector<HE2_ImageHandle> getImageHandles();

	static std::vector<HE2_ShaderFeature>  MaterialShaderFeatures(int grouping =1 );
protected:
	//To allow children to happen without actual images
	HE2_BlinnPhongMaterial(HE2_Shader * shader);


};

