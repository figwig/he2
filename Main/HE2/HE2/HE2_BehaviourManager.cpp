#include "HE2_BehaviourManager.h"
#include <atomic>
#include "HE2_JobCentre.h"
#include <string>

std::vector<HE2_Behaviour*> HE2_BehaviourManager::behaviours;
std::vector<HE2_ComponentBase*> HE2_BehaviourManager::componentAsBehaviours;
std::vector<HE2_Behaviour*> HE2_BehaviourManager::unThreadableBehaviours;

void HE2_BehaviourManager::addBehaviour(HE2_Behaviour* b)
{
	if(b->threadable)
		behaviours.push_back(b);
	else
		unThreadableBehaviours.push_back(b);
}

void HE2_BehaviourManager::addComponentAsBehaviour(HE2_ComponentBase* cb)
{
	componentAsBehaviours.push_back(cb);
}

void HE2_BehaviourManager::removeBehaviour(HE2_Behaviour* b)
{
	//Remove the behaviour
	int i = 0;
	for (auto bx : behaviours)
	{
		if (b == bx)
		{
			behaviours.erase(behaviours.begin() + i);
			return;
		}
		i++;
	}	
	//If not returned, it's probably in the unthreadable list
	i = 0;
	for (auto bx : unThreadableBehaviours)
	{
		if (b == bx)
		{
			unThreadableBehaviours.erase(unThreadableBehaviours.begin() + i);
			return;
		}
		i++;
	}
}


void HE2_BehaviourManager::updateBehaviours()
{
	HE2_JobCentre* jc = HE2_JobCentre::singleton();

	int behavioursSize = behaviours.size();
	for (auto a : behaviours)
	{
		jc->QueueJob("Behaviour Update", [=] {a->update(); });
	}

	for (auto a : unThreadableBehaviours)
	{
		a->update();
	}
	for (auto a : componentAsBehaviours)
	{
		a->update();
	}

	//while (behavioursDone != behavioursSize)
	//{
	//	std::this_thread::sleep_for(std::chrono::microseconds(1));
	//}

	jc->waitUntilAllIdle();

}