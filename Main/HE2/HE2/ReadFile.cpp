#include "ReadFile.h"
#include <stdexcept>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <Windows.h>
#include <Shlobj.h>

std::vector<char> HE2_Files::readFile(const std::string& filename) 
{

	std::ifstream file(filename, std::ios::ate | std::ios::binary);

	if (!file.is_open()) {
		throw std::runtime_error("failed to open file " + filename);
	}

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);

	file.close();

	return buffer;
}