#pragma once
#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
#include <map>
#include <HE2_Behaviour.h>
#include <HE2.h>
#include "HE2_PhysicsComponent.h"
#include <functional>

struct RayCastResult
{
	float distanceTravelled;
	glm::vec3 hitPos;
	bool hit;

	const btCollisionShape* collider = nullptr;
	HE2_Object* object = nullptr;
};

class BulletPhysicsHandler :
	public HE2_GlobalBehaviour
{
public:
	static BulletPhysicsHandler* getInstance();

	void doPhysics(double);
	void doLatePhysics(double);

	RayCastResult castRayDown(glm::vec3 position);
	RayCastResult castRay(glm::vec3 position, glm::vec3 towards);



	std::function<void()> physicsFunction;

	bool physicsDone = false;

	void update();

private:
	friend class BulletPhysicsComponent;
	void addToSimulation(BulletPhysicsComponent*);

	void removeFromSimulation(BulletPhysicsComponent*);

	void setup();
	static BulletPhysicsHandler* thisPointer;
	BulletPhysicsHandler();
	~BulletPhysicsHandler();

	std::vector<BulletPhysicsComponent*> components;

	btDiscreteDynamicsWorld* dynWorld;
	btDefaultCollisionConfiguration* config;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;
	btDbvtBroadphase* overlappingPairCache;

	std::map<const btCollisionShape*, HE2_Object*> colObjects;
};

