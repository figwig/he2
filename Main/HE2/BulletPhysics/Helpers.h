#pragma once
#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
#include <HE2_GLM.h>

glm::vec3 convert(btVector3);

btVector3 convert(glm::vec3);