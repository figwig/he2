#include "HE2_PhysicsComponent.h"
#include "Helpers.h"
#include "BulletPhysicsHandler.h"
#include "BulletCollisionShapeFactory.h"
#include <HE2_RenderComponent.h>


BulletPhysicsComponent::BulletPhysicsComponent(HE2_Object* h, float m) 
{
	mass = m;

	host = h;

	collisionShape = BulletCollisionShapeFactory::makeBoxFromExtent(h->getComponent<HE2_RenderComponent>()->getModel()->extents.extent);

	BulletPhysicsHandler::getInstance()->addToSimulation(this);
}

BulletPhysicsComponent::BulletPhysicsComponent(HE2_Object* h)
{
	
	host = h;
	trigger = true;
	BulletPhysicsHandler::getInstance()->addToSimulation(this);
}

BulletPhysicsComponent::BulletPhysicsComponent(btCollisionShape* vol, glm::mat4 position) 
{
	noHostComp = true;
	collisionShape = vol;
	hostlessTransform = position;
	BulletPhysicsHandler::getInstance()->addToSimulation(this);
}

BulletPhysicsComponent::~BulletPhysicsComponent()
{

}

void BulletPhysicsComponent::setMass(float m)
{
	mass = m;
}

void BulletPhysicsComponent::applyForce(glm::vec3 f)
{
	body->activate(true);
	body->applyForce(convert(f), convert(glm::vec3(0)));
}

void BulletPhysicsComponent::applyForce(Force f)
{
	body->activate(true);
	body->applyForce(convert(f.magnitude), convert(f.posRelCG));
}

void BulletPhysicsComponent::setHostPosition(glm::vec3 p)
{
	if (host == nullptr)return;
	host->setPos(p);
}

void BulletPhysicsComponent::setHostOrientation(glm::quat q)
{
	if (host == nullptr)return;
	host->setRot(q);
}

void BulletPhysicsComponent::applyAcceleration(glm::vec3 a)
{
	glm::vec3 force = a * mass;
	applyForce(force);
}

void BulletPhysicsComponent::applyImpulse(glm::vec3 impulse)
{
	body->activate(true);
	body->applyCentralImpulse(convert(impulse));
}

btCollisionShape* BulletPhysicsComponent::getBtShape()
{

	return collisionShape;
	//if (host != nullptr)
	//{
	//	return host->getCollisionShape();
	//}
	//else if (noHostComp)
	//{
	//	return bulletHostless;
	//}
	//else
	//{
	//	std::cout << "physics component doesn't have host, but isn't hostless\n";
	//	return nullptr;
	//}
}

void BulletPhysicsComponent::setupBullet()
{
	//Setup the transform
	transform.setIdentity();

	if (host != nullptr) transform.setFromOpenGLMatrix((const btScalar*)&host->getModelMatrix());
	else  transform.setFromOpenGLMatrix((const btScalar*)glm::value_ptr(hostlessTransform));

	if (!trigger)
	{
		bool isDynamic = (mass != 0.f);

		btCollisionShape* shape = getBtShape();

		btVector3 localInertia(0, 0, 0);
		if (isDynamic)
		{
			getBtShape()->calculateLocalInertia(mass, localInertia);
		}

		motionState = new btDefaultMotionState(transform);

		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, getBtShape(), localInertia);

		body = new btRigidBody(rbInfo);

		body->setUserPointer((void*)this);
	}
	else
	{
		ghosty = new btGhostObject();

		ghosty->setCollisionShape(getBtShape());

		ghosty->setWorldTransform(transform);

	}

	setup = true;
}

void BulletPhysicsComponent::btUpdate(double timeStep)
{
	if (host == nullptr)return;

	if (bulletIndependant)
	{
		btTransform trans = body->getWorldTransform();

		glm::vec3 rot = host->getRot();

		trans.setRotation(btQuaternion(rot.y, rot.x, rot.z));

		body->setWorldTransform(trans);

		//Calulate an appropriate force to apply to the box in order for it to be moved correctly
		btVector3 newPos = convert(host->getPos());

		btVector3 oldPos = body->getWorldTransform().getOrigin();

		btVector3 dist = newPos - oldPos;

		btVector3 currVel = body->getLinearVelocity();

		btVector3 acc = 2 * ((newPos - oldPos - (currVel * timeStep)) / (pow(timeStep, 2)));

		body->applyCentralForce(mass * acc / glm::max(50.0f, responsiveness));

		//Anti-Gravity
		body->applyCentralForce(btVector3(0, mass * 10.0f, 0));
	}

	if (ghosty != nullptr)
	{
		transform.setFromOpenGLMatrix((const btScalar*)&host->getModelMatrix());

		ghosty->setWorldTransform(transform);
	}

	//btTransform trans;
	//trans.setFromOpenGLMatrix(glm::value_ptr(host->getTransMat() * host->getRotMat()));

	//motionState->setWorldTransform(trans);
	//body->setWorldTransform(trans);

	if (body != nullptr)
		body->activate(true);
}
