#pragma once
#include <btBulletCollisionCommon.h>
#include <HE2_GLM.h>
class BulletCollisionShapeFactory
{
public:
	static btBoxShape *makeBoxFromExtent(glm::vec3 extent);
};

