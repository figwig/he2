#pragma once
#include <HE2_Component.h>
#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>

#include <HE2.h>

struct Force
{
	glm::vec4 magnitude;
	glm::vec3 posRelCG;
	Force(glm::vec3 mag, glm::vec3 pos)
	{
		magnitude = glm::vec4(mag, 1);
		posRelCG = pos;
	}
	Force operator * (glm::mat4 toApply) const
	{
		return Force(toApply * magnitude, posRelCG);
	}
};

class BulletPhysicsComponent : public HE2_Component<HE2_Object>
{
public:
	BulletPhysicsComponent(HE2_Object*, float m);
	BulletPhysicsComponent(HE2_Object*);
	//Hostless
	BulletPhysicsComponent(btCollisionShape*, glm::mat4 transform);
	~BulletPhysicsComponent();

	//perform movement required for this frame based on values calculated by the physics handler
	void btUpdate(double);

	void applyForce(glm::vec3);
	void applyForce(Force);
	void applyAcceleration(glm::vec3);
	void setMass(float m);
	void applyImpulse(glm::vec3);
	float mass = 0.0f;
	bool noHostComp = false;
	bool deleteMe = false;

	//Bullet Stuff
	btCollisionShape * collisionShape;
	btTransform  transform;
	btRigidBody* body;
	btCollisionShape* getBtShape();

	void setupBullet();
	float responsiveness = 100.0f;
	bool bulletIndependant = false;
	bool setup = false;

	bool trigger = false;
	btGhostObject* getGhost() { return ghosty; }

private:
	friend class BulletPhysicsHandler;
	btDefaultMotionState* motionState;

	glm::mat4 hostlessTransform;
	//Only to be used by physics handlers
	void setHostPosition(glm::vec3); //to be used sparingly.
	//Only to be used by physics handlers
	void setHostOrientation(glm::quat);

	btGhostObject* ghosty = nullptr;

};

