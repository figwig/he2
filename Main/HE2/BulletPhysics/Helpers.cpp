#include "Helpers.h"

glm::vec3 convert(btVector3 v)
{
	return glm::vec3(v.x(), v.y(), v.z());
}

btVector3 convert(glm::vec3 v)
{
	return btVector3(v.x, v.y, v.z);
}