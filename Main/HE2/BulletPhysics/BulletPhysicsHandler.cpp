#include "BulletPhysicsHandler.h"
#include "Helpers.h"
#include <Time.h>

BulletPhysicsHandler* BulletPhysicsHandler::thisPointer = nullptr;

BulletPhysicsHandler::BulletPhysicsHandler()
{
	physicsFunction = ([=] {
		physicsDone = false;
		doPhysics(0);
		physicsDone = true;
		});
}

void BulletPhysicsHandler::setup()
{
	config = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(config);
	overlappingPairCache = new btDbvtBroadphase();
	solver = new btSequentialImpulseConstraintSolver();

	dynWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, config);


	dynWorld->setGravity(btVector3(0, -10, 0));

	std::cout << "Bullet Physics setup finished\n";
}

BulletPhysicsHandler::~BulletPhysicsHandler()
{
	//cleanup in the reverse order of creation/initialization

	///-----cleanup_start----
	//remove the rigidbodies from the dynamics world and delete them

	for (int i = dynWorld->getNumCollisionObjects() - 1; i >= 0; i--)
	{
		btCollisionObject* obj = dynWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);

		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}

		dynWorld->removeCollisionObject(obj);
		delete obj;
	}

	//delete collision shapes
	for (int j = 0; j < components.size(); j++)
	{
		BulletPhysicsComponent* shape = components[j];
		components[j] = 0;
		delete shape;
	}

	//delete dynamics world
	delete dynWorld;

	//delete solver
	delete solver;

	//delete broadphase
	delete overlappingPairCache;

	//delete dispatcher
	delete dispatcher;

	delete config;

	//next line is optional: it will be cleared by the destructor when the array goes out of scope
	components.clear();
}

BulletPhysicsHandler* BulletPhysicsHandler::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new BulletPhysicsHandler();
		thisPointer->setup();
	}
	return thisPointer;
}

void BulletPhysicsHandler::doPhysics(double)
{
	if (components.size() == 0) return;

	double timeStep = Time::deltaTime;

	for(BulletPhysicsComponent * comp : components)
	{
		comp->btUpdate(timeStep);
	}

	//actually step the simulation
	dynWorld->stepSimulation((float)timeStep, 8);
}

void BulletPhysicsHandler::update()
{
	doPhysics(Time::deltaTime);
	doLatePhysics(Time::deltaTime);
}

void BulletPhysicsHandler::doLatePhysics(double)
{

	if (components.size() == 0) return;

	// update positions of all objects
	for (int j = dynWorld->getNumCollisionObjects() - 1; j >= 0; j--)
	{
		btCollisionObject* obj = dynWorld->getCollisionObjectArray()[j];
		btRigidBody* body = btRigidBody::upcast(obj);
		btTransform trans;

		if (body && body->getMotionState())
		{
			void* userPointer = body->getUserPointer();
			if (userPointer)
			{
				body->getMotionState()->getWorldTransform(trans);
				btQuaternion orientation = trans.getRotation();
				BulletPhysicsComponent* comp = static_cast<BulletPhysicsComponent*>(userPointer);
				if (!comp->trigger)
				{
					comp->setHostPosition(glm::vec3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
					comp->setHostOrientation(glm::quat(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
				}
			}

		}
		else
		{
			trans = obj->getWorldTransform();
		}

	}
}

void BulletPhysicsHandler::addToSimulation(BulletPhysicsComponent* x)
{
	for (int count = 0; count < components.size(); count++)
	{
		//found the object already!
		if (components[count] == x) return;
	}
	components.push_back(x);

	if (!x->setup) x->setupBullet();

	if (!x->trigger)

		dynWorld->addRigidBody(x->body);
	else
	{

		dynWorld->addCollisionObject(x->getGhost());
		//dynWorld->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
	}
	colObjects.insert(std::make_pair(x->getBtShape(), x->host));
}

void BulletPhysicsHandler::removeFromSimulation(BulletPhysicsComponent* x)
{
	btRigidBody* body = x->body;
	for (int count = 0; count < components.size(); count++)
	{
		//found the object already!
		if (components[count] == x)
		{
			components.erase(components.begin() + count);
			if (!x->trigger)
				dynWorld->removeRigidBody(body);
			else
				dynWorld->removeCollisionObject(x->getGhost());
			return;
		}

	}
}


RayCastResult BulletPhysicsHandler::castRayDown(glm::vec3 position)
{
	btVector3 start = convert(position);

	btVector3 towards = start - btVector3(0, 10000, 0);

	btCollisionWorld::ClosestRayResultCallback closestResults(start, towards);

	dynWorld->rayTest(start, towards, closestResults);

	RayCastResult res;

	res.hit = closestResults.hasHit();

	res.hitPos = convert(closestResults.m_hitPointWorld);

	res.distanceTravelled = position.y - res.hitPos.y;

	return res;
}


RayCastResult BulletPhysicsHandler::castRay(glm::vec3 position, glm::vec3 towards)
{
	btVector3 btStart = convert(position);

	btVector3 btTowards = convert(towards);

	btCollisionWorld::ClosestRayResultCallback closestResults(btStart, btTowards);

	dynWorld->rayTest(btStart, btTowards, closestResults);

	RayCastResult res;

	res.hit = closestResults.hasHit();

	res.hitPos = convert(closestResults.m_hitPointWorld);

	res.distanceTravelled = position.y - res.hitPos.y;

	const btCollisionObject* bt = closestResults.m_collisionObject;

	if (bt != nullptr)
	{
		res.collider = bt->getCollisionShape();

		if (res.collider != nullptr)
		{
			res.object = colObjects[res.collider];
		}
	}
	
	return res;
}