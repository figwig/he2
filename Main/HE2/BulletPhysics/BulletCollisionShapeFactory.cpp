#include "BulletCollisionShapeFactory.h"
#include "Helpers.h"

btBoxShape* BulletCollisionShapeFactory::makeBoxFromExtent(glm::vec3 extent)
{
	return new btBoxShape(convert(extent * 0.5f));
}

