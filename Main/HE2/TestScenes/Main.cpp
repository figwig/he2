#include <HE2.h>
#include <VulkanBackend.h>
#include <stdexcept>
#include <HE2_Behaviour.h>
#include <HE2_Time.h>
#include <HE2_Scene.h>
#include <FPSUIElement.h>
#include "TestingBehaviours.h"
#include <FlameGraphBehaviour.h>
#include <StandardAssets.h>
#include <HE2_TextureManager.h>
#include <SceneGraphDisplayBehaviour.h>
#include <NormalDisplayShader.h>
#include <SimpleCameraKeyboardMove.h>
#include "AddObjectOnClick.h"
#include <ConsoleUI.h>

int main()
{
	HE2_VulkanBackend* pipeline = nullptr;

	HE2_Instance* heInstance = nullptr;

	HE2_Model* cubeModel = nullptr;
	HE2_Model* planetModel = nullptr;
	HE2_Model* treeModel = nullptr;

	HE2_Model *spaceShip = nullptr;

	HE2_ObjectInstance cube1;

	HE2_ObjectInstance cube2;

	HE2_ObjectInstance skybox;

	HE2_ObjectInstance tree;

	HE2_SceneCreator* testScene = nullptr;

	CameraWire* camBeh = nullptr;

	FPSUIElement* fps = nullptr;
	FlameGraphBehaviour* flame = nullptr;

	SceneGraphDisplayBehaviour* sceneGraph = nullptr;

	try
	{
		pipeline = new HE2_VulkanBackend();

		heInstance = new HE2_Instance(pipeline);

		heInstance->Init();
	
		cubeModel = HE2_MeshManager::importModel("models/cubeSimple.obj");

		treeModel = HE2_MeshManager::importModel("models/tree_a.obj");

		planetModel = HE2_MeshManager::importModel("models/geosphere.obj");
		//spaceShip = HE2_MeshManager::importModel("models/transport shuttle_obj.obj", {false, false});


		HE2_BlinnPhongMaterial* grassyMaterial = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage("Textures/grass02.jpg"), HE2_TextureManager::getImage("Textures/grass02_s.jpg"), HE2_TextureManager::getImage("Textures/grass02_n.jpg",true), HE2_Instance::renderBackend->deferredShader);


		HE2_BlinnPhongMaterial* starsMaterial = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage("Textures/stars.jpg"), HE2_TextureManager::getImage("Textures/Grass02_s.jpg"), HE2_TextureManager::getImage("Textures/blankNormal.png"),HE2_Instance::renderBackend->deferredShader);
			
		cube1 = HE2_ObjectInstance(cubeModel, cubeModel->defaultMaterial, glm::vec3(0, 0, -15), glm::vec3(1) );



		std::vector<std::type_index> types2 = { typeid(HE2_ShaderBufferGetter) };

		cube2 = HE2_ObjectInstance(cubeModel, cubeModel->defaultMaterial, glm::vec3(0, 20, 15), glm::vec3(1.2));

		skybox = HE2_ObjectInstance(StandardAssets::skybox, grassyMaterial, glm::vec3(0, 0, 0), glm::vec3(100));

		tree = HE2_ObjectInstance(treeModel, grassyMaterial, glm::vec3(0,0,0), glm::vec3(1));

		std::vector<HE2_ObjectInstance*> objects;
		objects.push_back(&cube1);
		objects.push_back(&cube2);
		objects.push_back(&skybox);
		objects.push_back(new HE2_ObjectInstance(planetModel, planetModel->defaultMaterial, glm::vec3(-14,0,0),glm::vec3(1)));
		objects.push_back(new HE2_ObjectInstance(planetModel, planetModel->defaultMaterial, glm::vec3(14,0,0),glm::vec3(1)));

		testScene = new HE2_SceneCreator("Test Scene", objects);

		HE2_SceneCreator::CreateSceneObjects(testScene);

		//NormalDisplayUpdater* ndu = new NormalDisplayUpdater(cube1normal.generatedObject);

		//ndc->object = cube1normal.generatedObject;
		
		//camBeh = new CameraWire(testScene->camera);
		SimpleCameraKeyboardMove* sckm = new SimpleCameraKeyboardMove(HE2_Camera::main);

		AddObjectOnClick* aooc = new AddObjectOnClick(cubeModel, treeModel);

		testScene->camera->setPos(glm::vec3(23, 0, -128));

		fps = new FPSUIElement();

		flame = new FlameGraphBehaviour();

		sceneGraph = new SceneGraphDisplayBehaviour();

		new ConsoleUI();

		heInstance->RunGame();
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}

	if (pipeline) delete pipeline;
	if (heInstance) delete heInstance;
	

	while (true);
	return 0;
}