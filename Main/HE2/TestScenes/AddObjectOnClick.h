#pragma once
#include <HE2.h>
class AddObjectOnClick : public HE2_GlobalBehaviour
{
public:
	AddObjectOnClick(HE2_Model* model, HE2_Model* model2);

	void update();
private:
	HE2_Model* model = nullptr;
	HE2_Model* model2 = nullptr;

	float ticker = 0.0f;

	std::vector<HE2_Object*> objects;
};

