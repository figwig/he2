#pragma once
#include <HE2.h>
class UpAndDown : public HE2_ObjectBehaviour
{
public:
	UpAndDown(HE2_Object* host) : HE2_ObjectBehaviour(host) {}

	void update()
	{
		object->setRot(object->getRot() + (Time::deltaTime * glm::vec3(0, 0.7f, 0.0f)));
	}
};


class CameraWire : public HE2_ObjectBehaviour {
public:
	CameraWire(HE2_Object* cam) : HE2_ObjectBehaviour(cam)
	{
		object = cam;
	}
	void update()
	{
		float x = std::sin(Time::time);

		object->setPos(glm::vec3(50 + x * 10, 80, 00));
	}
};
