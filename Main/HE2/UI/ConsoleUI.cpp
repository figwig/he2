#include "ConsoleUI.h"
#include <imgui.h>
#include <HE2_Debug.h>
#include <iomanip>
#include <iostream>
#include <chrono>
#include <ctime>
#include <sstream>

std::string ConsoleUI::time_format = { "%H:%M:%S" };


static int TextEditCallbackStub(ImGuiInputTextCallbackData* data) // In C++11 you are better off using lambdas for this sort of forwarding callbacks
{
    ConsoleUI* console = (ConsoleUI*)data->UserData;
    return console->TextEditCallback(data);
}

void ConsoleUI::update()
{
	bool open = false;

	ticker -= Time::deltaTime;
	if (ticker < 0.0f && glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_GRAVE_ACCENT))
	{
		ticker = 1.0f;
		show = !show;

        if (show)
        {
            focus = true;
        }
	}


	if (!show) return;
	ImGui::Begin("Console", &open, 0);


	const float footer_height_to_reserve = ImGui::GetStyle().ItemSpacing.y + ImGui::GetFrameHeightWithSpacing();

	ImGui::BeginChild("ScrollingRegion", ImVec2(0, -footer_height_to_reserve), false, ImGuiWindowFlags_HorizontalScrollbar);

	ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1));

	int firstMessage = glm::max(0,((int)HE2_Debug::messages.size() - MAX_CONSOLE_ENTRIES));

	for (int i = firstMessage; i < HE2_Debug::messages.size(); i++)
	{

		std::string fullString = ( HE2_Debug::messages[i].message);

		const char* item = fullString.c_str();


		// Normally you would store more information in your item (e.g. make Items[] an array of structure, store color/type etc.)
		bool pop_color = false;
		if (strstr(item, "[error]")) 
		{ 
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.4f, 0.4f, 1.0f)); 
			pop_color = true; 
		}

		else if (strncmp(item, "# ", 2) == 0) 
		{ 
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.8f, 0.6f, 1.0f)); 
			pop_color = true; 
		}

		std::string time = HE2_Debug::messages[i].timeAsString;

		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.6f, 0.6f, 0.6f, 1.0f));
		ImGui::TextUnformatted((" " + time).c_str());
		ImGui::PopStyleColor();

		ImGui::SameLine();
		ImGui::TextUnformatted(item);

		if (pop_color)
			ImGui::PopStyleColor();
	}


	if ( ImGui::GetScrollY() >= ImGui::GetScrollMaxY())
		ImGui::SetScrollHereY(1.0f);

	ImGui::PopStyleVar();

	ImGui::EndChild();
    if (focus)
    {
        ImGui::SetKeyboardFocusHere(0);
        focus = false;
    }
	bool reclaim_focus = false;
	if (ImGui::InputText("Input", InputBuf, IM_ARRAYSIZE(InputBuf), ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_CallbackCompletion | ImGuiInputTextFlags_CallbackHistory,&TextEditCallbackStub, (void*)this))
	{
		char* s = InputBuf;
		Strtrim(s);
        if (s[0])
        {
            ExecCommand(s);
        }
		strcpy(s, "");
		reclaim_focus = true;
	}


	ImGui::End();

}

void ConsoleUI::ExecCommand(const char* command_line)
{
    DebugLog(command_line);

    // Insert into history. First find match and delete it so it can be pushed to the back. This isn't trying to be smart or optimal.
    historyPos = -1;
    for (int i = history.size() - 1; i >= 0; i--)
        if (history[i] == command_line)
        {
           // free(History[i]);
            history.erase(history.begin() + i);
            break;
        }
    history.push_back((command_line));

    HE2_Debug::executeCommand(command_line);

    //Replace this with a command object.

    // Process command
    
    //ScrollToBottom = true;
}

int  ConsoleUI::TextEditCallback(ImGuiInputTextCallbackData* data)
{
    //AddLog("cursor: %d, selection: %d-%d", data->CursorPos, data->SelectionStart, data->SelectionEnd);
    //switch (data->EventFlag)
    //{
    //case ImGuiInputTextFlags_CallbackCompletion:
    //{
    //    // Example of TEXT COMPLETION

    //    // Locate beginning of current word
    //    const char* word_end = data->Buf + data->CursorPos;
    //    const char* word_start = word_end;
    //    while (word_start > data->Buf)
    //    {
    //        const char c = word_start[-1];
    //        if (c == ' ' || c == '\t' || c == ',' || c == ';')
    //            break;
    //        word_start--;
    //    }

    //    // Build a list of candidates
    //    ImVector<const char*> candidates;
    //    for (int i = 0; i < Commands.Size; i++)
    //        if (Strnicmp(Commands[i], word_start, (int)(word_end - word_start)) == 0)
    //            candidates.push_back(Commands[i]);

    //    if (candidates.Size == 0)
    //    {
    //        // No match
    //        AddLog("No match for \"%.*s\"!\n", (int)(word_end - word_start), word_start);
    //    }
    //    else if (candidates.Size == 1)
    //    {
    //        // Single match. Delete the beginning of the word and replace it entirely so we've got nice casing
    //        data->DeleteChars((int)(word_start - data->Buf), (int)(word_end - word_start));
    //        data->InsertChars(data->CursorPos, candidates[0]);
    //        data->InsertChars(data->CursorPos, " ");
    //    }
    //    else
    //    {
    //        // Multiple matches. Complete as much as we can, so inputing "C" will complete to "CL" and display "CLEAR" and "CLASSIFY"
    //        int match_len = (int)(word_end - word_start);
    //        for (;;)
    //        {
    //            int c = 0;
    //            bool all_candidates_matches = true;
    //            for (int i = 0; i < candidates.Size && all_candidates_matches; i++)
    //                if (i == 0)
    //                    c = toupper(candidates[i][match_len]);
    //                else if (c == 0 || c != toupper(candidates[i][match_len]))
    //                    all_candidates_matches = false;
    //            if (!all_candidates_matches)
    //                break;
    //            match_len++;
    //        }

    //        if (match_len > 0)
    //        {
    //            data->DeleteChars((int)(word_start - data->Buf), (int)(word_end - word_start));
    //            data->InsertChars(data->CursorPos, candidates[0], candidates[0] + match_len);
    //        }

    //        // List matches
    //        AddLog("Possible matches:\n");
    //        for (int i = 0; i < candidates.Size; i++)
    //            AddLog("- %s\n", candidates[i]);
    //    }

    //    break;
    //}
    //case ImGuiInputTextFlags_CallbackHistory:
    //{
    //    // Example of HISTORY
    //    const int prev_history_pos = HistoryPos;
    //    if (data->EventKey == ImGuiKey_UpArrow)
    //    {
    //        if (HistoryPos == -1)
    //            HistoryPos = History.Size - 1;
    //        else if (HistoryPos > 0)
    //            HistoryPos--;
    //    }
    //    else if (data->EventKey == ImGuiKey_DownArrow)
    //    {
    //        if (HistoryPos != -1)
    //            if (++HistoryPos >= History.Size)
    //                HistoryPos = -1;
    //    }

    //    // A better implementation would preserve the data on the current input line along with cursor position.
    //    if (prev_history_pos != HistoryPos)
    //    {
    //        const char* history_str = (HistoryPos >= 0) ? History[HistoryPos] : "";
    //        data->DeleteChars(0, data->BufTextLen);
    //        data->InsertChars(0, history_str);
    //    }
    //}
    //}
    //return 0;

    return 0;
}
