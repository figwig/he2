#pragma once
#include <HE2.h>
#include <iosfwd>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <string>
#include <cstdint>
#include <imgui.h>

class ConsoleUI : public HE2_GlobalBehaviour
{
public:
    static std::string time_format;
    
    void set_time_format(const std::string& format)
    {
        time_format = format;
    }
    
	void update();
    int TextEditCallback(ImGuiInputTextCallbackData* data);

private:
    void ExecCommand(const char* command_line);

    int MAX_CONSOLE_ENTRIES = 40;

    bool show = false;

    bool focus = false;

    float ticker = 0.0f;

    char                  InputBuf[256];

    static void  Strtrim(char* str) { char* str_end = str + strlen(str); while (str_end > str&& str_end[-1] == ' ') str_end--; *str_end = 0; }

    int historyPos = 0;

    std::vector<std::string> history;
};


