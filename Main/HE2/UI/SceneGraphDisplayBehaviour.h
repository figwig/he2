#pragma once
#include <HE2.h>
class SceneGraphDisplayBehaviour : public HE2_GlobalBehaviour
{
public:
	SceneGraphDisplayBehaviour();
	~SceneGraphDisplayBehaviour();

	void update();
private:
	bool open = true;
	void renderListForItem(HE2_Object*);

	void ifClickMoveTo(HE2_Object * obj);
};

