#pragma once
#include <HE2_GLM.h>
#include <string>
#include <imgui.h>
class GenericUIElements
{

public:
	static void Vec2Slider(std::string, glm::vec2&, glm::vec2 low, glm::vec2 high, bool newWindow);
	static void Vec4Slider(std::string, glm::vec4&, glm::vec4 low, glm::vec4 high, bool newWindow);

	static void Vec4ReadOut(std::string, glm::vec4&);
	static void Vec3ReadOut(std::string, glm::vec3&);
	static void Vec2ReadOut(std::string, glm::vec2&);
};

