#include "FlameGraphBehaviour.h"
#include "imgui_widget_flamegraph.h"
#include <HE2_Profiler.h>
#include <HE2_Time.h>
#include "windows.h"
#include "psapi.h"

void FlameGraphBehaviour::update()
{
	bool open;
	ImGui::Begin("Debug View", &open, ImGuiWindowFlags_NoDocking);

	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", Time::readableDeltaTime * 1000.0f, 1.0f / Time::readableDeltaTime);

	ImGuiWidgetFlameGraph::PlotFlame("Profiler", &HE2_Profiler::GetSomeValues, &HE2_Profiler::entries[HE2_Profiler::GetCurrentEntryIndex()],HE2_Profiler::StageCount , 0, "Main Thead", 0.0f, FLT_MAX, ImVec2(600, 0));


	//Ram Used by Me
	PROCESS_MEMORY_COUNTERS pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	SIZE_T virtualMemUsedByMe = pmc.PagefileUsage;


	//Total RAM Used system wide
	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);
	DWORDLONG physMemUsed = memInfo.ullTotalPhys - memInfo.ullAvailPhys;

	ImGui::Text(("RAM Usage: " + std::to_string(virtualMemUsedByMe/1000000) + " MB").c_str());

	ImGui::End();

}