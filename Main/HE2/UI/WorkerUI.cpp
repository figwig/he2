#include "WorkerUI.h"
#include <imgui.h>
#include  <HE2_JobCentre.h>

void WorkerUI::update()
{
	ImGui::Begin("Worker Threads");

	HE2_JobCentre* jc = HE2_JobCentre::singleton();

	ImGui::Text("Worker 0: Main Thread");

	for (int i = 0; i < jc->workers.size(); i++)
	{
		std::string job = jc->workers[i]->currentJobName;
		//ImGui::Text(("Worker " + std::to_string(i + 1) + ": " + job).c_str());

		ImGui::Text("Worker");
		ImGui::SameLine();
		ImGui::Text(std::to_string(i + 1).c_str());
		ImGui::SameLine();
		ImGui::Text(":");
		ImGui::SameLine();
		ImGui::Text(job.c_str());
	}

	ImGui::End();
}