#include "GUITexture.h"
#include <imgui.h>

void GUITexture::update()
{
	ImGui::Begin(name.c_str());

	ImGui::Image(textureID, ImVec2(200, 200));

	ImGui::End();
}
