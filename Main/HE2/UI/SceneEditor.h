#pragma once
#include "SceneGraphDisplayBehaviour.h"
#include "FlameGraphBehaviour.h"
#include "WorkerUI.h"
#include "GUITexture.h"
#include <HE2.h>
#include<filesystem>

class SceneEditor : public HE2_GlobalBehaviour
{
public:
	void update();
	SceneEditor();
private:
	void ShowDirectory(const std::string& path, const std::vector<std::string>& content, ImGuiTextFilter* filter, bool asDetail);

	void displayFileDetails(std::filesystem::path path);

	HE2_Image* noImage = nullptr;

	static std::vector<std::string> includedExtensions ;
};

