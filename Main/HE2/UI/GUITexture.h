#pragma once
#include <HE2.h>

class GUITexture : public HE2_GlobalBehaviour
{
public:
	GUITexture(ImTextureID textureID, std::string name):textureID(textureID),name(name) {}
	GUITexture(HE2_Image * image, std::string name) : name(name)
	{
		textureID = HE2_Instance::renderBackend->getImGuiTextureHandle(image);
	}
	~GUITexture() {}

	void update();

private:

	ImTextureID textureID = 0;
	std::string name;

};

