#include "SceneEditor.h"
#include <filesystem>
#include <string>

std::vector<std::string> SceneEditor::includedExtensions = { ".obj" };


SceneEditor::SceneEditor()
{
	noImage = HE2_TextureManager::getImage("Textures/NoImageLoaded.png");

	HE2_Instance::renderBackend->getImGuiTextureHandle(noImage);
}


void SceneEditor::update()
{
	std::string path = "models/";

	ImGui::Begin("Model Browser");

    std::vector<std::string> content;

	for (const auto& entry : std::filesystem::directory_iterator(path))
	{
		//displayFileDetails(entry.path());
        content.push_back(entry.path().string());
	}

    ShowDirectory(path, content, nullptr, true);

	ImGui::End();
}

void SceneEditor::displayFileDetails(std::filesystem::path path)
{
	//Get the model details
	if (path.extension().string() == ".obj" || path.extension().string() == ".fbx")
	{
		HE2_Model* model = HE2_MeshManager::importModel(path.string());

		if (model->previewImage == nullptr)
		{
			ImGui::Image(noImage->imGuiHandle, ImVec2(70, 70));
		}

		ImGui::Text(model->name.c_str());

		
	}
}

void SceneEditor::ShowDirectory(const std::string& path, const std::vector<std::string>& content, ImGuiTextFilter* filter, bool asDetail)
{
    //auto imElem = BlockExe::GetInst()->GetImGuiElem();

    float contentWidth = ImGui::GetContentRegionAvailWidth();
    float contentHeight = ImGui::GetContentRegionAvail().y;

    ImGui::BeginChild("##ContentRegion", { contentWidth, contentHeight }, true);

    const float w = contentWidth;
    const float itemSize = asDetail ? 48 : 128;

    int index = 0;
    int columns = contentWidth / (asDetail ? 256 : 128);
    columns = columns < 1 ? 1 : columns;

    float curX = 0.0f;
    ImGui::Columns(columns, nullptr, false);

    static std::string assetPopupPath;

    for (size_t i = 0; i < content.size(); ++i)
    {
        std::string item = content[i];
        std::string itemPath = path + "/" + item;
        if (item == "." || item == "..")
            continue;
        bool cancel = true;
        for (auto include : includedExtensions)
            if (item.substr(item.size() -4) == include)
                cancel = false;
        if (cancel)
            continue;

        //if (!filter->PassFilter(item.c_str()))
        //    continue;

        ImGui::PushID(i);

        // filtering makes this really really tricky to cache
        ImTextureID thumb;
        HE2_Model* model = HE2_MeshManager::importModel(item);

        if (model->previewImage == nullptr)
        {
            thumb = noImage->imGuiHandle;
        }

       // ImGui::Text(model->name.c_str());

        std::string filName = model->name;

        ImGui::BeginGroup();
        ImGui::ImageButton(thumb, { itemSize - 20, itemSize - 16 });// , { 0, 1 }, { 1,0 });
        //if (ImGui::IsMouseDoubleClicked(0) && ImGui::IsItemHovered()) // change target directory
        //{
        //    if (IsFile(itemPath))
        //    {
        //        if (!DocumentManager::Get()->OpenPath(itemPath))
        //            BlockExe::GetInst()->GetContext()->GetSubsystem<FileSystem>()->SystemOpen(itemPath);
        //    }
        //    else
        //        directory_ = itemPath;
        //}
        //else if (ImGui::IsMouseClicked(1) && ImGui::IsItemHovered())
        //{
        //    ImGui::OpenPopup("##asset_browser_popup");
        //    assetPopupPath = itemPath;
        //}
        //else if (ImGui::IsItemHovered() && asDetail && filName.Contains('.'))
        //{
        //    ImGui::BeginTooltip();
        //    ImGui::Image(thumb.Get(), { 128, 128 });
        //    ImGui::EndTooltip();
        //}

        //if (ImGui::BeginPopup("##asset_browser_popup"))
        //{
        //    auto ext = Urho3D::GetExtension(assetPopupPath);
        //    auto found = fileHandlers_.Find(ext);
        //    if (found != fileHandlers_.End())
        //        found->second_(assetPopupPath);
        //    else
        //    {
        //        if (ImGui::MenuItem(ICON_FA_EDIT " Open (system)"))
        //            BlockExe::GetInst()->GetSubsystem<FileSystem>()->SystemOpen(assetPopupPath);
        //    }

        //    // Script extension point
        //    auto& eventData = BlockExe::GetInst()->GetEventDataMap();
        //    eventData["SelectedAssetPath"] = assetPopupPath;
        //    eventData["SelectedAssetExt"] = ext;
        //    if (ext == ".xml")
        //    {
        //        XMLFile file(BlockExe::GetInst()->GetContext());
        //        if (file.LoadFile(assetPopupPath))
        //            eventData["XmlRoot"] = file.GetRoot().GetName().ToLower();
        //    }
        //    BlockExe::GetInst()->SendEvent("ASSET_BROWSER_CONTEXT", eventData);

        //    ImGui::EndPopup();
        //}

        if (asDetail)
            ImGui::SameLine();
        ImGui::TextWrapped(filName.c_str());
        if (ImGui::IsItemHovered() && !asDetail)
            ImGui::SetTooltip(filName.c_str());
        ImGui::EndGroup();

        //if (!ImGui::IsPopupOpen((ImGuiID)0) && ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceAllowNullID))
        //{
        //    if (std::find(recentDirectories_.begin(), recentDirectories_.end(), directory_) == recentDirectories_.end())
        //        recentDirectories_.insert(recentDirectories_.begin(), directory_);

        //    ImGui::SetDragDropPayload("U_RES", itemPath.CString(), itemPath.Length());
        //    ImGui::Image(thumb.Get(), { 128, 128 });
        //    ImGui::Text(filName.CString());
        //    ImGui::EndDragDropSource();
        //}

        ImGui::PopID();
        ImGui::NextColumn();
    }

    ImGui::EndChild();
}