#include "SceneGraphDisplayBehaviour.h"
#include <HE2_Scene.h>
#include <imgui.h>
#include <HE2_Debug.h>
SceneGraphDisplayBehaviour::SceneGraphDisplayBehaviour()
{

}

SceneGraphDisplayBehaviour::~SceneGraphDisplayBehaviour()
{

}

void SceneGraphDisplayBehaviour::update()
{

	ImGui::Begin("Scene Heirarchy", &open);
	if (!open)
	{
		ImGui::End();
		return;
	}
	std::vector<HE2_Object*> objects = HE2_Scene::currentScene->sceneGraph.getRootNodes();

	for (auto x : objects)
	{
		renderListForItem(x);
	}

	ImGui::End();
}

void SceneGraphDisplayBehaviour::renderListForItem(HE2_Object* o)
{
	std::string name = o->name;
	if (name == "") name = "Unnamed Object";

	if (o->getChildren().size() == 0)
	{
		//ImGui::Indent(ImGui::GetTreeNodeToLabelSpacing());

		if (!o->active)
			ImGui::PushStyleColor(ImGuiCol_Header, ImVec4(0.1, 0.1, 0.3, 1.0));

		ImGui::CollapsingHeader(name.c_str());
		ifClickMoveTo(o);

		if (!o->active)
			ImGui::PopStyleColor();


		//ImGui::Unindent(ImGui::GetTreeNodeToLabelSpacing());
	}
	else
	{
		name += "(" + std::to_string(o->getChildren().size()) + ")";


		bool open = ImGui::CollapsingHeader(name.c_str());
		ifClickMoveTo(o);

		if (open)
		{
			ImGui::Indent();
			for (auto x : o->getChildren())
			{
				renderListForItem(x);
			}
			ImGui::Unindent();

			//ImGui::TreePop();
		}

	}


}
//bool BeginPopupContextItem(const char* str_id, int mouse_button)
//{
//	if (ImGui::IsItemHovered() && ImGui::IsMouseClicked(mouse_button))
//		ImGui::OpenPopupEx(str_id, false);
//	return ImGui::BeginPopup(str_id);
//}

void SceneGraphDisplayBehaviour::ifClickMoveTo(HE2_Object * obj)
{
	if (obj == HE2_Camera::main) return;

	if (ImGui::IsMouseHoveringRect(ImGui::GetItemRectMin(), ImGui::GetItemRectMax()))
	{
		if (glfwGetMouseButton(HE2_WindowHandler::window, 0) == GLFW_PRESS)
		{
			HE2_Camera::main->moveToTarget(obj->getPos());
		}
	}
}