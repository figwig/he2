#include "GenericUIElements.h"

void GenericUIElements::Vec2Slider(std::string name, glm::vec2& vec,glm::vec2 low, glm::vec2 high,bool newWindow)
{
	if (newWindow)
		ImGui::Begin(name.c_str());

	ImGui::SliderFloat((name + "x").c_str(),&vec.x,low.x, high.x);
	ImGui::SliderFloat((name + "y").c_str(),&vec.y,low.y, high.y);

	if (newWindow)
		ImGui::End();
}


void GenericUIElements::Vec4Slider(std::string name, glm::vec4& vec, glm::vec4 low, glm::vec4 high, bool newWindow)
{
	if (newWindow)
		ImGui::Begin(name.c_str());

	ImGui::SliderFloat((name + "x").c_str(), &vec.x, low.x, high.x);
	ImGui::SliderFloat((name + "y").c_str(), &vec.y, low.y, high.y);
	ImGui::SliderFloat((name + "z").c_str(), &vec.z, low.z, high.z);
	ImGui::SliderFloat((name + "w").c_str(), &vec.w, low.w, high.w);

	if (newWindow)
		ImGui::End();
}

void GenericUIElements::Vec4ReadOut(std::string title, glm::vec4& x)
{
	ImGui::Text((title + std::to_string(x.x) + ", " + std::to_string(x.y) + ", " + std::to_string(x.z) + ", " + std::to_string(x.w)).c_str());
}

void GenericUIElements::Vec3ReadOut(std::string title, glm::vec3& x)
{
	ImGui::Text((title + std::to_string(x.x) + ", " + std::to_string(x.y) + ", " + std::to_string(x.z)).c_str());
}

void GenericUIElements::Vec2ReadOut(std::string title, glm::vec2& x)
{
	ImGui::Text((title + std::to_string(x.x) + ", " + std::to_string(x.y)).c_str());
}