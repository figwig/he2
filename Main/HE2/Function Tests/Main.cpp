#include <HE2.h>
#include <VulkanBackend.h>
#include <stdexcept>
#include <vector>
#include <tiny_obj_loader.h>
#include <HE2_JobCentre.h>
#include <cstdint>
#include <sstream>
#include <cereal/cereal.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/polymorphic.hpp>
#include <fstream>
#include <sstream>
#include <HE2_Serialize.h>

void primeChecker(int start, int range)
{
	int i, num = start, primes = 0;

	while (num <= start + range) {
		i = 2;
		while (i <= num) {
			if (num % i == 0)
				break;
			i++;
		}
		if (i == num)
			primes++;


		num++;
	}
	printf("%d prime numbers calculated between %d and %d\n", primes, start, range);
}


int main()
{
#pragma region Job Centre Testing
	//HE2_JobCentre* jc = HE2_JobCentre::singleton();

	//std::function <void()> job1 = [=]
	//{
	//	primeChecker(1, 6000000);
	//};
	//std::function <void()> job2 = [=]
	//{
	//	primeChecker(60000, 600000);
	//};
	//std::function <void()> job3 = [=]
	//{
	//	primeChecker(80000, 80000);
	//};
	//std::function <void()> job4 = [=]
	//{
	//	primeChecker(120000, 74000);
	//};

	//std::function <void()> job5 = [=]
	//{
	//	primeChecker(1, 60000);
	//};
	//std::function <void()> job6 = [=]
	//{
	//	primeChecker(60000, 140000);
	//};
	//std::function <void()> job7 = [=]
	//{
	//	primeChecker(80000, 800000);
	//};
	//std::function <void()> job8 = [=]
	//{
	//	primeChecker(120000, 740000);
	//};

	//jc->QueueJob("Test Job1", job1);
	//jc->QueueJob("Test Job2", job2);
	//jc->QueueJob("Test Job2", job3);
	//jc->QueueJob("Test Job2", job4);
	//jc->QueueJob("Test Job2", job5);
	//jc->QueueJob("Test Job2", job6);
	//jc->QueueJob("Test Job2", job7);
	//jc->QueueJob("Test Job2", job8);

	//jc->QueueJob("Test Job3", job);
	//jc->QueueJob("Test Job4", job);


	//jc->waitUntilAllIdle();

	//std::cout << "All threads were idle" << std::endl;

	//jc->release();
#pragma endregion

	HE2_Object* object = new HE2_Object();
	object->setPos(glm::vec3(45342, 32, 1));
	object->getModelMatrix();
	object->name = "Test Object";


	HE2_Serialize::writeObjectToFile(object, "saveFile.sav");

	HE2_Object* object2 = HE2_Serialize::getObjectFromFile("saveFile.sav");

	if (!(*object == *object2))
		std::cout << "Was not the same!" << std::endl;
	else
		std::cout << "Was the same!" << std::endl;

	//while (true)
	//{
	//	std::cout << "Enter Load to load an object, and save to save an Object" << std::endl;

	//	std::string input;

	//	std::cin >> input;

	//	HE2_Object* obj = nullptr;

	//	if (input == "load")
	//	{
	//		obj = new HE2_Object();

	//		std::fstream file("saveTest.sav", std::ios::binary | std::fstream::in);

	//		cereal::BinaryInputArchive iarchive(file); // Create an output archive

	//		iarchive(*obj); // Write the data to the archive

	//		file.close();
	//	}
	//	else if (input == "save")
	//	{
	//		//Create an object and give it some data
	//		obj = new HE2_Object();
	//		obj->setPos(glm::vec3(40, 50, 450));
	//		obj->getModelMatrix();

	//		std::fstream file("saveTest.sav", std::ios::binary | std::fstream::out);

	//		cereal::BinaryOutputArchive oarchive(file); // Create an output archive

	//		oarchive(*obj); // Write the data to the archive

	//		file.close();
	//		
	//	}
	//	else
	//	{
	//		std::cout << " not a valid input" << std::endl;
	//	}


	//	if (obj) delete obj;
	//}

	while (true);

	return 0;
}