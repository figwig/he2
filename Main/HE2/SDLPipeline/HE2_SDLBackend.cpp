#include "HE2_SDLBackend.h"

HE2_SDLBackend::HE2_SDLBackend()
{


    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
    }

    window = SDL_CreateWindow(
        "hello_sdl2",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        1000, 1000,
        SDL_WINDOW_SHOWN
    );
    if (window == NULL) {
        fprintf(stderr, "could not create window: %s\n", SDL_GetError());
    }
    screenSurface = SDL_GetWindowSurface(window);
    SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0xFF, 0xFF, 0xFF));
    SDL_UpdateWindowSurface(window);
    SDL_Delay(2000);

}

HE2_SDLBackend::~HE2_SDLBackend()
{
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void HE2_SDLBackend::onAddObject(HE2_Object* object)
{
}

void HE2_SDLBackend::onAddMaterial(HE2_BlinnPhongMaterial* mat)
{
}

void HE2_SDLBackend::setupAPI()
{
}

void HE2_SDLBackend::render()
{
}

void HE2_SDLBackend::cleanup()
{
}
