#pragma once
#include <HE2.h>
#include <include/SDL.h>

class HE2_SDLBackend : public HE2_RenderBackend
{
public:
	HE2_SDLBackend();
	~HE2_SDLBackend();



	void onAddObject(HE2_Object* object);

	void onAddModel(HE2_Model* model)
	{
		//SDL doesn't use models - ignore this call
	}

	//In SDL - single image materials are used
	void onAddMaterial(HE2_BlinnPhongMaterial* mat);
		

	void prepareGPUTask(HE2_GPUTask*)
	{
		//SDL doesn't support COmpute - ignore this call
	}

	HE2_Image* createEmptyImage(bool, glm::vec2) { throw std::runtime_error("This backend does not support ths feature"); }

	HE2_BufferDataHandle makeGPUBuffer(void* data, size_t size, bool updatable) { throw std::runtime_error("This backend does not support ths feature"); }

	void updateGPUBuffer(HE2_BufferDataHandle, void* data, size_t size) { throw std::runtime_error("This backend does not support ths feature"); }

	void queueGPUTask(HE2_GPUTask* task) { throw std::runtime_error("This backend does not support ths feature"); }

	std::vector<HE2_Image*> getDeferredBuffers() { throw std::runtime_error("This backend does not support ths feature"); }


	void rebuildShader(HE2_Shader* shader) { throw std::runtime_error("This backend does not support ths feature"); }

private:
	void setupAPI();

	void render();

	void cleanup();

	void buildShader(HE2_Shader* shader, bool)
	{
		//SDL doesn't use shaders - ignore this call
	}

	void destroyShader(HE2_Shader*)
	{

	}

	HE2_TextureAllocator* getAllocator()
	{
		return nullptr;
	}

	//SDL Window and surface type stuff
	SDL_Window* window = NULL;
	SDL_Surface* screenSurface = NULL;
};

