#include <HE2.h>
#include <VulkanBackend.h>
#include <iostream>
#include <PlanetScene.h>
#include <FlameGraphBehaviour.h>
#include <TestingBehaviours.h>
#include <FPSUIElement.h>
#include <SceneGraphDisplayBehaviour.h>
#include <ConsoleUI.h>
#include <WorkerUI.h>
#include <PlanetGenerationManager.h>
#include <HE2_JobCentre.h>
#include <FlyingCamera.h>
#include <PlanetRenderingComponent.h>

class WelcomeUI : public HE2_GlobalBehaviour
{
public:
	
	void update()
	{
		if (!closed)
		{
			ImGui::Begin("Welcome!");
			ImGui::TextWrapped("Welcome to this planet generation demo! Use WASD to move, the mouse to look around, and X to swap between local view and map view. Enjoy!");

			if(ImGui::Button("Close"))
			{
				closed = true;
				//fc->switchNextFrame = true;
			}

			ImGui::End();
		}
	}

	bool closed = false;
	FlyingCamera* fc = nullptr;
};


float randomNormalizedFloat()
{
	return (rand() % 1000) / 1000.0f;
}


class Randomize : public HE2_GlobalBehaviour
{
public:
	void update()
	{
		ImGui::Begin("Randomizer");
		if (ImGui::Button("Randomize Planet"))
		{
			srand(Time::time);

			pg->ridgeSeed = rand() % 100;
			pg->pub.radius = (rand() % 100) + 100;
			pg->pub.noiseOffset.x = (rand() % 10000) + 0;
			pg->pub.oceanLevel = pow(rand() % 180 + 10, 2) / 100.0f;

			pg->planetObject->getComponent<PlanetRenderingComponent>()->fib.baseColourMultiplier = glm::vec4(randomNormalizedFloat(), randomNormalizedFloat(), randomNormalizedFloat(), randomNormalizedFloat());

			pgm->generatePlanet();
		}
		ImGui::End();
	}

	AgentPlanetGenerator* pg = nullptr;
	PlanetGenerationManager* pgm = nullptr;
};

int main()
{
	HE2_Model* cubeModel = nullptr;
	HE2_Model* planetModel = nullptr;

	HE2_ObjectInstance cube1;

	HE2_ObjectInstance cube2;

	HE2_SceneCreator* testScene = nullptr;

	CameraWire* camBeh = nullptr;

	FlameGraphBehaviour* flame = nullptr;
	try {
		HE2_InstanceSettings Is = { "Will Hain Terrain Assignment", true };

		HE2_VulkanBackend* vulkanBackend = new HE2_VulkanBackend();
		HE2_Instance* instance = new HE2_Instance(vulkanBackend, Is);

		instance->Init();

		PlanetScene* ps = new PlanetScene();
		ps->init();
		ps->makeShaders();
		ps->loadAllObjects();

		//ps->pgm->setRadiusSeed(65);
		ps->pgm->pg->ridgeSeed = 45535;

		ps->pgm->firstGenerate();
		
		Randomize  * r = new Randomize();

		r->pgm = ps->pgm;
		r->pg = ps->pgm->pg;

		SceneGraphDisplayBehaviour* sceneGraph = new SceneGraphDisplayBehaviour();

		ConsoleUI* consoleUI = new ConsoleUI();

		WelcomeUI* ui = new WelcomeUI();

		flame = new FlameGraphBehaviour();

		ui->fc = ps->fc;
		instance->RunGame();

		HE2_JobCentre::singleton()->release();

		glfwTerminate();

		delete sceneGraph;
		delete vulkanBackend;
		delete ui;
		delete consoleUI;
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;

		glfwTerminate();
		while (true);
	}
	catch (...)
	{
		std::cout << "Unknown Execption" << std::endl;
		glfwTerminate();
		while (true);
	}

	return 0;
}