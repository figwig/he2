#include "NormalDisplayShader.h"
NormalDisplayShader* NormalDisplayShader::thisPointer = nullptr;

NormalDisplayShader* NormalDisplayShader::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new NormalDisplayShader();
	}
	return thisPointer;
}


NormalDisplayShader::NormalDisplayShader()
{
	shaderSources.vertexShaderSource = vertString;
	shaderSources.fragmentShaderSource = fragString;
	shaderSources.geometryShaderSource = geomString;

	HE2_ShaderFeature feature = {};

	vertexInputDesc = HE2_Vertex::getAttributeDescriptionsHE2();

	perVertexDataDesc.stride = sizeof(HE2_Vertex);

	HE2_ShaderFeature vpMat = viewProjectionFeature;
	//The vp feature in this is in the geometry shader
	vpMat.stage = HE2_ShaderStage::HE2_SHADER_STAGE_GEOMETRY;
	HE2_ShaderFeature modelMat = modelMatrixUniformFeature;
	modelMat.stage = HE2_ShaderStage::HE2_SHADER_STAGE_GEOMETRY;
	features.push_back(modelMat);

	features.push_back(vpMat);

	setupShader();
}