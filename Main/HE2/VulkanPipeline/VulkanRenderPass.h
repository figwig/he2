#pragma once
#include <vulkan/vulkan.h>
#include <vector>
#include <HE2.h>
#include "VulkanImageReference.h"
#include "VulkanAllocator.h"
#include <HE2_RenderRun.h>
#include "ResourceGraph.h"
#include <HE2_RenderComponent.h>

class HE2_VulkanBackend;

class VulkanRenderRun : public HE2_RenderRun
	//A render run is a set of the following steps:
		//Name								Output
		//Deferred Render Pass				Offscreen Buffers (x3)
		//Deferred Resolve Render Pass		MSImage
		//Immediate Render Pass				MSImage	
		//Mutlisampl Resolve				OutputImage
{
public:
	VulkanRenderRun(HE2_VulkanBackend* backend, HE2_RenderContext * context) :backend(backend), HE2_RenderRun(context)
	{
		
	}

	bool doesWantObject(HE2_RenderComponent* rc) {
		bool iWant = true;

		//Check against render flags and context
		
		if (renderContext != rc->renderContext) iWant = false;

		if (parentRun != nullptr) 
			return iWant && parentRun->doesWantObject(rc);
		else 
			return iWant;
	}

	void addObject(HE2_Object* object)
	{
		runGraph->onAddObject(object);
	}

	void createResources(int swapchainImageCount, VkFormat swapchainFormat, VkSampleCountFlagBits msaaSamples, glm::vec2 swapchainExtent);

	void createAttachment(VulkanImageReference& ref, VkImageLayout layout, VkImageUsageFlags usage, VkFormat format, glm::vec2 extent, VkSampleCountFlagBits msaaSamples, VkImageAspectFlagBits aspect = VK_IMAGE_ASPECT_COLOR_BIT);

	void cleanupResources();

	std::vector<VkCommandBuffer> deferredCommandBuffers;
	std::vector<VkCommandBuffer> resolveCommandBuffers;
	std::vector<VkCommandBuffer> forwardLitCommandBuffers;

	std::vector<VkFramebuffer> deferredRenderFramebuffers;
	std::vector<VkFramebuffer> deferredResolveFramebuffers;
	std::vector<VkFramebuffer> forwardLitFramebuffers;

	std::vector<VkDescriptorSet> deferredDescriptorSets;
	
	VulkanImageReference defAlbedoRef;

	VulkanImageReference defNormalRef;

	VulkanImageReference defPositionRef;

	VulkanImageReference depthRef;

	VulkanImageReference MSImageRef;

	VulkanImageReference outputRef;

	VulkanAllocator* vulkanAlloc = nullptr;

	bool renderToScreen = true;

	VulkanResourceGraph* runGraph = nullptr;
private:
	HE2_VulkanBackend* backend = nullptr;

};

