#pragma once
#include <vulkan/vulkan.h>
#include <HE2_Component.h>
#include "VulkanPipeline.h"
#include <HE2_RenderComponent.h>
#include <HE2_Object.h>
#include "vk_mem_alloc.h"

struct DescriptorSetCollection
{
	std::vector<VkDescriptorSet> sets;
};


class VulkanObjectAttachment : public HE2_ComponentBase
{
public:
	HE2_RenderComponent* renderComponent = nullptr;

	//These are organized as (index * swapChainImages.size()) + imageIndex
	std::vector<VkDescriptorSet> specificFeatureDescriptorSets;
	VulkanPipeline* shaderPipeline;

	int ID = 0;

	HE2_Object* object = nullptr;

	HE2_UniformValueGetter* pushConstantValue = nullptr;

};



