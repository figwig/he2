#pragma once
#include <HE2.h>
class NormalDisplayShader : public HE2_Shader
{
public:
	static NormalDisplayShader* getInstance();

	struct MVPMatrixBuffer
	{
		glm::mat4 mvp;
	};

private:
	static NormalDisplayShader* thisPointer;

	NormalDisplayShader();

	std::string vertString = "shaders/planets/normals.vert.spv";
	std::string fragString = "shaders/planets/normals.frag.spv";
	std::string geomString = "shaders/planets/normals.geom.spv";
	
};

