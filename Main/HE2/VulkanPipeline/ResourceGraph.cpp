#include "ResourceGraph.h"
#include <HE2_RenderPass.h>
#include <HE2_RenderComponent.h>
#include <iostream>
#include <HE2_Object.h>
#include "VulkanObjectAttachment.h"
#include <HE2_Material.h>
#include "VulkanModelAttachment.h"
#include <HE2_Model.h>
#include "VulkanMaterialAttachment.h"


void VulkanResourceGraph::setupBasePasses()
{
	//Allocate all of the Pass and Render data classes
	preRenderPasses = new RenderData();
	allRenders = new RenderData();

	opaquePass = new PassData();
	deferredOpaquePass = new PassData();
	transparentPass = new PassData();
	deferredTransparentPass = new PassData();
	GUIpass = new PassData();


	//Setup master pass and masterpass data (used to store all objects in scene organised
	allRendersPass = new PassData();

	allRendersPass->name = "Master Pass";

	screenBuffer = new HE2_ScreenBuffer(HE2_PassFlagTypes::AllObjects | HE2_PassFlagTypes::GUI1 | HE2_PassFlagTypes::GUI2);

	allRendersPass->commonPart = screenBuffer;

	allRenders->contents.push_back(allRendersPass);

	//Add the opaque pass first with just opaque objects
	allRenders->contents.push_back(opaquePass);

	opaquePass->commonPart = new HE2_ScreenBuffer(HE2_PassFlagTypes::OpaqueObjects | HE2_PassFlagTypes::Reflective);

	opaquePass->name = "Opaque Pass";

	//Next, the transparent pass
	allRenders->contents.push_back(transparentPass);

	transparentPass->commonPart = new HE2_ScreenBuffer(HE2_PassFlagTypes::Transparent);

	transparentPass->name = "Transparent Pass";

	//Add the opaque pass first with just opaque objects
	allRenders->contents.push_back(deferredOpaquePass);

	deferredOpaquePass->commonPart = new HE2_ScreenBuffer(HE2_PassFlagTypes::OpaqueObjects | HE2_PassFlagTypes::Reflective | HE2_PassFlagTypes::Deferred);

	deferredOpaquePass->name = "Deferred Opaque Pass";

	//Next, the transparent pass
	allRenders->contents.push_back(deferredTransparentPass);

	deferredTransparentPass->commonPart = new HE2_ScreenBuffer(HE2_PassFlagTypes::Transparent | HE2_PassFlagTypes::Deferred);

	deferredTransparentPass->name = "Deferred Transparent Pass";


	//The two GUI passes
	GUIpass->commonPart = new HE2_ScreenBuffer(HE2_PassFlagTypes::GUI1);

	GUIpass->name = "GUI1 Pass";

	allRenders->contents.push_back(GUIpass);


}

void VulkanResourceGraph::destroyPasses()
{
	delete allRenders;
	delete preRenderPasses;

	delete allRendersPass;
	delete opaquePass;
	delete transparentPass;
	delete GUIpass;

	delete screenBuffer;
}


void VulkanResourceGraph::onAddObject(HE2_Object* obj)
{
	passesAddedTo = 0;
	HE2_RenderComponent* rc = obj->getComponent<HE2_RenderComponent>();

	rc->changeFlag = false;

	if (rc == nullptr)
	{
		std::cout << "No render component\n";
		return;
	}

	for (PassData* pd : allRenders->contents)
	{
		addToPassData(pd, obj);
	}

	for (PassData* pd : preRenderPasses->contents)
	{
		addToPassData(pd, obj);
	}

	//for  (auto pair : shadowData)
	//{
	//	PassData* pd = pair.second;
	//	addToPassData(pd, go);
	//}

	//std::cout << "Added " << obj->name << " to ResourceGraph in " << passesAddedTo << " passes\n\n";
}

void VulkanResourceGraph::addToPassData(PassData* pd, HE2_Object* go)
{
	HE2_RenderComponent* rc = go->getComponent<HE2_RenderComponent>();

	//First check if this pass wants this object
	HE2_PassFlags pf = pd->commonPart->flagsWantedByPass;

	//If the deferred flag doesn't match, return
	if ((rc->renderType & HE2_PassFlagTypes::Deferred) != (pf & HE2_PassFlagTypes::Deferred))
	{
		return;
	}

	//if all objects and neither GUI1 or 2 add to any pass that wants all objects
	if (pf & HE2_PassFlagTypes::AllObjects &&
		!((rc->renderType & HE2_PassFlagTypes::GUI1) || (rc->renderType & HE2_PassFlagTypes::GUI2)))
	{
		//std::cout << "Added to an AllObjects Pass, pass name " << pd->name << "\n";
	}
	else if ((pf & HE2_PassFlagTypes::OpaqueObjects) && (rc->renderType & HE2_PassFlagTypes::OpaqueObjects))
	{
		//std::cout << "Added to an Opaque Pass, pass name " << pd->name << "\n";
	}
	else if (pf & HE2_PassFlagTypes::Transparent && rc->renderType & HE2_PassFlagTypes::Transparent)
	{
		//std::cout << "Added to an Transparent Pass, pass name " << pd->name << "\n";
	}
	else if (pf & HE2_PassFlagTypes::Reflective && rc->renderType & HE2_PassFlagTypes::Reflective)
	{
		//std::cout << "Added to an reflective Pass, pass name " << pd->name << "\n";
	}
	else if (pf & HE2_PassFlagTypes::GUI1 && rc->renderType & HE2_PassFlagTypes::GUI1)
	{
		//std::cout << "Added to an GUI1 Pass, pass name " << pd->name << "\n";

	}
	else if (pf & HE2_PassFlagTypes::GUI2 && rc->renderType & HE2_PassFlagTypes::GUI2)
	{
		//std::cout << "Added to an GUI2 Pass, pass name " << pd->name << "\n";

	}
	else
	{
		return;
	}

	//To be replaced with vulkan material attachment
	VulkanObjectAttachment* voa = go->getComponent<VulkanObjectAttachment>();

	//For every shader type already in the pass data, check if this is the shader in use. If not already featured, add it to the passdata's shader list
	ShaderData* correctShader = nullptr;
	for(ShaderData * sd : pd->contents)
	{
		//
		if (voa->shaderPipeline == sd->commonPart)
		{
			//This is the right shader!
			correctShader = sd;
			break;
		}
	}

	if (correctShader != nullptr)
	{
		//The shader is featured this pass! Nothing to be done :)
	}
	else
	{
		//The shader isn't featured in this pass yet!
		correctShader = new ShaderData();
		correctShader->commonPart = voa->shaderPipeline;
		pd->contents.push_back(correctShader);
		//std::cout << "New Shader for "<< pd->name << " \n";
	}

	//If shader is instanced, add it anyway, will never be used in a regular pass but needed for shadows etc
	addToShaderData(correctShader, go);
}

void VulkanResourceGraph::addToShaderData(ShaderData* sd, HE2_Object* go)
{
	//For every material featured in the shader data, check if this is the correct material, and add to it. If the materiala is not found, add a new material data

	MaterialData* correctMaterial = nullptr;
	VulkanMaterialAttachment *vma = go->getComponent<VulkanObjectAttachment>()->renderComponent->getMaterial()->getComponent<VulkanMaterialAttachment>();

	for (MaterialData * md : sd->contents)
	{
		//
		if (vma == md->commonPart)
		{
			//This is the right Material!
			correctMaterial = md;
			break;
		}
	}

	if (correctMaterial != nullptr)
	{
		//The Material is featured this pass! Nothing to be done :)
	}
	else
	{
		//The shader isn't featured in this pass yet!
		correctMaterial = new MaterialData();
		correctMaterial->commonPart = vma;
		sd->contents.push_back(correctMaterial);
	}

	addToMaterialData(correctMaterial, go);
	
}

void VulkanResourceGraph::addToMaterialData(MaterialData* md, HE2_Object* go)
{
	//For every model featured in the shader data, check if this is the correct model, and add to it. If the model is not found, add a new model data
	ModelData* correctModel = nullptr;
	HE2_RenderComponent* rc = go->getComponent<HE2_RenderComponent>();

	for (ModelData * md : md->contents)
	{
		//
		if (rc->getModel()->getComponent<VulkanModelAttachment>() == md->commonPart)
		{
			//This is the right model!
			correctModel = md;
			break;
		}
	}

	if (correctModel != nullptr)
	{
		//The model is featured this pass! Nothing to be done :)
	}
	else
	{
		//The model isn't featured in this pass yet!
		correctModel = new ModelData();
		correctModel->commonPart = rc->getModel()->getComponent<VulkanModelAttachment>();
		md->contents.push_back(correctModel);
	}

	addToModelData(correctModel, go);
}

void VulkanResourceGraph::addToModelData(ModelData* md, HE2_Object* go)
{
	md->contents.push_back(go->getComponent<VulkanObjectAttachment>());
	passesAddedTo++;
}