#pragma once
#include <vulkan/vulkan.h>
#include <stdexcept>
#include <vector>
#include <HE2_Vertex.h>
#include "vk_mem_alloc.h"
#include "VulkanImageReference.h"

class VulkanDeviceInstance;

class VulkanAllocator
{
public:
	VulkanAllocator(VulkanDeviceInstance* vdi, VmaAllocator* alloc) : vdi(vdi), allocator(alloc) {}
	VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels);

	void createEmptyImage(uint32_t texWidth, uint32_t texHeight, uint32_t mipLevels, VkSampleCountFlagBits samples, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VmaAllocation& imageMemory, VmaMemoryUsage memUsage = VMA_MEMORY_USAGE_GPU_ONLY);



	VulkanDeviceInstance *vdi = nullptr;

	VmaAllocator* allocator = nullptr;

	VkFormat findDepthFormat();

	VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

	bool hasStencilComponent(VkFormat format);

	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

	void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VmaMemoryUsage properties, VkBuffer& buffer, VmaAllocation& allocation);

	void createVertexBuffer(VkBuffer& buffer, VmaAllocation& allocation, std::vector<HE2_Vertex>* vertices);

	void createIndexBuffer(VkBuffer& buffer, VmaAllocation& allocation, std::vector<uint32_t>* indices);

	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);

	void copyImage(VulkanImageReference srcImage, VulkanImageReference dstImage);

};

