#include "VulkanHelpers.h"
#include "VulkanDeviceInstance.h"
#include "VulkanBackend.h"

VkCommandBuffer beginSingleTimeCommands()
{
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = HE2_VulkanBackend::commandPool;
	allocInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(VulkanDeviceInstance::device, &allocInfo, &commandBuffer);

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	return commandBuffer;
}

void endSingleTimeCommands(VkCommandBuffer commandBuffer)
{
	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	vkQueueSubmit(VulkanDeviceInstance::graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(VulkanDeviceInstance::graphicsQueue);
	vkFreeCommandBuffers(VulkanDeviceInstance::device, HE2_VulkanBackend::commandPool, 1, &commandBuffer);
}


#pragma region Macro Rerouters
uint32_t layoutBindingDescriptorType(uint32_t bindingType)
{
	switch (bindingType)
	{
	case HE2_SHADER_FEATURE_TYPE_SAMPLER2D:
		return VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		break;
	case HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK:
		return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		break;
	case HE2_SHADER_FEATURE_STORAGE_IMAGE:
		return VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		break;

	case HE2_SHADER_FEATURE_STORAGE_BUFFER:
		return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		break;
	default:
		//To allow to use actual vulkan binding names as well as HE2_ ones
		return bindingType;
		break;
	}
}

uint32_t layoutBindingStageFlag(uint32_t bindingType)
{
	switch (bindingType)
	{
	case HE2_SHADER_STAGE_FRAGMENT:
		return VK_SHADER_STAGE_FRAGMENT_BIT;
		break;
	case HE2_SHADER_STAGE_VERTEX:
		return VK_SHADER_STAGE_VERTEX_BIT;
		break;
	case HE2_SHADER_STAGE_TESSC:
		return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
		break;
	case HE2_SHADER_STAGE_TESSE:
		return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
		break;
	case HE2_SHADER_STAGE_COMPUTE:
		return VK_SHADER_STAGE_COMPUTE_BIT;
		break;
	case HE2_SHADER_STAGE_GEOMETRY:
		return VK_SHADER_STAGE_GEOMETRY_BIT;
		break;
	default:
		//To allow to use actual vulkan binding names as well as HE2_ ones
		return bindingType;
		break;
	}
}

uint32_t layoutPipelineStageFlag(uint32_t bindingType)
{
	switch (bindingType)
	{
	case HE2_SHADER_STAGE_FRAGMENT:
		return VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		break;
	case HE2_SHADER_STAGE_VERTEX:
		return VK_PIPELINE_STAGE_VERTEX_SHADER_BIT;
		break;
	case HE2_SHADER_STAGE_TESSC:
		return VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT;
		break;
	case HE2_SHADER_STAGE_TESSE:
		return VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT;
		break;
	default:
		//To allow to use actual vulkan binding names as well as HE2_ ones
		return bindingType;
		break;
	}
}

VkFormat dataInputFormat(uint32_t format)
{
	switch (format)
	{
	case HE2_DATA_INPUT_FLOAT:
		return VK_FORMAT_R32_SFLOAT;
		break;
	case HE2_DATA_INPUT_UINT_32:
		return VK_FORMAT_R32_UINT;
		break;
	case HE2_DATA_INPUT_VEC2:
		return VK_FORMAT_R32G32_SFLOAT;
		break;
	case HE2_DATA_INPUT_VEC3:
		return VK_FORMAT_R32G32B32_SFLOAT;
		break;
	case HE2_DATA_INPUT_VEC4:
		return VK_FORMAT_R32G32B32A32_SFLOAT;
		break;
	default:
		throw std::runtime_error("Unsupported data input format");
		break;
	}

}

VkFormat imageFormat(HE2_ShaderOutputFormat format)
{
	switch (format)
	{
	case HE2_ShaderOutputFormat::HE2_RGBA: return VK_FORMAT_R8G8B8A8_UNORM;
	case HE2_ShaderOutputFormat::HE2_RGBA_HIGHP: 
		return VK_FORMAT_R16G16B16A16_SFLOAT;
		//return VK_FORMAT_R32G32B32A32_SFLOAT;
	case HE2_ShaderOutputFormat::HE2_DEPTH32: throw std::runtime_error("Error: Query vdi for depth format");
	}
}
#pragma endregion

VkDescriptorSetLayout getLayoutFromFeatures(std::vector < HE2_ShaderFeature> features, int grouping)
{
	std::vector<HE2_ShaderFeature> requiredFeatures;
	VkDescriptorSetLayout newLayout = {};
	for (auto a : features)
	{
		//We don't need a layout binding for push constants
		if (a.grouping != grouping)
		{
			continue;
		}
		//Add the feature to the appropriate group
		requiredFeatures.push_back(a);
	}

	std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
	//Create a binding per feature in this group
	for (auto a : requiredFeatures)
	{
		VkDescriptorSetLayoutBinding newBinding = {};
		newBinding.descriptorType = (VkDescriptorType)layoutBindingDescriptorType(a.featureType);
		newBinding.binding = a.binding;
		newBinding.stageFlags = (VkShaderStageFlags)layoutBindingStageFlag(a.stage);
		newBinding.descriptorCount = 1;
		newBinding.pImmutableSamplers = nullptr;
		layoutBindings.push_back(newBinding);
	}

	//Then create a layout based on that binding
	VkDescriptorSetLayoutCreateInfo layoutCreateInfo = {};
	layoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;

	layoutCreateInfo.bindingCount = static_cast<uint32_t>(layoutBindings.size());

	layoutCreateInfo.pBindings = layoutBindings.data();

	if (vkCreateDescriptorSetLayout(VulkanDeviceInstance::device, &layoutCreateInfo, nullptr, &newLayout) != VK_SUCCESS)
	{
		throw std::runtime_error("Failed to create descriptor set layout");
	}

	return newLayout;
}

VkPrimitiveTopology primitiveType(HE2_PrimitiveType type)
{
	switch (type)
	{
	case HE2_PrimitiveType::HE2_TRIANGLE_LIST:
		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		break;
	case HE2_PrimitiveType::HE2_PATCH_LIST:
		return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
		break;
	default:
		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	}
}