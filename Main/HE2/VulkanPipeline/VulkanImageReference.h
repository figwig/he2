#pragma once
#include <HE2.h>
#include <vulkan/vulkan.h>
#include "vk_mem_alloc.h"
#include <HE2_GLM.h>

class VulkanImageReference
{
public:
	HE2_Image* image = nullptr;
	VkImage vkImage;
	VkImageView imageView;
	VkImageLayout layout;

	VkFormat format;
	uint32_t mipLevels = 0;
	VmaAllocation allocation;

	uint32_t width = 0;
	uint32_t height = 0;
};