#pragma once
#include <HE2_TextureAllocator.h>
#include "VulkanAllocator.h"
#include <vulkan/vulkan.h>
#include "VulkanImageReference.h"


class VulkanTextures : public HE2_TextureAllocator
{
public:
	VulkanTextures(VulkanAllocator* va, VmaAllocator* vma);
	HE2_Image* getImage(std::string filepath, bool gammaCorrected = true);
	HE2_Image* getImage(HE2_ImageHandle handle) { return images[handle].image; }
	static VulkanImageReference getRef(HE2_Image* img);
	static HE2_ImageHandle addImage(VulkanImageReference ref );
	static std::vector<VulkanImageReference> images;

	static void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels, VkImageAspectFlagBits aspect = VK_IMAGE_ASPECT_COLOR_BIT);

	static void cleanUpVkResources() {}
	static void recreateVkResources() {}
private:
	VulkanAllocator* vulkanAlloc = nullptr;
	VmaAllocator* allocator = nullptr;

	static std::map<HE2_Image*, VulkanImageReference > refMap;

	void generateMipmaps(VkImage image, VkFormat imageFormat, int32_t texWidth, int32_t texHeight, uint32_t mipLevels);

	void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
};

