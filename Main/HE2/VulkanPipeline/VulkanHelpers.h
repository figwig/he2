#pragma once
#include <vulkan/vulkan.h>
#include <vector>
#include <HE2.h>
#include <HE2_Shader.h>



VkCommandBuffer beginSingleTimeCommands();

void endSingleTimeCommands(VkCommandBuffer commandBuffer);

#pragma region Macro Rerouters
uint32_t layoutBindingDescriptorType(uint32_t);
uint32_t layoutBindingStageFlag(uint32_t);
uint32_t layoutPipelineStageFlag(uint32_t);
VkFormat dataInputFormat(uint32_t);
VkFormat imageFormat(HE2_ShaderOutputFormat);
VkPrimitiveTopology primitiveType(HE2_PrimitiveType);
#pragma endregion

VkDescriptorSetLayout getLayoutFromFeatures(std::vector <HE2_ShaderFeature>, int grouping);