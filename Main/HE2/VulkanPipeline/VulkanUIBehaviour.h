#pragma once
#include <HE2.h>
#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#include <glfw/glfw3.h>
#include <vulkan/vulkan.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include "imgui_impl_vulkan.h"
#include <imconfig.h>
#include <imgui_internal.h>

#include "VulkanDeviceInstance.h"


class VulkanUIBackend
{
public:
	VulkanUIBackend(ImGui_ImplVulkan_InitInfo init, VkRenderPass guiRenderPass,  VulkanDeviceInstance * vdi);
	~VulkanUIBackend();


	void startUI();
	void finishUI();
	void renderUI();
	void callUICmds(VkCommandBuffer * renderCommandBuffer);


	void createFonts();

	ImGuiIO io;


	VulkanDeviceInstance* vdi = nullptr;
	bool p_openReal = false;
	bool* p_open = &p_openReal;

	bool frameStarted = false;
};

