#pragma once
#include <HE2.h>
#include "NormalDisplayShader.h"



class NormalDisplayComponent : public HE2_ShaderBufferGetter
{
public:
	NormalDisplayComponent();

	void update();

	HE2_BufferDataHandle getBufferHandle(int group, int binding)
	{
		return bufferHandle;
	}


	HE2_Object* object = nullptr;
private:
	NormalDisplayShader::MVPMatrixBuffer buffer;


	HE2_BufferDataHandle bufferHandle;

};

class NormalDisplayUpdater : public HE2_ObjectBehaviour
{
public:
	NormalDisplayUpdater(HE2_Object* obj) : HE2_ObjectBehaviour(obj)
	{
		ndc = obj->getComponent<NormalDisplayComponent>();
	}

	void update()

	{
		ndc->update();
	}
private:
	NormalDisplayComponent* ndc = nullptr;
};
