#pragma once
#include <HE2.h>
#include <vulkan/vulkan.h>
#include <HE2_GPUTask.h>



class VulkanGPUTaskAttachment : public HE2_Component<HE2_GPUTask>
{
public:
	std::vector<VkFramebuffer> framebuffers;
	std::vector<VkImage> outputImages;
	std::vector<VkImageView> outputImageViews;
	std::vector<VmaAllocation> outputImageAllocations;

	std::vector<VkDescriptorSet> specificFeatureDescriptorSets;
	HE2_UniformValueGetter* pushConstantValue;

	VulkanPipeline* shaderPipeline;

	VkFence generatingFence;
};