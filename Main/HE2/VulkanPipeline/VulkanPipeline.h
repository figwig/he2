#pragma once
#include <HE2_Shader.h>
#include <vulkan/vulkan.h>
#include <HE2_Component.h>
//A pipeline represents a shader program
class VulkanPipeline : public HE2_ComponentBase
{
public:
	std::vector<std::pair<std::vector<HE2_ShaderFeature>, VkDescriptorSetLayout>> featureLayouts;

	HE2_Shader* shader = nullptr;
	std::vector<VkDescriptorSetLayout> shaderSetLayouts;
	VkPipelineLayout pipelineLayout;
	VkPipeline pipeline;
	VkDescriptorPool descriptorPool;

	std::vector<VkDescriptorSet> viewProjectionDescriptorSets;

	static VkShaderModule createShaderModule(std::vector<char> code);

	VkRenderPass uniqueRenderpass;
	std::vector<VkClearValue> clearValues;
};

