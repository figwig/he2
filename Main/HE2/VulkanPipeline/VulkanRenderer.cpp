#include "VulkanRenderer.h"
#include "VulkanModelAttachment.h"
#include "VulkanObjectAttachment.h"
#include "VulkanMaterialAttachment.h"
#include "VulkanPipeline.h"
#include <HE2_Object.h>
#include <iostream>
#include "VulkanHelpers.h"


void VulkanRenderer::renderPass(RenderData* rd, bool binding)
{


	for (PassData * pd : rd->contents)
	{
		renderPassData(pd, binding);
	}
}

void VulkanRenderer::renderPassData(PassData* pd,  bool binding)
{
	currentPassData = pd;

	//This stuff will likely change - will be controlled by vulkan's subpass organization system rather than procedurally
	//if (binding) {
	//	pd->commonPart->bindFBOByIndex(i);
	//	pd->commonPart->prepareToRender();
	//}


	for (ShaderData * sd : pd->contents)
	{
		renderShaderData(sd);
	}

	//if (binding) {
	//	pd->commonPart->unBind();
	//	pd->commonPart->finishRender();
	//}
}

void VulkanRenderer::renderShaderData(ShaderData* sd, bool binding)
{
	//Shader Overrides - was used for shadows. Won't use like this, as shaders are defined by pipelines
	//if (shaderOverride == nullptr)
	//	currentShader = sd->commonPart;
	//else
	//	currentShader = shaderOverride;

	//glUseProgram(currentShader->getProgramID());


	//Bind new pipeline here

	if (binding)
	{
		vkCmdBindPipeline(*commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, sd->commonPart->pipeline);
		currentPipeline = sd->commonPart;
	}
	if(sd->commonPart->shader->shaderSettings.useVPFeature)
		vkCmdBindDescriptorSets(*commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, currentPipeline->pipelineLayout, 0, 1, &currentPipeline->viewProjectionDescriptorSets[frameIndex], 0, nullptr);

	for (MaterialData * md : sd->contents)
	{
		renderMaterialData(md);
	}

}

void VulkanRenderer::renderMaterialData(MaterialData* Md)
{
	//Bind material set here
	//
	for (int i = 0; i < Md->commonPart->groupings; i++)
	{
		vkCmdBindDescriptorSets(*commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, currentPipeline->pipelineLayout, Md->commonPart->material->getMaterialShaderFeatures()[0].grouping, 1, &Md->commonPart->descriptors[frameIndex], 0, nullptr);
	}

	for (ModelData * md : Md->contents)
	{
		renderModelData(md);
	}
}

void VulkanRenderer::renderModelData(ModelData* md)
{
	//Bind model here
	VulkanModelAttachment* vma = md->commonPart;

	//bindVAO(md->commonPart->getVAO());
	VkBuffer vertexBuffers[] = { vma->vertexBuffer };
	VkDeviceSize offsets[] = { 0 };



	vkCmdBindVertexBuffers(*commandBuffer, 0, 1, vertexBuffers, offsets);

	vkCmdBindIndexBuffer(*commandBuffer, vma->indexBuffer, 0, VK_INDEX_TYPE_UINT32);

	if (false)
	{
		//The 1 here is bind point 1, for second bound buffer


			
		//No push constants right now!
		//Below is super lazy - just uses the first instances descriptor sets! There should only ever be 1 instance because this impl is based off the idea of one model expanding to many instances rather than using an instanced call to automatically draw lots of models. I suspect this will be removed after dissertation is done!
		//Final implementation will used an "automatic instancing" system, where all per instance data is stuffed into an instancing buffer (model matrices basically). This will allow a primitive batching system, where all models of the same material and shader are drawn with 1 call. 
		int setOffset = 0;
		for (auto x : currentPipeline->shader->shaderSpecificFeatures)
		{
			int setIndex = (setOffset * HE2_RenderBackend::imageCount) + frameIndex;

			vkCmdBindDescriptorSets(*commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, currentPipeline->pipelineLayout, x.first, 1, &md->contents[0]->specificFeatureDescriptorSets[setIndex], 0, nullptr);
			setOffset++;
		}

		vkCmdDrawIndexed(*commandBuffer, md->commonPart->indices, md->contents[0]->renderComponent->instances, 0, 0, 0);
	
	}
	else
	{

		

		for (VulkanObjectAttachment* voa : md->contents)
		{
			if (voa->renderComponent->changeFlag)
			{
				rcsChanged.push_back(voa);
				continue;
			}
			if (!voa->object->active) continue;

			for (HE2_ShaderFeature pushConstantFeature : currentPipeline->shader->uniformFeatures)
			{
				//Temporary fix - there should be a better system for grabbing specific internal data
				if (pushConstantFeature.source == HE2_SHADER_BUFFER_SOURCE_MODEL_BUFFER)
					vkCmdPushConstants(*commandBuffer, currentPipeline->pipelineLayout, layoutBindingStageFlag(pushConstantFeature.stage), 0, sizeof(glm::mat4), glm::value_ptr(voa->object->getModelMatrix()));
				//Shader specific push constants do not work
				else if (pushConstantFeature.source == HE2_SHADER_BUFFER_SOURCE_SHADER_SPECIFIC)
				{
					vkCmdPushConstants(*commandBuffer, currentPipeline->pipelineLayout, layoutBindingStageFlag(pushConstantFeature.stage), 0, pushConstantFeature.size, voa->pushConstantValue->getData(pushConstantFeature.grouping));
				}
			}
			//Bind all descriptor sets needed
			int setOffset = 0;
			for (auto x : currentPipeline->shader->shaderSpecificFeatures)
			{
				int setIndex = (setOffset * HE2_RenderBackend::imageCount) + frameIndex;

				vkCmdBindDescriptorSets(*commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, currentPipeline->pipelineLayout, x.first, 1, &voa->specificFeatureDescriptorSets[setIndex], 0, nullptr);
				setOffset++;
			}

			//Draw all indexed - this is still per object right now
			vkCmdDrawIndexed(*commandBuffer, static_cast<uint32_t>(vma->indices), voa->renderComponent->instances, 0, 0, 0);
		}
	}
}


void VulkanRenderer::reSortData(std::vector<VulkanRenderRun>& runs)
{
	for (auto voa : rcsChanged)
	{
		bool found = false;
		//Find each pass this rc is featured in 
		for (auto run : runs)
		{
			for (auto pass : run.runGraph->allRenders->contents)
			{
				for (auto shader : pass->contents)
				{
					for (auto material : shader->contents)
					{
						for (auto model : material->contents)
						{
							for (int index = 0; index < model->contents.size(); index++)
							{
								if (model->contents[index] == voa)
								{
									//Remove that bitch!
									model->contents.erase(model->contents.begin() + index);
									found = true;
									break;
								}

							}
							if (found) break;
						}
						if (found) break;
					}
					if (found) break;
				}
				if (found) break;
			}
			if (run.doesWantObject(voa->renderComponent))
				run.addObject(voa->object);
		}
	}

	rcsChanged.clear();
}