#pragma once
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include "vk_mem_alloc.h"
#include <vulkan/vulkan.h>

template <class ContentType, class CommonType>
class RenderGroup
{
public:
	int size = 0;

	std::vector<ContentType*> contents;

	CommonType* commonPart = nullptr;

	std::string name = "";
};

class VulkanModelAttachment;
class VulkanObjectAttachment;
class VulkanMaterialAttachment;
class VulkanPipeline;
class HE2_RenderPass;
class HE2_SceneCreator;
class HE2_Object;

typedef RenderGroup<VulkanObjectAttachment, VulkanModelAttachment> ModelData;

typedef RenderGroup<ModelData, VulkanMaterialAttachment> MaterialData;

typedef RenderGroup<MaterialData, VulkanPipeline> ShaderData;

typedef RenderGroup<ShaderData, HE2_RenderPass> PassData;

typedef RenderGroup<PassData, HE2_SceneCreator> RenderData;


struct InstancedModelData
{
	VkBuffer instancingBuffer;
	VmaAllocation buffAlloc;

	uint32_t instances = 0;
};

class VulkanResourceGraph
{
public:
	RenderData* allRenders;
	RenderData* preRenderPasses;

	PassData* allRendersPass;
	PassData* opaquePass;
	PassData* transparentPass;
	PassData* deferredOpaquePass;
	PassData* deferredTransparentPass;
	PassData* GUIpass;

	HE2_RenderPass* screenBuffer;

	void setupBasePasses();
	void destroyPasses();

	void onAddObject(HE2_Object*);
	std::map<ModelData*, InstancedModelData> instancingDataMap;
private:

	int passesAddedTo;

	void addToPassData(PassData*, HE2_Object*);

	void addToShaderData(ShaderData*, HE2_Object*);

	void addToMaterialData(MaterialData*, HE2_Object*);

	void addToModelData(ModelData*, HE2_Object*);

	
};

