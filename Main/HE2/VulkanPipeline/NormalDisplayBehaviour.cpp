#include "NormalDisplayBehaviour.h"

NormalDisplayComponent::NormalDisplayComponent()
{
	bufferHandle = HE2_Instance::renderBackend->makeGPUBuffer(&buffer, sizeof(buffer), true);
}

void NormalDisplayComponent::update()
{
	buffer.mvp = object->getModelMatrix() * HE2_Camera::main->getCameraMatrix() * HE2_Camera::main->getProjectionMatrix();


	buffer.mvp = HE2_Camera::main->getProjectionMatrix() * HE2_Camera::main->getCameraMatrix() * object->getModelMatrix();

	HE2_Instance::renderBackend->updateGPUBuffer(bufferHandle, &buffer, sizeof(buffer));
}