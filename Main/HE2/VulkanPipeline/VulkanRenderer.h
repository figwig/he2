#pragma once
#include <HE2_RenderPass.h>
#include "ResourceGraph.h"
#include <vulkan/vulkan.h>
#include <cstdint>
#include "VulkanRenderPass.h"

class HE2_Object;
class HE2_RenderComponent;

class VulkanRenderer
{
public:
	static const int VULKAN_BP_MAT_SET_INDEX =  1;

	void renderPass(RenderData*, bool binding = true);

	void renderPassData(PassData*,  bool binding = true);

	void renderShadows() {}

	VkCommandBuffer* commandBuffer = nullptr;
	uint32_t frameIndex = 0;
	VulkanPipeline* currentPipeline = nullptr;
	
	void reSortData(std::vector<VulkanRenderRun>& runs);

private:
	void renderShaderData(ShaderData*, bool binding = true);

	void renderMaterialData(MaterialData*);

	void renderModelData(ModelData*);

	PassData* currentPassData = nullptr;

	std::vector<VulkanObjectAttachment*> rcsChanged;


	std::vector<VkDescriptorSet> viewProjectionDescriptors;
};

