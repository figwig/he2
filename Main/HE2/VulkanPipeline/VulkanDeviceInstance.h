#pragma once
#define GLFW_INCLUDE_VULKAN
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#define GLM_ENABLE_EXPERIMENTAL

#include <vulkan/vulkan.h>

#include <glfw/glfw3.h>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <cstdint>
#include <optional>
#include <set>
#include <HE2_GLM.h>
#include <array>
#include <unordered_map>
#include <HE2_WindowHandler.h>


const std::vector<const char*> validationLayers = {
 "VK_LAYER_LUNARG_standard_validation"
 //,	"VK_LAYER_NV_optimus"
//, "VK_LAYER_LUNARG_monitor"
};
const std::vector<const char*> deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

struct QueueFamilyIndices {
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;
	std::optional<uint32_t> computeFamily;

	bool isComplete(bool makeComputeQueue) {
		return graphicsFamily.has_value() && presentFamily.has_value()
			&&
			(computeFamily.has_value()|| !makeComputeQueue);
	}
};

class HE2_VulkanBackend;
class VulkanAllocator;

class VulkanDeviceInstance
{
public:
	static VkQueue graphicsQueue;
	static VkQueue gpuRenderTaskQueue;
	static VkQueue presentQueue;
	static VkQueue computeQueue;
	static VkDevice device;
	static VkPhysicalDevice physicalDevice;
	static VkAllocationCallbacks allocationCallbacks;

	static uint32_t graphicsQueueFamily;


	static VkFormat findDepthFormat();

private:
	bool enableValidationLayers = true;


	bool makeComputeQueue = false;
	VkInstance instance;
	VkDebugUtilsMessengerEXT debugMessenger;
	VkSurfaceKHR surface;

	VulkanDeviceInstance(bool makeComputeQueue = false);
	~VulkanDeviceInstance();

	void createInstance();
	void setupDebugMessenger();
	void createSurface();
	void pickPhysicalDevice();
	void createLogicalDevice();

	void cleanUp();

	friend class HE2_VulkanBackend;
	friend class VulkanAllocator;

	VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger);
	
	void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator);


	void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData);

	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);

	bool isDeviceSuitable(VkPhysicalDevice device);

	bool checkDeviceExtensionSupport(VkPhysicalDevice device);

	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

	std::vector<const char*> getRequiredExtensions();

	bool checkValidationLayerSupport();

	VkSampleCountFlagBits getMaxUsableSampleCount();

	static VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

};

