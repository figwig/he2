#include "VulkanBackend.h"
#include <HE2_GLM.h>
#include <ReadFile.h>
#include "VulkanObjectAttachment.h"
#include "VulkanTextures.h"
#include <HE2_Material.h>
#include <HE2_TextureManager.h>
#include <HE2_Model.h>
#include "VulkanModelAttachment.h"
#include <HE2_Time.h>
#include <HE2_Object.h>
#include "vk_mem_alloc.h"
#include "ResourceGraph.h"
#include <HE2_Camera.h>
#include <HE2_Profiler.h>
#include "VulkanHelpers.h"
#include "VulkanMaterialAttachment.h"
#include <HE2_Material.h>


void HE2_VulkanBackend::buildShader(HE2_Shader* newShader, bool wireFrame)
{
	//Create an empty pipeline
	pipelines.push_back(new VulkanPipeline());
	size_t index = pipelines.size() - 1;

	pipelines[index]->shader = newShader;

	makePipeline(newShader, index, wireFrame);
}


/// <summary>
/// Builds a shader.1
/// </summary>
/// <param name="newShader">The new shader.</param>
/// <param name="wireFrame">if set to <c>true</c> [wire frame].</param>
void HE2_VulkanBackend::makePipeline(HE2_Shader* newShader, int index,  bool wireFrame)
{
	//In case this is a rebuild
	pipelines[index]->featureLayouts.clear();
	pipelines[index]->shaderSetLayouts.clear();
	bool isCompute = newShader->shaderSettings.shaderType > HE2_ShaderType::HE2_COMPUTE;


#pragma region Features
	bool useVPDesc = false;


	std::vector<VkPushConstantRange> pushConstants;
	//Sort out  all the push constants first. Everything forward will then ignore the push constants
	for (auto a : newShader->uniformFeatures)
	{
		VkPushConstantRange pushConstantRange = { };
		pushConstantRange.stageFlags = (VkShaderStageFlags)layoutBindingStageFlag(a.stage);
		pushConstantRange.size = a.size;
		pushConstantRange.offset = a.offset;
		pushConstants.push_back(pushConstantRange);
	}

	//Resize the container for the layout bindings
	pipelines[index]->featureLayouts.resize(newShader->featureGroups);

	for (auto a : newShader->features)
	{
		//We don't need a layout binding for push constants
		if (a.featureType == HE2_SHADER_FEATURE_TYPE_UNIFORM)
		{
			continue;
		}
		//Add the feature to the appropriate group
		pipelines[index]->featureLayouts[a.grouping].first.push_back(a);
	}
	//For each group now
	for (size_t i = 0; i < newShader->featureGroups; i++)
	{
		//if (pipelines[index]->featureLayouts[i].first.size() == 0) continue;

		std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
		//Create a binding per feature in this group
		for (auto a : pipelines[index]->featureLayouts[i].first)
		{
			VkDescriptorSetLayoutBinding newBinding = {};
			newBinding.descriptorType = (VkDescriptorType)layoutBindingDescriptorType(a.featureType);
			newBinding.binding = a.binding;
			newBinding.stageFlags = (VkShaderStageFlags)layoutBindingStageFlag(a.stage);
			newBinding.descriptorCount = 1;
			newBinding.pImmutableSamplers = nullptr;
			layoutBindings.push_back(newBinding);

			if (a.source == HE2_SHADER_BUFFER_SOURCE_VIEW_PROJECTION_BUFFER) useVPDesc = true;
		}

		//Then create a layout based on that binding
		VkDescriptorSetLayoutCreateInfo layoutCreateInfo = {};
		layoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;

		layoutCreateInfo.bindingCount = static_cast<uint32_t>(layoutBindings.size());

		layoutCreateInfo.pBindings = layoutBindings.data();



		if (vkCreateDescriptorSetLayout(vdi->device, &layoutCreateInfo, nullptr, &pipelines[index]->featureLayouts[i].second) != VK_SUCCESS)
		{
			throw std::runtime_error("Failed to create descriptor set layout");
		}

		pipelines[index]->shaderSetLayouts.push_back(pipelines[index]->featureLayouts[i].second);
	}
#pragma endregion

#pragma region Shader Stages
	//Collates all the shader stages
	std::vector<VkPipelineShaderStageCreateInfo> shaderStageCreates;
	std::vector<VkShaderModule> shaderModules;

	VkShaderModule computeModule = VK_NULL_HANDLE;
	VkPipelineShaderStageCreateInfo computeStage;
	if (!isCompute)
	{
		//For each type of shader, do the following stuff
		if (newShader->shaderSources.vertexShaderSource.has_value())
		{
			VkShaderModule vertShaderModule = VulkanPipeline::createShaderModule(HE2_Files::readFile(newShader->shaderSources.vertexShaderSource.value()));

			shaderModules.push_back(vertShaderModule);

			VkPipelineShaderStageCreateInfo createInfoVertex = {};

			createInfoVertex.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			createInfoVertex.stage = VK_SHADER_STAGE_VERTEX_BIT;
			createInfoVertex.module = vertShaderModule;
			createInfoVertex.pName = "main";

			shaderStageCreates.push_back(createInfoVertex);
		}

		//Again for the fragment shader
		if (newShader->shaderSources.fragmentShaderSource.has_value())
		{
			VkShaderModule fragShaderModule = VulkanPipeline::createShaderModule(HE2_Files::readFile(newShader->shaderSources.fragmentShaderSource.value()));

			shaderModules.push_back(fragShaderModule);

			VkPipelineShaderStageCreateInfo createInfoFragment = {};

			createInfoFragment.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			createInfoFragment.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
			createInfoFragment.module = fragShaderModule;
			createInfoFragment.pName = "main";

			shaderStageCreates.push_back(createInfoFragment);
		}

		//And the tesse Shader
		if (newShader->shaderSources.tessalationEvaluationShaderSource.has_value())
		{
			VkShaderModule tesseShaderModule = VulkanPipeline::createShaderModule(HE2_Files::readFile(newShader->shaderSources.tessalationEvaluationShaderSource.value()));

			shaderModules.push_back(tesseShaderModule);

			VkPipelineShaderStageCreateInfo createInfoTessE = {};

			createInfoTessE.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			createInfoTessE.stage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
			createInfoTessE.module = tesseShaderModule;
			createInfoTessE.pName = "main";

			shaderStageCreates.push_back(createInfoTessE);
		}

		if (newShader->shaderSources.tessalationControlShaderSource.has_value())
		{
			VkShaderModule tesscShaderModule = VulkanPipeline::createShaderModule(HE2_Files::readFile(newShader->shaderSources.tessalationControlShaderSource.value()));

			shaderModules.push_back(tesscShaderModule);

			VkPipelineShaderStageCreateInfo createInfoTessc = {};

			createInfoTessc.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			createInfoTessc.stage = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
			createInfoTessc.module = tesscShaderModule;
			createInfoTessc.pName = "main";

			shaderStageCreates.push_back(createInfoTessc);
		}

		if (newShader->shaderSources.geometryShaderSource.has_value())
		{
			VkShaderModule geoShaderModule = VulkanPipeline::createShaderModule(HE2_Files::readFile(newShader->shaderSources.geometryShaderSource.value()));

			shaderModules.push_back(geoShaderModule);

			VkPipelineShaderStageCreateInfo createInfoGeo = {};

			createInfoGeo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			createInfoGeo.stage = VK_SHADER_STAGE_GEOMETRY_BIT;
			createInfoGeo.module = geoShaderModule;
			createInfoGeo.pName = "main";

			shaderStageCreates.push_back(createInfoGeo);
		}
	}
	else
	{

		computeModule = VulkanPipeline::createShaderModule(HE2_Files::readFile(newShader->shaderSources.computeShaderSource.value()));

		computeStage = {};
		computeStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		computeStage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
		computeStage.module = computeModule;
		computeStage.pName = "main";
		computeStage.pNext = VK_NULL_HANDLE;

	}
#pragma endregion

#pragma region Vertex Input
	//Next, Vulkan the format of all of the data that this shader type likes! 
	std::vector<VkVertexInputAttributeDescription> attributeDescs;

	//Fill in the data input description
	for (int i = 0; i < newShader->vertexInputDesc.size(); i++)
	{
		VkVertexInputAttributeDescription attrib = {};
		HE2_DataInputDesc data = newShader->vertexInputDesc[i];
		attrib.binding = data.binding;
		attrib.offset = data.offset;
		attrib.location = data.location;
		attrib.format = dataInputFormat(data.format);
		attributeDescs.push_back(attrib);
	}

	std::vector< VkVertexInputBindingDescription> bindingDescs;

	

	VkVertexInputBindingDescription bindingDescription = {};
	if (!newShader->perVertexDataDesc.nullData)
	{
		bindingDescription.binding = 0;
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		bindingDescription.stride = newShader->perVertexDataDesc.stride;
		bindingDescs.push_back(bindingDescription);
	}


	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;


	if (newShader->shaderSettings.isInstanced)
	{
		//VkVertexInputBindingDescription instancedDataDesc = {};

		//instancedDataDesc.binding = 1;
		//instancedDataDesc.inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;
		//instancedDataDesc.stride = newShader->perInstanceDataDesc.stride;

		//for (auto desc : newShader->instancedInputDesc)
		//{
		//	VkVertexInputAttributeDescription attrib = {};
		//	attrib.binding = 1;
		//	attrib.offset = desc.offset;
		//	attrib.location = desc.location;
		//	attrib.format = dataInputFormat(desc.format);
		//	attributeDescs.push_back(attrib);
		//}

		//bindingDescs.push_back(instancedDataDesc);
	}
	

	vertexInputInfo.pVertexBindingDescriptions = bindingDescs.data();
	vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingDescs.size());

	vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescs.size());

	vertexInputInfo.pVertexAttributeDescriptions = attributeDescs.data();




#pragma endregion

#pragma region Output Settings, Depth and Multisample
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = primitiveType(newShader->shaderSettings.primitiveType);
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	//Viewport size
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;

	uint32_t width = swapChainExtent.width;
	uint32_t height = swapChainExtent.height;
	VkExtent2D extent = swapChainExtent;
	if (newShader->shaderSettings.outputs.size() > 0)
	{
		glm::vec2 size = newShader->shaderSettings.outputs[0].size;
		if (size != glm::vec2(0))
		{
			width = static_cast<uint32_t>(size.x);
			height = static_cast<uint32_t>(size.y);
			extent = { width, height };
		}
	}


	viewport.width = width;
	viewport.height = height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	//Sissor Size
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = extent;

	//Viewport State
	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;

	if (newShader->shaderSettings.shaderType != HE2_ShaderType::HE2_SHADER_DEFERRED || true)
	{
		rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	}
	else
	{
		rasterizer.polygonMode = VK_POLYGON_MODE_LINE;
	}
	rasterizer.lineWidth = 1.0f;

	
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;

	if (!newShader->shaderSettings.cullBack)
	{
		rasterizer.cullMode = VK_CULL_MODE_NONE;
	}

	//rasterizer.cullMode = VK_CULL_MODE_NONE;

	rasterizer.depthBiasEnable = VK_FALSE;

	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_TRUE;
	multisampling.minSampleShading = 0.2f;

	switch (newShader->shaderSettings.shaderType)
	{
	//case HE2_ShaderType::HE2_SHADER_DEFERRED:
	case HE2_ShaderType::HE2_SHADER_FRAGMENT_GPU_TASK:
		multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		break;
	default:
		multisampling.rasterizationSamples = msaaSamples;
		break;
	}

	VkPipelineDepthStencilStateCreateInfo depthStencil = {};
	depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;

	depthStencil.depthTestEnable = VK_TRUE;
	//switch (newShader->shaderSettings.shaderType)
	//{
	//case HE2_ShaderType::HE2_SHADER_DEFERRED_RESOLVE:
	//	depthStencil.depthTestEnable = VK_TRUE;
	//	break;
	//default:
	//	depthStencil.depthTestEnable = VK_TRUE;
	//	break;
	//}

	depthStencil.depthWriteEnable = VK_TRUE;
	depthStencil.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	//depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
	depthStencil.depthBoundsTestEnable = VK_FALSE;
	depthStencil.stencilTestEnable = VK_FALSE;
	depthStencil.back.compareOp = VK_COMPARE_OP_ALWAYS;
	depthStencil.front = depthStencil.back;
#pragma endregion

#pragma region Output Attachments and blending

	std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments;

	size_t colourAttachmentCount;


	switch (newShader->shaderSettings.shaderType)
	{
	case HE2_ShaderType::HE2_SHADER_DEFERRED:
		colourAttachmentCount = 3;
		break;
	case HE2_ShaderType::HE2_SHADER_FRAGMENT_GPU_TASK:
		colourAttachmentCount = newShader->shaderSettings.outputs.size();
		makeRenderpassForShader(newShader, pipelines[index]->uniqueRenderpass);
		break;
	default:
		colourAttachmentCount = 1;
		break;
	}

	for (size_t i = 0; i < colourAttachmentCount; i++)
	{
		VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

		if (newShader->shaderSettings.allowBlending)
		{
			colorBlendAttachment.blendEnable = VK_TRUE;
			colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
			colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
			colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
			colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
			colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;
		}
		else
		{
			colorBlendAttachment.blendEnable = VK_FALSE;
		}

		colorBlendAttachments.push_back(colorBlendAttachment);
	}

	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY;
	colorBlending.attachmentCount = static_cast<uint32_t>(colorBlendAttachments.size());
	colorBlending.pAttachments = colorBlendAttachments.data();
	colorBlending.blendConstants[0] = 0.0f;
	colorBlending.blendConstants[1] = 0.0f;
	colorBlending.blendConstants[2] = 0.0f;
	colorBlending.blendConstants[3] = 0.0f;

#pragma endregion

#pragma region Layout Creation
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = static_cast<uint32_t>(pipelines[index]->shaderSetLayouts.size());
	pipelineLayoutInfo.pSetLayouts = pipelines[index]->shaderSetLayouts.data();

	pipelineLayoutInfo.pushConstantRangeCount = static_cast<uint32_t>(pushConstants.size());
	pipelineLayoutInfo.pPushConstantRanges = pushConstants.data();

	if (vkCreatePipelineLayout(VulkanDeviceInstance::device, &pipelineLayoutInfo, nullptr, &pipelines[index]->pipelineLayout) != VK_SUCCESS) {
		throw std::runtime_error("failed to create pipeline layout!");
	}

#pragma endregion

#pragma region Render Pipeline Creation
	if (!isCompute)
	{


		VkGraphicsPipelineCreateInfo pipelineInfo = {};

		VkPipelineTessellationStateCreateInfo tessalationState = {};

		if (newShader->shaderSources.tessalationControlShaderSource.has_value())
		{


			VkPhysicalDeviceProperties props = {};
			vkGetPhysicalDeviceProperties(vdi->physicalDevice, &props);

			tessalationState.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
			tessalationState.patchControlPoints = static_cast<uint32_t>(newShader->shaderSettings.tessalationControlPoints);
			tessalationState.pNext = nullptr;
			tessalationState.flags = 0;

			pipelineInfo.pTessellationState = &tessalationState;
		}

		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineInfo.stageCount = static_cast<uint32_t>(shaderStageCreates.size());
		pipelineInfo.pStages = shaderStageCreates.data();
		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &inputAssembly;
		pipelineInfo.pViewportState = &viewportState;
		pipelineInfo.pRasterizationState = &rasterizer;
		pipelineInfo.pMultisampleState = &multisampling;
		pipelineInfo.pDepthStencilState = &depthStencil;
		pipelineInfo.pColorBlendState = &colorBlending;
		pipelineInfo.layout = pipelines[index]->pipelineLayout;

		switch (newShader->shaderSettings.shaderType)
		{
		case HE2_ShaderType::HE2_SHADER_DEFERRED_RESOLVE:
			pipelineInfo.renderPass = resolveRenderPass;
			break;
		case HE2_ShaderType::HE2_SHADER_IMMEDIATE:
			pipelineInfo.renderPass = forwardLitRenderPass;

			break;
		case HE2_ShaderType::HE2_SHADER_DEFERRED:
			pipelineInfo.renderPass = offScreenRenderPass;
			break;
		case HE2_ShaderType::HE2_SHADER_UI:
			pipelineInfo.renderPass = uiRenderPass;
			break;
		case HE2_ShaderType::HE2_SHADER_FRAGMENT_GPU_TASK:
			pipelineInfo.renderPass = pipelines[index]->uniqueRenderpass;
			break;

		}

		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

		if (vkCreateGraphicsPipelines(vdi->device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipelines[index]->pipeline) != VK_SUCCESS) {
			throw std::runtime_error("failed to create graphics pipeline!");
		}

		for (int i = 0; i < shaderModules.size(); i++)
		{
			vkDestroyShaderModule(vdi->device, shaderModules[i], nullptr);
		}
	}

#pragma endregion

#pragma region Compute pipleine creation
	//Compute Shader
	else
	{
		assert(computeModule != VK_NULL_HANDLE);

		VkComputePipelineCreateInfo pipelineInfo = {};

		pipelineInfo.layout = pipelines[index]->pipelineLayout;
		pipelineInfo.stage = computeStage;
		pipelineInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;


		if (vkCreateComputePipelines(vdi->device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipelines[index]->pipeline));

		vkDestroyShaderModule(vdi->device, computeModule, nullptr);
	}


	//Add the component to the shader itself
	newShader->addComponent(pipelines[index]);

	//Create a descriptor pool!
	createDescriptorPool(index);

	if (useVPDesc)
		createViewProjectionDescs(pipelines[index]);

	std::cout << "Built a shader with " << newShader->features.size() << " features" << std::endl;
#pragma endregion
}

void HE2_VulkanBackend::makeRenderpassForShader(HE2_Shader* shader, VkRenderPass& renderpass)
{
	std::vector<VkAttachmentDescription> imageAttachments;
	std::vector<VkAttachmentReference> imageAttachmentRefs;
	for (int i = 0; i < shader->shaderSettings.outputs.size(); i++)
	{
		VkAttachmentDescription colorAttachment = {};
		colorAttachment.format = imageFormat(shader->shaderSettings.outputs[i].format);
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		if (shader->shaderSettings.outputs[i].storageCapability)
			colorAttachment.finalLayout = VK_IMAGE_LAYOUT_GENERAL;
		else
			colorAttachment.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		imageAttachments.push_back(colorAttachment);

		VkAttachmentReference colorAttachmentRef = {};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		imageAttachmentRefs.push_back(colorAttachmentRef);
	}

	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = static_cast<uint32_t>(imageAttachmentRefs.size());
	subpass.pColorAttachments = imageAttachmentRefs.data();

	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(imageAttachments.size());
	renderPassInfo.pAttachments = imageAttachments.data();
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &dependency;


	if (vkCreateRenderPass(vdi->device, &renderPassInfo, nullptr, &renderpass) != VK_SUCCESS) {
		throw std::runtime_error("failed to create render pass!");
	}

}

//std::vector<HE2_Object*> objects;
//
//objects.push_back(new HE2_Object());
//objects.push_back(new HE2_Object());
//objects.push_back(new HE2_Object());
//objects.push_back(new HE2_Object());
//objects.push_back(new HE2_Object());
//
//
//HE2_Object objectWeWannaDelete = new HE2_Object();
//
//objects.push_back(objectWeWannaDelete);
//
//for (int i = 0; i < objects.size(); i++)
//{
//	if (objects[i] == objectWeWannaDelete)
//	{
//		objects.erase(objects.begin() + i);
//		break;
//	}
//}