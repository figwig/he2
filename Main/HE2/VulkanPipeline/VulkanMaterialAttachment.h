#pragma once
#include <HE2_Component.h>
#include <HE2_Material.h>
#include <vulkan/vulkan.h>
class VulkanPipeline;

class VulkanMaterialAttachment : public HE2_ComponentBase
{
public:
	HE2_Material* material = nullptr;
	std::vector<VkDescriptorSet> descriptors;
	int groupings = 0;
	VulkanPipeline* pipelineAllocatedWith = nullptr;
};

