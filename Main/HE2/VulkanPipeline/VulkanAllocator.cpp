#include "VulkanAllocator.h"
#include "VulkanDeviceInstance.h"
#include "VulkanHelpers.h"
#include <HE2_Vertex.h>
#include "VulkanTextures.h"

VkImageView VulkanAllocator::createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels) {
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = mipLevels;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	VkImageView imageView;
	if (vkCreateImageView(vdi->device, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
		throw std::runtime_error("failed to create texture image view!");
	}

	return imageView;
}

void VulkanAllocator::createEmptyImage(uint32_t texWidth, uint32_t texHeight, uint32_t mipLevels, VkSampleCountFlagBits samples, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VmaAllocation& allocation, VmaMemoryUsage memUsage)
{
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = static_cast<uint32_t>(texWidth);
	imageInfo.extent.height = static_cast<uint32_t>(texHeight);
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = mipLevels;
	imageInfo.arrayLayers = 1;

	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = usage;
	
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = samples;

	VmaAllocationCreateInfo createAllocInfo = {};
	createAllocInfo.usage = memUsage;

	VkResult res = vmaCreateImage(*allocator, &imageInfo, &createAllocInfo, &image, &allocation, nullptr);

	if (res != VK_SUCCESS)
	{
		throw std::runtime_error("Couldn't create image");
	}

}



VkFormat VulkanAllocator::findDepthFormat()
{
	return findSupportedFormat({ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT }, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);

}

VkFormat VulkanAllocator::findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
{
	for (VkFormat format : candidates) {
		VkFormatProperties props;
		vkGetPhysicalDeviceFormatProperties(vdi->physicalDevice, format, &props);

		if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
			return format;
		}
		else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
			return format;
		}
	}

	throw std::runtime_error("failed to find supported format!");
}

bool VulkanAllocator::hasStencilComponent(VkFormat format)
{
	return format == VK_FORMAT_D32_SFLOAT || format == VK_FORMAT_D32_SFLOAT_S8_UINT;
}

uint32_t VulkanAllocator::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
	VkPhysicalDeviceMemoryProperties memProps;
	vkGetPhysicalDeviceMemoryProperties(vdi->physicalDevice, &memProps);

	for (uint32_t i = 0; i < memProps.memoryTypeCount; i++)
	{
		if ((typeFilter & (1 << i)) && (memProps.memoryTypes[i].propertyFlags & properties) == properties)
		{
			return i;
		}
	}

	throw std::runtime_error("failed to find suitable memory type");
}


void VulkanAllocator::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VmaMemoryUsage properties, VkBuffer& buffer, VmaAllocation& allocation)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;

	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = properties;

	vmaCreateBuffer((*allocator), &bufferInfo, &allocInfo, &buffer, &allocation, nullptr);
}

void VulkanAllocator::createVertexBuffer(VkBuffer& buffer, VmaAllocation& allocation, std::vector<HE2_Vertex>* vertices)
{
	VkDeviceSize bufferSize = sizeof(HE2_Vertex) * vertices->size();

	VkBuffer stagingBuffer;
	VmaAllocation stagingAllocation;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, stagingBuffer, stagingAllocation);

	void* data;
	vmaMapMemory((*allocator), stagingAllocation, &data);
	memcpy(data, vertices->data(), bufferSize);
	vmaUnmapMemory((*allocator), stagingAllocation);

	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_ONLY, buffer, allocation);

	copyBuffer(stagingBuffer, buffer, bufferSize);

	vmaDestroyBuffer(*allocator, stagingBuffer, stagingAllocation);
}

void VulkanAllocator::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
{
	VkCommandBuffer commandBuffer = beginSingleTimeCommands();

	VkBufferCopy copyRegion = {};
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = 0;
	copyRegion.size = size;

	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

	endSingleTimeCommands(commandBuffer);
}

void VulkanAllocator::copyImage(VulkanImageReference srcRef, VulkanImageReference dstRef)
{
	//Transition both images to an appropriate layout for thr transfer
	VulkanTextures::transitionImageLayout(srcRef.vkImage, srcRef.format, srcRef.layout, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, 1);

	VulkanTextures::transitionImageLayout(dstRef.vkImage, dstRef.format, dstRef.layout, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1);

	VkCommandBuffer commandBuffer = beginSingleTimeCommands();

	//VkImageCopy copyRegion = {};
	//copyRegion.dstOffset = { 0,0,0 };
	//copyRegion.srcOffset = { 0,0,0 };

	//copyRegion.dstSubresource.mipLevel = 0;
	//copyRegion.srcSubresource.mipLevel = 0;

	//copyRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	//copyRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

	//copyRegion.dstSubresource.baseArrayLayer = 0;
	//copyRegion.srcSubresource.baseArrayLayer = 0;

	//copyRegion.dstSubresource.layerCount = 1;
	//copyRegion.srcSubresource.layerCount = 1;

	//copyRegion.extent = { srcRef.width, srcRef.height, 1 };


	//vkCmdCopyImage(commandBuffer, srcRef.vkImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, dstRef.vkImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copyRegion);

	VkImageBlit blit = {};
	blit.srcOffsets[0] = { 0, 0, 0 };
	blit.srcOffsets[1] = { static_cast<int32_t>(srcRef.width), static_cast<int32_t>(srcRef.height), 1 };
	blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	blit.srcSubresource.mipLevel = 0;
	blit.srcSubresource.baseArrayLayer = 0;
	blit.srcSubresource.layerCount = 1;
	blit.dstOffsets[0] = { 0, 0, 0 };
	blit.dstOffsets[1] = {static_cast<int32_t>(dstRef.width), static_cast<int32_t>(dstRef.height),1 };
	blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	blit.dstSubresource.mipLevel = 0;
	blit.dstSubresource.baseArrayLayer = 0;
	blit.dstSubresource.layerCount = 1;

	vkCmdBlitImage(commandBuffer,
		srcRef.vkImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
		dstRef.vkImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1, &blit,
		VK_FILTER_LINEAR);

	endSingleTimeCommands(commandBuffer);


	//Transition them back again once they're done
	VulkanTextures::transitionImageLayout(srcRef.vkImage, srcRef.format,  VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, srcRef.layout, 1);


	VulkanTextures::transitionImageLayout(dstRef.vkImage, dstRef.format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, dstRef.layout, 1);
}

void VulkanAllocator::createIndexBuffer(VkBuffer& buffer, VmaAllocation& allocation, std::vector<uint32_t>* indices)
{

	VkDeviceSize bufferSize = sizeof(uint32_t) * indices->size();

	VkBuffer stagingBuffer;
	VmaAllocation stagingAllocation;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, stagingBuffer, stagingAllocation);

	void* data;
	vmaMapMemory((*allocator), stagingAllocation, &data);
	memcpy(data, indices->data(), bufferSize);
	vmaUnmapMemory((*allocator), stagingAllocation);

	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_ONLY, buffer, allocation);

	copyBuffer(stagingBuffer, buffer, bufferSize);

	vmaDestroyBuffer(*allocator, stagingBuffer, stagingAllocation);
}

