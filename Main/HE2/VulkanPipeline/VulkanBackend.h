#pragma once
#include <HE2_RenderBackend.h>
#include "VulkanDeviceInstance.h"
#include "VulkanAllocator.h"
#include "VulkanPipeline.h"
#include <HE2_Vertex.h>
#include <HE2_Object.h>
#include "VulkanObjectAttachment.h"
#include "VulkanModelAttachment.h"
#include "vk_mem_alloc.h"
#include "VulkanRenderer.h"
#include "VulkanUIBehaviour.h"
#include <queue>
#include <HE2_ConsoleCommand.h>
#include <HE2_Debug.h>
#include <thread>
#include <imgui.h>
#include "VulkanRenderPass.h"

class VulkanGPUTaskAttachment;



//Temporary
struct ViewProjectionBuffer
{
	glm::mat4 view;
	glm::mat4 proj;
};

struct ModelBuffer
{
	glm::mat4 model;
	glm::mat4 model2;
};

struct FrameBufferAttachment {
	VkImage image;
	VmaAllocation mem;
	VkImageView view;
	VkFormat format;
	HE2_Image* he2Image = nullptr;
};

class HE2_VulkanBackend : public HE2_RenderBackend
{
public:

	static uint32_t MAX_OBJECTS ;

	HE2_VulkanBackend(HE2_RenderBackendSettings settings = {});
	~HE2_VulkanBackend();

	static VkSampleCountFlagBits msaaSamples;

	void setupAPI();

	void render();

	void onAddObject(HE2_Object*);
	void onAddModel(HE2_Model*);
	void onAddMaterial(HE2_Material*);

	void prepareGPUTask(HE2_GPUTask*);

	HE2_TextureAllocator* getAllocator();

	static VkCommandPool commandPool;
	static VkCommandPool computeCommandPool;

	HE2_BufferDataHandle makeGPUBuffer(void *data,size_t size, bool updatable);
	HE2_StorageBufferDataHandle makeStorageBuffer(size_t size);
	void updateGPUBuffer(HE2_BufferDataHandle, void* data, size_t size);

	void queueGPUTask(HE2_GPUTask*);

	std::vector<HE2_Image*> getDeferredBuffers();

	void rebuildShader(HE2_Shader* shader);

	HE2_Image* createEmptyImage(bool forCompute, glm::vec2, bool cpuVisible);

	void copyImage(HE2_Image* srcImage, HE2_Image* dstImage);

	void copyImageDataToBuffer(void* data, size_t& size, HE2_Image* image);

	ImTextureID getImGuiTextureHandle(HE2_Image*);

	HE2_Image* getGameImage() { return sceneEditorImageHE2; }
	static VulkanAllocator* vulkanAlloc ;

#pragma region RenderPass Objects
	static VkRenderPass forwardLitRenderPass;
	static VkRenderPass uiRenderPass;
	static VkRenderPass offScreenRenderPass;
	static VkRenderPass resolveRenderPass;
#pragma endregion
	static std::vector<VkImageView> swapChainImageViews;
	static VkSampler defaultSampler;
	static VkSampler deferredSampler;
	static VkSampler exactSampler;

	VkBuffer getSpecificBuffer(int i) { return shaderSpecificBuffers[i]; }
private:
	friend class VulkanRenderRun;

	VmaAllocator allocator;
	VulkanRenderer* renderer = nullptr;

	void buildShader(HE2_Shader*, bool wireFrame = true);
	void makePipeline(HE2_Shader*, int index, bool wireFrame);
	void destroyShader(HE2_Shader *);

	void processObject(HE2_Object*);

	void cleanup();
	void cleanupSwapChain();

	void makeRenderpassForShader(HE2_Shader* shader, VkRenderPass&);

	void makeUniqueFramebuffers(HE2_GPUTask*);

	VulkanDeviceInstance* vdi = nullptr;

	const static int MAX_FRAMES_IN_FLIGHT = 3;

	VulkanUIBackend* ui = nullptr;

	void createSwapChain();
	void createImageViews();
	void createForwardLitRenderPass();
	void createResolveRenderPass();
	void createOffScreenRenderPass();
	void createUIRenderPass();
	void createCommandPool();
	void createFramebuffers();
	void createDefaultGraphicsPipeline();
	void createSampler();
	void createCommandBuffers();
	void createUniformBuffers();
	void createSyncObjects();
	void createViewProjectionDescs(VulkanPipeline *);

	void createDescriptorPool(uint32_t index);
	void createDescriptorSetsForObject(VulkanObjectAttachment*);

	void recreateSwapChain();

	void buildMainCommandBuffers(uint32_t index);
	void buildUICommandBuffers(uint32_t index);
	void buildDeferredResolveCommandBuffers(uint32_t index);
	void buildImmediateCommandBuffers(uint32_t index);
	void buildGPUTaskCommandBuffer(uint32_t index);
	void buildComputeCommandBuffer(uint32_t index);
	
	
	void recordImmediateCommands(VulkanResourceGraph * runGraph);
	void recordDeferredCommands(VulkanResourceGraph* runGraph);

	void updateViewProjectionBuffer(uint32_t);
	void makeLightingBuffer();
	void updateLighting();

#pragma region SwapChain Resources
	VkSwapchainKHR swapChain;
	std::vector<VkImage> swapChainImages;
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;

	std::vector<VkFramebuffer> swapChainUIFramebuffers;

#pragma region CommandBuffers
	std::vector<VkCommandBuffer> uiCommandBuffers;

	std::vector<VkCommandBuffer> gpuTaskCommandBuffers;
	std::vector<VkCommandBuffer> computeCommandBuffers;
#pragma endregion
	//New implementation - we're gonna have one constant resolve command buffer, but induvidual command buffers per renderpass for immediate and for deferred
	std::vector<VulkanRenderRun> renderRuns;

	HE2_Image *  sceneEditorImageHE2;
	VkImage sceneEditorImage;
	VmaAllocation sceneEditorImageAlloc;
	VkImageView sceneEditorImageView;

	bool framebufferResized = false;
#pragma endregion

#pragma region Sync Objects
	std::vector<VkSemaphore> imageAvailableSemaphores;
	std::vector<VkSemaphore> offScreenFinishedSemaphores;
	std::vector<VkSemaphore> resolveFinishedSemaphores;
	std::vector<VkSemaphore> immediateFinishedSemaphores;
	std::vector<VkSemaphore> singleTimeFinishedSemaphores;


	std::vector<VkFence> singleTimeRenderFences;
	std::vector<VkFence> immediateInFlightFences;
	std::vector<VkFence> resolveInFlightFences;
	std::vector<VkFence> offScreenInFlightFences;

	std::vector<VkFence> offScreenImagesInFlight;
	std::vector<VkFence> resolveImagesInFlight;
	std::vector<VkFence> immediateImagesInFlight;
	uint32_t currentFrame = 0;
#pragma endregion


	uint32_t defaultMipLevels;



#pragma region Shader Objects
	std::vector<VulkanPipeline*> pipelines;
#pragma endregion

#pragma region Choose Methods
	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);
#pragma endregion

	std::vector<VulkanObjectAttachment*> objectAttachments;
	std::vector<VulkanMaterialAttachment*> materialAttachments;

	//Temporary Essentials Buffer stuff
	std::vector<VkBuffer> viewProjectionBuffers;
	std::vector<VmaAllocation> viewProjectionBuffersAllocation;

	//Temporary Model Buffer stuff
	std::vector<VkBuffer> shaderSpecificBuffers;
	std::vector<VmaAllocation> shaderSpecificBuffersAllocation;


	static HE2_BufferDataHandle lightingBufferHandle;

	glm::mat4 projection;

	HE2_TextureAllocator* textureAlloc = nullptr;

	struct Light {
		glm::vec4 position;
		glm::vec4 color;
		float radius;

	};

	struct LightingBuffer {
		Light lights[200];
		glm::vec4 viewPos;
		int actLightCount;

		glm::vec2 textureSize = glm::vec2(1800, 1000);

		bool operator == (const LightingBuffer& other) const
		{
			if (actLightCount != other.actLightCount)
				return false;
			if (viewPos != other.viewPos)
				return false;

			//Already established same number of lights so this is safe
			for (int i = 0 ; i < actLightCount; i ++)
			{
				if (lights[i].position != other.lights[i].position)
					return false;
			}
			return true;
		}
	};

	LightingBuffer lastLights = {};

	std::queue<HE2_GPUTask*> pendingTasks;
	std::queue<HE2_GPUTask*> pendingComputeTasks;
	VulkanGPUTaskAttachment* pendingTask = nullptr;
	VulkanGPUTaskAttachment* pendingComputeTask = nullptr;

	bool runGPUTasks = false;
	bool runComputeGPUTasks = false;
	
	bool waitingOnSingleTimeRenderResult = false;

	std::vector<VkBuffer> storageBuffers;
	std::vector<VmaAllocation> storageBufferAllocations;

};


class HardwareCommand : public HE2_ConsoleCommand
{
public:
	HardwareCommand() : HE2_ConsoleCommand("hardware")
	{
		desc = "Returns a summary of the currently running device";
	}
	void execute(std::string params)
	{
		DebugLog("Hardware Info:");
		VkPhysicalDeviceProperties props;
		vkGetPhysicalDeviceProperties(VulkanDeviceInstance::physicalDevice, &props);
		std::string gpuName = props.deviceName;

		DebugLog("Graphics Device: " + gpuName);
		DebugLog("Graphics API: Vulkan");
		unsigned int threads = std::thread::hardware_concurrency();
		DebugLog("Logical Threads: " + std::to_string(threads));
	}
};