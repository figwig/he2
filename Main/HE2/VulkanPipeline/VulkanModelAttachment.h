#pragma once
#include <vulkan/vulkan.h>
#include "vk_mem_alloc.h"
#include <HE2_Component.h>
class HE2_Model;
class VulkanModelAttachment : public HE2_ComponentBase
{
public:
	VkBuffer vertexBuffer;
	VkDeviceMemory vertexBufferMemory;
	VmaAllocation vertexAllocation;
	VkBuffer indexBuffer;
	VmaAllocation indexAllocation;
	VkDeviceMemory indexBufferMemory;
	uint32_t indices;

	HE2_Model* model = nullptr;
};

