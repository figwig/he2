#include <stb_image.h>
#include "VulkanBackend.h"
#include <HE2_GLM.h>
#include <ReadFile.h>
#include "VulkanObjectAttachment.h"
#include "VulkanTextures.h"
#include <HE2_Material.h>
#include <HE2_TextureManager.h>
#include <HE2_Model.h>
#include "VulkanModelAttachment.h"
#include <HE2_Time.h>
#include <HE2_Object.h>
#include "vk_mem_alloc.h"
#include "ResourceGraph.h"
#include <HE2_Camera.h>
#include <HE2_Profiler.h>
#include "VulkanHelpers.h"
#include "VulkanMaterialAttachment.h"
#include <HE2_Material.h>
#include <HE2_Lighting.h>
#include <HE2_Light.h>
#include "VulkanGPUTaskAttachment.h"
#include <HE2_GPUTask.h>
#include <HE2_Debug.h>
#include <half.h>
#include <HE2_RenderRun.h>
#include "VulkanMaterialAttachment.h"

VkSampleCountFlagBits HE2_VulkanBackend::msaaSamples = VK_SAMPLE_COUNT_1_BIT;
VkCommandPool HE2_VulkanBackend::commandPool;
VkCommandPool HE2_VulkanBackend::computeCommandPool;	
VkRenderPass HE2_VulkanBackend::forwardLitRenderPass;
VkRenderPass HE2_VulkanBackend::uiRenderPass;
VkRenderPass HE2_VulkanBackend::offScreenRenderPass;
VkRenderPass HE2_VulkanBackend::resolveRenderPass;
VulkanAllocator* HE2_VulkanBackend::vulkanAlloc; 
std::vector<VkImageView> HE2_VulkanBackend::swapChainImageViews;
VkSampler HE2_VulkanBackend::defaultSampler;
VkSampler HE2_VulkanBackend::deferredSampler;
VkSampler HE2_VulkanBackend::exactSampler;
HE2_BufferDataHandle HE2_VulkanBackend::lightingBufferHandle = 0;


uint32_t HE2_VulkanBackend::MAX_OBJECTS = 1024;

HE2_VulkanBackend::HE2_VulkanBackend(HE2_RenderBackendSettings settings) : HE2_RenderBackend(settings)
{
	//Dangling pointer is okay - this registers itself with he2_debug
	new HardwareCommand();

	renderRuns.push_back(VulkanRenderRun(this, finalContext));

}

HE2_VulkanBackend::~HE2_VulkanBackend()
{
	cleanup();
	delete vdi;
	delete vulkanAlloc;
	delete textureAlloc;
	delete renderer;

}

void HE2_VulkanBackend::setupAPI()
{
	DebugLog("Setting Up Vulkan Backend...");

	//Create the device Instance - used to manage the vulkan devices and releated properties6
	vdi = new VulkanDeviceInstance(HE2_Instance::instanceSettings.useComputeShader);
	VmaAllocatorCreateInfo allocatorInfo = {};
	allocatorInfo.physicalDevice = vdi->physicalDevice;
	allocatorInfo.device = vdi->device;

	DebugLog("Vulkan Device located and setup");

	VkPhysicalDeviceProperties props = {};
	vkGetPhysicalDeviceProperties(vdi->physicalDevice, &props);

	DebugLog("Max tessalation levels: " + std::to_string(props.limits.maxTessellationGenerationLevel));
	DebugLog("Max tessalation patch size: " + std::to_string(props.limits.maxTessellationPatchSize));

	vmaCreateAllocator(&allocatorInfo, &allocator);

	vulkanAlloc = new VulkanAllocator(vdi, &allocator);
	textureAlloc = new VulkanTextures(vulkanAlloc, &allocator);

	renderer = new VulkanRenderer();

	DebugLog("Vulkan allocator loaded");

	setTextureManagerAllocator(textureAlloc);
	
	defaultMipLevels = 10;

	createSwapChain();

	createImageViews();


	createForwardLitRenderPass();

	createOffScreenRenderPass();

	createResolveRenderPass();

	createUIRenderPass();

	createCommandPool();

	createSampler();


	createFramebuffers();

	createUniformBuffers();

	createCommandBuffers();

	createSyncObjects();

	createDefaultGraphicsPipeline();

	makeLightingBuffer();

	//Stuff like this is soon to replace the below
	renderRuns[0].createResources(swapChainImages.size(), swapChainImageFormat, msaaSamples, glm::vec2(swapChainExtent.width, swapChainExtent.height));


	ImGui_ImplVulkan_InitInfo initInfo = {};
	initInfo.Allocator = nullptr;
	initInfo.DescriptorPool = pipelines[0]->descriptorPool;
	initInfo.Device = vdi->device;
	initInfo.ImageCount = 2; //I think?
	initInfo.Instance = vdi->instance;
	initInfo.MinImageCount = 2; //I think?
	initInfo.MSAASamples = VK_SAMPLE_COUNT_1_BIT;
	initInfo.PhysicalDevice = vdi->physicalDevice;
	initInfo.Queue = vdi->graphicsQueue;
	initInfo.QueueFamily = vdi->graphicsQueueFamily;
	initInfo.CheckVkResultFn = nullptr;

	ui = new VulkanUIBackend(initInfo, uiRenderPass, vdi);

	defaultMaterial = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage("textures/brickwall.jpg"), HE2_TextureManager::getImage("textures/brickwall_s.jpg"), HE2_TextureManager::getImage("textures/Brickwall_normal.jpg",true), deferredShader);

	defaultImmediateMaterial = new HE2_BlinnPhongMaterial(HE2_TextureManager::getImage("textures/brickwall.jpg"), HE2_TextureManager::getImage("textures/brickwall_s.jpg"), HE2_TextureManager::getImage("textures/Brickwall_normal.jpg", true), defaultShader);




	buildMainCommandBuffers(0);
	buildUICommandBuffers(0);
	buildImmediateCommandBuffers(0);

	for (int i = 0; i < swapChainImages.size(); i++)
	{	
		buildDeferredResolveCommandBuffers(i);
	}


}

HE2_TextureAllocator* HE2_VulkanBackend::getAllocator()
{
	return textureAlloc;
}

void HE2_VulkanBackend::updateViewProjectionBuffer(uint32_t i)
{
	ViewProjectionBuffer ubo = {};

	ubo.view = HE2_Camera::main->getCameraMatrix();

	ubo.proj = HE2_Camera::main->getProjectionMatrix();


	void* data;

	vmaMapMemory(allocator, viewProjectionBuffersAllocation[i], &data);
	memcpy(data, &ubo, sizeof(ubo));
	vmaUnmapMemory(allocator, viewProjectionBuffersAllocation[i]);

}

HE2_BufferDataHandle HE2_VulkanBackend::makeGPUBuffer(void* data, size_t size, bool cpuCoherent)
{
	HE2_BufferDataHandle handle = shaderSpecificBuffers.size() / swapChainImages.size();
	for (int i = 0; i < swapChainImages.size(); i++)
	{
		int index = shaderSpecificBuffers.size();
		shaderSpecificBuffers.push_back({});
		shaderSpecificBuffersAllocation.push_back({});
		VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
		bufferInfo.size = size;
		bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

		VmaAllocationCreateInfo allocInfo = {};
		if (cpuCoherent)
			allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
		else
		{
			//ToDO: implement this - will need a staging buffer to sort
			allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

		}

		vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &shaderSpecificBuffers[index], &shaderSpecificBuffersAllocation[index], nullptr);


		if (!cpuCoherent) throw std::runtime_error("Please mark this buffer as updatable, non-updatable ones are not implemented yet");

		void* map;

		vmaMapMemory(allocator, shaderSpecificBuffersAllocation[index], &map);
		memcpy(map, data, size);
		vmaUnmapMemory(allocator, shaderSpecificBuffersAllocation[index]);
		
	}
	return handle;
}

HE2_BufferDataHandle HE2_VulkanBackend::makeStorageBuffer(size_t size)
{
	HE2_BufferDataHandle handle = storageBuffers.size();

	int index = storageBuffers.size();

	storageBuffers.push_back({});
	storageBufferAllocations.push_back({});

	VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
	bufferInfo.size = size;
	bufferInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

	VmaAllocationCreateInfo allocInfo = {};

	//ToDO: implement this - will need a staging buffer to sort
	allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

	vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &storageBuffers[index], &storageBufferAllocations[index], nullptr);
	
	return handle;
}

/// <summary>
/// Updates a gpu buffer.
/// </summary>
/// <param name="handle">The handle.</param>
/// <param name="data">The data.</param>
/// <param name="size">The size.</param>
void HE2_VulkanBackend::updateGPUBuffer(HE2_BufferDataHandle handle, void* data, size_t size)
{
	int index = handle * 3;
	for (int i = 0; i < swapChainImages.size(); i++)
	{
		void* map;

		vmaMapMemory(allocator, shaderSpecificBuffersAllocation[index + i], &map);
		memcpy(map, data, size);
		vmaUnmapMemory(allocator, shaderSpecificBuffersAllocation[index + i]);

	}
}

void HE2_VulkanBackend::render()
{
	//Wait for the graphics queue to be idle
	HE2_Profiler::Begin(HE2_Profiler::Stage::WaitForQueueIdle);
	vkQueueWaitIdle(vdi->graphicsQueue);
	HE2_Profiler::End(HE2_Profiler::Stage::WaitForQueueIdle);

#pragma region Aquire Image and wait for frame
	//Wait for this frame's fences
	{
		HE2_Profiler::Begin(HE2_Profiler::Stage::WaitForFences);
		vkWaitForFences(vdi->device, 1, &immediateInFlightFences[currentFrame], VK_TRUE, UINT64_MAX);
		vkWaitForFences(vdi->device, 1, &resolveInFlightFences[currentFrame], VK_TRUE, UINT64_MAX);
		vkWaitForFences(vdi->device, 1, &offScreenInFlightFences[currentFrame], VK_TRUE, UINT64_MAX);
		HE2_Profiler::End(HE2_Profiler::Stage::WaitForFences);
	}

	HE2_Profiler::Begin(HE2_Profiler::Stage::AquireImage);
	uint32_t imageIndex;

	//Grab us the image
	VkResult result = vkAcquireNextImageKHR(vdi->device, swapChain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);

	//Out of date? 
	if (result == VK_ERROR_OUT_OF_DATE_KHR || glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_F12)) {
		recreateSwapChain();
		return;
	}
	else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
		throw std::runtime_error("failed to acquire swap chain image!");
	}
	
	HE2_Profiler::End(HE2_Profiler::Stage::AquireImage);

	//Update the buffers
	{
		HE2_Profiler::Begin(HE2_Profiler::Stage::UpdateBuffers);
		updateViewProjectionBuffer(imageIndex);
		updateLighting();
		HE2_Profiler::End(HE2_Profiler::Stage::UpdateBuffers);
	}

	//Wait for the fences for this image
	
	HE2_Profiler::Begin(HE2_Profiler::Stage::WaitForFences);

	if (offScreenImagesInFlight[imageIndex] != VK_NULL_HANDLE || resolveImagesInFlight[imageIndex] != VK_NULL_HANDLE || immediateImagesInFlight[imageIndex] != VK_NULL_HANDLE)
	{
		if(offScreenImagesInFlight[imageIndex] == VK_NULL_HANDLE)
			vkWaitForFences(vdi->device, 1, &offScreenImagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);

		if (resolveImagesInFlight[imageIndex] == VK_NULL_HANDLE)
			vkWaitForFences(vdi->device, 1, &resolveImagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);

		if (immediateImagesInFlight[imageIndex] == VK_NULL_HANDLE)
			vkWaitForFences(vdi->device, 1, &immediateImagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);
	}
	offScreenImagesInFlight[imageIndex] = offScreenInFlightFences[currentFrame];
	resolveImagesInFlight[imageIndex] = resolveInFlightFences[currentFrame];
	immediateImagesInFlight[imageIndex] = immediateInFlightFences[currentFrame];

	HE2_Profiler::End(HE2_Profiler::Stage::WaitForFences);
#pragma endregion

	VkSemaphore signalRenderCommandDoneSemaphores[] = { imageAvailableSemaphores[currentFrame] };
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSemaphores[] = { imageAvailableSemaphores[currentFrame] };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT }; //
	submitInfo.pWaitDstStageMask = waitStages;

	//Do any single time renders
	if (runGPUTasks)
	{
		runGPUTasks = false;


		VkCommandBuffer buffers[1] = { gpuTaskCommandBuffers[currentFrame] };
		//VkCommandBuffer buffers[1] = { singleTimeRenderCommandBuffers[currentFrame] };
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = buffers;

		pendingTask = pendingTasks.front()->getComponent<VulkanGPUTaskAttachment>();

		//And submit the command buffers
		vkResetFences(vdi->device, 1, &pendingTask->generatingFence);
		VkResult res = vkQueueSubmit(vdi->gpuRenderTaskQueue, 1, &submitInfo, pendingTask->generatingFence);

		pendingTasks.pop();

		//Replace the wait semaphores for the next submits (or not?)
		//waitSemaphores[0] =  singleTimeFinishedSemaphores[currentFrame];
	}
	if (runComputeGPUTasks)
	{
		runComputeGPUTasks = false;

		VkCommandBuffer buffers[1] = { computeCommandBuffers[currentFrame] };
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = buffers;

		pendingComputeTask = pendingComputeTasks.front()->getComponent<VulkanGPUTaskAttachment>();

		vkResetFences(vdi->device, 1, &pendingComputeTask->generatingFence);
		vkQueueSubmit(vdi->computeQueue, 1, &submitInfo, pendingComputeTask->generatingFence);

		pendingComputeTasks.pop();

	}

	HE2_Profiler::Begin(HE2_Profiler::Stage::RenderObjects);


	for (VulkanRenderRun& run : renderRuns)
	{
		//Time to render!
		//Either be the image available semaphore, or the singletime finished
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;


		//Add a semaphore to the submit
		VkCommandBuffer buffers[2] = { run.deferredCommandBuffers[currentFrame] };
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = buffers;

		//grab the render finished semaphore
		signalRenderCommandDoneSemaphores [0]= offScreenFinishedSemaphores[currentFrame];
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalRenderCommandDoneSemaphores;

		vkResetFences(vdi->device, 1, &offScreenInFlightFences[currentFrame]);
		//And submit the command buffers

		VkResult res = vkQueueSubmit(vdi->graphicsQueue, 1, &submitInfo, offScreenInFlightFences[currentFrame]);

		if (res != VK_SUCCESS)
		{

			std::cout << "Error submitting offscreen command buffer";
			switch (res)
			{
			case VK_ERROR_DEVICE_LOST:
				std::cout << ": Device lost";
				break;
			case VK_ERROR_OUT_OF_HOST_MEMORY:
				std::cout << ": out of Host memory";
				break;

			case	VK_ERROR_OUT_OF_DEVICE_MEMORY:
				std::cout << ": out of device memory";
				break;
				//VK_ERROR_INITIALIZATION_FAILED = -3,

				//VK_ERROR_MEMORY_MAP_FAILED = -5,
				//VK_ERROR_LAYER_NOT_PRESENT = -6,
				//VK_ERROR_EXTENSION_NOT_PRESENT = -7,
				//VK_ERROR_FEATURE_NOT_PRESENT = -8,
				//VK_ERROR_INCOMPATIBLE_DRIVER = -9,
				//VK_ERROR_TOO_MANY_OBJECTS = -10,
				//VK_ERROR_FORMAT_NOT_SUPPORTED = -11,
				//VK_ERROR_FRAGMENTED_POOL = -12
			}
			std::cout << std::endl;

			throw std::runtime_error("Failed to submit command buffer!");
		}

		//Render The deferred results
		waitSemaphores[0] = offScreenFinishedSemaphores[currentFrame];
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;

		signalRenderCommandDoneSemaphores[0] = resolveFinishedSemaphores[currentFrame];
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalRenderCommandDoneSemaphores;


		buffers[0] =run.resolveCommandBuffers[currentFrame];
		submitInfo.pCommandBuffers = buffers;
		submitInfo.commandBufferCount = 1;

		vkResetFences(vdi->device, 1, &resolveInFlightFences[currentFrame]);
		res = vkQueueSubmit(vdi->graphicsQueue, 1, &submitInfo, resolveInFlightFences[currentFrame]);


		if (res != VK_SUCCESS)
		{
			std::cout << "Error submitting deferred resolve command buffer";
			switch (res)
			{
			case VK_ERROR_DEVICE_LOST:
				std::cout << ": Device lost";
				break;
			case VK_ERROR_OUT_OF_HOST_MEMORY:
				std::cout << ": out of Host memory";
				break;

			case	VK_ERROR_OUT_OF_DEVICE_MEMORY:
				std::cout << ": out of device memory";
				break;
				//VK_ERROR_INITIALIZATION_FAILED = -3,

				//VK_ERROR_MEMORY_MAP_FAILED = -5,
				//VK_ERROR_LAYER_NOT_PRESENT = -6,
				//VK_ERROR_EXTENSION_NOT_PRESENT = -7,
				//VK_ERROR_FEATURE_NOT_PRESENT = -8,
				//VK_ERROR_INCOMPATIBLE_DRIVER = -9,
				//VK_ERROR_TOO_MANY_OBJECTS = -10,
				//VK_ERROR_FORMAT_NOT_SUPPORTED = -11,
				//VK_ERROR_FRAGMENTED_POOL = -12
			}
			std::cout << std::endl;

			throw std::runtime_error("Failed to submit command buffer!");
		}

		//	//Immediate and GUI
		waitSemaphores[0] = resolveFinishedSemaphores[currentFrame];
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;

		signalRenderCommandDoneSemaphores[0] = immediateFinishedSemaphores[currentFrame];
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalRenderCommandDoneSemaphores;



		buffers[0] = run.forwardLitCommandBuffers[currentFrame];
		buffers[1] = uiCommandBuffers[currentFrame];
		if (renderUI)//renderui
			submitInfo.commandBufferCount = 2;
		else
			submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = buffers;

		vkResetFences(vdi->device, 1, &immediateInFlightFences[currentFrame]);
		res = vkQueueSubmit(vdi->graphicsQueue, 1, &submitInfo, immediateInFlightFences[currentFrame]);

		if (res != VK_SUCCESS)
		{
			std::cout << "Error submitting immediate or UI command buffer";
			switch (res)
			{
			case VK_ERROR_DEVICE_LOST:
				std::cout << ": Device lost";
				break;
			case VK_ERROR_OUT_OF_HOST_MEMORY:
				std::cout << ": out of Host memory";
				break;

			case	VK_ERROR_OUT_OF_DEVICE_MEMORY:
				std::cout << ": out of device memory";
				break;
				//VK_ERROR_INITIALIZATION_FAILED = -3,

				//VK_ERROR_MEMORY_MAP_FAILED = -5,
				//VK_ERROR_LAYER_NOT_PRESENT = -6,
				//VK_ERROR_EXTENSION_NOT_PRESENT = -7,
				//VK_ERROR_FEATURE_NOT_PRESENT = -8,
				//VK_ERROR_INCOMPATIBLE_DRIVER = -9,
				//VK_ERROR_TOO_MANY_OBJECTS = -10,
				//VK_ERROR_FORMAT_NOT_SUPPORTED = -11,
				//VK_ERROR_FRAGMENTED_POOL = -12
			}
			std::cout << std::endl;
			throw std::runtime_error("Failed to submit command buffer!");
		}
		HE2_Profiler::End(HE2_Profiler::Stage::RenderObjects);

	}

	//Present
	HE2_Profiler::Begin(HE2_Profiler::Stage::Present);

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalRenderCommandDoneSemaphores;

	VkSwapchainKHR swapChains[] = { swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;

	result = vkQueuePresentKHR(vdi->presentQueue, &presentInfo);

	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized ) {
		framebufferResized = false;
		recreateSwapChain();
	}
	else if (result != VK_SUCCESS) {
		throw std::runtime_error("failed to present swap chain image!");
	}

	currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
	HE2_Profiler::End(HE2_Profiler::Stage::Present);
	

	//Command Buffer Build
	{
		//Rebuild the comand buffers if needed
		HE2_Profiler::Begin(HE2_Profiler::Stage::RebuildObjectCommandBuffers);
		buildMainCommandBuffers(currentFrame);
		buildImmediateCommandBuffers(currentFrame);
		HE2_Profiler::End(HE2_Profiler::Stage::RebuildObjectCommandBuffers);

		//SHould be render UI in here but there's no saving if renderUI turned back on yet, because buffer will still be in use
		if (true)
		{
			HE2_Profiler::Begin(HE2_Profiler::Stage::RebuildUICommandBuffers);
			buildUICommandBuffers(currentFrame);
			HE2_Profiler::End(HE2_Profiler::Stage::RebuildUICommandBuffers);
		}

		HE2_Profiler::Begin(HE2_Profiler::Stage::GPUTaskManagement);
		if (pendingTask != nullptr)
		{
			if (vkGetFenceStatus(vdi->device, pendingTask->generatingFence) == VK_SUCCESS)
			{
				vkResetFences(vdi->device, 1, &pendingTask->generatingFence);
				pendingTask->host->onComplete();
				pendingTask = nullptr;
			}
		}
		else if (gpuTasks.size() > 0 && !runGPUTasks)
		{
			buildGPUTaskCommandBuffer(currentFrame);
			runGPUTasks = true;
		}

		if (pendingComputeTask != nullptr)
		{
			if (vkGetFenceStatus(vdi->device, pendingComputeTask->generatingFence) == VK_SUCCESS)
			{
				vkResetFences(vdi->device, 1, &pendingComputeTask->generatingFence);
				pendingComputeTask->host->onComplete();
				pendingComputeTask = nullptr;
			}
		}
		else if (gpuComputeTasks.size() > 0 && !runComputeGPUTasks)
		{
			buildComputeCommandBuffer(currentFrame);
			runComputeGPUTasks = true;
		}
		HE2_Profiler::End(HE2_Profiler::Stage::GPUTaskManagement);
		

	}

	renderer->reSortData(renderRuns);
}

void HE2_VulkanBackend::queueGPUTask(HE2_GPUTask * task)
{
	if (task->isCompute)
		gpuComputeTasks.push(task);
	else
		gpuTasks.push(task);
}

ImTextureID HE2_VulkanBackend::getImGuiTextureHandle(HE2_Image* image)
{
	VulkanImageReference ref = VulkanTextures::getRef(image);

	ImTextureID id = ImGui_ImplVulkan_AddTexture(defaultSampler, ref.imageView, ref.layout);

	image->imGuiHandle = id;

	return id;
}

//Command Buffers
void HE2_VulkanBackend::buildMainCommandBuffers(uint32_t i)
{
	vkWaitForFences(vdi->device, 1, &offScreenInFlightFences[i], VK_TRUE, UINT64_MAX);

	//For now, this is going to do every render run. The intention is to make this much more selective

	for (VulkanRenderRun& run : renderRuns)
	{

		vkResetCommandBuffer(run.deferredCommandBuffers[i], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);

		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

		if (vkBeginCommandBuffer(run.deferredCommandBuffers[i], &beginInfo) != VK_SUCCESS) {
			throw std::runtime_error("failed to begin recording command buffer!");
		}
		VkRenderPassBeginInfo renderPassInfo = {};

		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;

		std::array<VkClearValue, 4> clearValues4 = {};
		clearValues4[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
		clearValues4[1].color = { 0.0f, 0.0f, 0.0f, 1.0f };
		clearValues4[2].color = { 0.0f, 0.0f, 0.0f, 1.0f };
		clearValues4[3].depthStencil = { 1.0f, 0 };

		renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues4.size());
		renderPassInfo.pClearValues = clearValues4.data();

		renderPassInfo.framebuffer = run.deferredRenderFramebuffers[i];
		renderPassInfo.renderPass = offScreenRenderPass;
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = swapChainExtent;


		{
			vkCmdBeginRenderPass(run.deferredCommandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

			vkCmdBindPipeline(run.deferredCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, deferredShader->getComponent<VulkanPipeline>()->pipeline);
			renderer->commandBuffer = &run.deferredCommandBuffers[i];
			renderer->frameIndex = i;

			

			recordDeferredCommands(run.runGraph);

			vkCmdEndRenderPass(run.deferredCommandBuffers[i]);
		}

		if (vkEndCommandBuffer(run.deferredCommandBuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to record command buffer!");
		}
	}
}

void HE2_VulkanBackend::buildUICommandBuffers(uint32_t i)
{
	vkWaitForFences(vdi->device, 1, &immediateInFlightFences[i], VK_TRUE, UINT64_MAX);



	ui->renderUI();

	vkResetCommandBuffer(uiCommandBuffers[i], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

	if (vkBeginCommandBuffer(uiCommandBuffers[i], &beginInfo) != VK_SUCCESS) {
		throw std::runtime_error("failed to begin recording command buffer!");
	}

	VkRenderPassBeginInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = uiRenderPass;
	renderPassInfo.framebuffer = swapChainUIFramebuffers[i];
	renderPassInfo.renderArea.offset = { 0, 0 };
	renderPassInfo.renderArea.extent = swapChainExtent;

	std::array<VkClearValue, 2> clearValues = {};
	clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
	clearValues[1].depthStencil = { 1.0f,  0 };

	renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
	renderPassInfo.pClearValues = clearValues.data();
	//Render that shit
	vkCmdBeginRenderPass(uiCommandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	ui->callUICmds(&uiCommandBuffers[i]);

	vkCmdEndRenderPass(uiCommandBuffers[i]);


	if (vkEndCommandBuffer(uiCommandBuffers[i]) != VK_SUCCESS) {
		throw std::runtime_error("failed to record command buffer!");
	}
	
	ui->finishUI();
}

void HE2_VulkanBackend::buildDeferredResolveCommandBuffers(uint32_t i)
{
	vkWaitForFences(vdi->device, 1, &resolveInFlightFences[i], VK_TRUE, UINT64_MAX);

	for (VulkanRenderRun& run : renderRuns)
	{

		vkResetCommandBuffer(run.resolveCommandBuffers[i], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);

		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

		if (vkBeginCommandBuffer(run.resolveCommandBuffers[i], &beginInfo) != VK_SUCCESS) {
			throw std::runtime_error("failed to begin recording command buffer!");
		}

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = resolveRenderPass;
		renderPassInfo.framebuffer = run.deferredResolveFramebuffers[i];
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = swapChainExtent;

		std::array<VkClearValue, 1> clearValues = {};
		clearValues[0].color = { 0.0f, 0.0f, 0.0f, 0.0f };


		renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
		renderPassInfo.pClearValues = clearValues.data();

		//Resolve
		{
			vkCmdBeginRenderPass(run.resolveCommandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

			vkCmdBindPipeline(run.resolveCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, deferredResolveShader->getComponent<VulkanPipeline>()->pipeline);

			renderer->commandBuffer = &run.resolveCommandBuffers[i];
			renderer->frameIndex = i;

			//Bind Descriptor set with the 3 images
			vkCmdBindDescriptorSets(run.resolveCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, deferredResolveShader->getComponent<VulkanPipeline>()->pipelineLayout, 0, 1, &run.deferredDescriptorSets[i], 0, 0);

			//Draw
			vkCmdDraw(run.resolveCommandBuffers[i], 6, 1, 0, 0);

			vkCmdEndRenderPass(run.resolveCommandBuffers[i]);
		}

		if (vkEndCommandBuffer(run.resolveCommandBuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to record command buffer!");
		}
	}

}

void HE2_VulkanBackend::buildImmediateCommandBuffers(uint32_t i)
{
	vkWaitForFences(vdi->device, 1, &immediateInFlightFences[i], VK_TRUE, UINT64_MAX);

	for (VulkanRenderRun& run : renderRuns)
	{

		//dirty = false;
		vkResetCommandBuffer(run.forwardLitCommandBuffers[i], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);

		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

		if (vkBeginCommandBuffer(run.forwardLitCommandBuffers[i], &beginInfo) != VK_SUCCESS) {
			throw std::runtime_error("failed to begin recording command buffer!");
		}

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = forwardLitRenderPass;
		renderPassInfo.framebuffer = run.forwardLitFramebuffers[i];
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = swapChainExtent;

		std::array<VkClearValue, 3> clearValues = {};
		clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
		clearValues[1].color = { 0.0f, 0.0f, 0.0f, 0.0f };
		clearValues[2].depthStencil = { 1.0f, 0 };

		std::array<VkClearValue, 3> clearValuesEditorMode = {};
		clearValuesEditorMode[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
		clearValuesEditorMode[1].color = { 0.0f, 0.0f, 0.0f, 0.0f };
		clearValuesEditorMode[2].depthStencil = { 1.0f, 0 };



		renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
		renderPassInfo.pClearValues = clearValues.data();

		if (settings.sceneEditorMode)
		{
			renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValuesEditorMode.size());
			renderPassInfo.pClearValues = clearValuesEditorMode.data();
		}

		//First renderpass
		{
			vkCmdBeginRenderPass(run.forwardLitCommandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

			renderer->commandBuffer = &run.forwardLitCommandBuffers[i];
			renderer->frameIndex = i;

			recordImmediateCommands(run.runGraph);

			vkCmdEndRenderPass(run.forwardLitCommandBuffers[i]);
		}

		if (vkEndCommandBuffer(run.forwardLitCommandBuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to record command buffer!");
		}
	}
}

void HE2_VulkanBackend::buildGPUTaskCommandBuffer(uint32_t imageIndex)
{
	vkQueueWaitIdle(vdi->gpuRenderTaskQueue);

	vkResetCommandBuffer(gpuTaskCommandBuffers[imageIndex], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

	if (vkBeginCommandBuffer(gpuTaskCommandBuffers[imageIndex], &beginInfo) != VK_SUCCESS) {
		throw std::runtime_error("failed to begin recording command buffer!");
	}


	HE2_GPUTask* task = gpuTasks.front();
	VulkanPipeline * pipeline = task->shader->getComponent<VulkanPipeline>();
	VulkanGPUTaskAttachment* taskAtt = task->getComponent<VulkanGPUTaskAttachment>();

	gpuTasks.pop();

	pendingTasks.push(task);

	VkRenderPassBeginInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = pipeline->uniqueRenderpass;
	renderPassInfo.framebuffer = taskAtt->framebuffers[imageIndex];
	renderPassInfo.renderArea.offset = { 0, 0 };

	float width = swapChainExtent.width;
	float height = swapChainExtent.height;

	glm::vec2 size =pipeline->shader->shaderSettings.outputs[0].size;
	if (size != glm::vec2(0))
	{
		width = size.x;
		height = size.y;
	}

	VkExtent2D extent = { (uint32_t)width, (uint32_t)height };
	renderPassInfo.renderArea.extent = extent;

	renderPassInfo.clearValueCount = static_cast<uint32_t>(pipeline->clearValues.size());
	renderPassInfo.pClearValues = pipeline->clearValues.data();

	vkCmdBeginRenderPass(gpuTaskCommandBuffers[imageIndex], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
		
	int setOffset = 0;
	for (auto x : pipeline->shader->shaderSpecificFeatures)
	{
		int setIndex = (setOffset * HE2_RenderBackend::imageCount) + imageIndex;

		vkCmdBindDescriptorSets(gpuTaskCommandBuffers[imageIndex], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->pipelineLayout, x.first, 1, &taskAtt->specificFeatureDescriptorSets[setIndex], 0, nullptr);

		setOffset++;
	}

	vkCmdBindPipeline(gpuTaskCommandBuffers[imageIndex], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->pipeline);

	vkCmdDraw(gpuTaskCommandBuffers[imageIndex], task->drawCount, 1, 0, 0);

	vkCmdEndRenderPass(gpuTaskCommandBuffers[imageIndex]);
		
	//Tranisition image to sampled layout
	
	if (vkEndCommandBuffer(gpuTaskCommandBuffers[imageIndex]) != VK_SUCCESS) {
		throw std::runtime_error("failed to record command buffer!");
	}
}

void HE2_VulkanBackend::buildComputeCommandBuffer(uint32_t imageIndex)
{
	vkQueueWaitIdle(vdi->computeQueue);

	vkResetCommandBuffer(computeCommandBuffers[imageIndex], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

	if (vkBeginCommandBuffer(computeCommandBuffers[imageIndex], &beginInfo) != VK_SUCCESS) {
		throw std::runtime_error("failed to begin recording command buffer!");
	}


	HE2_GPUTask* task = gpuComputeTasks.front();
	VulkanPipeline* pipeline = task->shader->getComponent<VulkanPipeline>();
	VulkanGPUTaskAttachment* taskAtt = task->getComponent<VulkanGPUTaskAttachment>();

	gpuComputeTasks.pop();

	pendingComputeTasks.push(task);


	int setOffset = 0;
	for (auto x : pipeline->shader->shaderSpecificFeatures)
	{
		int setIndex = (setOffset * HE2_RenderBackend::imageCount) + imageIndex;

		vkCmdBindDescriptorSets(computeCommandBuffers[imageIndex], VK_PIPELINE_BIND_POINT_COMPUTE, pipeline->pipelineLayout, x.first, 1, &taskAtt->specificFeatureDescriptorSets[setIndex], 0, nullptr);

		setOffset++;
	}

	vkCmdBindPipeline(computeCommandBuffers[imageIndex], VK_PIPELINE_BIND_POINT_COMPUTE, pipeline->pipeline);

	//x,y,z task size
	vkCmdDispatch(computeCommandBuffers[imageIndex], task->workGroupCompute.x / task->tileSizeCompute.x, task->workGroupCompute.y/ task->tileSizeCompute.y, task->workGroupCompute.z /task->tileSizeCompute.z);

	if (vkEndCommandBuffer(computeCommandBuffers[imageIndex]) != VK_SUCCESS) {
		throw std::runtime_error("failed to record command buffer!");
	}

}

void HE2_VulkanBackend::recordImmediateCommands(VulkanResourceGraph* runGraph)
{
	//Render to all targets that are not the screen (Water/Mirrors/Cubemaps etc)
	renderer->renderPass(runGraph->preRenderPasses);

	//Generate all shadow maps
	renderer->renderShadows();

	//Render all opaque objects
	renderer->renderPassData(runGraph->opaquePass,  true);

	//Render all transparent objects
	renderer->renderPassData(runGraph->transparentPass,  true);

	//Render all UI objects
	renderer->renderPassData(runGraph->GUIpass, true);
	
}

void HE2_VulkanBackend::recordDeferredCommands(VulkanResourceGraph* runGraph)
{

	renderer->currentPipeline = deferredShader->getComponent<VulkanPipeline>();

	renderer->renderPassData(runGraph->deferredOpaquePass, true);

	renderer->renderPassData(runGraph->deferredTransparentPass, true);
}

void HE2_VulkanBackend::updateLighting()
{
	void* lightingData;

	auto lights = HE2_Lighting::getLights();

	LightingBuffer buff = {};
	buff.actLightCount = lights.size();
	for (int lightIndex = 0; lightIndex < lights.size() && lightIndex < 200; lightIndex++)
	{
		buff.lights[lightIndex].color = lights[lightIndex]->colour;

		//lightPos.y = -lightPos.y;
		buff.lights[lightIndex].position = glm::vec4( lights[lightIndex]->getPos(), 1.0f);
		buff.lights[lightIndex].radius = lights[lightIndex]->radius;
	}

	glm::vec3 pos = HE2_Camera::main->getPos();

	buff.viewPos = glm::vec4(pos, 1.0f);

	//Check if change
	if (lastLights == buff)
	{
		return;
	}
	//Go ahead with update!

	lastLights = buff;

	updateGPUBuffer(lightingBufferHandle, &buff, sizeof(buff));
}

std::vector<HE2_Image*> HE2_VulkanBackend::getDeferredBuffers()
{
	std::vector<HE2_Image*>  images;
	images.push_back(renderRuns[0].defAlbedoRef.image);
	images.push_back(renderRuns[0].defNormalRef.image);
	images.push_back(renderRuns[0].defPositionRef.image);
	return images;
}

void HE2_VulkanBackend::rebuildShader(HE2_Shader* shader)
{
	destroyShader(shader);
	
	for (int i = 0; i < pipelines.size(); i++)
	{
		if (pipelines[i]->shader == shader)
		{
			pipelines.erase(pipelines.begin() + i);
			break;
		}
	}

	buildShader(shader);
}

void HE2_VulkanBackend::copyImage(HE2_Image* srcImage, HE2_Image* dstImage)
{
	VulkanImageReference srcRef = VulkanTextures::getRef(srcImage);
	VulkanImageReference dstRef = VulkanTextures::getRef(dstImage);
	
	vulkanAlloc->copyImage(srcRef, dstRef);
}

void HE2_VulkanBackend::copyImageDataToBuffer(void* output, size_t& outSize, HE2_Image* image)
{
	VulkanImageReference ref = VulkanTextures::getRef(image);

	uint32_t formatSizeBytes = 0;

	if (ref.format == VK_FORMAT_R16G16B16A16_SFLOAT)
		formatSizeBytes = 8;
	else if
		(ref.format == VK_FORMAT_R8G8B8A8_UNORM)
		formatSizeBytes = 4;
	else if
		(ref.format == VK_FORMAT_R32G32B32A32_SFLOAT)
		formatSizeBytes = 16;

	if (formatSizeBytes == 0)
		throw std::runtime_error("No size for format");

	uint32_t imageSizeBytes = ref.width * ref.height * formatSizeBytes;

	VkBuffer transferBuffer;
	VmaAllocation transferAlloc;

	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = imageSizeBytes;
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT;

	outSize = imageSizeBytes;

	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;

	vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &transferBuffer, &transferAlloc, nullptr);

	//Transition the image
	VulkanTextures::transitionImageLayout(ref.vkImage, ref.format, ref.layout, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, ref.mipLevels);

	//Method 2: blit the image to something with linear tiling
	VkImage transferImage;
	VmaAllocation transferImageAllocation;

	VmaAllocationCreateInfo allocInfo2 = {};
	allocInfo2.usage = VMA_MEMORY_USAGE_GPU_ONLY;

	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = static_cast<uint32_t>(ref.width);
	imageInfo.extent.height = static_cast<uint32_t>(ref.height);
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = ref.mipLevels;
	imageInfo.arrayLayers = 1;

	imageInfo.format = ref.format;
	imageInfo.tiling = VK_IMAGE_TILING_LINEAR;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;

	vmaCreateImage(allocator, &imageInfo, &allocInfo2, &transferImage, &transferImageAllocation, nullptr);

	VulkanTextures::transitionImageLayout(transferImage, ref.format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1);



	VkCommandBuffer cmdbffer = beginSingleTimeCommands();

	
	//VkImageBlit blit = {};
	//blit.srcOffsets[0] = { 0, 0, 0 };
	//blit.srcOffsets[1] = { static_cast<int32_t>(ref.width), static_cast<int32_t>(ref.height), 1 };
	//blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	//blit.srcSubresource.mipLevel = 0;
	//blit.srcSubresource.baseArrayLayer = 0;
	//blit.srcSubresource.layerCount = 1;
	//blit.dstOffsets[0] = { 0, 0, 0 };
	//blit.dstOffsets[1] = { static_cast<int32_t>(ref.width), static_cast<int32_t>(ref.height),1 };
	//blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	//blit.dstSubresource.mipLevel = 0;
	//blit.dstSubresource.baseArrayLayer = 0;
	//blit.dstSubresource.layerCount = 1;

	////Blit the image to an image with linear tiling
	//vkCmdBlitImage(cmdbffer, ref.vkImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, transferImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &blit, VK_FILTER_LINEAR);

	VkImageCopy copy = {};

	copy.dstOffset = { 0,0,0 };
	copy.srcOffset = { 0,0,0 };
	copy.extent = { ref.width,ref.height,1 };

	copy.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	copy.srcSubresource.mipLevel = 0;
	copy.srcSubresource.baseArrayLayer = 0;
	copy.srcSubresource.layerCount = 1;

	copy.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	copy.dstSubresource.mipLevel = 0;
	copy.dstSubresource.baseArrayLayer = 0;
	copy.dstSubresource.layerCount = 1;

	vkCmdCopyImage(cmdbffer, ref.vkImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, transferImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy);

	endSingleTimeCommands(cmdbffer);

	VulkanTextures::transitionImageLayout(transferImage, ref.format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, 1);

	cmdbffer = beginSingleTimeCommands();

	VkBufferImageCopy region = {};
	region.bufferOffset = 0;
	region.bufferImageHeight = 0;
	region.bufferRowLength = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.layerCount = 1;
	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	region.imageExtent = { ref.width, ref.height, 1 };
	region.imageOffset = { 0,0,0 };

	//Copy the image to a buffer
	vkCmdCopyImageToBuffer(cmdbffer, transferImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, transferBuffer, 1, &region);

	endSingleTimeCommands(cmdbffer);

	VkImageSubresource subResource{ VK_IMAGE_ASPECT_COLOR_BIT, 0, 0 };
	VkSubresourceLayout subResourceLayout;
	vkGetImageSubresourceLayout(vdi->device, transferImage, &subResource, &subResourceLayout);

	//Transition the original image back for use later
	VulkanTextures::transitionImageLayout(ref.vkImage, ref.format, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, ref.layout,  ref.mipLevels);
	
	void* data = nullptr;
	vmaMapMemory(allocator, transferAlloc, &data);

	memcpy(output, data, imageSizeBytes);

	vmaUnmapMemory(allocator, transferAlloc);

	vkDestroyBuffer(vdi->device, transferBuffer, nullptr);
	vmaFreeMemory(allocator, transferAlloc);

	vmaDestroyImage(allocator, transferImage, transferImageAllocation);

	outSize = subResourceLayout.size;
}

#pragma region On Add Processes

void HE2_VulkanBackend::onAddObject(HE2_Object* obj)
{
	processObject(obj);
}

void HE2_VulkanBackend::processObject(HE2_Object* obj)
{
	//Add an object attachment
	VulkanObjectAttachment* voa = new VulkanObjectAttachment();

	//Assign an ID
	voa->ID = objectAttachments.size();

	

	voa->object = obj;

	voa->renderComponent = obj->getComponent<HE2_RenderComponent>();

	if (voa->renderComponent == nullptr) 
		throw std::runtime_error("Rendering component was nullptr");

	//Add to list!
	obj->addComponent(voa);
	objectAttachments.push_back(voa);

	//Grab the correct Pipeline for this shader
	for (auto a : pipelines)
	{
		if (a->shader == voa->renderComponent->getMaterial()->shader)
		{
			voa->shaderPipeline = a;
			break;
		}
	}
	if (voa->shaderPipeline == nullptr)
	{
		throw std::runtime_error("Shader doesn't exist!");
	}

	//Sets for shader-specific features
	createDescriptorSetsForObject(voa);

	//Add to the resource graph
	if (obj->getComponent<HE2_RenderComponent>()->getMaterial()->shader->shaderSettings.shaderType != HE2_ShaderType::HE2_SHADER_FRAGMENT_GPU_TASK)
	{
		for (auto& run : renderRuns)
		{
			if (run.doesWantObject(voa->renderComponent))
			{
				run.addObject(obj);
			}
		}

	}
	//Create a seperate gameobject for each of the model children
	for (HE2_Model* model : voa->renderComponent->getModel
	()->childMeshes)
	{
		HE2_RenderComponent* rc = new HE2_RenderComponent();

		rc->setModel(model);
		//Set material to default material
		rc->setMaterial(model->defaultMaterial);

		HE2_Object* object = new HE2_Object(obj, {rc});
	}
}

HE2_Image* HE2_VulkanBackend::createEmptyImage(bool forCompute, glm::vec2 size, bool cpuVisible)
{
	if (cpuVisible)
	{
		throw std::runtime_error("Creating CPU Visible images is not implemented yet!");
	}

	VkImageLayout layout = forCompute ? VK_IMAGE_LAYOUT_GENERAL : VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

	HE2_Image* image = new HE2_Image();



	VkFormat format =forCompute ? VK_FORMAT_R16G16B16A16_SFLOAT : VK_FORMAT_R8G8B8A8_UNORM;



	float width = swapChainExtent.width;
	float height = swapChainExtent.height;

	if (size != glm::vec2(0))
	{
		width = size.x;
		height = size.y;
	}

	VkImageUsageFlags usage = 0;
	VmaMemoryUsage memUsage = VMA_MEMORY_USAGE_GPU_ONLY;
	VkImageTiling tiling = VK_IMAGE_TILING_OPTIMAL;


	if (cpuVisible)
	{
		usage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		layout = VK_IMAGE_LAYOUT_GENERAL;
		memUsage = VMA_MEMORY_USAGE_CPU_ONLY;
		//tiling = VK_IMAGE_TILING_LINEAR;
	}
	else
	{
		usage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT |VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
	}

	if (forCompute)
		usage |= VK_IMAGE_USAGE_STORAGE_BIT;
	

	VulkanImageReference ref = {};
	ref.image = image;
	ref.mipLevels = 1;
	ref.layout = layout;
	ref.width = size.x;
	ref.height = size.y;
	ref.format = format;

	vulkanAlloc->createEmptyImage(width, height, 1, VK_SAMPLE_COUNT_1_BIT, format, tiling, usage, 0, ref.vkImage, ref.allocation, memUsage);

	if(!cpuVisible)
		ref.imageView = vulkanAlloc->createImageView(ref.vkImage, format, VK_IMAGE_ASPECT_COLOR_BIT, 1);

	VulkanTextures::transitionImageLayout(ref.vkImage, format, VK_IMAGE_LAYOUT_UNDEFINED, layout, 1);


	image->handle = VulkanTextures::addImage(ref);

	return image;
}

void HE2_VulkanBackend::makeUniqueFramebuffers(HE2_GPUTask* task)
{
	//TO DO: transition image to a general or src optimal layout, then tranisition it back to whatever it started in! :)


	VulkanGPUTaskAttachment* taskAtt = task->getComponent<VulkanGPUTaskAttachment>();

	size_t outputCount = task->shader->shaderSettings.outputs.size();

	taskAtt->framebuffers.resize(swapChainImageViews.size());

	taskAtt->outputImageAllocations.resize(outputCount);
	taskAtt->outputImages.resize(outputCount);
	taskAtt->outputImageViews.resize(outputCount);

	//Create resources
	std::vector<VkImageView> attachments;
	for (int imageIndex = 0; imageIndex < outputCount; imageIndex++)
	{
		HE2_Image* image = new HE2_Image();

		VkFormat format = imageFormat(task->shader->shaderSettings.outputs[imageIndex].format);

		float width = swapChainExtent.width;
		float height = swapChainExtent.height;
		glm::vec2 size = task->shader->shaderSettings.outputs[imageIndex].size;
		if (size != glm::vec2(0))
		{
			width = size.x;
			height = size.y;
		}

		VkImageUsageFlags usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

		if (task->shader->shaderSettings.outputs[imageIndex].storageCapability)
			usage |= VK_IMAGE_USAGE_STORAGE_BIT;

		vulkanAlloc->createEmptyImage(width, height, 1, VK_SAMPLE_COUNT_1_BIT, format, VK_IMAGE_TILING_OPTIMAL, usage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, taskAtt->outputImages[imageIndex], taskAtt->outputImageAllocations[imageIndex]);//, VMA_MEMORY_USAGE_GPU_TO_CPU


		taskAtt->outputImageViews[imageIndex] = vulkanAlloc->createImageView(taskAtt->outputImages[imageIndex], format, VK_IMAGE_ASPECT_COLOR_BIT, 1);

		VkImageLayout layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		if (task->shader->shaderSettings.outputs[imageIndex].storageCapability)
		{
			layout = VK_IMAGE_LAYOUT_GENERAL;
		}

		VulkanTextures::transitionImageLayout(taskAtt->outputImages[imageIndex], format, VK_IMAGE_LAYOUT_UNDEFINED, layout, 1);

		attachments.push_back(taskAtt->outputImageViews[imageIndex]);


		VulkanImageReference ref = {};
		ref.image = image;
		ref.vkImage = taskAtt->outputImages[imageIndex];
		ref.imageView = taskAtt->outputImageViews[imageIndex];
		ref.allocation = taskAtt->outputImageAllocations[imageIndex];
		ref.mipLevels = 1;
		ref.layout = layout;
		ref.format = format;
		ref.width = width;
		ref.height = height;
	
		task->getComponent<HE2_ShaderImageReturn>()->setImageOutput(image, imageIndex);

		image->handle = VulkanTextures::addImage(ref);
	}

	//And attach them to each framebuffer
	for (size_t i = 0; i < swapChainImageViews.size(); i++) 
	{
		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = task->shader->getComponent<VulkanPipeline>()->uniqueRenderpass;
		framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebufferInfo.pAttachments = attachments.data();

		float width = swapChainExtent.width;
		float height = swapChainExtent.height;
		glm::vec2 size = task->shader->shaderSettings.outputs[0].size;

		if (size != glm::vec2(0))
		{
			width = size.x;
			height = size.y;
		}


		framebufferInfo.width = width;
		framebufferInfo.height = height;
		framebufferInfo.layers = 1;

		if (vkCreateFramebuffer(vdi->device, &framebufferInfo, nullptr, &taskAtt->framebuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to create framebuffer!");
		}
	}
}

void HE2_VulkanBackend::onAddModel(HE2_Model* model)
{

	VulkanModelAttachment* vma = new VulkanModelAttachment();
	model->addComponent(vma);
	vma->model = model;

	if (model->indices.size() > 0 && model->vertices.size() > 0)
	{
		vulkanAlloc->createIndexBuffer(vma->indexBuffer, vma->indexAllocation, &model->indices);

		vulkanAlloc->createVertexBuffer(vma->vertexBuffer, vma->vertexAllocation, &model->vertices);

		vma->indices = model->indices.size();

		//Also import all meshes for children
		for (int i = 0; i < model->childMeshes.size(); i++)
		{
			onAddModel(model->childMeshes[i]);
		}
	}

	if (!model->retainVertexData)
	{
		model->vertices.clear();
		model->indices.clear();
	}
}

void HE2_VulkanBackend::createDescriptorSetsForObject(VulkanObjectAttachment* voa)
{
	if (voa->shaderPipeline->shader->shaderSpecificFeatures.size() == 0) return;


	HE2_ShaderBufferGetter* bufferGetter = voa->object->getComponent<HE2_ShaderBufferGetter>();
	HE2_ShaderImageHandleGetter* imageGetter = voa->object->getComponent<HE2_ShaderImageHandleGetter>();
	HE2_UniformValueGetter* uniformGetter = voa->object->getComponent<HE2_UniformValueGetter>();	HE2_ShaderStorageBufferGetter* storageBufferGetter = voa->object->getComponent<HE2_ShaderStorageBufferGetter>();

	if (bufferGetter == nullptr && imageGetter == nullptr && uniformGetter == nullptr)
		throw std::runtime_error("Buffergetting, uniform getter and imagegetter are nullptr!");
	voa->pushConstantValue = uniformGetter;

	for (auto group : voa->shaderPipeline->shader->shaderSpecificFeatures)
	{
		//Allocate enough sets for this group
		//

		int totalSetsToAllocateForThisGroup = swapChainImages.size();
		int startingIndex = voa->specificFeatureDescriptorSets.size();
		//Add the correct number of entries to the vector
		for (int i = 0; i < totalSetsToAllocateForThisGroup; i++)
			voa->specificFeatureDescriptorSets.push_back({});

		//Create a list of layouts
		std::vector<VkDescriptorSetLayout> layouts(totalSetsToAllocateForThisGroup, getLayoutFromFeatures(group.second, group.first));

		//Allocate the descriptors
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = voa->shaderPipeline->descriptorPool;
		allocInfo.descriptorSetCount = static_cast<uint32_t>(totalSetsToAllocateForThisGroup);
		allocInfo.pSetLayouts = layouts.data();

		if (vkAllocateDescriptorSets(vdi->device, &allocInfo, &voa->specificFeatureDescriptorSets[startingIndex]) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate descriptor sets!");
		}



		//For each set of commandBuffers (frames in flight +1)
		for (size_t i = 0; i < swapChainImages.size(); i++) {
			//Writes for this frame set
			std::vector<VkWriteDescriptorSet> descriptorWrites;

			VkDescriptorImageInfo imageInfos[64] = {};
			VkDescriptorBufferInfo bufferInfos[64] = {};

			int imageIndex = 0;
			int bufferIndex = 0;
			int featureIndex = 0;
			//per feature
			for (auto a : group.second)
			{
				//Stuff for buffers or images
				//Create a new descriptor write
				uint32_t index = descriptorWrites.size();
				descriptorWrites.push_back({});
				descriptorWrites[index].dstBinding = a.binding;
				descriptorWrites[index].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				descriptorWrites[index].dstSet = voa->specificFeatureDescriptorSets[startingIndex +i];
				descriptorWrites[index].descriptorCount = 1;
				descriptorWrites[index].dstArrayElement = 0;

				if (a.featureType == HE2_SHADER_FEATURE_TYPE_SAMPLER2D || a.featureType == HE2_SHADER_FEATURE_STORAGE_IMAGE)
				{
					if (imageGetter == nullptr)
						throw std::runtime_error("No Image getter component when shader type requires one!");
					VulkanImageReference ref = VulkanTextures::images[imageGetter->getImageHandle(a.grouping, a.binding)];
					imageInfos[imageIndex] = {};
					imageInfos[imageIndex].imageLayout = ref.layout;
					imageInfos[imageIndex].imageView = ref.imageView;


					if (!a.exactRead)
					{
						imageInfos[imageIndex].sampler = defaultSampler;
					}
					else
					{
						imageInfos[imageIndex].sampler = exactSampler;
					}
					if (a.featureType == HE2_SHADER_FEATURE_TYPE_SAMPLER2D)
						descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
					else
						descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;


					descriptorWrites[index].pImageInfo = &imageInfos[imageIndex];
					imageIndex++;

				}
				else if (a.featureType == HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK || HE2_SHADER_FEATURE_STORAGE_BUFFER)
				{

					bufferInfos[bufferIndex] = {};
					//Get buffer from handle (handle * frameCount + i)
					if (a.featureType == HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK)
					{
						bufferInfos[bufferIndex].buffer = shaderSpecificBuffers[(bufferGetter->getBufferHandle(a.grouping, a.binding) * swapChainImages.size()) + i];
					}
					else
					{
						bufferInfos[bufferIndex].buffer = storageBuffers[storageBufferGetter->getStorageBufferHandle(a.grouping, a.binding)];
					}

					bufferInfos[bufferIndex].offset = 0;
					bufferInfos[bufferIndex].range = a.size;

					if (a.featureType == HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK)
						descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
					else
						descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

					descriptorWrites[index].pBufferInfo = &bufferInfos[bufferIndex];
					bufferIndex++;

				}

			}
			vkUpdateDescriptorSets(vdi->device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
		}
	}
	
}

void HE2_VulkanBackend::createDescriptorPool(uint32_t pipelineIndex)
 {
	if (pipelines[pipelineIndex]->shader->features.size() == 0) return;

	 std::vector < VkDescriptorPoolSize> poolSizes;

	 for (int i = 0; i < pipelines[pipelineIndex]->shader->features.size(); i++)
	 {
		 VkDescriptorPoolSize ps = {};

		 ps.type =(VkDescriptorType)layoutBindingDescriptorType(pipelines[pipelineIndex]->shader->features[i].featureType);
		 ps.descriptorCount = static_cast<uint32_t>(swapChainImages.size() * MAX_OBJECTS);
		 
		 poolSizes.push_back(ps);
		 poolSizes.push_back(ps);
		 poolSizes.push_back(ps);
	 }

	 VkDescriptorPoolCreateInfo poolInfo = {};
	 poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	 poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	 poolInfo.pPoolSizes = poolSizes.data();
	 poolInfo.maxSets = static_cast<uint32_t>(swapChainImages.size() * MAX_OBJECTS);

	 if (vkCreateDescriptorPool(vdi->device, &poolInfo, nullptr, &pipelines[pipelineIndex]->descriptorPool) != VK_SUCCESS)
	 {
		 throw std::runtime_error("failed to create descriptor pool");
	 }

 }

void HE2_VulkanBackend::onAddMaterial(HE2_Material* material)
{
	VulkanMaterialAttachment* vma = new VulkanMaterialAttachment();
	vma->material = material;
	material->addComponent(vma);
	materialAttachments.push_back(vma);


	//Get the pipeline for this material
	VulkanPipeline* pipeline = vma->material->shader->getComponent<VulkanPipeline>();

	vma->pipelineAllocatedWith = pipeline;
	std::vector<HE2_ShaderFeature> materialFeatures = material->getMaterialShaderFeatures();

	if (materialFeatures.size() == 0)
	{
		//This is a bind-less material
		vma->groupings = 0;
		return;
	}
	else
	{
		//Supporting single-group materials only at the moment
		vma->groupings = 1;
	}

	std::vector<HE2_ImageHandle> imageHandles = material->getImageHandles();


	int shaderFeatureCount = materialFeatures.size();
	int totalSetsToAllocate = swapChainImages.size() * shaderFeatureCount;

	int grouping = materialFeatures[0].grouping;

	//Create a list of layouts
	std::vector<VkDescriptorSetLayout> layouts(totalSetsToAllocate, pipeline->featureLayouts[grouping].second);

	//Allocate the descriptors
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = pipeline->descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(totalSetsToAllocate);
	allocInfo.pSetLayouts = layouts.data();
	vma->descriptors.resize(totalSetsToAllocate);
	if (vkAllocateDescriptorSets(vdi->device, &allocInfo, vma->descriptors.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate descriptor sets!");
	}

	//For each set of commandBuffers (frames in flight +1)
	for (size_t i = 0; i < swapChainImages.size(); i++) {
		//Writes for this frame set
		std::vector<VkWriteDescriptorSet> descriptorWrites;

		std::vector<VkDescriptorImageInfo> imageInfos;
		imageInfos.resize(materialFeatures.size());
		int k = 0;
		//per feature
		for (auto a : materialFeatures)
		{
			imageInfos[k] = {};
			if (a.featureType != HE2_SHADER_FEATURE_TYPE_SAMPLER2D)
			{
				//Don't need to do anything here
				throw std::runtime_error("Non-Sampler material features are not implemented yet");
			}
			//Create a new descriptor write
			uint32_t index = descriptorWrites.size();

			descriptorWrites.push_back({});

			descriptorWrites[index].dstBinding = a.binding;
			descriptorWrites[index].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;

			descriptorWrites[index].dstSet = vma->descriptors[i];
			descriptorWrites[index].descriptorCount = 1;
			descriptorWrites[index].dstArrayElement = 0;

			VulkanImageReference ref = VulkanTextures::images[imageHandles[k]];

			imageInfos[k].imageLayout = ref.layout;
			imageInfos[k].imageView = ref.imageView;
			imageInfos[k].sampler = defaultSampler;

			descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			descriptorWrites[index].pImageInfo = &imageInfos[k];
			k++;
		}
		vkUpdateDescriptorSets(vdi->device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
	}

}

void HE2_VulkanBackend::createViewProjectionDescs(VulkanPipeline* pipeline)
{
	int totalSetsToAllocate = swapChainImages.size();

	//This is because it's possible to edit default features, for example to change their stage
	bool found = false;
	HE2_ShaderFeature accVPFeature = {};
	for (auto a : pipeline->shader->features)
	{
		if (a.source == HE2_SHADER_BUFFER_SOURCE_VIEW_PROJECTION_BUFFER)
		{
			accVPFeature = a;
			found = true;
			break;
		}
	}
	if (!found) throw std::runtime_error("Unable to find VP feature");

	//Create a list of layouts
	std::vector<VkDescriptorSetLayout> layouts(totalSetsToAllocate, getLayoutFromFeatures({ accVPFeature }, accVPFeature.grouping));

	//Allocate the descriptors
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = pipeline->descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(totalSetsToAllocate);
	allocInfo.pSetLayouts = layouts.data();
	pipeline->viewProjectionDescriptorSets.resize(swapChainImages.size());

	if (vkAllocateDescriptorSets(vdi->device, &allocInfo, pipeline->viewProjectionDescriptorSets.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate descriptor sets!");
	}

	//For each set of commandBuffers (frames in flight +1)
	for (size_t i = 0; i < swapChainImages.size(); i++) {
		//Writes for this frame set
		std::vector<VkWriteDescriptorSet> descriptorWrites;

		//per feature
		HE2_ShaderFeature a = accVPFeature;

		//Create a new descriptor write
		uint32_t index = descriptorWrites.size();

		descriptorWrites.push_back({});

		VkDescriptorBufferInfo bufferInfo = {};
		bufferInfo.offset = 0;
		bufferInfo.range = sizeof(ViewProjectionBuffer);
		bufferInfo.buffer = viewProjectionBuffers[i];

		descriptorWrites[index].dstBinding = a.binding;
		descriptorWrites[index].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;

		descriptorWrites[index].dstSet = pipeline->viewProjectionDescriptorSets[i];
		descriptorWrites[index].descriptorCount = 1;
		descriptorWrites[index].dstArrayElement = 0;

		descriptorWrites[index].pBufferInfo = &bufferInfo;

		descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;


		vkUpdateDescriptorSets(vdi->device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
	}
}

void HE2_VulkanBackend::prepareGPUTask(HE2_GPUTask* task)
{
	VulkanGPUTaskAttachment* taskAtt = new VulkanGPUTaskAttachment();

	taskAtt->shaderPipeline = task->shader->getComponent<VulkanPipeline>();
	taskAtt->host = task;
	VkFenceCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	vkCreateFence(vdi->device, &info, nullptr, &taskAtt->generatingFence);

	task->addComponent(taskAtt);

	if(task->shader->shaderSettings.outputs.size() > 0)
		makeUniqueFramebuffers(task);

	if (task->shader->shaderSpecificFeatures.size() == 0) return;

	HE2_ShaderBufferGetter* bufferGetter = task->getComponent<HE2_ShaderBufferGetter>();
	HE2_ShaderImageHandleGetter* imageGetter = task->getComponent<HE2_ShaderImageHandleGetter>();
	HE2_UniformValueGetter* uniformGetter = task->getComponent<HE2_UniformValueGetter>();
	HE2_ShaderImageReturn* imageReturn = task->getComponent<HE2_ShaderImageReturn>();
	HE2_ShaderStorageBufferGetter* storageBufferGetter = task->getComponent<HE2_ShaderStorageBufferGetter>();

	if (bufferGetter == nullptr && imageGetter == nullptr && uniformGetter == nullptr && imageReturn == nullptr)
		throw std::runtime_error("Buffergetting, uniform getter, image return and imagegetter are nullptr!");

	taskAtt->pushConstantValue = uniformGetter;

	for (auto group : task->shader->shaderSpecificFeatures)
	{
		//Allocate enough sets for this group
		//

		int totalSetsToAllocateForThisGroup = swapChainImages.size();
		int startingIndex = taskAtt->specificFeatureDescriptorSets.size();

		//Add the correct number of entries to the vector
		for (int i = 0; i < totalSetsToAllocateForThisGroup; i++)
			taskAtt->specificFeatureDescriptorSets.push_back({});

		//Create a list of layouts
		std::vector<VkDescriptorSetLayout> layouts(totalSetsToAllocateForThisGroup, getLayoutFromFeatures(group.second, group.first));

		//Allocate the descriptors
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = taskAtt->shaderPipeline->descriptorPool;
		allocInfo.descriptorSetCount = static_cast<uint32_t>(totalSetsToAllocateForThisGroup);
		allocInfo.pSetLayouts = layouts.data();

		if (vkAllocateDescriptorSets(vdi->device, &allocInfo, &taskAtt->specificFeatureDescriptorSets[startingIndex]) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate descriptor sets!");
		}



		//For each set of commandBuffers (frames in flight +1)
		for (size_t i = 0; i < swapChainImages.size(); i++) {
			//Writes for this frame set
			std::vector<VkWriteDescriptorSet> descriptorWrites;

			VkDescriptorImageInfo imageInfos[64] = {};
			VkDescriptorBufferInfo bufferInfos[64] = {};

			int imageIndex = 0;
			int bufferIndex = 0;
			int featureIndex = 0;
			//per feature
			for (auto a : group.second)
			{
				//Stuff for buffers or images
				//Create a new descriptor write
				uint32_t index = descriptorWrites.size();
				descriptorWrites.push_back({});
				descriptorWrites[index].dstBinding = a.binding;
				descriptorWrites[index].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				descriptorWrites[index].dstSet = taskAtt->specificFeatureDescriptorSets[startingIndex + i];
				descriptorWrites[index].descriptorCount = 1;
				descriptorWrites[index].dstArrayElement = 0;

				if (a.featureType == HE2_SHADER_FEATURE_TYPE_SAMPLER2D || a.featureType == HE2_SHADER_FEATURE_STORAGE_IMAGE)
				{
					if (imageGetter == nullptr)
						throw std::runtime_error("No Image getter component when shader type requires one!");
					VulkanImageReference ref = VulkanTextures::images[imageGetter->getImageHandle(a.grouping, a.binding)];
					imageInfos[imageIndex] = {};
					imageInfos[imageIndex].imageLayout = ref.layout;
					imageInfos[imageIndex].imageView = ref.imageView;
					

					if (!a.exactRead)
					{
						imageInfos[imageIndex].sampler = defaultSampler;
					}
					else
					{
						imageInfos[imageIndex].sampler = exactSampler;
					}
					if(a.featureType == HE2_SHADER_FEATURE_TYPE_SAMPLER2D)
						descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
					else
						descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;


					descriptorWrites[index].pImageInfo = &imageInfos[imageIndex];
					imageIndex++;

				}
				else if (a.featureType == HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK || HE2_SHADER_FEATURE_STORAGE_BUFFER)
				{

					bufferInfos[bufferIndex] = {};
					//Get buffer from handle (handle * frameCount + i)
					if (a.featureType == HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK)
					{
						bufferInfos[bufferIndex].buffer = shaderSpecificBuffers[(bufferGetter->getBufferHandle(a.grouping, a.binding) * swapChainImages.size()) + i];
					}
					else
					{
						bufferInfos[bufferIndex].buffer = storageBuffers[storageBufferGetter->getStorageBufferHandle(a.grouping, a.binding)];
					}

					bufferInfos[bufferIndex].offset = 0;
					bufferInfos[bufferIndex].range = a.size;

					if(a.featureType == HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK)
						descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
					else
						descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

					descriptorWrites[index].pBufferInfo = &bufferInfos[bufferIndex];
					bufferIndex++;

				}



			}
			vkUpdateDescriptorSets(vdi->device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
		}
	}
}

#pragma endregion

#pragma region CleanUp Methods

void HE2_VulkanBackend::destroyShader(HE2_Shader* shader)
{
	VulkanPipeline* x = nullptr;
	for (auto y : pipelines)
	{
		if (y->shader == shader)
		{
			x = y;
			break;
		}
	}

	vkDestroyPipeline(vdi->device, x->pipeline, nullptr);
	vkDestroyPipelineLayout(vdi->device, x->pipelineLayout, nullptr);
	vkDestroyDescriptorPool(vdi->device, x->descriptorPool, nullptr);
}

void HE2_VulkanBackend::cleanup()
{
	cleanupSwapChain();

	vkDestroySampler(vdi->device, defaultSampler, nullptr);
	vkDestroySampler(vdi->device, deferredSampler, nullptr);
	vkDestroySampler(vdi->device, exactSampler, nullptr);

	//Destroy all images, imageviews and device memory associated

	//Destroy all Descriptor set layouts

	//Destroy and free all vertex and index buffers

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		vkDestroySemaphore(vdi->device, immediateFinishedSemaphores[i], nullptr);
		vkDestroySemaphore(vdi->device, imageAvailableSemaphores[i], nullptr);
		vkDestroyFence(vdi->device, immediateInFlightFences[i], nullptr);
	}

	vmaDestroyAllocator(allocator);

	vkDestroyCommandPool(vdi->device, commandPool, nullptr);

	vkDestroyDevice(vdi->device, nullptr);

	vdi->cleanUp();
}

void HE2_VulkanBackend::cleanupSwapChain()
{
	//Clean up all render runs
	for (auto& run : renderRuns)
	{
		run.cleanupResources();
	}

	VulkanTextures::cleanUpVkResources();


	//For each pipeline, destroy the vkPipeline, descriptor pool and piplineLayout
	//vkDestroyPipeline(vdi->device, graphicsPipeline, nullptr);
	//vkDestroyPipelineLayout(vdi->device, pipelineLayout, nullptr);
	//vkDestroyDescriptorPool(vdi->device, descriptorPool, nullptr);

	for (auto x : pipelines)
	{
		destroyShader(x->shader);
	}

	vkDestroyRenderPass(vdi->device, forwardLitRenderPass, nullptr);
	vkDestroyRenderPass(vdi->device, uiRenderPass, nullptr);
	vkDestroyRenderPass(vdi->device, offScreenRenderPass, nullptr);
	vkDestroyRenderPass(vdi->device, resolveRenderPass, nullptr);

	for (auto imageView : swapChainImageViews) {
		vkDestroyImageView(vdi->device, imageView, nullptr);
	}

	vkDestroySwapchainKHR(vdi->device, swapChain, nullptr);

	for (size_t i = 0; i < swapChainImages.size(); i++) {
		vkDestroyBuffer(vdi->device, viewProjectionBuffers[i], nullptr);
		vmaFreeMemory(allocator, viewProjectionBuffersAllocation[i]);
	}
}

#pragma endregion

#pragma region Setup Methods

void HE2_VulkanBackend::createSwapChain()
{
	SwapChainSupportDetails swapChainSupport = querySwapChainSupport(vdi->physicalDevice);

	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
	VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
	VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);

	imageCount = swapChainSupport.capabilities.minImageCount + 1;
	if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
		imageCount = swapChainSupport.capabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = vdi->surface;

	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	QueueFamilyIndices indices = vdi->findQueueFamilies(vdi->physicalDevice);
	uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };

	if (indices.graphicsFamily != indices.presentFamily) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}

	createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;

	createInfo.oldSwapchain = VK_NULL_HANDLE;

	if (vkCreateSwapchainKHR(vdi->device, &createInfo, nullptr, &swapChain) != VK_SUCCESS) {
		throw std::runtime_error("failed to create swap chain!");
	}

	vkGetSwapchainImagesKHR(vdi->device, swapChain, &imageCount, nullptr);
	swapChainImages.resize(imageCount);
	vkGetSwapchainImagesKHR(vdi->device, swapChain, &imageCount, swapChainImages.data());

	swapChainImageFormat = surfaceFormat.format;
	swapChainExtent = extent;
}

void HE2_VulkanBackend::recreateSwapChain()
{
	std::cout << "Rebuilding Swapchain Time!" << std::endl << std::endl;


	int width = 0, height = 0;
	glfwGetFramebufferSize(HE2_WindowHandler::window, &width, &height);
	while (width == 0 || height == 0) {
		glfwGetFramebufferSize(HE2_WindowHandler::window, &width, &height);
		glfwWaitEvents();
	}

	vkDeviceWaitIdle(vdi->device);

	cleanupSwapChain();

	createSwapChain();
	createImageViews();

	createForwardLitRenderPass();
	createUIRenderPass();
	createOffScreenRenderPass();
	createResolveRenderPass();
	
	createCommandPool();

	createSampler();

	createSyncObjects();

	createFramebuffers();

	createUniformBuffers();
	
	createCommandBuffers();

	//Recreate all Pipelines, descriptor pools and descriptor sets
	for (int i = 0; i < pipelines.size(); i ++)
	{
		makePipeline(pipelines[i]->shader, i, false);
	}

	for (auto& run : renderRuns)
	{
		run.createResources(swapChainImages.size(), swapChainImageFormat, msaaSamples, glm::vec2(swapChainExtent.width, swapChainExtent.height
		));
	}

	buildMainCommandBuffers(0);
	buildUICommandBuffers(0);
	buildImmediateCommandBuffers(0);

	for (int i = 0; i < swapChainImages.size(); i++)
	{
		buildDeferredResolveCommandBuffers(i);
	}

	//Loop through all VOAs and regenerate their descriptor sets
	std::vector<VulkanObjectAttachment*> voas = objectAttachments;
	objectAttachments.clear();
	for (auto i : voas)
	{
		for (auto desc : i->specificFeatureDescriptorSets)
		{
			vkFreeDescriptorSets(VulkanDeviceInstance::device, i->shaderPipeline->descriptorPool, 1, &desc);
		}
		i->specificFeatureDescriptorSets.clear();
		
		
		processObject(i->object);

		delete i;
	}

	//Same for VMAs
	std::vector<VulkanMaterialAttachment*> vmas = materialAttachments;
	materialAttachments.clear();
	for (auto i : vmas)
	{
		for (auto desc : i->descriptors)
		{
			vkFreeDescriptorSets(VulkanDeviceInstance::device, i->pipelineAllocatedWith->descriptorPool, 1, &desc);
		}		
		
		i->material->removeComponent<VulkanMaterialAttachment>();

		onAddMaterial(i->material);

		delete i;
	}

	//models don't need this treatment! :)

	VulkanTextures::recreateVkResources();
}

void HE2_VulkanBackend::createImageViews()
{
	swapChainImageViews.resize(swapChainImages.size());

	for (uint32_t i = 0; i < swapChainImages.size(); i++) {
		swapChainImageViews[i] = vulkanAlloc->createImageView(swapChainImages[i], swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1);
	}
}

void HE2_VulkanBackend::createDefaultGraphicsPipeline()
{
	HE2_ShaderSources shaderSources = {};
	shaderSources.vertexShaderSource = "Shaders/vert.spv";
	shaderSources.fragmentShaderSource = "Shaders/frag.spv";

	std::vector<HE2_DataInputDesc> dataDesc = HE2_Vertex::getAttributeDescriptionsHE2();

	HE2_PerVertexDataDesc vertInfoDesc = {};
	vertInfoDesc.stride = sizeof(HE2_Vertex);



	std::vector<HE2_ShaderFeature> features;
	features.push_back(HE2_Shader::viewProjectionFeature);
	features.push_back(HE2_Shader::modelMatrixUniformFeature);

	for (auto a : HE2_BlinnPhongMaterial::MaterialShaderFeatures())
		features.push_back(a);

	HE2_ShaderSettings basicSettings = {};
	basicSettings.allowBlending = true;
	HE2_Shader* basicShader = new HE2_Shader("Immediate Basic", shaderSources, features, dataDesc, vertInfoDesc, basicSettings);

	defaultShader = basicShader;

	//Deffered Resolution Shader
	HE2_ShaderSources defResSources = {};
	defResSources.vertexShaderSource = "shaders/deferred/deferred.vert.spv";
	defResSources.fragmentShaderSource = "shaders/deferred/deferred.frag.spv";

	std::vector<HE2_ShaderFeature> defResFeatures;

	HE2_ShaderFeature GBufferPosition = {};
	GBufferPosition.grouping = 0;
	GBufferPosition.binding = 1;
	GBufferPosition.stage = HE2_SHADER_STAGE_FRAGMENT;
	GBufferPosition.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	GBufferPosition.source = HE2_SHADER_IMAGE_SOURCE_GBUFFER_POSITION;
	
	HE2_ShaderFeature GBufferNormal = {};
	GBufferNormal.grouping = 0;
	GBufferNormal.binding = 2;
	GBufferNormal.stage = HE2_SHADER_STAGE_FRAGMENT;
	GBufferNormal.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	GBufferNormal.source = HE2_SHADER_IMAGE_SOURCE_GBUFFER_NORMAL;

	HE2_ShaderFeature GBufferAlbedo = {};
	GBufferAlbedo.grouping = 0;
	GBufferAlbedo.binding = 3;
	GBufferAlbedo.stage = HE2_SHADER_STAGE_FRAGMENT;
	GBufferAlbedo.featureType = HE2_SHADER_FEATURE_TYPE_SAMPLER2D;
	GBufferAlbedo.source = HE2_SHADER_IMAGE_SOURCE_GBUFFER_ALBEDO;

	HE2_ShaderFeature lights = {};
	lights.grouping = 0;
	lights.binding = 4;
	lights.featureType = HE2_SHADER_FEATURE_TYPE_UNIFORM_BLOCK;
	lights.source = HE2_SHADER_BUFFER_SOURCE_DEFERRED_LIGHTING_BUFFER;
	lights.stage = HE2_SHADER_STAGE_FRAGMENT;
	lights.size = sizeof(LightingBuffer);

	defResFeatures = { GBufferPosition, GBufferNormal, GBufferAlbedo, lights };

	HE2_PerVertexDataDesc defResDataDesc = {};

	defResDataDesc.stride = 0;
	defResDataDesc.nullData = true;

	HE2_ShaderSettings deffResSettings = {};
	deffResSettings.shaderType = HE2_ShaderType::HE2_SHADER_DEFERRED_RESOLVE;
	deffResSettings.allowBlending = true;

	deferredResolveShader = new HE2_Shader("Resolve", defResSources, defResFeatures, {}, defResDataDesc, deffResSettings);



	//Deffered Object Shader
	HE2_ShaderSources defObjSources = {};
	defObjSources.vertexShaderSource = "shaders/deferred/mrt.vert.spv";
	defObjSources.fragmentShaderSource = "shaders/deferred/mrt.frag.spv";


	//use almost everything the same as standard shader

	HE2_ShaderSettings defSettings = {};
	defSettings.shaderType = HE2_ShaderType::HE2_SHADER_DEFERRED;


	deferredShader = new HE2_Shader("Deferred", defObjSources, features, dataDesc, vertInfoDesc, defSettings);
}

void HE2_VulkanBackend::createCommandPool()
{
	QueueFamilyIndices queueFamilyIndices = vdi->findQueueFamilies(vdi->physicalDevice);

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	if (vkCreateCommandPool(vdi->device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
		throw std::runtime_error("failed to create command pool!");
	}

	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.computeFamily.value();

	if (vkCreateCommandPool(vdi->device, &poolInfo, nullptr, &computeCommandPool) != VK_SUCCESS) {
		throw std::runtime_error("failed to create command pool!");
	}
}

//FrameBuffers
void HE2_VulkanBackend::createFramebuffers()
{
	//UI Framebuffer
	swapChainUIFramebuffers.resize(swapChainImageViews.size());

	for (size_t i = 0; i < swapChainUIFramebuffers.size(); i++) {
		std::array<VkImageView, 1> attachments = {
			swapChainImageViews[i],
		};
		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = uiRenderPass;
		framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebufferInfo.pAttachments = attachments.data();
		framebufferInfo.width = swapChainExtent.width;
		framebufferInfo.height = swapChainExtent.height;
		framebufferInfo.layers = 1;

		if (vkCreateFramebuffer(vdi->device, &framebufferInfo, nullptr, &swapChainUIFramebuffers[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to create framebuffer!");
		}
	}
}

//Render Passes
void HE2_VulkanBackend::createForwardLitRenderPass()
{
	VkAttachmentDescription colorAttachment = {};
	colorAttachment.format = swapChainImageFormat;
	colorAttachment.samples = HE2_VulkanBackend::msaaSamples;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentDescription depthAttachment = {};
	depthAttachment.format = vdi->findDepthFormat();
	depthAttachment.samples = msaaSamples;
	depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	//depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentDescription colorAttachmentResolve = {};
	colorAttachmentResolve.format = swapChainImageFormat;
	colorAttachmentResolve.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachmentResolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachmentResolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachmentResolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	colorAttachmentResolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachmentResolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachmentResolve.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;


	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference depthAttachmentRef = {};
	depthAttachmentRef.attachment = 1;
	depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;


	VkAttachmentReference colorAttachmentResolveRef = {};
	colorAttachmentResolveRef.attachment = 2;
	colorAttachmentResolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;


	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;
	subpass.pDepthStencilAttachment = &depthAttachmentRef;
	subpass.pResolveAttachments = &colorAttachmentResolveRef;

	VkAttachmentReference resolveRef = {};
	resolveRef.attachment = 2;
	resolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	//This is only used on scene editor mode - so it's designed to resolve to a standard using image
	VkAttachmentDescription colorAttachmentResolveScene = {};
	colorAttachmentResolveScene.format = swapChainImageFormat;
	colorAttachmentResolveScene.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachmentResolveScene.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachmentResolveScene.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachmentResolveScene.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	colorAttachmentResolveScene.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachmentResolveScene.initialLayout = VK_IMAGE_LAYOUT_GENERAL;
	colorAttachmentResolveScene.finalLayout = VK_IMAGE_LAYOUT_GENERAL;


	VkSubpassDependency dependencyIn = {};
	dependencyIn.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencyIn.dstSubpass = 0;
	dependencyIn.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencyIn.srcAccessMask = 0;
	dependencyIn.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
	dependencyIn.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;


	VkSubpassDependency dependencyOut = {};
	dependencyOut.srcSubpass = 0;
	dependencyOut.dstSubpass = VK_SUBPASS_EXTERNAL;
	dependencyOut.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencyOut.srcAccessMask = 0;
	dependencyOut.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
	dependencyOut.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkSubpassDependency dependencies[] = { dependencyIn, dependencyOut };

	std::array<VkAttachmentDescription, 3> attachments = { colorAttachment, depthAttachment, colorAttachmentResolve };

	std::array<VkAttachmentDescription, 3> sceneEditorAttachments = { colorAttachment, depthAttachment, colorAttachmentResolveScene };


	VkRenderPassCreateInfo renderPassInfo = {};

	

	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderPassInfo.pAttachments = attachments.data();

	if (settings.sceneEditorMode)
	{
		renderPassInfo.attachmentCount = static_cast<uint32_t>(sceneEditorAttachments.size());
		renderPassInfo.pAttachments = sceneEditorAttachments.data();
		subpass.pResolveAttachments = &resolveRef;
	}
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	renderPassInfo.dependencyCount = 2;
	renderPassInfo.pDependencies = dependencies;

	if (vkCreateRenderPass(vdi->device, &renderPassInfo, nullptr, &forwardLitRenderPass) != VK_SUCCESS) {
		throw std::runtime_error("failed to create render pass!");
	}
}

void HE2_VulkanBackend::createOffScreenRenderPass()
{
	std::array<VkAttachmentDescription, 4> attachmentDescs = {};

	//VkSampleCountFlagBits msaaSamples = VK_SAMPLE_COUNT_1_BIT;

	// Init attachment properties
	for (uint32_t i = 0; i < 4; ++i)
	{
		attachmentDescs[i].samples = msaaSamples;
		attachmentDescs[i].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		//attachmentDescs[i].loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		attachmentDescs[i].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachmentDescs[i].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		attachmentDescs[i].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		if (i == 3)
		{
			attachmentDescs[i].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			attachmentDescs[i].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

			//attachmentDescs[i].loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			//attachmentDescs[i].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		}
		else
		{
			attachmentDescs[i].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			attachmentDescs[i].finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		}
	}

	// Formats
	attachmentDescs[0].format = VK_FORMAT_R16G16B16A16_SFLOAT;
	attachmentDescs[1].format = VK_FORMAT_R16G16B16A16_SFLOAT;
	attachmentDescs[2].format = VK_FORMAT_R8G8B8A8_UNORM;
	attachmentDescs[3].format = vdi->findDepthFormat();

	std::vector<VkAttachmentReference> colorReferences;
	colorReferences.push_back({ 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL });
	colorReferences.push_back({ 1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL });
	colorReferences.push_back({ 2, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL });

	VkAttachmentReference depthReference = {};
	depthReference.attachment = 3;
	depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.pColorAttachments = colorReferences.data();
	subpass.colorAttachmentCount = static_cast<uint32_t>(colorReferences.size());
	subpass.pDepthStencilAttachment = &depthReference;

	std::array<VkSubpassDependency, 2> dependencies;

	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[0].dstSubpass = 0;
	dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	dependencies[1].srcSubpass = 0;
	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
	dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;


	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.pAttachments = attachmentDescs.data();
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachmentDescs.size());
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	renderPassInfo.dependencyCount = 2;
	renderPassInfo.pDependencies = dependencies.data();

	if (vkCreateRenderPass(vdi->device, &renderPassInfo, nullptr, &offScreenRenderPass) != VK_SUCCESS)
	{
		throw std::runtime_error("Couldn't create offscreen render pass!");
	}
}

void HE2_VulkanBackend::createResolveRenderPass()
{
	VkAttachmentDescription colorAttachment = {};
	colorAttachment.format = swapChainImageFormat;
	colorAttachment.samples = HE2_VulkanBackend::msaaSamples;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;



	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;



	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;

	VkSubpassDependency dependencyIn = {};
	dependencyIn.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencyIn.dstSubpass = 0;
	dependencyIn.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencyIn.srcAccessMask = 0;
	dependencyIn.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
	dependencyIn.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;


	VkSubpassDependency dependencyOut = {};
	dependencyOut.srcSubpass = 0;
	dependencyOut.dstSubpass = VK_SUBPASS_EXTERNAL;
	dependencyOut.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencyOut.srcAccessMask = 0;
	dependencyOut.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
	dependencyOut.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkSubpassDependency dependencies[] = { dependencyIn, dependencyOut };

	std::array<VkAttachmentDescription, 1> attachments = { colorAttachment };
	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	renderPassInfo.dependencyCount = 2;
	renderPassInfo.pDependencies = dependencies;

	if (vkCreateRenderPass(vdi->device, &renderPassInfo, nullptr, &resolveRenderPass) != VK_SUCCESS) {
		throw std::runtime_error("failed to create render pass!");
	}
}

void HE2_VulkanBackend::createUIRenderPass()
{
	//The (only) main difference between this pass and the default one is it doens't clear when it's done

	VkAttachmentDescription colorAttachment = {};	
	//VkAttachmentDescription colorAttachmentResolve = {};

	colorAttachment.format = swapChainImageFormat;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;


	//colorAttachmentResolve.format = swapChainImageFormat;
	//colorAttachmentResolve.samples = VK_SAMPLE_COUNT_1_BIT;
	//colorAttachmentResolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	//colorAttachmentResolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	//colorAttachmentResolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
	//colorAttachmentResolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	//colorAttachmentResolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED; 
	//colorAttachmentResolve.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	
	//If we're in scene editor mode, this is the only pass to load the ui image, so we should clear it at the start of each frame
	if (settings.sceneEditorMode)
	{
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	}

	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;


	//VkAttachmentReference colorAttachmentResolveRef = {};
	//colorAttachmentResolveRef.attachment = 2;
	//colorAttachmentResolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;
	//subpass.pResolveAttachments = &colorAttachmentResolveRef;

	VkSubpassDependency dependencyIn = {};
	dependencyIn.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependencyIn.dstSubpass = 0;
	dependencyIn.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencyIn.srcAccessMask = 0;
	dependencyIn.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
	dependencyIn.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;


	VkSubpassDependency dependencyOut = {};
	dependencyOut.srcSubpass = 0;
	dependencyOut.dstSubpass = VK_SUBPASS_EXTERNAL;
	dependencyOut.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencyOut.srcAccessMask = 0;
	dependencyOut.dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
	dependencyOut.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkSubpassDependency dependencies[] = { dependencyIn, dependencyOut };

	std::array<VkAttachmentDescription, 1> attachments = { colorAttachment};
	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	renderPassInfo.dependencyCount = 2;
	renderPassInfo.pDependencies = dependencies;

	if (vkCreateRenderPass(vdi->device, &renderPassInfo, nullptr, &uiRenderPass) != VK_SUCCESS) {
		throw std::runtime_error("failed to create render pass!");
	}
}

//Other Stuff
void HE2_VulkanBackend::createSampler()
{
	VkPhysicalDeviceProperties props;
	vkGetPhysicalDeviceProperties(vdi->physicalDevice, &props);
	float maxAA = props.limits.maxSamplerAnisotropy;


	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.anisotropyEnable = VK_TRUE;
	samplerInfo.maxAnisotropy = maxAA;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_WHITE;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.minLod = 0;
	samplerInfo.maxLod = static_cast<float>(defaultMipLevels);
	samplerInfo.mipLodBias = 0;

	if (vkCreateSampler(vdi->device, &samplerInfo, nullptr, &defaultSampler) != VK_SUCCESS) {
		throw std::runtime_error("failed to create texture sampler!");
	}

	samplerInfo.magFilter = VK_FILTER_NEAREST;
	samplerInfo.minFilter = VK_FILTER_NEAREST;
	samplerInfo.anisotropyEnable = VK_FALSE;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
	

	if (vkCreateSampler(vdi->device, &samplerInfo, nullptr, &exactSampler) != VK_SUCCESS) {
		throw std::runtime_error("failed to create texture sampler!");
	}

	// Create sampler to sample from the color attachments
	VkSamplerCreateInfo sampler = {};
	sampler.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	sampler.magFilter = VK_FILTER_NEAREST;
	sampler.minFilter = VK_FILTER_NEAREST;
	sampler.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	sampler.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	sampler.addressModeV = sampler.addressModeU;
	sampler.addressModeW = sampler.addressModeU;
	sampler.mipLodBias = 0.0f;
	sampler.maxAnisotropy = 8.0f;
	sampler.minLod = 0.0f;
	sampler.minLod = 0.0f;
	sampler.maxLod = 1.0f;
	sampler.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;

	if (vkCreateSampler(vdi->device, &sampler, nullptr, &deferredSampler) != VK_SUCCESS)
	{
		throw std::runtime_error("Couldn't create offscreen sampler");
	}
}

void HE2_VulkanBackend::createCommandBuffers()
{

	VkCommandBufferAllocateInfo allocInfo = {};

	//Make the standard command buffers
	gpuTaskCommandBuffers.resize(swapChainImages.size());

	allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)gpuTaskCommandBuffers.size();

	if (vkAllocateCommandBuffers(vdi->device, &allocInfo, gpuTaskCommandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate command buffers!");
	}

	//And the UI ones
	uiCommandBuffers.resize(swapChainImages.size());

	allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)uiCommandBuffers.size();

	if (vkAllocateCommandBuffers(vdi->device, &allocInfo, uiCommandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate UI command buffers!");
	}

	computeCommandBuffers.resize(swapChainImages.size());

	allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = computeCommandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)computeCommandBuffers.size();

	if (vkAllocateCommandBuffers(vdi->device, &allocInfo, computeCommandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate command buffers!");
	}
}

void HE2_VulkanBackend::createUniformBuffers()
{
	VkDeviceSize vpBufferSize = sizeof(ViewProjectionBuffer);

	viewProjectionBuffers.resize(swapChainImages.size());
	viewProjectionBuffersAllocation.resize(swapChainImages.size());

	for (size_t i = 0; i < swapChainImages.size(); i++)
	{
		//vulkanAlloc->createBuffer(vpBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, viewProjectionBuffers[i], viewProjectionBuffersAllocation[i]);
		
		VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
		bufferInfo.size = vpBufferSize;
		bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

		VmaAllocationCreateInfo allocInfo = {};
		allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

		vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &viewProjectionBuffers[i], &viewProjectionBuffersAllocation[i], nullptr);

	}
}

void HE2_VulkanBackend::createSyncObjects()
{
	imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	immediateFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	offScreenFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	resolveFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	singleTimeFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);

	immediateInFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
	resolveInFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
	offScreenInFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

	singleTimeRenderFences.resize(MAX_FRAMES_IN_FLIGHT);

	offScreenImagesInFlight.resize(swapChainImages.size(), VK_NULL_HANDLE);
	resolveImagesInFlight.resize(swapChainImages.size(), VK_NULL_HANDLE);
	immediateImagesInFlight.resize(swapChainImages.size(), VK_NULL_HANDLE);

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;



	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		if (vkCreateSemaphore(vdi->device, &semaphoreInfo, nullptr,				&imageAvailableSemaphores[i]) != VK_SUCCESS ||
			vkCreateSemaphore(vdi->device, &semaphoreInfo, nullptr, &immediateFinishedSemaphores[i]) != VK_SUCCESS || 

			vkCreateSemaphore(vdi->device, &semaphoreInfo, nullptr, &offScreenFinishedSemaphores[i]) != VK_SUCCESS ||

			vkCreateSemaphore(vdi->device, &semaphoreInfo, nullptr, &singleTimeFinishedSemaphores[i]) != VK_SUCCESS || 

			vkCreateFence(vdi->device, &fenceInfo, nullptr, &immediateInFlightFences[i]) != VK_SUCCESS ||

			vkCreateFence(vdi->device, &fenceInfo, nullptr, &resolveInFlightFences[i]) != VK_SUCCESS ||

			vkCreateFence(vdi->device, &fenceInfo, nullptr, &offScreenInFlightFences[i]) != VK_SUCCESS ||

			vkCreateSemaphore(vdi->device, &semaphoreInfo, nullptr, &resolveFinishedSemaphores[i]) != VK_SUCCESS )
		{
			throw std::runtime_error("failed to create synchronization objects for a frame!");
		}
	}

}

void HE2_VulkanBackend::makeLightingBuffer()
{
	void* lightingData;
	// White
	// White


	LightingBuffer buff = {};
	lightingData = &buff;

	lightingBufferHandle = makeGPUBuffer(lightingData, sizeof(buff), true);
}
#pragma endregion

#pragma region Choose Methods
VkSurfaceFormatKHR HE2_VulkanBackend::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
	for (const auto& availableFormat : availableFormats) {
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return availableFormat;
		}
	}

	return availableFormats[0];
}

VkPresentModeKHR HE2_VulkanBackend::chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) {
	for (const auto& availablePresentMode : availablePresentModes) {
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			return availablePresentMode;
		}
	}

	return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D HE2_VulkanBackend::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
	if (capabilities.currentExtent.width != UINT32_MAX) {
		return capabilities.currentExtent;
	}
	else {
		int width, height;

		glfwGetFramebufferSize(HE2_WindowHandler::window, &width, &height);


		VkExtent2D actualExtent = { static_cast<uint32_t> (width), static_cast<uint32_t> (height) };

		actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

		return actualExtent;
	}
}

SwapChainSupportDetails HE2_VulkanBackend::querySwapChainSupport(VkPhysicalDevice device) {
	SwapChainSupportDetails details;

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, vdi->surface, &details.capabilities);

	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, vdi->surface, &formatCount, nullptr);

	if (formatCount != 0) {
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, vdi->surface, &formatCount, details.formats.data());
	}

	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, vdi->surface, &presentModeCount, nullptr);

	if (presentModeCount != 0) {
		details.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, vdi->surface, &presentModeCount, details.presentModes.data());
	}

	return details;
}
#pragma endregion

