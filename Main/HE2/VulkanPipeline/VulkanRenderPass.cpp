#include "VulkanRenderPass.h"
#include "VulkanAllocator.h"
#include "VulkanBackend.h"
#include "VulkanTextures.h"
#include "VulkanPipeline.h"
#include "VulkanHelpers.h"

void VulkanRenderRun::createResources(int swapchainImageCount, VkFormat swapchainFormat, VkSampleCountFlagBits msaaSamples, glm::vec2 swapChainExtent)
{
	//Create Image Resources
	vulkanAlloc = HE2_VulkanBackend::vulkanAlloc;

	if (runGraph) delete runGraph;

	runGraph = new VulkanResourceGraph();

	runGraph->setupBasePasses();

	//Output Image - our final output!
	createAttachment(outputRef, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, swapchainFormat, swapChainExtent, VK_SAMPLE_COUNT_1_BIT);

	//This time, our MSImage
	createAttachment(MSImageRef, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, swapchainFormat, swapChainExtent, msaaSamples);

	//Our depth image
	createAttachment(depthRef, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VulkanDeviceInstance::findDepthFormat(), swapChainExtent, msaaSamples, VK_IMAGE_ASPECT_DEPTH_BIT);

	//Our albedo image
	createAttachment(defAlbedoRef, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_FORMAT_R8G8B8A8_UNORM, swapChainExtent, msaaSamples);

	//Our position image
	createAttachment(defPositionRef, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_FORMAT_R16G16B16A16_SFLOAT, swapChainExtent, msaaSamples);

	//And our normal image
	createAttachment(defNormalRef, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_FORMAT_R16G16B16A16_SFLOAT, swapChainExtent, msaaSamples);

	//Create Framebuffers
	//So we need 3 types of framebuffer (and x number of them)
	deferredRenderFramebuffers.resize(swapchainImageCount);
	deferredResolveFramebuffers.resize(swapchainImageCount);
	forwardLitFramebuffers.resize(swapchainImageCount);

	for (int framebufferCount = 0; framebufferCount < swapchainImageCount; framebufferCount++)
	{
		{
			//Deferred Framebuffer (3 offscreen images + depth)
			std::array<VkImageView, 4> attachments;
			attachments[0] = defPositionRef.imageView;
			attachments[1] = defNormalRef.imageView;
			attachments[2] = defAlbedoRef.imageView;
			attachments[3] = depthRef.imageView;
			//attachments[3] = depthImageView;

			VkFramebufferCreateInfo fbufCreateInfo = {};
			fbufCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			fbufCreateInfo.pNext = NULL;
			fbufCreateInfo.renderPass = HE2_VulkanBackend::offScreenRenderPass;
			fbufCreateInfo.pAttachments = attachments.data();
			fbufCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
			fbufCreateInfo.width = swapChainExtent.x;
			fbufCreateInfo.height = swapChainExtent.y;
			fbufCreateInfo.layers = 1;

			if (vkCreateFramebuffer(VulkanDeviceInstance::device, &fbufCreateInfo, nullptr, &deferredRenderFramebuffers[framebufferCount]) != VK_SUCCESS)
			{
				throw std::runtime_error("Couldn't create off screen framebuffers!");
			}
		}

		{
			//Resolve Framebuffer (1 image)
			std::array<VkImageView, 1 >attachments2 = {
				MSImageRef.imageView
			};

			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = HE2_VulkanBackend::resolveRenderPass;
			framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments2.size());
			framebufferInfo.pAttachments = attachments2.data();
			framebufferInfo.width = swapChainExtent.x;
			framebufferInfo.height = swapChainExtent.y;
			framebufferInfo.layers = 1;

			if (vkCreateFramebuffer(VulkanDeviceInstance::device, &framebufferInfo, nullptr, &deferredResolveFramebuffers[framebufferCount]) != VK_SUCCESS) {
				throw std::runtime_error("failed to create framebuffer!");
			}
		}
		//Immediate Framebuffer (1 image, 1 depth, 1 resolve)
		{
			std::array<VkImageView, 3> attachments = {
				MSImageRef.imageView,
				depthRef.imageView,
				HE2_VulkanBackend::swapChainImageViews[framebufferCount]
			};
			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = HE2_VulkanBackend::forwardLitRenderPass;
			framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
			framebufferInfo.pAttachments = attachments.data();

			framebufferInfo.width = swapChainExtent.x;
			framebufferInfo.height = swapChainExtent.y;
			framebufferInfo.layers = 1;

			if (vkCreateFramebuffer(VulkanDeviceInstance::device, &framebufferInfo, nullptr, &forwardLitFramebuffers[framebufferCount]) != VK_SUCCESS) {
				throw std::runtime_error("failed to create framebuffer!");
			}
		}
	}
	
	//Create Command Buffers (what actually happens in this run)


	deferredCommandBuffers.resize(swapchainImageCount);

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = HE2_VulkanBackend::commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)deferredCommandBuffers.size();

	if (vkAllocateCommandBuffers(VulkanDeviceInstance::device, &allocInfo, deferredCommandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate command buffers!");
	}


	resolveCommandBuffers.resize(swapchainImageCount);

	allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = HE2_VulkanBackend::commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)resolveCommandBuffers.size();

	if (vkAllocateCommandBuffers(VulkanDeviceInstance::device, &allocInfo, resolveCommandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate command buffers!");
	}
	
	forwardLitCommandBuffers.resize(swapchainImageCount);

	allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = HE2_VulkanBackend::commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)forwardLitCommandBuffers.size();

	if (vkAllocateCommandBuffers(VulkanDeviceInstance::device, &allocInfo, forwardLitCommandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate command buffers!");
	}
	//I need descriptor sets describing the GBUffer images!
	VulkanPipeline* vp = backend->deferredResolveShader->getComponent<VulkanPipeline>();
	//Create a list of layouts
	std::vector<VkDescriptorSetLayout> layouts(backend->swapChainImages.size(), getLayoutFromFeatures(vp->shader->features, 0));

	deferredDescriptorSets.resize(swapchainImageCount);

	//Allocate the descriptors
	VkDescriptorSetAllocateInfo descAllocInfo = {};
	descAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	descAllocInfo.descriptorPool = vp->descriptorPool;
	descAllocInfo.descriptorSetCount = static_cast<uint32_t>(swapchainImageCount);
	descAllocInfo.pSetLayouts = layouts.data();

	if (vkAllocateDescriptorSets(VulkanDeviceInstance::device, &descAllocInfo, deferredDescriptorSets.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate descriptor sets!");
	}

	//For each set of commandBuffers (frames in flight +1)
	for (size_t i = 0; i < swapchainImageCount; i++) {
		//Writes for this frame set
		std::vector<VkWriteDescriptorSet> descriptorWrites;

		VkDescriptorImageInfo imageInfos[64] = {};
		VkDescriptorBufferInfo lightingBufferInfo = {};

		int imageIndex = 0;
		int featureIndex = 0;
		//per feature
		for (auto a : vp->shader->features)
		{
			//Stuff for buffers or images
			//Create a new descriptor write
			uint32_t index = descriptorWrites.size();
			descriptorWrites.push_back({});
			descriptorWrites[index].dstBinding = a.binding;
			descriptorWrites[index].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[index].dstSet = deferredDescriptorSets[i];
			descriptorWrites[index].descriptorCount = 1;
			descriptorWrites[index].dstArrayElement = 0;

			if (a.binding < 4)
			{
				imageInfos[imageIndex] = {};
				imageInfos[imageIndex].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

				switch (a.source)
				{
				case HE2_SHADER_IMAGE_SOURCE_GBUFFER_POSITION:
					imageInfos[imageIndex].imageView = defPositionRef.imageView;
					break;
				case HE2_SHADER_IMAGE_SOURCE_GBUFFER_NORMAL:
					imageInfos[imageIndex].imageView = defNormalRef.imageView;
					break;
				case HE2_SHADER_IMAGE_SOURCE_GBUFFER_ALBEDO:
					imageInfos[imageIndex].imageView = defAlbedoRef.imageView;
					break;
				}


				imageInfos[imageIndex].sampler = HE2_VulkanBackend::deferredSampler;

				descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				descriptorWrites[index].pImageInfo = &imageInfos[imageIndex];
				imageIndex++;
			}
			else
			{
				//Lighting Buffer
				lightingBufferInfo.buffer =backend->getSpecificBuffer((backend->lightingBufferHandle * swapchainImageCount) + i);
				lightingBufferInfo.offset = 0;
				lightingBufferInfo.range = a.size;
				descriptorWrites[index].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
				descriptorWrites[index].pBufferInfo = &lightingBufferInfo;
			}
			featureIndex++;
		}


		vkUpdateDescriptorSets(VulkanDeviceInstance::device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
	}

	//And we're good to go!
}

void VulkanRenderRun::createAttachment(VulkanImageReference& ref, VkImageLayout layout, VkImageUsageFlags usage, VkFormat format, glm::vec2 extent, VkSampleCountFlagBits msaaSamples, VkImageAspectFlagBits aspect)
{
	vulkanAlloc->createEmptyImage(extent.x, extent.y, 1, msaaSamples, format, VK_IMAGE_TILING_OPTIMAL, usage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, ref.vkImage, ref.allocation);

	if (layout != VK_IMAGE_LAYOUT_UNDEFINED)
	{
		VulkanTextures::transitionImageLayout(ref.vkImage, format, VK_IMAGE_LAYOUT_UNDEFINED, layout, 1, aspect);
	}
	else
	{
		layout;
	}

	ref.imageView = vulkanAlloc->createImageView(ref.vkImage, format, aspect, 1);

	ref.image = new HE2_Image();

	ref.image->handle = VulkanTextures::addImage(ref);
}

void VulkanRenderRun::cleanupResources()
{
	for (int i = 0; i < deferredCommandBuffers.size(); i++)
	{
		vkFreeCommandBuffers(VulkanDeviceInstance::device, backend->commandPool, 1, &deferredCommandBuffers[i]);
		vkFreeCommandBuffers(VulkanDeviceInstance::device, backend->commandPool, 1, &resolveCommandBuffers[i]);
		vkFreeCommandBuffers(VulkanDeviceInstance::device, backend->commandPool, 1, &forwardLitCommandBuffers[i]);
	}

	//Destroy the framebuffers
	for (int i = 0; i < deferredRenderFramebuffers.size(); i++)
	{
		vkDestroyFramebuffer(VulkanDeviceInstance::device, deferredRenderFramebuffers[i], nullptr);
		vkDestroyFramebuffer(VulkanDeviceInstance::device, deferredResolveFramebuffers[i], nullptr);
		vkDestroyFramebuffer(VulkanDeviceInstance::device, forwardLitFramebuffers[i], nullptr);
	}

	std::vector<VulkanImageReference> refs = { defAlbedoRef, defPositionRef, defNormalRef, MSImageRef, outputRef, depthRef };
	for (auto& ref : refs)
	{
		vkDestroyImageView(VulkanDeviceInstance::device ,ref.imageView, nullptr);
		vkDestroyImage(VulkanDeviceInstance::device, ref.vkImage, nullptr);
		vmaFreeMemory(backend->allocator, ref.allocation);
	}

	//DescriptorSets are also cleaned up when their descriptor pool is
}