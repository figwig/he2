#include "VulkanUIBehaviour.h"
#include "VulkanHelpers.h"
#include <HE2_Time.h>
	
VulkanUIBackend::VulkanUIBackend(ImGui_ImplVulkan_InitInfo init_info, VkRenderPass guiRenderPass,  VulkanDeviceInstance * vdi) :  vdi(vdi)
{
	ImGui::CreateContext();

	io = ImGui::GetIO();


	ImGui::StyleColorsDark();
	
	ImGui_ImplGlfw_InitForVulkan(HE2_WindowHandler::window,true);

	ImGui_ImplVulkan_Init(&init_info, guiRenderPass);

	createFonts();

	startUI();
}

VulkanUIBackend::~VulkanUIBackend()
{
	ImGui_ImplVulkan_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

}

void VulkanUIBackend::startUI()
{
	ImGui_ImplVulkan_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void VulkanUIBackend::renderUI()
{
	ImGui::Render();
}

void VulkanUIBackend::callUICmds(VkCommandBuffer* renderCommandBuffer)
{
	ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), *renderCommandBuffer);
}

void VulkanUIBackend::finishUI()
{
	startUI();
}

void VulkanUIBackend::createFonts()
{
	VkCommandBuffer command_buffer = beginSingleTimeCommands();

	ImGui_ImplVulkan_CreateFontsTexture(command_buffer);

	endSingleTimeCommands(command_buffer);

	ImGui_ImplVulkan_DestroyFontUploadObjects();
}