#include "FlockingBehaviour.h"
#include <Helpers.h>
#include <chrono>


float FlockingBehaviour::commonDimension = 400.0f;
glm::vec3 FlockingBehaviour::boxPos = glm::vec3(0, 0, 0);
glm::vec3 FlockingBehaviour::boxMaxExtent = glm::vec3(commonDimension, 50, commonDimension) + boxPos;
glm::vec3 FlockingBehaviour::boxMinExtent = glm::vec3(-commonDimension, 0.3f, -commonDimension) + boxPos;

float FlockingBehaviour::cohesionMod = 1.098f;
float FlockingBehaviour::alignmentMod = 4.7f;
float FlockingBehaviour::separationMod = 0.95f;
float FlockingBehaviour::cohesionRadius = 100.0f;
float FlockingBehaviour::alignmentRadius = 34.0f;
float FlockingBehaviour::separationRadius = 10.0f;

FlockingBehaviour::FlockingBehaviour(HE2_Object * host, Flock * flock): flock(flock), HE2_ObjectBehaviour(host, true )
{
	flock->addMember(this);
	object->setPos(object->getPos() + boxPos);
	
	srand(std::chrono::system_clock::now().time_since_epoch().count());

	velocity = glm::normalize(glm::vec3((rand() % 1000) / 1000.0f - 500.0f, (rand() % 1000) / 1000.0f, (rand() % 1000) / 1000.0f - 500.0f));

	glm::vec4 lightColour = glm::vec4(rand() %20 + 8, rand() % 20 + 8, rand() % 20 + 8, rand() % 100 + 100);

	light = new HE2_Light(HE2_Light::HE2_LightType::HE2_DirectionalLight, lightColour * 0.04f, 80.0f);
}

void FlockingBehaviour::update()
{
	//velocity = glm::vec3(0);

	addAlignment();

	addCohesion();

	addSeparation();

	//Normalize the velocity
	velocity = glm::normalize(velocity) * 40.0f;

	//glm::vec3 pos = getPosition();
	//if (pos.y > 45.0f)
	//{
	//	float amount = pos.y - 45.0f; 
	//	amount *= 3.0f;

	//	velocity.y -= amount;
	//}

	//Move the object
	object->setPos(object->getPos() + velocity * Time::deltaTime);

	glm::mat4 rotationMat = glm::transpose(glm::lookAt(object->getPos(), object->getPos() + velocity, glm::vec3(0, 1, 0)));

	object->setRotMat(rotationMat);

	teleportIfNeeded();

	light->setPos(object->getPos());
}

void FlockingBehaviour::addAlignment()
{

	std::vector<FlockingBehaviour*> neighbours;
	flock->getNeighbours(this, alignmentRadius, neighbours);
	//If there are no neighbours, we can't do anything here! 
	if (neighbours.size() == 0) return;

	//Run through neighbours, calculate their alignment, add this together
	glm::vec3 cumulativeAlignment = glm::vec3(0);

	for (auto a : neighbours)
		cumulativeAlignment += a->getVelocity();
	
	//Divide by the number of neighbours to get the average alignment
	glm::vec3 averageAlignment = cumulativeAlignment / float(neighbours.size());

	//Do something with this
	velocity += alignmentMod* glm::normalize(averageAlignment);
}

void FlockingBehaviour::addCohesion()
{
	std::vector<FlockingBehaviour*> neighbours;
	flock->getNeighbours(this, cohesionRadius,neighbours);
	//If there are no neighbours, we can't do anything here! 
	if (neighbours.size() == 0) return;

	//Run through neighbours, calculate their alignment, add this together
	glm::vec3 cumulativePosition = glm::vec3(0);

	for (auto a : neighbours)
		cumulativePosition += a->object->getPos();

	//Divide by the number of neighbours to get the average alignment
	glm::vec3 averagePosition = cumulativePosition / float(neighbours.size());

	//Do something with this
	glm::vec3 cohesionForce = averagePosition - getPosition();
	if(cohesionForce != glm::vec3(0.0f,0.0f,0.0f))
		velocity += cohesionMod * glm::normalize(cohesionForce);
}

void FlockingBehaviour::addSeparation()
{
	std::vector<FlockingBehaviour*> neighbours;;
	flock->getNeighbours(this, separationRadius, neighbours);
	//If there are no neighbours, we can't do anything here! 
	if (neighbours.size() == 0) return;

	//Run through neighbours, calculate their alignment, add this together
	glm::vec3 cumulativeDiversion = glm::vec3(0);

	for (auto a : neighbours)
		cumulativeDiversion += (a->object->getPos() - object->getPos());

	glm::vec3 camPos = HE2_Camera::main->getPos();
	int camInt = 0;
	if (distSquared(camPos, getPosition()) < pow(separationRadius * 20.0f,2))
	{
		camInt = 1;
		cumulativeDiversion += 10.0f * (camPos - getPosition());
	}

	//Divide by the number of neighbours to get the average alignment
	glm::vec3 actualDiversion = cumulativeDiversion / float(neighbours.size() + camInt);

	actualDiversion *= -1.0f;

	if(actualDiversion != glm::vec3(0.0f, 0.0f, 0.0f))
		velocity += separationMod * actualDiversion;
}

void FlockingBehaviour::teleportIfNeeded()
{
	glm::vec3 position = getPosition();

	if (position.x > boxMaxExtent.x)
	{
		position.x = boxMinExtent.x;
	}
	else if (position.x < boxMinExtent.x)
	{
		position.x = boxMaxExtent.x;
	}

	if (position.y > boxMaxExtent.y)
	{
		position.y = boxMinExtent.y;
	}
	else if (position.y < boxMinExtent.y)
	{
		position.y = boxMaxExtent.y;
	}

	if (position.z > boxMaxExtent.z)
	{
		position.z = boxMinExtent.z;
	}
	else if (position.z < boxMinExtent.z)
	{
		position.z = boxMaxExtent.z;
	}
	 
	object->setPos(position);
}

//////Flock


void Flock::addMember(FlockingBehaviour* member)
{
	flock.push_back(member);
}

void Flock::getNeighbours(FlockingBehaviour* member, float radius, std::vector<FlockingBehaviour*>& membersClose)
{
	glm::vec3 memberPos = member->getPosition();

	for (auto otherMember : flock)
	{
		glm::vec3 otherMemberPos = otherMember->getPosition();
		float distSq = pow(memberPos.x - otherMemberPos.x,2) + pow(memberPos.y - otherMemberPos.y,2)+  pow(memberPos.z - otherMemberPos.z,2);

		if (distSq < (radius * radius))
		{
			if(otherMember!= member)
				membersClose.push_back(otherMember);
		}

	}
}

void Flock::update()
{
	//ImGui::Begin("Flocking Settings");
	//ImGui::SliderFloat("Cohesion Modifier", &FlockingBehaviour::cohesionMod, 0.1f, 5.0f);
	//ImGui::SliderFloat("Alignment Modifier", &FlockingBehaviour::alignmentMod, 0.1f, 5.0f);
	//ImGui::SliderFloat("Separation Modifier", &FlockingBehaviour::separationMod, 0.1f, 5.0f);
	//ImGui::SliderFloat("Cohesion Range", &FlockingBehaviour::cohesionRadius, 0.1f, 150.0f, "%.3f", 2.0f);
	//ImGui::SliderFloat("Alignment Range", &FlockingBehaviour::alignmentRadius, 0.1f, 150.0f, "%.3f", 2.0f);
	//ImGui::SliderFloat("Separation Range", &FlockingBehaviour::separationRadius, 0.1f, 150.0f, "%.3f", 2.0f);
	//ImGui::End();
}