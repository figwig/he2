#pragma once
#include <HE2.h>
#include <HE2_PhysicsComponent.h>
class UpAndDown : public HE2_ObjectBehaviour
{
public:
	UpAndDown(HE2_Object* host) : HE2_ObjectBehaviour(host) {}

	void update()
	{
		object->setRot(object->getRot() + (Time::deltaTime * glm::vec3(0, 0.7f, 0.0f)));
	}
};


class CameraWire : public HE2_ObjectBehaviour {
public:
	CameraWire(HE2_Object* cam) : HE2_ObjectBehaviour(cam)
	{
		object = cam;
	}
	void update()
	{
		float x = std::sin(Time::time);

		object->setPos(glm::vec3(50 + x * 10, 80, 00));
	}
};


class ForceOnKey : public HE2_ObjectBehaviour
{
public:
	ForceOnKey(HE2_Object* host) : HE2_ObjectBehaviour(host) {}

	void update()
	{
		if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_X))
		{
			object->getComponent<BulletPhysicsComponent>()->applyForce(glm::vec3(0,50000,0));
		}
		if (glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_C))
		{
			object->getComponent<BulletPhysicsComponent>()->applyForce(glm::vec3(0, 0, 3000));
		}
	}
};