#include "AddObjectOnClick.h"
#include <HE2_RenderComponent.h>

AddObjectOnClick::AddObjectOnClick(HE2_Model* model, HE2_Model  * model2) : model(model), model2(model2)
{

}

void AddObjectOnClick::update()
{

	
	ticker -= Time::deltaTime;
	if (ticker < 0.0f && glfwGetMouseButton(HE2_WindowHandler::window, 0))
	{
		ticker = 0.2f;

		int x = objects.size() * 3;;

		for (int i = 0; i < 20; i++)
		{
			HE2_RenderComponent* rc = new HE2_RenderComponent(model, model->defaultMaterial);

			HE2_Object* obj = new HE2_Object({ rc });
			objects.push_back(obj);
			obj->setPos(glm::vec3(x, 0, i * 20));
		}
	}

	if (ticker < 0.0f && glfwGetKey(HE2_WindowHandler::window, GLFW_KEY_X))
	{
		ticker = 0.2f;
		for (auto x : objects)
		{
			x->getComponent<HE2_RenderComponent>()->setModel(model2);
			x->getComponent<HE2_RenderComponent>()->setMaterial(model2->defaultMaterial);
		}

	}

}