#pragma once
#include <HE2.h>

class FlockingBehaviour;

class Flock : public HE2_GlobalBehaviour
{
public:
	void update();
	void addMember(FlockingBehaviour* member);

	//For now this is implemented as a vector, will be changed to be a quadtree soon
	void getNeighbours(FlockingBehaviour* member, float radius, std::vector<FlockingBehaviour*>&);

private:
	std::vector<FlockingBehaviour*> flock;
};

class FlockingBehaviour : public HE2_ObjectBehaviour
{
public:
	FlockingBehaviour(HE2_Object * host, Flock * );
	
	void update();

	glm::vec3 getVelocity() { return velocity; }

	glm::vec3 getPosition() { return object->getPos(); }

private:
	glm::vec3 position = glm::vec3(0);

	HE2_Light* light = nullptr;

	void addAlignment();
	void addCohesion();
	void addSeparation();

	void teleportIfNeeded();

	glm::vec3 velocity = glm::vec3(0,1,0);


	static float commonDimension;
	static glm::vec3 boxMaxExtent;
	static glm::vec3 boxMinExtent;
	static glm::vec3 boxPos;

	Flock* flock = nullptr;

		static float cohesionMod;
	static float alignmentMod;
	static float separationMod;
	static float cohesionRadius;
	static float alignmentRadius;
	static float separationRadius;
};

