#include <HE2.h>
#include <VulkanBackend.h>
#include <stdexcept>
#include <HE2_Behaviour.h>
#include <HE2_Time.h>
#include <HE2_Scene.h>
#include <FPSUIElement.h>
#include "TestingBehaviours.h"
#include <FlameGraphBehaviour.h>
#include <StandardAssets.h>
#include <HE2_TextureManager.h>
#include <SceneGraphDisplayBehaviour.h>
#include <NormalDisplayShader.h>
#include <SimpleCameraKeyboardMove.h>
#include "AddObjectOnClick.h"
#include <ConsoleUI.h>
#include <GUITexture.h>
#include <SceneEditor.h>
#include "FlockingBehaviour.h"
#include <HE2_PhysicsComponent.h>

int main()
{
	HE2_VulkanBackend* pipeline = nullptr;

	HE2_Instance* heInstance = nullptr;

	HE2_Model* cubeModel = nullptr;
	HE2_Model* planetModel = nullptr;
	HE2_Model* treeModel = nullptr;

	HE2_Model *spaceShip = nullptr;

	HE2_ObjectInstance cube1;

	HE2_ObjectInstance cube2;

	HE2_ObjectInstance skybox;

	HE2_ObjectInstance tree;

	HE2_SceneCreator* testScene = nullptr;

	CameraWire* camBeh = nullptr;

	FPSUIElement* fps = nullptr;
	FlameGraphBehaviour* flame = nullptr;

	SceneGraphDisplayBehaviour* sceneGraph = nullptr;

	GUITexture* guiTexture = nullptr;

	try
	{
		pipeline = new HE2_VulkanBackend({ false });
		heInstance = new HE2_Instance(pipeline);
		heInstance->Init();


		std::vector<HE2_ObjectInstance*> objects;
		testScene = new HE2_SceneCreator("Test Scene", objects);

		new SceneEditor();

		//Some Objects go in here

		//HE2_Object* cube = new HE2_Object({ rc });
		//cube->name = "Cube";
		//cube->setPos(glm::vec3(0, 0, 10));


		//////Flocking!
		//HE2_RenderComponent* flockRC = new HE2_RenderComponent(HE2_MeshManager::importModel("models/boid.obj"));
		//Flock* flock = new Flock();

		//for(float x = 0.0f; x < 6.0f; x+= 0.5f)
		//	for (float y = 0.0f; y < 6.0f; y += 0.5f)
		//	{
		//		HE2_Object* flockMember = new HE2_Object({ flockRC });
		//		//Dangling Pointer is okay - is collected by the behaviour manager
		//		new FlockingBehaviour(flockMember, flock);

		//		flockMember->name = "Flock Member";
		//		flockMember->setScale(glm::vec3(0.15f));
		//		flockMember->setPos(glm::vec3(x * 7.0f, 15.0f, y * 7.0f));

		//	}

		//Terrain
		HE2_RenderComponent* rc2 = new HE2_RenderComponent(HE2_MeshManager::importModel("models/laketerrain.obj"));
		HE2_Object* lake = new HE2_Object({ rc2 });

		lake->name = "Terrain";

		HE2_RenderComponent* rc = new HE2_RenderComponent(HE2_MeshManager::importModel("models/cube.obj"));
		for (int i = 0; i < 5; i++)
		{

			HE2_Object * cube = new HE2_Object({ rc });
			BulletPhysicsComponent* bullet = new BulletPhysicsComponent(cube, 500.0f);
			cube->addComponent(bullet);
			cube->setPos(glm::vec3(0, 87 + i *5, i * 17));
			new ForceOnKey(cube);
		}

		btStaticPlaneShape* plane = new btStaticPlaneShape(btVector3(0, 1, 0), 0.0f);

		BulletPhysicsComponent* planeComp = new BulletPhysicsComponent(plane, glm::mat4(1));


		//Setup Scene
		HE2_SceneCreator::CreateSceneObjects(testScene);

		SimpleCameraKeyboardMove* sckm = new SimpleCameraKeyboardMove(HE2_Camera::main);
		//testScene->camera->setPos(glm::vec3(0, 15, 33));


		fps = new FPSUIElement();
		flame = new FlameGraphBehaviour();
		sceneGraph = new SceneGraphDisplayBehaviour();
		new ConsoleUI();

		HE2_Light * mainLight = new HE2_Light(HE2_Light::HE2_LightType::HE2_DirectionalLight, glm::vec4(1000, 1000, 900, 1000), 10000, 0.0f);

		mainLight->setPos(glm::vec3(0, 400, 0));

		HE2_Serialize::writeSceneToFile(HE2_Scene::currentScene, "sceneSaveTest.scene");

		heInstance->RunGame();

	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}

	if (pipeline) delete pipeline;
	if (heInstance) delete heInstance;
	

	while (true);
	return 0;
}