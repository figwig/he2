@echo off

:loop
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V Vvertex.vert
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V Vfragment.frag
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V Vquad.vert -o vquad.vert.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V Vquad.frag -o vquad.frag.spv

echo Built Standard Shaders

cd deferred
call generate-spirv.bat

cd ..
cd planets
call compile.bat

cd ..

echo Build Done!
echo **
echo **

set /P DUMMY = ENTER to build again...
goto loop



pause