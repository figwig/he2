#version 450

layout (set = 1,binding = 0) uniform sampler2D samplerColor;
layout (set = 1, binding = 1) uniform sampler2D samplerSpecMap;
layout (set = 1,binding = 2) uniform sampler2D samplerNormalMap;

layout(location = 0) in vec3 inWorldPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec3 inBitangent;
layout(location = 4) in vec2 inTextureCoord;

layout (location = 0) out vec4 outPosition;
layout (location = 1) out vec4 outNormal;
layout (location = 2) out vec4 outAlbedo;

void main() 
{
	outPosition = vec4(inWorldPos, 1.0);

	// Calculate normal in tangent space
	vec3 N = normalize(inNormal);
	N.y = N.y;
	vec3 T = normalize(inTangent);
	T.y = T.y;
	vec3 B = normalize(inBitangent);
	B.y = B.y;

	// if (dot(cross(N, T), B) < 0.0f) {
	// 	T = T * -1.0f;
	// }

	mat3 TBN = mat3(T, B, N);


	vec3 tnorm = TBN * normalize(texture(samplerNormalMap, inTextureCoord).xyz * 2.0 - vec3(1.0));

	outNormal = vec4(tnorm, 1.0);


	outAlbedo = texture(samplerColor, inTextureCoord);
	outAlbedo.a = texture(samplerSpecMap, inTextureCoord).r;
	//outAlbedo.a = 1.0f;
}