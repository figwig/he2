#version 450

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec3 inBitangent;
layout(location = 4) in vec2 inTextureCoord;

layout(binding = 0) uniform ViewProjectionBuffer {
	mat4 view;
	mat4 proj;
} ubo;

layout( push_constant ) uniform ModelBuffer {
  mat4 model;
};


layout(location = 0) out vec3 outWorldPos;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outTangent;
layout(location = 3) out vec3 outBitangent;
layout(location = 4) out vec2 outTextureCoord;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main() 
{
	vec4 tmpPos = vec4(inPosition,1.0f);

	gl_Position = ubo.proj * ubo.view * model * tmpPos;
	
	outTextureCoord = inTextureCoord;
	outTextureCoord.t = 1.0 - outTextureCoord.t;

	// Vertex position in world space
	outWorldPos = vec3(model * tmpPos);
	// GL to Vulkan coord space
	//outWorldPos.y = -outWorldPos.y;
	
	// Normal in world space
	mat3 mNormal = transpose(inverse(mat3(model)));
	mNormal = mat3(model);
	outNormal = mNormal * normalize(inNormal);	
	outTangent = mNormal * normalize(inTangent);
	outBitangent = mNormal * normalize(inBitangent);
}
