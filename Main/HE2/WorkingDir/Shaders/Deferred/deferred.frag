#version 450

layout (set = 0, binding = 1) uniform sampler2DMS samplerposition;
layout (set = 0, binding = 2) uniform sampler2DMS samplerNormal;
layout (set = 0, binding = 3) uniform sampler2DMS samplerAlbedo;

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragcolor;

struct Light {
	vec4 position;
	vec4 color;
	float radius;
};

layout (set =0, binding = 4) uniform UBO 
{
	Light lights[200];
	vec4 viewPos;
	int actualLights;
	vec2 textureSize;
} ubo;

float alpha = 1.0f;

void main() 
{
	int multisampleCount = 4;

	// Get G-Buffer values
	vec3 fragPos = vec3(0);
	vec3 normal = vec3(0);
	vec4 albedo = vec4(0);


	for(int i = 0; i  < multisampleCount; i++)
	{	
		fragPos += texelFetch(samplerposition, ivec2(inUV * ubo.textureSize), i).rgb;
		normal += texelFetch(samplerNormal, ivec2(inUV * ubo.textureSize), i).rgb;
		albedo += texelFetch(samplerAlbedo, ivec2(inUV * ubo.textureSize), i);
	}

	fragPos/= float(multisampleCount);
	normal/= float(multisampleCount);
	albedo/= float(multisampleCount);

	
	#define ambient 0.02
	
	// Ambient part
	vec3 fragcolor  = albedo.rgb * ambient;

	//fragPos.y = -fragPos.y;
	vec4 viewPos = ubo.viewPos;
	//viewPos.y = -viewPos.y;
	
	for(int i = 0; i < ubo.actualLights; ++i)
	{
		// Vector to ligt
		vec3 lightPos =ubo.lights[i].position.xyz;
		//lightPos.y =- lightPos.y;

		vec3 L =  lightPos - fragPos;
		// Distance from light to fragment position
		float dist = length(L);

		// Viewer to fragment
		vec3 V = viewPos.xyz - fragPos;
		V = normalize(V);
		
		//if(dist < ubo.lights[i].radius)
		
		// Light to fragment
		L = normalize(L);

		// Attenuation
		float atten = ubo.lights[i].radius / (pow(dist, 2.0) + 1.0);

		// Diffuse part
		vec3 N = normalize(normal);
		float NdotL = max(0.0, dot(N, L));
		vec3 diff = ubo.lights[i].color.rgb * albedo.rgb * NdotL * atten;

		// Specular part
		// Specular map values are stored in alpha of albedo mrt
		vec3 R = reflect(-L, N);
		float NdotR = max(0.0, dot(R, V));
		vec3 spec = ubo.lights[i].color.rgb * albedo.a * pow(NdotR, 16.0) * atten;

		fragcolor += diff + spec;	
	}    	
   
    outFragcolor = vec4(fragcolor,alpha);	
    float gamma = 2.2;
    outFragcolor.rgb = pow(outFragcolor.rgb, vec3(1.0/gamma));
}