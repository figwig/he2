#version 450

layout (location = 0) out vec2 outUV;

vec2 positions[6] = vec2[](
    vec2(-1.0, -1.0),
    vec2(1.0, 1.0),
    vec2(-1.0, 1.0),

	vec2(-1.0, -1.0),
	vec2(1.0, -1.0),
	vec2(1.0, 1.0)
);

void main() 
{
	vec2 xy = vec2(positions[gl_VertexIndex].x, -positions[gl_VertexIndex].y);

	outUV = (xy + vec2(1.0f)) / 2.0f;

	gl_Position = vec4(xy, 0, 1);
}
