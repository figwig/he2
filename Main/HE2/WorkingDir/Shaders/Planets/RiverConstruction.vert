#version 450
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 tangent;
layout(location = 3) in vec3 bitangent;
layout(location = 4) in vec2 textureCoord;

layout(location = 0)out Vertex
{
  vec4 normal;
  int instanceIndex;
} vertex;



void main()
{
  gl_Position = vec4(position,0);
  vertex.normal = vec4(normal,0);
  vertex.instanceIndex = gl_InstanceIndex;
}