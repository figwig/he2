#version 450
layout(triangles) in;

float M_PI = 3.14159265358979323846;
layout(triangle_strip, max_vertices=45) out;

float normal_length = 10.0f;


layout(location = 0)in Vertex
{
  vec4 normal;
  vec4 color;
  int instanceID;
}vertex[];

layout(binding = 0) uniform ViewProjectionBuffer {
	mat4 view;
	mat4 proj;
};

layout(location = 0) out packet
{
  vec3 vertPos;
  vec3 normal;
  vec2 texCoord;
};

mat4 vp;


layout(set = 1, binding = 0) uniform Coords
{
  vec4 requiredSection;
  vec4 cameraPos;
  vec4 quadrant;
  float objScale;
  int seedOffset;
};

layout(set =2, binding = 0) uniform sampler2D heightMap;

float radius  = 3150.0f;
vec2 dimensions;

const float PHI = 1.61803398874989484820459; // Φ = Golden Ratio 

float gold_noise(in vec2 xy, in float seed)
{
    return fract(tan(distance(xy*PHI, xy)*seed)*xy.x);
}

vec2 getAdjustedTexCoord(vec2 coord)
{
  vec2 heightMapCoord = coord;

	//heightMapCoord.y = 1.0f - heightMapCoord.y;
	heightMapCoord = (heightMapCoord - requiredSection.xy)*(1.0f/dimensions);

  return heightMapCoord;
}


float getNoiseAtPoint(vec2 tex)
{
	float baseHeight = 3.15f * texture(heightMap, tex).r;

	return baseHeight;
}


mat4 transformMatrix(mat4 mat, vec3 trans)
{
  mat[3][0] = trans.x;
  mat[3][1] = trans.y;
  mat[3][2] = trans.z;

  return mat;
}

vec3 getWorldPoint(vec2 position)
{

	float s = (position.x-0.25) * M_PI * 2;
	float t = position.y * M_PI;

	float z = radius * cos(s) * sin(t);
	float x = radius * sin(s) * sin(t);
	float y = radius * cos(t);


	return vec3(x,y,z);
}
vec2 getTexCoordFromPos(vec4 worldPos)
{
  vec4 n = normalize(worldPos);
	float u = atan(n.x, n.z) / (2.0f * M_PI) + 0.25f;

	float v = acos(worldPos.y /radius) / M_PI;

	vec2 texCoord = vec2(u,v);

  return texCoord;
}

void drawInstance(mat4 vp, mat4 m)
{  

  mat4 matrix = vp* m;
  mat3 normalMat = transpose(inverse(mat3(m)));
  vec3 pos = gl_in[0].gl_Position.xyz;
  gl_Position = matrix *vec4(pos, 1.0);
  vertPos = pos;
  normal = normalMat*vertex[0].normal.xyz;

  EmitVertex();
  pos = gl_in[1].gl_Position.xyz ;
  gl_Position = matrix * vec4(pos, 1.0);
  vertPos = pos;
  normal = normalMat*vertex[1].normal.xyz;
  EmitVertex();

  pos = gl_in[2].gl_Position.xyz;
  gl_Position = matrix * vec4(pos, 1.0);
  vertPos = pos;
  normal = normalMat*vertex[2].normal.xyz;

  EmitVertex();
  
  EndPrimitive();

}


void drawInstanceOnSphere(vec4 worldPos, vec2 sphericalCoords)
{
  mat4 m = mat4(1);


  //Add any number, normalize it, slightly difference world pos, vector between them is an x Axis
  vec3 xAxis = normalize(normalize(vec3(worldPos) + vec3(0.0,0.2,0.0)) - normalize(vec3(worldPos)));
  //Planet is at 0,0,0 so world pos can be used as the normal and vector from origin
  vec3 yAxis = normalize(vec3(worldPos));
  //We should be able to cross the y and x axis like a normal and tangent to generate a bitangent
  vec3 zAxis = normalize(cross(yAxis, xAxis));

  mat3 baseMat = mat3(xAxis, yAxis, zAxis);

  m = mat4(baseMat);


  //Scale all by 0.01f
  float scale = objScale;
  //scale = 0.105f;


  //Transform m by the Radius in the y axis to put it on the surface


  mat4 m2 = mat4(1);
  m2[0][0]*= scale;
  m2[1][1]*= scale;
  m2[2][2]*= scale;
  float height = 12* getNoiseAtPoint(sphericalCoords);

  m2 = transformMatrix(m2, vec3(0,radius + height,0));

  m = m  * m2;

  drawInstance(vp, m);
}

vec3 randomVec3(vec2 xy, int seed)
{
  return vec3(gold_noise(xy, seed * 3), gold_noise(xy, seed * 3 +2), gold_noise(xy, seed *3 +4));
}

void placeTrees()
{
  //All following code assumes planet positon of 0,0,0, and no planet rotation
  //Find the point on the planet closest to the camera
  vec4 closestPoint = normalize(cameraPos) * radius;


  vec2 closestPointCoordAdjusted;
  if(distance(closestPoint, cameraPos) < 3000)
  {
    float quadrantDiff = 1000;

    //Doing this in c++ now
    vec2 quadrantCoord = quadrant.xy;

    vec4 quadrantWorldPoint = (vec4(getWorldPoint(quadrantCoord),0));

    //drawInstanceOnSphere(quadrantWorldPoint, getAdjustedTexCoord(quadrantCoord));

    for (int treeCount = 0; treeCount < 1; treeCount++)
    {
      float range = 8000 / quadrantDiff;
      vec4 treePos = normalize(quadrantWorldPoint + range *  (vec4(2.0 * randomVec3(quadrantCoord, int( treeCount + 15 * vertex[0].instanceID)) - vec3(1),1))) * radius;
      //vec4 treePos = normalize(closestPoint +  vec4(5.1, 5.1,5.1,0) * treeCount) * radius;
      vec2 treeCoord = getAdjustedTexCoord(getTexCoordFromPos(treePos));

      drawInstanceOnSphere(treePos, treeCoord);
    }
  }

}


void main()
{ 
	dimensions.x = requiredSection.z - requiredSection.x;
	dimensions.y = ( requiredSection.w - requiredSection.y);

  vp = proj * view;

  placeTrees();

}