#version 450


// Output vertex packet
layout(location =0) out packet{
	vec2 textureCoord;
} outputVertex;

vec2 positions[6] = vec2[](
    vec2(-1.0, -1.0),
    vec2(1.0, 1.0),
    vec2(-1.0, 1.0),

	vec2(-1.0, -1.0),
	vec2(1.0, -1.0),
	vec2(1.0, 1.0)
);

void main(void) 
{
	vec2 xy = vec2(positions[gl_VertexIndex].x, -positions[gl_VertexIndex].y);


	outputVertex.textureCoord = (xy + vec2(1.0f)) / 2.0f;

	gl_Position = vec4(xy, 0, 1);
}