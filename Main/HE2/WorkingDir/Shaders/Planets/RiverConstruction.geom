#version 450
layout(triangles) in;

float M_PI = 3.14159265358979323846;
layout(triangle_strip, max_vertices=66) out;


struct River {
	vec4 points[35];
	int actPoints;
};

int riversActual = 8;

layout(location = 0) in Vertex
{
  vec4 normal;
  int instanceIndex;
}vertex[];

layout(location = 0) out packet
{
  vec3 vertPos;
  vec3 normal;
  vec2 texCoord;
};

layout(binding = 0) uniform ViewProjectionBuffer {
	mat4 view;
	mat4 proj;
};

layout(set = 1, binding = 0) uniform Coords
{
  vec4 requiredSection;

};

layout(binding = 1, set = 1) buffer readonly RiversOutput
{
	River rivers[108];
};

layout(binding = 2, set =1) uniform sampler2D heightMap;

mat4 vp;
mat4 mvp;

float radius  = 3150.0f;
vec2 dimensions;

const int MAX_POINTS = 30;
vec4 points[MAX_POINTS];
int actRenderPoints = 0;

mat4 transformMatrix(mat4 mat, vec3 trans)
{
  mat[3][0] = trans.x;
  mat[3][1] = trans.y;
  mat[3][2] = trans.z;

  return mat;
}

vec2 getAdjustedTexCoord(vec2 coord)
{
  vec2 heightMapCoord = coord;

	//heightMapCoord.y = 1.0f - heightMapCoord.y;
	heightMapCoord = (heightMapCoord - requiredSection.xy)*(1.0f/dimensions);

  return heightMapCoord;
}

vec3 getWorldPoint(vec2 position)
{

	float s = (position.x-0.25) * M_PI * 2;
	float t = position.y * M_PI;

	float z = radius * cos(s) * sin(t);
	float x = radius * sin(s) * sin(t);
	float y = radius * cos(t);


	return vec3(x,y,z);
}


vec2 getTexCoordFromPos(vec4 worldPos)
{
  vec4 n = normalize(worldPos);
	float u = atan(n.x, n.z) / (2.0f * M_PI) + 0.25f;

	float v = acos(worldPos.y /radius) / M_PI;

	vec2 texCoord = vec2(u,v);

  return texCoord;
}

void addPointToRenderList(vec4 point)
{
	points[actRenderPoints] = point;
	actRenderPoints++;
}

void render()
{
	for(int pointIndex = 0; pointIndex < actRenderPoints/3; pointIndex++)
	{
		int point3Index = pointIndex *3;

		gl_Position= vp *points[point3Index];
		vertPos = points[point3Index].xyz;

		EmitVertex();

		gl_Position= vp *points[point3Index+1];
		vertPos = points[point3Index+1].xyz;
		EmitVertex();

		gl_Position= vp *points[point3Index+2];
		vertPos = points[point3Index+2].xyz;
		EmitVertex();

		EndPrimitive();
	}

}

float getNoiseAtPoint(vec2 tex)
{
	float baseHeight = 3.15f * texture(heightMap, tex).r;

	return baseHeight;
}


void main()
{   
  	dimensions.x = requiredSection.z - requiredSection.x;
	dimensions.y = ( requiredSection.w - requiredSection.y);


	vp = proj * view;

	int riverIndex = vertex[0].instanceIndex;
	//int riverIndex = gl_InstanceIndex;

	// addPointToRenderList(proj * view * vec4(3850,0,0,1));

	// addPointToRenderList( proj * view *vec4(3850,0,100,1));
	
	// addPointToRenderList( proj * view *vec4(3850,-100,100,1));

	vec4 n = rivers[riverIndex].points[0] * 3150;
	vec4 np1 = rivers[riverIndex].points[1] *3150;

	float lastHeight = (3150.0f )+ (13.55f* getNoiseAtPoint(getAdjustedTexCoord(getTexCoordFromPos(n))));	

	n/=3150;
	n*=lastHeight;

	np1/=3150;
	np1*=lastHeight;

	np1.w = 1.0f;
	n.w = 1.0f;
	
	vec4 normal = normalize(n);
	vec4 direction = normalize(np1 - n);
	vec4 lastLeftVec = vec4(cross(normal.xyz, direction.xyz),0);
	vec4 lastRightVec = - lastLeftVec;


	for(int pointIndex = 0; pointIndex< min(rivers[riverIndex].actPoints -1,12); pointIndex++)
	{
		//makeSegment(rivers[riverIndex].points[pointIndex] * 3150.0f, );

		float height = (3150.0f)+ (13.55f* getNoiseAtPoint(getAdjustedTexCoord(getTexCoordFromPos(np1))));	

		n = (rivers[riverIndex].points[pointIndex]) *lastHeight;
		np1 = (rivers[riverIndex].points[pointIndex+1])*height;

		np1.w = 1.0f;
		n.w = 1.0f;
		
		float width = 8.5f;

		vec4 normal = normalize(n);
		vec4 direction = normalize(np1 - n);
		vec4 leftVec = vec4(cross(normal.xyz, direction.xyz),0);
		vec4 rightVec =  - leftVec;

		//Tri 1
		//right of n
		addPointToRenderList(normalize((n + (lastRightVec * width))));
		//right of np1
		addPointToRenderList(normalize((np1 + (rightVec * width))));
		//left of n
		addPointToRenderList(normalize((n + (lastLeftVec * width))));

		//Tri2 
		//left of n
		addPointToRenderList(normalize((n + (lastLeftVec * width))));
		//right of np1
		addPointToRenderList(normalize((np1 + (rightVec * width))));
		//left of np1
		addPointToRenderList( normalize((np1 + (leftVec * width))));

				//right of n
		// addPointToRenderList(normalize((n + (lastRightVec * width))));
		// //right of np1
		// addPointToRenderList(normalize((np1 + (rightVec * width))));
		// //left of n
		// addPointToRenderList(normalize((n + (lastLeftVec * width))));

		// //Tri2 
		// //left of n
		// addPointToRenderList(normalize((n + (lastLeftVec * width))));
		// //right of np1
		// addPointToRenderList(normalize((np1 + (rightVec * width))));
		// //left of np1
		// addPointToRenderList(normalize((np1 + (leftVec * width))));

		lastLeftVec = leftVec;
		lastRightVec = rightVec;
		lastHeight = height;
	}

	render();
}