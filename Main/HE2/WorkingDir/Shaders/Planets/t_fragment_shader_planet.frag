#version 450

layout(set = 2, binding = 0)uniform sampler2D textureImage;
layout(set = 2, binding = 1)uniform sampler2D specularImage;
layout(set = 2, binding = 2)uniform sampler2D normalMap;

vec4 grassColour;

layout(set = 3, binding = 0)uniform sampler2D waterTex;
layout(set = 3, binding = 1)uniform sampler2D waterTexSpec;
layout(set = 3, binding = 2)uniform sampler2D waterTexNorm;



layout(set = 4, binding = 0)uniform sampler2D dirtTex;
layout(set = 4, binding = 1)uniform sampler2D dirtSpec;
layout(set = 4, binding = 2)uniform sampler2D dirtNorm;

vec4 dirtColour;

layout(set = 5, binding = 0)uniform sampler2D rockTex;
layout(set = 5, binding = 1)uniform sampler2D rockSpec;
layout(set = 5, binding = 2)uniform sampler2D rockNorm;

vec4 rockColour;

layout(set = 1 , binding = 1)uniform sampler2D fragHeightMap;
layout(set = 1 , binding = 3)uniform sampler2D biomeMap;
layout(set =1, binding = 4) uniform sampler2D perlinNoise;

layout( set =2 , binding =3  ) uniform PlanetFragmentBlock {
	float tilingFactor;
	float radius;
	vec4 sectionOfMap;
	vec4 sectionOfMapBiome;
	vec4 baseColourMultiplier;
	float seaLevel;
	float basePlanetMix;
};



int riversActual = 8 ;

int oceanIntKey = 0;
int desertIntKey = 1;
int savannaIntKey =2;
int tropSeasonRainforestIntKey = 3;
int tropRainforestIntKey = 4;
int grasslandIntKey = 5;
int temperateForestIntKey = 6;
int temperateRainforestIntKey = 7;
int taigaIntKey = 8;
int tundraIntKey = 9;

vec4 basePlanetColour;


float M_PI = 3.14159265358979323846;
float delta = 0.001f;


float when_eq(float x, float y) {
	return 1.0 - abs(sign(x - y));
}

layout(location = 0)in packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

	vec3 cameraPos;
	vec3 debugTexture;

	float biomeMix;

	vec3 tangent;
	vec3 bitangent;
	vec3 debugColour;

} inputFragment;

float distanceToCam;

float circumfrence;

vec2 ogTextureCoord;

float fadeDist;
float texelSize;
vec3 mapSpec;
float heightAddition;

mat3 calcedTBN;
vec3 calcedNormal;

vec2 heightMapCoord;
vec2 dimensions;
vec2 biomeMapCoord;
vec2 biomeDimensions;

float gradient = 0.0f;

float shadowTestingMultiplier = 1.0f;
float mixPortion = .22f;

//The three outputs
layout (location = 0) out vec4 outPosition;
layout (location = 1) out vec4 outNormal;
layout (location = 2) out vec4 outAlbedo;


float getNoiseAtPoint(vec2 tex)
{
	//The 1000 is a fluff number
	return texture(fragHeightMap, tex).r * 1200.0f;
	vec4 readIn = texture(fragHeightMap, tex);


	return readIn.r* 1200.0f;
}

float getUnitLengthXAtY(float y)
{
	//Angle from top 
	float angle = y * M_PI;

	//Covert to latitude
	angle -= M_PI / 2.0f;

	float lengthOfLongitudeLine = cos(angle) * circumfrence;

	float unitX = 1.0f / lengthOfLongitudeLine;

	return unitX;
}

vec3 calculateNormal()
{
	vec3 tangent = inputFragment.tangent;

	vec3 bitangent = inputFragment.bitangent;

	vec2 pointCoord = heightMapCoord;


	//Delta is always delta in 

	vec2 pointCoordL = pointCoord + (vec2(0.5f*-getUnitLengthXAtY(pointCoord.y), 0) *(1.0f/dimensions));
	vec2 pointCoordR = pointCoord + (vec2(0.5f*getUnitLengthXAtY(pointCoord.y), 0)*(1.0f/dimensions));
	vec2 pointCoordU = pointCoord + (vec2(0, delta)*(1.0f/dimensions));
	vec2 pointCoordD = pointCoord + (vec2(0, -delta)*(1.0f/dimensions));

	//Use the original TBN matrix to calculate this properly
	float heightL = (radius + 12.0f* getNoiseAtPoint(pointCoordL));
	float heightR = (radius + 12.0f* getNoiseAtPoint(pointCoordR));
	float heightU = (radius + 12.0f* getNoiseAtPoint(pointCoordU));
	float heightD = (radius + 12.0f* getNoiseAtPoint(pointCoordD));

	vec3 tangentNormal = normalize(0.25f * vec3(2.0f * (heightL - heightR), 2.0f * (heightD - heightU), -4.0f));


	mat3 TBN = mat3(tangent, bitangent, normalize(-inputFragment.normal));


	vec3 worldNormal = normalize(TBN * tangentNormal);

	return worldNormal;
}



vec4 getColourBiomeGrassLand(float height, float gradient)
{
	vec4 tempFragColor;
	float spec;
	//This order allows the colours to layer, but will be less frame-friendly. Maybe look at this 

	//Grass texture
	tempFragColor = grassColour;
	spec = 0.1f;

	float portion = gradient;
	portion*=30.0f;
	portion = min(portion, 1.0f);
	tempFragColor = mix(tempFragColor, dirtColour, portion);

	if(height > 600)
	{
	
		float portion = gradient;
		portion*=50.0f;
		portion = min(portion, 1.0f);

		tempFragColor = mix(tempFragColor,rockColour,  clamp(portion *(1.0f - (700 - height))/100, 0.0,1.0));
	}

	tempFragColor.a = spec;

	return 0.85f*tempFragColor;
}

vec4 getColourBiomeDesert(float height, float gradient)
{
	//return dirtColour *vec4(1.8, 0.98, 1.55,0.1f);
	return 2.25f*dirtColour *vec4(1.58, 1.58, 0.55,0.1f);
}

vec4 getColourBiomeSavanna(float height, float gradient)
{
	vec4 tempFragColor;
	float spec;
	//This order allows the colours to layer, but will be less frame-friendly. Maybe look at this 

	//Grass texture
	tempFragColor = dirtColour * vec4(1.28f, 1.25f, 0.95f,1.0f);
	spec = 0.1f;

//This checks if the height is greater than 20
	if(height > 20)
	{
		float portion = gradient;
		portion+=0.3f;
		portion = (min(portion, 1.0f));

		tempFragColor = mix(tempFragColor, dirtColour,  clamp(portion *(1.0f - (300 - height))/20, 0.0,1.0));
	}

	tempFragColor.a = spec;

	return 0.85f*tempFragColor;
}

vec4 getColourBiomeTropSeasonRf(float height, float gradient)
{
	return 0.85f*grassColour + vec4(0,0.05,0.0f,0.0f);
}

vec4 getColourBiomeTropRf(float height, float gradient)
{
	return 0.85f*grassColour -vec4(0.1,0.025,0,1);
}

vec4 getColourBiomeTempForest(float height, float gradient)
{
	return 0.85f*grassColour -vec4(0.1,0.025,gradient *0.1,0.0f);
}


vec4 getColourBiomeTempRainForest(float height, float gradient)
{
	

    vec4 tempFragColor = 0.85f*grassColour + vec4(0,0.08,0.13, 0.0f);
	float spec  =0.2f;


	tempFragColor -= vec4(0.1, 0.25, 0.0, 0) * 0.1f* gradient;

	tempFragColor.a = spec;

	return 0.85f*tempFragColor;
}

vec4 getColourBiomeTaiga(float height, float gradient)
{
	vec4 tempFragColor;
	float spec;

	if(height >seaLevel)
	{
		//Grass texture
		tempFragColor = grassColour * 0.55f + dirtColour * 0.3f;
		spec = texture(specularImage,ogTextureCoord * tilingFactor).r;

		vec4 dirtColour = texture(dirtTex,ogTextureCoord * tilingFactor);

		float portion = gradient;
		portion*=60.0f;
		portion = min(portion, 1.0f);
		tempFragColor = mix(tempFragColor, dirtColour, portion);
	}
	if(height > 300)
	{
		float portion = gradient;
		portion*=10.0f;
		portion = min(portion, 1.0f);

		tempFragColor = mix(tempFragColor,rockColour,  clamp(portion *(1.0f - (600 - height))/100, 0.0,1.0));
	}

	tempFragColor.a = spec;

	return 0.85f*tempFragColor;
}

vec4 getColourBiomeTundra(float height, float gradient)
{
	return 0.85f*vec4(0.98f,0.95f,0.98f,0);
}
//New
float distToSegment(vec3 p1, vec3 p2, vec3 checkPoint)
{
	vec3 ab = p2 - p1;
	vec3 av = checkPoint - p1;

	if(dot(av, ab) < 0.0f)
		return length(av);

	
	vec3 bv = checkPoint - p2;

	if(dot(bv, ab) >= 0.0f)
		return length(bv);

	return length(cross(ab, av)) / length(ab);
}


vec4 getColourForPosition(vec2 heightmapPos, vec2 biomePos)
{
	//I think texture co-ords stay the same for samplers that aren't the height map
	float height = getNoiseAtPoint(heightmapPos);

	vec4 tempFragColor = vec4(0);
	//Check if below sea level
	if(height < seaLevel)
	{
		tempFragColor = 0.3f *texture(waterTex, ogTextureCoord * tilingFactor);
		tempFragColor.a = texture(waterTexSpec,ogTextureCoord* tilingFactor).r;
	}
	else
	{
		vec3 biomeColCen = texture(biomeMap, biomePos).rgb;
		float biomeID =int(biomeColCen.b ); 

	
		tempFragColor += when_eq(biomeID, float(grasslandIntKey)) * getColourBiomeGrassLand(height,gradient);

		tempFragColor += when_eq(biomeID, float(desertIntKey)) * getColourBiomeDesert(height,gradient);
		
		tempFragColor += when_eq(biomeID, float(savannaIntKey)) * getColourBiomeSavanna(height,gradient);

		tempFragColor += when_eq(biomeID, float(tropSeasonRainforestIntKey)) * getColourBiomeTropSeasonRf(height,gradient);

		tempFragColor += when_eq(biomeID, float(tropRainforestIntKey)) * getColourBiomeTropRf(height,gradient);

		tempFragColor += when_eq(biomeID, float(temperateForestIntKey)) * getColourBiomeTempForest(height,gradient);

		tempFragColor += when_eq(biomeID, float(temperateRainforestIntKey)) * getColourBiomeTempRainForest(height,gradient);

		tempFragColor += when_eq(biomeID, float(taigaIntKey)) * getColourBiomeTaiga(height,gradient);

		tempFragColor += when_eq(biomeID, float(tundraIntKey)) * getColourBiomeTundra(height,gradient);

		tempFragColor.a = 0.01f;

		tempFragColor = mix(basePlanetColour, tempFragColor, mixPortion);

	}

	return tempFragColor;
}

vec4 getColourForFragment()
{
	float stepSize = 5.0f;
	vec4 colour = getColourForPosition(heightMapCoord, biomeMapCoord);

	float samples = 1.0f;
	
	int steps = 0;



	// //PCF - absolutely murders performance
	// for(int x = -steps; x <= steps; ++x)
	// {
	// 	for(int y = -steps; y <= steps; ++y)
	// 	{
	// 		colour += getColourForPosition(heightMapCoord + (vec2(x * stepSize * getUnitLengthXAtY(heightMapCoord.y), y * stepSize * delta)*(1.0f/dimensions)));

	// 		samples++;
	// 	}    
	// }

	return colour / samples;
}

void sampleTextures()
{
	grassColour = texture(textureImage, ogTextureCoord * tilingFactor);
	dirtColour =texture(dirtTex, ogTextureCoord* tilingFactor);
	rockColour = texture(rockTex, ogTextureCoord * tilingFactor);

	//Use the perlin noise to vary between the grass and dirt textures
	float grassMix = texture(perlinNoise, ogTextureCoord * tilingFactor * 0.01f).r;

	basePlanetColour = (grassColour * 0.95f) + (dirtColour *0.1f);
	basePlanetColour = mix(grassColour, dirtColour, basePlanetMix +  grassMix + 0.3f * gradient);

	basePlanetColour*=baseColourMultiplier;
	basePlanetColour.a = 0.01f;

	
}

void main(void) {

	ogTextureCoord = vec2(inputFragment.textureCoord);


	//Calulate the heightmap coord based on the adjusted heightmap
	heightMapCoord = ogTextureCoord;


	dimensions.x = sectionOfMap.z - sectionOfMap.x;
	dimensions.y = ( sectionOfMap.w - sectionOfMap.y);

	heightMapCoord = (heightMapCoord - sectionOfMap.xy)*(1.0f/dimensions);


	biomeMapCoord = ogTextureCoord;

	biomeDimensions.x = sectionOfMapBiome.z - sectionOfMapBiome.x;
	biomeDimensions.y = sectionOfMapBiome.w - sectionOfMapBiome.y;

	biomeMapCoord = (biomeMapCoord - sectionOfMapBiome.xy) * (1.0f/biomeDimensions);

	if(heightMapCoord.x < -0.2f || heightMapCoord.y < -0.2f || heightMapCoord.x >1.2f || heightMapCoord.y > 1.2f)
	{
		discard;
	}


	circumfrence = radius * 2.0f * M_PI;

	delta =( 0.5f / (circumfrence / 2.0f)) * 12.0f;

	calcedNormal = calculateNormal();
	gradient = 1.0f - max(dot(calcedNormal, inputFragment.normal),0.0f);

	sampleTextures();


	outAlbedo = getColourForFragment();

	vec4 outPos = vec4(inputFragment.vert, 1.0);

	outPosition = outPos;
	outNormal = vec4(calcedNormal,0);
}


