#version 450

layout (location = 0) out vec4 outPosition;
layout (location = 1) out vec4 outNormal;
layout (location = 2) out vec4 outAlbedo;

layout(set = 1, binding = 0) uniform sampler2D albedo;

layout(location = 0) in packet
{
  vec3 vertPos;
  vec3 normal;
  vec2 texCoord;
};

void main()
{
  vec2 tx =texCoord;
  tx.y = 1.0f -tx.y;
  outAlbedo = texture(albedo, tx);
  outNormal = vec4(normal,1);

  vec3 vkPos = vertPos;

  outPosition = vec4(vkPos,1);  

  if(outAlbedo.a < 0.4f) discard;
}

