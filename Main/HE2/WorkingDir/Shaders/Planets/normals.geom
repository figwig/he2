#version 450
layout(triangles) in;

// Three lines will be generated: 6 vertices
layout(line_strip, max_vertices=6) out;

float normal_length = 10.0f;
layout (set = 0, binding = 0) uniform InputBuffer
{
  mat4 gxl3d_ModelViewProjectionMatrix;
};

layout(location = 0)in Vertex
{
  vec4 normal;
  vec4 color;
} vertex[];

void main()
{   
    vec3 P = gl_in[0].gl_Position.xyz;
    vec3 N = vertex[0].normal.xyz;
    gl_Position = gxl3d_ModelViewProjectionMatrix * vec4(P, 1.0);
    EmitVertex();
    
    gl_Position = gxl3d_ModelViewProjectionMatrix * vec4(P + N * normal_length, 1.0);
    EmitVertex();
    
    EndPrimitive();

  int i;
  for(i=0; i<gl_in.length(); i++)
  {
    vec3 P = gl_in[i].gl_Position.xyz;
    vec3 N = vertex[i].normal.xyz;
    
    gl_Position = gxl3d_ModelViewProjectionMatrix * vec4(P, 1.0);
    EmitVertex();
    
    gl_Position = gxl3d_ModelViewProjectionMatrix * vec4(P + N * normal_length, 1.0);
    EmitVertex();
    
    EndPrimitive();
  }
}