@echo off
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V t_vertex_shader_planet.vert

C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V tcs_planet.tesc

C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V tes_planet.tese

C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V t_fragment_shader_planet.frag


C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V fragment_terrain_noise.frag -o generator.frag.spv


C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V vertex_terrain_noise.vert -o generator.vert.spv


C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V agents.vert -o agents.vert.spv


C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V agents.frag -o agents.frag.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V ridgesAndRiversMap.frag -o riversAndRidgesMap.frag.spv

C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V biomes.frag -o biomes.frag.spv

C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V biomes.vert -o biomes.vert.spv

C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V ridge.comp -o rr.comp.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V copy.comp -o copy.comp.spv

C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V normals.geom -o normals.geom.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V normals.frag -o normals.frag.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V normals.vert -o normals.vert.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V atmosVert.vert -o atmos.vert.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V atmosFrag.frag -o atmos.frag.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V foliagetest.geom -o foliagetest.geom.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V foliagetest.frag -o foliagetest.frag.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V foliagetest.vert -o foliagetest.vert.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V riverconstruction.geom -o riverconstruction.geom.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V riverconstruction.frag -o riverconstruction.frag.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V riverconstruction.vert -o riverconstruction.vert.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V perlin.frag -o perlin.frag.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V innerAtmosDef.frag -o innerAtmosDef.frag.spv
C:/VulkanSDK/1.2.135.0/Bin/glslangValidator.exe -V iAtmosVert.vert -o innerAtmosDef.vert.spv
