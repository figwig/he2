#version 450
#extension GL_ARB_seperate_shader_objects : enable

layout (location =0) in vec3 position;
layout (location =1) in vec3 inNormal;
layout (location =2) in vec3 inTangent;
layout (location =3) in vec3 inBitangent;
layout (location =4) in vec2 inTexCoord;

layout (location = 0) out vec2 fragTexCoord;
layout (location = 1) out vec3 vecToCam;
layout (location = 2) out vec3 vecToPlanet;


layout(binding = 0) uniform ViewProjectionBuffer {
	mat4 view;
	mat4 proj;
} viewProjectionBuffer;


layout( push_constant ) uniform ModelBuffer {
  mat4 modelMatrix;
};

vec4 planetPos = vec4(0);

void main()
{
	fragTexCoord = inTexCoord;
	//gl_Position = viewProjectionBuffer.proj * viewProjectionBuffer.view * modelBuffer.model  * vec4(position, 1.0f);
	vec4 worldPos = modelMatrix*vec4(position, 1.0f);

	vec4 camPos = viewProjectionBuffer.view * worldPos;

	mat4 invViewMat = inverse(viewProjectionBuffer.view);

	vec4 actualCamPos = vec4(invViewMat[3][0], invViewMat[3][1],invViewMat[3][2],0);

	vecToCam = normalize(vec3(actualCamPos - worldPos));
	vecToPlanet = normalize(inNormal);

	gl_Position = viewProjectionBuffer.proj * camPos;
}