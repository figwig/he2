#version 450
#extension GL_ARB_seperate_shader_objects : enable


layout(set = 6,binding = 0) uniform InputBuffer
{
	mat4 transform; //Model Matrix
	vec3 origin;// The centre of the sphere
	vec3 cameraPos_World;
};

// Input vertex packet
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 tangent;
layout(location = 3) in vec3 bitangent;
layout(location = 4) in vec2 textureCoord;

// Output vertex packet
layout(location = 0) out packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

	vec3 tangent;
	vec3 bitangent;
	vec3 origin;
	vec3 cameraPos_World;

} outputVertex;

mat3 TBN;
vec3 pos;
mat3 normalMat;

void main(void) {
	
	outputVertex.cameraPos_World = cameraPos_World;

	//Calculate pos
	pos = position;

	//Transform matrix dealt with
	pos = vec3(transform * vec4(pos,1.0));

	//pos.y = -pos.y;

	gl_Position = vec4(pos,1.0);
	
	//Transform for vulkan's weirdness
	//pos.y =pos.y;

	//Pass through the texture coords
	outputVertex.textureCoord.x = textureCoord.x;
	outputVertex.textureCoord.y = 1.0 -textureCoord.y;

	//Output vertex pos
	outputVertex.vert = pos.xyz;
	outputVertex.origin = mat3(transform) * origin;

	//Work out the normal for lighting
	normalMat = transpose(inverse(mat3(transform)));

	outputVertex.normal = normalize(normalMat * normal);
	//outputVertex.normal.y = outputVertex.normal.y;

	vec3 T = normalMat * normalize(tangent);

	vec3 B = normalMat* normalize(bitangent);

	outputVertex.tangent =T;
	outputVertex.bitangent = B;
}
