#version 450

float M_PI = 3.14159265358979323846;
// input packet


// output packet
layout(location = 0) out vec4 fragmentColour;
layout(set =0, binding = 0) uniform sampler2D heightMap;
layout(set =0, binding = 1) uniform sampler2D biomeDescriptorImage;


layout(set =1, binding = 0) uniform BiomeBlock
{
	vec4 sectionOfMap;
	float seaLevel;
	float moistureCoordMod;
	float tempAddition;
	float moistureAddition;
};

layout(location = 0)in packet{
	vec2 textureCoord;
} inputFragment;

float radius = 1000.0f;
//float moistureCoordMod = 0.002f;

//	Classic Perlin 3D Noise 
//	by Stefan Gustavson
//
vec4 permute(vec4 x) { return mod(((x*34.0) + 1.0)*x, 289.0); }
vec4 taylorInvSqrt(vec4 r) { return 1.79284291400159 - 0.85373472095314 * r; }
vec3 fade(vec3 t) { return t * t*t*(t*(t*6.0 - 15.0) + 10.0); }

float random (vec2 seed) {
	return fract(sin(dot(seed, vec2(12.9898, 78.233)))*52560.153);
}
vec2 random2()
{
	return vec2(random(vec2(34.43, 56.2323)), random(vec2(45.32, 4.2)));
}

vec2 random2(vec2 seed)
{
	return vec2(random(seed), random(seed.yx));
}


int oceanIntKey = 0;

//vec3 desertKey = vec3(123.0f/255.0f,255.0f/255.0f,0.0f) ;
vec3 desertKey = vec3(123.0f,255.0f,0.0f) ;
int desertIntKey = 1;

//vec3 savannaKey = vec3(181.0f/255.0f,117.0f/255.0f,160.0f/255.0f);
vec3 savannaKey = vec3(181.0f,117.0f,160.0f);
int savannaIntKey = 2;

//vec3 tropicalSeasonalRainforestKey =  vec3(255.0f/255.0f,0.0f,76.0f/255.0f);
vec3 tropicalSeasonalRainforestKey =  vec3(255.0f,0.0f,76.0f);
int tropSeasonRainforestIntKey = 3;

//vec3 tropicalRainforestKey =   vec3(37.0f/255.0f, 76.0f/255.0f, 40.0f/255.0f);
vec3 tropicalRainforestKey =   vec3(37.0f, 76.0f, 40.0f);
int tropRainforestIntKey = 4;

//vec3 grasslandKey =  vec3(131.0f/255.0f,156.0f/255.0f, 114.0f/255.0f);
vec3 grasslandKey =  vec3(131.0f,156.0f, 114.0f);
int grasslandIntKey = 5;

//vec3 temperateForestKey =   vec3(255.0f/255.0f,233.0f/255.0f,0.0f);
vec3 temperateForestKey =   vec3(255.0f,233.0f,0.0f);
int temperateForestIntKey = 6;

//vec3 temperateRainforestKey =   vec3(81.0f/255.0f, 104.0f/255.0f, 85.0f/255.0f);
vec3 temperateRainforestKey =   vec3(81.0f, 104.0f, 85.0f);
int temperateRainforestIntKey = 7;

//vec3 taigaKey =  vec3(124.0f/255.0f,49.0f/255.0f,0.0f);
vec3 taigaKey =  vec3(124.0f,49.0f,0.0f);
int taigaIntKey = 8;

//vec3 tundraKey =  vec3(147.0f/255.0f,158.0f/255.0f,177.0f/255.0f);
vec3 tundraKey =  vec3(147.0f,158.0f,177.0f);
int tundraIntKey = 9;


float cnoise(vec3 P) {
	vec3 Pi0 = floor(P); // Integer part for indexing
	vec3 Pi1 = Pi0 + vec3(1.0); // Integer part + 1
	Pi0 = mod(Pi0, 289.0);
	Pi1 = mod(Pi1, 289.0);
	vec3 Pf0 = fract(P); // Fractional part for interpolation
	vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0
	vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
	vec4 iy = vec4(Pi0.yy, Pi1.yy);
	vec4 iz0 = Pi0.zzzz;
	vec4 iz1 = Pi1.zzzz;

	vec4 ixy = permute(permute(ix) + iy);
	vec4 ixy0 = permute(ixy + iz0);
	vec4 ixy1 = permute(ixy + iz1);

	vec4 gx0 = ixy0 / 7.0;
	vec4 gy0 = fract(floor(gx0) / 7.0) - 0.5;
	gx0 = fract(gx0);
	vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
	vec4 sz0 = step(gz0, vec4(0.0));
	gx0 -= sz0 * (step(0.0, gx0) - 0.5);
	gy0 -= sz0 * (step(0.0, gy0) - 0.5);

	vec4 gx1 = ixy1 / 7.0;
	vec4 gy1 = fract(floor(gx1) / 7.0) - 0.5;
	gx1 = fract(gx1);
	vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
	vec4 sz1 = step(gz1, vec4(0.0));
	gx1 -= sz1 * (step(0.0, gx1) - 0.5);
	gy1 -= sz1 * (step(0.0, gy1) - 0.5);

	vec3 g000 = vec3(gx0.x, gy0.x, gz0.x);
	vec3 g100 = vec3(gx0.y, gy0.y, gz0.y);
	vec3 g010 = vec3(gx0.z, gy0.z, gz0.z);
	vec3 g110 = vec3(gx0.w, gy0.w, gz0.w);
	vec3 g001 = vec3(gx1.x, gy1.x, gz1.x);
	vec3 g101 = vec3(gx1.y, gy1.y, gz1.y);
	vec3 g011 = vec3(gx1.z, gy1.z, gz1.z);
	vec3 g111 = vec3(gx1.w, gy1.w, gz1.w);

	vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
	g000 *= norm0.x;
	g010 *= norm0.y;
	g100 *= norm0.z;
	g110 *= norm0.w;
	vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
	g001 *= norm1.x;
	g011 *= norm1.y;
	g101 *= norm1.z;
	g111 *= norm1.w;

	float n000 = dot(g000, Pf0);
	float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
	float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
	float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
	float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
	float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
	float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
	float n111 = dot(g111, Pf1);

	vec3 fade_xyz = fade(Pf0);
	vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
	vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
	float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x);
	return 2.2 * n_xyz;
}

float octavedNoise(vec3 p, int octaves)
{
	    // Initial values
    float value = 0.0;
    float amplitude = .5;
    float frequency = 2.152;
    //
    // Loop of octaves
    for (int i = 0; i < octaves; i++) {
        value += amplitude * cnoise(p);
        p *= 2.;
        amplitude *= .5;
    }
	return value;
    //return (2.0f * value) -1.0f;
}

vec3 getWorldPoint(vec2 position)
{

	float s = position.x * M_PI * 2;
	float t = position.y * M_PI;

	float z = radius * cos(s) * sin(t);
	float x = radius * sin(s) * sin(t);
	float y = radius * cos(t);

	return vec3(x,y,z);
}

bool vec3Close(vec3 bc, vec3 comp)
{
	if(abs(length(bc-comp)) < 12.0f)
	{
		return true;
	}
	return false;
}

int pickBiomeID(vec3 biomeCol)
{

	biomeCol *= 255.0f;
	if(biomeCol == vec3(0,0,0))
	{
		return oceanIntKey;
	}
	else if(vec3Close(biomeCol, grasslandKey))
	{
		return grasslandIntKey;
	}
	else if(vec3Close(biomeCol, desertKey))
	{
		return desertIntKey;
	}
	else if(vec3Close(biomeCol ,savannaKey))
	{
		return savannaIntKey;
	}
	else if(vec3Close(biomeCol, tropicalSeasonalRainforestKey))
	{
		return tropSeasonRainforestIntKey;
	}
	else if(vec3Close(biomeCol ,tropicalRainforestKey))
	{
		return tropRainforestIntKey;
	}
	else if(vec3Close(biomeCol,temperateForestKey))
	{
		return temperateForestIntKey;
	}
	else if(vec3Close(biomeCol,temperateRainforestKey))
	{
		return temperateRainforestIntKey;
	}
	else if(vec3Close(biomeCol, taigaKey))
	{
		return taigaIntKey;
	}
	else if(vec3Close(biomeCol , tundraKey))
	{
		return tundraIntKey;
	}

	return -1;
}


vec3 getBiomeColour(vec2 biomeCoord)
{
	biomeCoord.x = clamp(biomeCoord.x, 0.0f, 1.0f);
	biomeCoord.y = clamp(biomeCoord.y, 0.0f, 1.0f);

	biomeCoord += 0.002f * random2(biomeCoord);

	vec4 bc = texture(biomeDescriptorImage,biomeCoord);
	//bc = vec4(temperateRainforestKey,1);
	int b  = (pickBiomeID(bc.rgb));
	//b  = tundraIntKey;
	while(b==-1)
	{
		biomeCoord*= vec2(0.99f,0.99f);
		bc = texture(biomeDescriptorImage,biomeCoord);
		b = pickBiomeID(bc.rgb);

	}

	return vec3(bc.rg, float(b));
}
void main(void) {
	vec2 st = inputFragment.textureCoord;
	vec2 ogCoord = st;
	vec2 dimensions;
	dimensions.x = sectionOfMap.z - sectionOfMap.x;
	dimensions.y = ( sectionOfMap.w - sectionOfMap.y);

	st.x *= (dimensions.x);
	st.y *= (dimensions.y);

	st+= sectionOfMap.xy;


	float height = texture(heightMap, ogCoord).r;


	if(height * 1200 < (seaLevel))
	//if(false)
	{
		fragmentColour = vec4(0,0,0,1);
	}
	else
	{
		//Base temperature is gained from distance from the equator
		float distFromEq = 1.0f - (2.0f*abs(0.5f - st.y));

		

		//Temperature is also affected by height
		float temp = distFromEq - 0.5f * pow(height,1.5f);

		temp = clamp(temp, 0.0f, 1.0f);

		//Moisture starts as perlin noise
		float moisture =  1.1f * pow( octavedNoise(getWorldPoint(st) * moistureCoordMod, 2), 1.0f) + 0.5f;

		float wetWhenCloseToSeaThresh = seaLevel * 0.05f;

		float closeToSea = (((height * 1200) - seaLevel)) / wetWhenCloseToSeaThresh;

		closeToSea = clamp(-closeToSea, 0, 4);

		//places close to the sea are wetter
		moisture += 0.08f * closeToSea;

		//Really hot places are dry
		moisture-= clamp(0.4f * pow(temp,5), 0 , 1);

		//Add our planet specifics
		moisture+=moistureAddition;
		temp+=tempAddition;


		//Clamp our variables
		temp = clamp(temp, 0.0f,1.0f);
		moisture = clamp(moisture, 0.0f, min(temp + 0.15f,1.0f));

		//moisture = clamp(moisture, 0.0f, 1.0f- temp);
		vec2 biomeCoord = vec2(moisture, 1.0f - temp);

		//fragmentColour = vec4(vec3(height) , 1);
		//fragmentColour = vec4(vec3(distFromEq) , 1);
		//fragmentColour = vec4(biomeCoord, 0,1);
		//return;
		fragmentColour = vec4(getBiomeColour(biomeCoord), 1.0f);


	}

	//float riverNoise =  1.1f * pow( octavedNoise(getWorldPoint(st) * moistureCoordMod, 2), 1.0f) + 0.5f;
	// vec2 startPoint = normalize(random2());

	// if(length(startPoint -st) < 0.01f)
	// {
	// 	fragmentColour+=vec4(1,0,0,0);
	// }

}


//Below is voroni noise, looks nice but going to be calculating whittaker biomes, not using randoms

    // Cell positions
    // vec2 point[12];
    // point[0] = vec2(0.1,0.5);
    // point[1] = vec2(0.430,0.410);
    // point[2] = vec2(0.28,0.64);
    // point[3] =  vec2(0.864,0.1215);
    // point[4] =  vec2(0.1,0.3);
    // point[5] =  vec2(0.79,0.74);
    // point[6] =  vec2(0.3,0.5);
    // point[7] =  vec2(0.94,0.92);
    // point[8] =  vec2(0.1,0.13);
    // point[9] =  vec2(0.84,0.36);
    // point[10] =  vec2(0.15,0.65);
    // point[11] =  vec2(0.65,0.7);


    // float m_dist = 1.;  // minimun distance
    // vec2 m_point;        // minimum position

    // // Iterate through the points positions
    // for (int i = 0; i < 12; i++) {
    //     float dist = distance(st, point[i]);
    //     if ( dist < m_dist ) {
    //         // Keep the closer distance
    //         m_dist = dist;

    //         // Kepp the position of the closer point
    //         m_point = point[i];
    //     }
    // }

    // // // Add distance field to closest point center
    // // color += m_dist*2.;

    // // tint acording the closest point position
    // color.rg = m_point;

    // // // Show isolines
    // // color -= abs(sin(80.0*m_dist))*0.07;

    // // // Draw point center
    // // color += 1.-step(.02, m_dist);