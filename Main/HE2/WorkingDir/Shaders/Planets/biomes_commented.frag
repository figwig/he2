//Base temperature is gained from distance from the equator
float distFromEq = 1.0f - (2.0f*abs(0.5f - st.y));

//Temperature is also affected by height. Raise the effect to a constant power. 
float temp = distFromEq - 0.5f * pow(height, temperatureHeightEffectRegulator);

//Moisture starts as noise.
float moisture = octavedNoise(getWorldPoint(coord) * moistureCoordModifer, 2) + 0.5f;

//When we're close to the sea, there is a higher level of moisture.
//Work out the threshold below which the effect can take place. Use a constant regulator to calculate this.
float wetWhenCloseToSeaThresh = seaLevel * seaLevelMoistureEffectRegulator;

//Work out if we're in range of the effect, and how close we are
float closeToSea = ((height ) - seaLevel)) / wetWhenCloseToSeaThresh;

//Clamp the result at 0, -4, so that we can only have a positive effect near the sea, and not a negative effect further out
closeToSea = clamp(-closeToSea, 0, 4);

//Add our close to sea modifier to the moisture value, multiplied by a constant regulator to limit the effect.
moisture += seaWetRegulator * closeToSea;

//Really hot places are dry
moisture-= clamp(0.4f * pow(temp,5), 0 , 1);

//Add our planet specifics
moisture+=moistureAddition;
temp+=tempAddition;

//Clamp our variables
temp = clamp(temp, 0.0f,1.0f);
moisture = clamp(moisture, 0.0f, min(temp + 0.15f,1.0f));

//Retrieve our biome key from the map
int biomeKey = texture(biomeMap, vec2(temp, moisture));

//Store this in the outputmap
outputColour = vec4(biomeKey);