#version 450
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 tangent;
layout(location = 3) in vec3 bitangent;
layout(location = 4) in vec2 textureCoord;

float M_PI = 3.14159265358979323846;
layout(location = 0)out Vertex
{
  vec3 vertPos;
  vec3 normal;
  vec2 texCoord;
} vertex;


layout(binding = 0) uniform ViewProjectionBuffer {
	mat4 view;
	mat4 proj;
};

layout(set = 2, binding = 0) uniform Coords
{
  vec4 requiredSection;
  vec4 cameraPos;
  vec4 quadrant;
  float objScale;
  int seedOffset;
};

layout(set =2, binding = 1) uniform sampler2D heightMap;

float radius  = 3150.0f;
vec2 dimensions;

const float PHI = 1.61803398874989484820459; // Φ = Golden Ratio 

float gold_noise(in vec2 xy, in float seed)
{
    return fract(tan(distance(xy*PHI, xy)*seed)*xy.x);
}


mat4 transformMatrix(mat4 mat, vec3 trans)
{
  mat[3][0] = trans.x;
  mat[3][1] = trans.y;
  mat[3][2] = trans.z;

  return mat;
}

vec2 getAdjustedTexCoord(vec2 coord)
{
  vec2 heightMapCoord = coord;

	//heightMapCoord.y = 1.0f - heightMapCoord.y;
	heightMapCoord = (heightMapCoord - requiredSection.xy)*(1.0f/dimensions);

  return heightMapCoord;
}


float getNoiseAtPoint(vec2 tex)
{
	float baseHeight = 3.15f * texture(heightMap, tex).r;

	return baseHeight;
}


vec3 getWorldPoint(vec2 position)
{

	float s = (position.x-0.25) * M_PI * 2;
	float t = position.y * M_PI;

	float z = radius * cos(s) * sin(t);
	float x = radius * sin(s) * sin(t);
	float y = radius * cos(t);


	return vec3(x,y,z);
}

vec2 getTexCoordFromPos(vec4 worldPos)
{
  vec4 n = normalize(worldPos);
	float u = atan(n.x, n.z) / (2.0f * M_PI) + 0.25f;

	float v = acos(worldPos.y /radius) / M_PI;

	vec2 texCoord = vec2(u,v);

  return texCoord;
}


mat4 getMatrix(vec4 worldPos, vec2 sphericalCoords)
{
  mat4 m = mat4(1);

  //Add any number, normalize it, slightly difference world pos, vector between them is an x Axis
  vec3 xAxis = normalize(normalize(vec3(worldPos) + vec3(0.0,0.2,0.0)) - normalize(vec3(worldPos)));
  //Planet is at 0,0,0 so world pos can be used as the normal and vector from origin
  vec3 yAxis = normalize(vec3(worldPos));
  //We should be able to cross the y and x axis like a normal and tangent to generate a bitangent
  vec3 zAxis = normalize(cross(yAxis, xAxis));

  mat3 baseMat = mat3(xAxis, yAxis, zAxis);

  m = mat4(baseMat);

  float scale = objScale;

  //Transform m by the Radius in the y axis to put it on the surface

  mat4 m2 = mat4(1);
  m2[0][0]*= scale;
  m2[1][1]*= scale;
  m2[2][2]*= scale;

  float height = 12* getNoiseAtPoint(sphericalCoords);

  //height = 0.0f;

  m2 = transformMatrix(m2, vec3(0,radius + height ,0));

  m = m  * m2;

  return m;
}

vec3 randomVec3(vec2 xy, int seed)
{
  return vec3(gold_noise(xy, seed * 3), gold_noise(xy, seed * 3 +2), gold_noise(xy, seed *3 +4));
}

void main()
{
  	dimensions.x = requiredSection.z - requiredSection.x;
	dimensions.y = ( requiredSection.w - requiredSection.y);

  vec4 closestPoint = normalize(cameraPos) * radius;
  mat4 m;

  float quadrantDiff = 1000;

  //Doing this in c++ now
  vec2 quadrantCoord = quadrant.xy;

  vec4 quadrantWorldPoint = (vec4(getWorldPoint(quadrantCoord),0));

  float range = 8000 / quadrantDiff;

  vec4 treePos = normalize(quadrantWorldPoint + range *  (vec4(2.0 * randomVec3(quadrantCoord * 20, int( gl_InstanceIndex)) - vec3(1),1))) * radius;

  treePos.w = 1.0f;

  vec2 treeCoord = getAdjustedTexCoord(getTexCoordFromPos(treePos));

  m =  getMatrix(treePos, treeCoord);

  vertex.vertPos = vec3(m * vec4(position,1));
  vec4 pos = proj * view * vec4(vertex.vertPos,1);
  //vertex.vertPos.y = -vertex.vertPos.y;
  gl_Position = pos;


  mat3 normalMat = transpose(inverse(mat3(m)));
  vertex.normal = normalMat * normalize(normal);

  vertex.texCoord = textureCoord;
}

// #version 450
// layout(location = 0) in vec3 position;
// layout(location = 1) in vec3 normal;
// layout(location = 2) in vec3 tangent;
// layout(location = 3) in vec3 bitangent;
// layout(location = 4) in vec2 textureCoord;

// layout(location = 0)out Vertex
// {
//   vec4 normal;
//   vec4 color;
//   int instanceID;
// } vertex;



// void main()
// {
//   gl_Position = vec4(position,0);
//   vertex.normal = vec4(normal,0);
//   vertex.color =  vec4(1.0, 1.0, 0.0, 1.0);
//   vertex.instanceID = gl_InstanceIndex;

// }