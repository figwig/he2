#version 450

//layout(triangles, equal_spacing, ccw) in;
layout(triangles, equal_spacing, cw) in;

float M_PI = 3.14159265358979323846;

layout(location = 0)in packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;

	vec3 cameraPos;
	vec3 tangent;
	vec3 bitangent;
	vec3 origin;
	vec3 debugColour;

} inputVertex[];

layout(location = 0)out packet{

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;
	vec3 cameraPos;
	vec3 debugTexture;

	float biomeMix;

	vec3 tangent;
	vec3 bitangent;
	vec3 debugColour;

} outputVertex;

layout( set =1, binding = 2) uniform CameraBlock 
{
	vec4 sectionOfMap;
};

layout(set =0, binding = 0) uniform ViewProjectionBuffer {
	mat4 view;
	mat4 proj;
};


float noiseScale = 12.0f;

layout(set =1, binding = 0) uniform sampler2D heightMap;

vec3 vecToOrigin;
vec4 position;
//For normal sampling
float delta = 0.001f;
vec3 modelXOffset;
vec3 modelYOffset;
float circumfrence;

//World pos of vertex
vec3 worldPos;

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)
{
	return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

vec3 interpolate3D(vec3 v0, vec3 v1, vec3 v2)
{
	return vec3(gl_TessCoord.x) * v0 + vec3(gl_TessCoord.y) * v1 + vec3(gl_TessCoord.z) * v2;
}

mat3 interpolateMat3(mat3 m0, mat3 m1, mat3 m2)
{
	return  m0 * gl_TessCoord.x + m1 * gl_TessCoord.y + m2 * gl_TessCoord.z;
}

vec2 getCorrectXTexCoord(vec2 tex)
{
	vec2 texCoord = vec2(0);
	texCoord.y = tex.y;


	//Angle from top 
	float angle = tex.y * M_PI;

	//Covert to latitude
	angle -= M_PI / 2.0f;

	float lengthOfLongitudeLine = cos(angle) * circumfrence;

	//I think this is the right way round, will become more obvious as the terrain is more defined
	float portion = lengthOfLongitudeLine / circumfrence;

	//shorten the x range for this longitude
	texCoord.x = portion * tex.x;
	//Move it to the centre
	texCoord.x += 0.5f * (1.0f - portion);

	texCoord *= 0.5f;

	return texCoord;
}

float getUnitLengthXAtY(float y)
{
	//Angle from top 
	float angle = y * M_PI;

	//Covert to latitude
	angle -= M_PI / 2.0f;

	float lengthOfLongitudeLine = cos(angle) * circumfrence;

	float unitX = 1.0f / lengthOfLongitudeLine;

	return unitX;
}

float getNoiseAtPoint(vec2 tex)
{
	float baseHeight = 3.15f * texture(heightMap, tex).r;

	return baseHeight;
}

void InterpolateAndPass()
{
	outputVertex.textureCoord = interpolate2D(inputVertex[0].textureCoord, inputVertex[1].textureCoord, inputVertex[2].textureCoord);

	outputVertex.normal = normalize(interpolate3D(inputVertex[0].normal, inputVertex[1].normal, inputVertex[2].normal));

	outputVertex.TBN = inputVertex[0].TBN;

	outputVertex.cameraPos = interpolate3D(inputVertex[0].cameraPos, inputVertex[1].cameraPos, inputVertex[2].cameraPos);

	outputVertex.vert = interpolate3D(inputVertex[0].vert, inputVertex[1].vert, inputVertex[2].vert);

	outputVertex.tangent = interpolate3D(inputVertex[0].tangent, inputVertex[1].tangent, inputVertex[2].tangent);

	outputVertex.bitangent = interpolate3D(inputVertex[0].bitangent, inputVertex[1].bitangent, inputVertex[2].bitangent);

	outputVertex.debugColour = interpolate3D(inputVertex[0].debugColour, inputVertex[1].debugColour, inputVertex[2].debugColour);
}

void main(void)
{
	//Pass Through
	InterpolateAndPass();

	//Interpolate the position
	position = (gl_TessCoord.x * gl_in[0].gl_Position + gl_TessCoord.y * gl_in[1].gl_Position + gl_TessCoord.z * gl_in[2].gl_Position);

	vec2 corrTex = vec2(outputVertex.textureCoord);

	vec2 dimensions;
	dimensions.x = sectionOfMap.z - sectionOfMap.x;
	dimensions.y = ( sectionOfMap.w - sectionOfMap.y);

	corrTex = (corrTex - sectionOfMap.xy)*(1.0f/dimensions);

	float Displacement = getNoiseAtPoint(corrTex);

	//Add on addtional height
	//Origin is the same regardless
	vec3 origin = inputVertex[0].origin;

	vec3 dispDirection = normalize(outputVertex.vert - origin);

	vec3 addition = dispDirection * Displacement * noiseScale;

	float additionValue = 3150 + Displacement + noiseScale;

	position = normalize(position);
	position *= 3150;

	//apply this to point
	position.x += addition.x;
	position.y += addition.y;
	position.z += addition.z;

	position.w = 1.0f;

	//and calculate the world position
	gl_Position = proj * view * position;
}
