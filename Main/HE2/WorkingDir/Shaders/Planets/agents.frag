#version 450

#extension GL_EXT_debug_printf : enable
float M_PI = 3.14159265358979323846;
// input packet


// output packet
layout(location = 0) out vec4 fragmentColour;
layout(location = 0)in packet{
	vec2 textureCoord;
} inputFragment;



struct Layer
{
	float coordModifier;
	float exponent;
	float scale;
	float constructive;

	float scaleDependsOn;
	float additionConst;
	float octaves;
	float lActive;
};

vec4 debugColour  =vec4(0,0,0,0);


layout(set = 1, binding =0) uniform Agents{
	vec4 requiredSection;
	float minHeightForMountain;

};

struct Ridge {
	vec4 points[35];
	int actPoints;
};

layout (set = 1,binding = 1) uniform RiversAndRidges
{
	Ridge ridges[30];
	int actRidges;

};

struct River {
	vec4 points[35];
	int actPoints;
};


layout(set =1, binding = 2) uniform sampler2D rrPregen;

layout(set = 0, binding = 0) uniform Layers
{
	Layer layers[5];
	int actualLayerCount;
	float oceanLevel;
	float radius;
	vec4 noiseOffset;
};

float actualRadius = 1000.0f;
int maxOctaveForDimension = 20;
vec2 adjustedTexCoord ;

float layerScales[5];

//	Classic Perlin 3D Noise 
//	by Stefan Gustavson
//
vec4 permute(vec4 x) { return mod(((x*34.0) + 1.0)*x, 289.0); }
vec4 taylorInvSqrt(vec4 r) { return 1.79284291400159 - 0.85373472095314 * r; }
vec3 fade(vec3 t) { return t * t*t*(t*(t*6.0 - 15.0) + 10.0); }


float cnoise(vec3 P) {
	vec3 Pi0 = floor(P); // Integer part for indexing
	vec3 Pi1 = Pi0 + vec3(1.0); // Integer part + 1
	Pi0 = mod(Pi0, 289.0);
	Pi1 = mod(Pi1, 289.0);
	vec3 Pf0 = fract(P); // Fractional part for interpolation
	vec3 Pf1 = Pf0 - vec3(1.0); // Fractional part - 1.0
	vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);
	vec4 iy = vec4(Pi0.yy, Pi1.yy);
	vec4 iz0 = Pi0.zzzz;
	vec4 iz1 = Pi1.zzzz;

	vec4 ixy = permute(permute(ix) + iy);
	vec4 ixy0 = permute(ixy + iz0);
	vec4 ixy1 = permute(ixy + iz1);

	vec4 gx0 = ixy0 / 7.0;
	vec4 gy0 = fract(floor(gx0) / 7.0) - 0.5;
	gx0 = fract(gx0);
	vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);
	vec4 sz0 = step(gz0, vec4(0.0));
	gx0 -= sz0 * (step(0.0, gx0) - 0.5);
	gy0 -= sz0 * (step(0.0, gy0) - 0.5);

	vec4 gx1 = ixy1 / 7.0;
	vec4 gy1 = fract(floor(gx1) / 7.0) - 0.5;
	gx1 = fract(gx1);
	vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);
	vec4 sz1 = step(gz1, vec4(0.0));
	gx1 -= sz1 * (step(0.0, gx1) - 0.5);
	gy1 -= sz1 * (step(0.0, gy1) - 0.5);

	vec3 g000 = vec3(gx0.x, gy0.x, gz0.x);
	vec3 g100 = vec3(gx0.y, gy0.y, gz0.y);
	vec3 g010 = vec3(gx0.z, gy0.z, gz0.z);
	vec3 g110 = vec3(gx0.w, gy0.w, gz0.w);
	vec3 g001 = vec3(gx1.x, gy1.x, gz1.x);
	vec3 g101 = vec3(gx1.y, gy1.y, gz1.y);
	vec3 g011 = vec3(gx1.z, gy1.z, gz1.z);
	vec3 g111 = vec3(gx1.w, gy1.w, gz1.w);

	vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));
	g000 *= norm0.x;
	g010 *= norm0.y;
	g100 *= norm0.z;
	g110 *= norm0.w;
	vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));
	g001 *= norm1.x;
	g011 *= norm1.y;
	g101 *= norm1.z;
	g111 *= norm1.w;

	float n000 = dot(g000, Pf0);
	float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));
	float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));
	float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));
	float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));
	float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));
	float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));
	float n111 = dot(g111, Pf1);

	vec3 fade_xyz = fade(Pf0);
	vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);
	vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);
	float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x);
	return 2.2 * n_xyz;
}

float noise (vec3 st) {
    return fract(sin(dot(st.xyz,
                         vec3(12.9898,78.233,134.5)))*
        43758.5453123);
}

float noise (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
}

float Hash(in float n)
{
    return fract(sin(n)*43758.5453123);
}

float cnoise2(vec3 x)
{
	//A potentially better perlin noise here?

    vec3 p = floor(x);
    vec3 f = fract(x);
    f = f*f*(3.0-2.0*f);
    float n = p.x + p.y*157.0 + 113.0*p.z;
    return mix(mix(mix( Hash(n+  0.0), Hash(n+  1.0),f.x),
                   mix( Hash(n+157.0), Hash(n+158.0),f.x),f.y),
               mix(mix( Hash(n+113.0), Hash(n+114.0),f.x),
                   mix( Hash(n+270.0), Hash(n+271.0),f.x),f.y),f.z);
}

float octavedNoise(vec3 p, int octaves)
{
	    // Initial values
    float value = 0.0;
    float amplitude = .5;
    float frequency = 2.152;
    //
    // Loop of octaves
    for (int i = 0; i < max(maxOctaveForDimension,octaves); i++) {
        value += amplitude * cnoise(p+noiseOffset.xyz);
        p *= 2.;
        amplitude *= .5;
    }
    return value;
}

float FractalNoiseH(in vec2 xy)
{
   float w = 0.7;
   float f = 0.0;
   for (int i = 0; i < 7; i++)
   {
      f += noise(xy) * w;
      w *= 0.5;
      xy *= 2.333;
   }
   return f;
}

float cnoise3(vec3 pos)
{
	return noise(pos.xy*0.899)*.5335 + FractalNoiseH(pos.yx*0.571)*0.693 - noise(cos(pos.yx*0.673)+sin(pos.xy*0.615));
}

vec3 getWorldPoint(vec2 position)
{
	float s = position.x * M_PI * 2;
	float t = position.y * M_PI;

	float z = radius * cos(s) * sin(t);
	float x = radius * sin(s) * sin(t);
	float y = radius * cos(t);

	return vec3(x,y,z);
}

float when_eq(float x, float y) {
	return 1.0 - abs(sign(x - y));
}

float distToLine(vec2 pt1, vec2 pt2, vec2 testPt)
{
  vec2 lineDir = pt2 - pt1;
  vec2 perpDir = vec2(lineDir.y, -lineDir.x);
  vec2 dirToPt1 = pt1 - testPt;
  return abs(dot(normalize(perpDir), dirToPt1));
}

float dist_sq(vec3 x, vec3 y)
{
	return pow(x.x+y.x,2) + pow(x.y +  y.y, 2) + pow(x.z + y.z, 2);
}

float distToLine( vec3 l1, vec3 l2, vec3 p) 
{
  float line_dist = dist_sq(l1 , l2);

  if (line_dist == 0) return dist_sq(p, l1);

  float t = ((p.x - l1.x) * (l2.x - l1.x) + (p.y - l1.y) * (l2.y - l1.y) + (p.z - l1.y) * (l2.y - l1.y)) / line_dist;

  t = clamp(t, 0, 1);

  return dist_sq(p, vec3(l1.x + t * (l2.x - l1.x), l1.y + t * (l2.y - l1.y), l1.z + t * (l2.z - l1.z)));
}

float distToSegment(vec3 p1, vec3 p2, vec3 checkPoint)
{
	vec3 ab = p2 - p1;
	vec3 av = checkPoint - p1;

	if(dot(av, ab) < 0.0f)
		return length(av);

	
	vec3 bv = checkPoint - p2;

	if(dot(bv, ab) >= 0.0f)
		return length(bv);

	return length(cross(ab, av)) / length(ab);
}

float getRidgeEffect(vec3 thisPoint )
{
	float value = 1.0f;

	vec3 point = normalize(thisPoint);

	//For each river system
	for(int ridgeCount = 0; ridgeCount < actRidges; ridgeCount++)
	{
		//Presumably do some calcs here to check that the river is in view

		for(int pointCount =0 ; pointCount  < ridges[ridgeCount].actPoints -1; pointCount++)
		{

			//Each point on the river, make a section 
			// vec3 biggerPoint = max(ridges[ridgeCount].points[pointCount], ridges[ridgeCount].points[pointCount+1]).xyz;
			// vec3 smallerPoint = min(ridges[ridgeCount].points[pointCount], ridges[ridgeCount].points[pointCount+1]).xyz;

			vec3 biggerPoint = ridges[ridgeCount].points[pointCount].xyz;
			vec3 smallerPoint = ridges[ridgeCount].points[pointCount+1].xyz;

			//if(point.x < smallerPoint.x || point.x > biggerPoint.x) continue;
			float distToSeg =distToSegment(normalize(smallerPoint), normalize(biggerPoint), point);

			float thresh = 0.03f;

			if(distToSeg < thresh)
			{
				//return 100.4f;
				float f = pow((thresh - distToSeg),2.1f);
				value += pow(f *850.0f,1.1);
				//debugColour+=vec4(0,normalize(f),0,0);
			}
			if(distance(point , normalize(ridges[ridgeCount].points[pointCount].xyz)) < thresh)
			{
				//debugColour+=vec4(0,0,1,0);
				// float f = pow((thresh - distToSeg),2.1f);
				// value += pow(f *2000.0f,1.5);

			}
		}
	}
	return value;

}

float getDispForLayer(vec3 spherePoint, int i)
{
	if (layers[i].lActive == 0.0f) 
	{
		layerScales[i] = 0.0f;
		return 0;
	}
	float disp;
	//disp = vec3(cnoise(spherePoint * layers[i].coordModifier));
	disp = octavedNoise(spherePoint * layers[i].coordModifier, int(layers[i].octaves));


	disp = pow(disp, layers[i].exponent);


	disp = clamp(disp, (0), (10));
		

	disp+= layers[i].additionConst ;

	if (layers[i].scaleDependsOn == -1.0f )
	{
		disp *= layers[i].scale;
	}
	else
	{
		disp *= layers[i].scale * layerScales[int(layers[i].scaleDependsOn)];
	}

	float constMod = (layers[i].constructive *2.0f) - 1.0f;

	//return constMod * disp;

	if (layers[i].constructive == 0.0f)
	{
		layerScales[i] = -disp;
		return -disp;
	}
	else
	{
		layerScales[i] = disp;
		return disp;
	}
}

void main(void) 
{

	adjustedTexCoord = inputFragment.textureCoord;
	
	vec2 dimensions;
	dimensions.x = requiredSection.z - requiredSection.x;
	dimensions.y = ( requiredSection.w - requiredSection.y);
	//Adjustment code
	adjustedTexCoord.x *= ( dimensions.x);
	adjustedTexCoord.y *= ( dimensions.y);

	adjustedTexCoord += requiredSection.xy;
	//adjustedTexCoord.y = - adjustedTexCoord.y;


	//Dim size == 1 at full size
	float dimSize = dimensions.x;

	//Work out the maximum number of octaves for noise allowed at the current dimension
	//As dim size gets smaller, take less off the 20 
	maxOctaveForDimension = 20 - int(((dimSize) * 10));

	vec3 spherePoint = getWorldPoint(adjustedTexCoord);

	vec3 tempDisp = vec3(0);

	float alpha = 0.0f;

	alpha = 1.0f;


	for(int i = 0; i < actualLayerCount; i++)
	{
		float x= getDispForLayer(spherePoint, i);
		tempDisp += vec3(x);
	}

	
	tempDisp *= (texture(rrPregen, adjustedTexCoord).rgb );

	tempDisp = max(vec3(oceanLevel / 1200.0f), tempDisp);

	fragmentColour = vec4(tempDisp, alpha) + debugColour;

}

