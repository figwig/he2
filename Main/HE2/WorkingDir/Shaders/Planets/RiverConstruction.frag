#version 450

layout (location = 0) out vec4 outPosition;
layout (location = 1) out vec4 outNormal;
layout (location = 2) out vec4 outAlbedo;

layout(location = 0) in packet
{
  vec3 vertPos;
  vec3 normal;
  vec2 texCoord;
};


void main()
{
  outAlbedo = vec4(0,0,1,1);
  outPosition = vec4(vertPos, 1.0f);
  outNormal = vec4(0,1,0,0);
}


