#version 450

float M_PI = 3.14159265358979323846;
// input packet


// output packet
layout(location = 0) out vec4 fragmentColour;

layout(location = 0)in packet{
	vec2 textureCoord;
} inputFragment;


vec4 debugColour  =vec4(0,0,0,0);

struct Ridge {
	vec4 points[35];
	int actPoints;
};

layout (set = 0,binding = 0) uniform Ridges
{
	Ridge ridges[30];
	int actRidges;

};

struct River {
	vec4 points[100];
	int actPoints;
};

layout(binding = 1, set = 0) buffer readonly RiversOutput
{
	River rivers[200];
};

int riversActual = 200;

float radius = 3150.0f;

int maxOctaveForDimension = 20;
vec2 adjustedTexCoord;

vec3 getWorldPoint(vec2 position)
{

	float s = (position.x) * M_PI * 2;
	float t = position.y * M_PI;

	float z = radius * cos(s) * sin(t);
	float x = radius * sin(s) * sin(t);
	float y = radius * cos(t);


	return vec3(x,y,z);
}

float when_eq(float x, float y) {
	return 1.0 - abs(sign(x - y));
}

float distToLine(vec2 pt1, vec2 pt2, vec2 testPt)
{
  vec2 lineDir = pt2 - pt1;
  vec2 perpDir = vec2(lineDir.y, -lineDir.x);
  vec2 dirToPt1 = pt1 - testPt;
  return abs(dot(normalize(perpDir), dirToPt1));
}

float dist_sq(vec3 x, vec3 y)
{
	return pow(x.x+y.x,2) + pow(x.y +  y.y, 2) + pow(x.z + y.z, 2);
}

float distToLine( vec3 l1, vec3 l2, vec3 p) 
{
  float line_dist = dist_sq(l1 , l2);

  if (line_dist == 0) return dist_sq(p, l1);

  float t = ((p.x - l1.x) * (l2.x - l1.x) + (p.y - l1.y) * (l2.y - l1.y) + (p.z - l1.y) * (l2.y - l1.y)) / line_dist;

  t = clamp(t, 0, 1);

  return dist_sq(p, vec3(l1.x + t * (l2.x - l1.x), l1.y + t * (l2.y - l1.y), l1.z + t * (l2.z - l1.z)));
}

float distToSegment(vec3 p1, vec3 p2, vec3 checkPoint)
{
	vec3 v = p2 - p1;
	vec3 w = checkPoint - p1;

	float c1 = dot(w,v);

	if(c1 <= 0.0f)
		return length(w);
	
	float c2 = dot(v,v);
	if(c2 <= c1)
		return length(checkPoint - p2);

	float b = c1/c2;

	vec3 pb = p1 +b * v;

	return length(checkPoint - pb);
}

float getRidgeEffect(vec3 thisPoint )
{
	float value = 1.0f;

	vec3 point = normalize(thisPoint);

	//For each river system
	for(int ridgeCount = 0; ridgeCount < actRidges; ridgeCount++)
	{
		//Presumably do some calcs here to check that the river is in view

		for(int pointCount =0 ; pointCount  < ridges[ridgeCount].actPoints -1; pointCount++)
		{

			//Each point on the river, make a section 
			// vec3 biggerPoint = max(ridges[ridgeCount].points[pointCount], ridges[ridgeCount].points[pointCount+1]).xyz;
			// vec3 smallerPoint = min(ridges[ridgeCount].points[pointCount], ridges[ridgeCount].points[pointCount+1]).xyz;

			vec3 biggerPoint = ridges[ridgeCount].points[pointCount].xyz;
			vec3 smallerPoint = ridges[ridgeCount].points[pointCount+1].xyz;

			//if(point.x < smallerPoint.x || point.x > biggerPoint.x) continue;
			float distToSeg =distToSegment(normalize(smallerPoint), normalize(biggerPoint), point);

			float thresh = 0.035f;// * ridges[ridgeCount].points[pointCount].w;

			if(distToSeg < thresh)
			{
				//return 100.4f;
				float f = pow((thresh - distToSeg),2.1f);
				value +=  pow(f *980.0f,1.15);
				debugColour+=vec4(0,normalize(f),0,0);
			}
			if(distance(point , normalize(ridges[ridgeCount].points[pointCount].xyz)) < thresh)
			{
				//debugColour+=vec4(0,0,1,0);
				// float f = pow((thresh - distToSeg),2.1f);
				// value += pow(f *2000.0f,1.5);

			}
		}
	}
	return value;

}

float getRiverEffect(vec3 thisPoint)
{
//All river points are recieved as normalized vec3s
	float value = 0.0f;

	vec3 point = normalize(thisPoint);

	//For each river system
	for(int riverCount = 0; riverCount < riversActual; riverCount++)
	{
		//Presumably do some calcs here to check that the river is in view

		for(int pointCount =0 ; pointCount  < rivers[riverCount].actPoints -1; pointCount++)
		{

			//Each point on the river, make a section 
			// vec3 biggerPoint = max(ridges[ridgeCount].points[pointCount], ridges[ridgeCount].points[pointCount+1]).xyz;
			// vec3 smallerPoint = min(ridges[ridgeCount].points[pointCount], ridges[ridgeCount].points[pointCount+1]).xyz;

			vec3 biggerPoint = rivers[riverCount].points[pointCount].xyz;
			vec3 smallerPoint = rivers[riverCount].points[pointCount+1].xyz;

			//if(point.x < smallerPoint.x || point.x > biggerPoint.x) continue;
			float distToSeg =distToSegment(normalize(smallerPoint), normalize(biggerPoint), point);

			float thresh = 0.0018f;
		
			if(distToSeg < thresh )//&& length(normalize(biggerPoint) -point) > thresh
			{
				//return 100.4f;
				float f = min(pow((thresh - distToSeg),2.1f), 0.2f);
				value += 20000 * f;
				//value+=0.5f;
				debugColour+=vec4(0,0,1,0);
				debugColour+=vec4(0,0,normalize(f),0);
				//return min(value, 1);
			}
			// if(distToSeg < thresh * 30 )//&& length(normalize(biggerPoint) -point) > thresh
			// {
			// 	float f = pow((thresh - distToSeg),2.1f);
			// 	value += 40000 * f;
			// 	debugColour+=vec4(0,0,1,0);
			// 	debugColour+=vec4(0,0,normalize(f),0);
	
			// }
		}
	}
	return min(value, 0.01f);
}

void main(void) 
{
	adjustedTexCoord = inputFragment.textureCoord;

	vec3 spherePoint = getWorldPoint(adjustedTexCoord);
	vec3 spherePointR = getWorldPoint(adjustedTexCoord- vec2(0.25, 0));

	vec3 tempDisp = vec3(0);

	float alpha = 0.0f;

	alpha = 1.0f;

	float x=1.0f * getRidgeEffect(spherePoint) - min(18.5f *getRiverEffect(spherePointR ),10);

	tempDisp+=vec3(x);

	fragmentColour = vec4(tempDisp, alpha) - debugColour ;

	//fragmentColour = vec4(1,0,0,1);

}

