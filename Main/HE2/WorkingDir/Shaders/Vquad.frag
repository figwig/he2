#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 texCoord;

layout(location = 0) out vec4 outColor;


layout(set = 1, binding = 0) uniform sampler2D texSampler;
layout(set = 1, binding = 1) uniform sampler2D specSampler;
layout(set = 1, binding = 2) uniform sampler2D normSampler;


void main()
{

	vec2 trueTex = texCoord;
	trueTex.y = 1.0f - trueTex.y;
	//Force alpha to 1, we want to see everything with this shader for now
	outColor = vec4(texture(texSampler, trueTex).rgb, 1.0);
	//outColor = vec4(1,0,0,1);
}