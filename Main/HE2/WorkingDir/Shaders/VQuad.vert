#version 450
#extension GL_ARB_seperate_shader_objects : enable

layout (location =0) in vec3 position;
layout (location =1) in vec3 inNormal;
layout (location =2) in vec3 inTangent;
layout (location =3) in vec3 inBitangent;
layout (location =4) in vec2 inTexCoord;

layout (location = 0) out vec2 fragTexCoord;

layout( push_constant ) uniform ModelBuffer {
  mat4 modelMatrix;
};

void main()
{
	fragTexCoord = (inTexCoord + vec2(1.0f))/ 2.0f;

	vec3 posi = position;
	posi.y = -posi.y;
	//gl_Position = viewProjectionBuffer.proj * viewProjectionBuffer.view * modelBuffer.model  * vec4(position, 1.0f);
	gl_Position = modelMatrix* vec4(0.2f*posi, 1.0f);
}